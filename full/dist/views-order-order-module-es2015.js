(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-order-order-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-cost-list/order-cost-list.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-cost-list/order-cost-list.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  order-cost-list works!\n</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-detail/order-detail.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-detail/order-detail.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  order-detail works!\n</p>\n<button mat-flat-button color=\"primary\" routerLink=\"/orders/2/costs\">Costs</button>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-list/order-list.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-list/order-list.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\n  order-list works!\n</p>\n<button mat-flat-button color=\"primary\" routerLink=\"/orders/2\">Details</button>\n\n");

/***/ }),

/***/ "./src/app/views/order/order-cost-list/order-cost-list.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/views/order/order-cost-list/order-cost-list.component.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvb3JkZXIvb3JkZXItY29zdC1saXN0L29yZGVyLWNvc3QtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/order/order-cost-list/order-cost-list.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/views/order/order-cost-list/order-cost-list.component.ts ***!
  \**************************************************************************/
/*! exports provided: OrderCostListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderCostListComponent", function() { return OrderCostListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let OrderCostListComponent = class OrderCostListComponent {
    constructor() { }
    ngOnInit() {
    }
};
OrderCostListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-order-cost-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./order-cost-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-cost-list/order-cost-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./order-cost-list.component.scss */ "./src/app/views/order/order-cost-list/order-cost-list.component.scss")).default]
    }),
    __metadata("design:paramtypes", [])
], OrderCostListComponent);



/***/ }),

/***/ "./src/app/views/order/order-detail/order-detail.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/views/order/order-detail/order-detail.component.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvb3JkZXIvb3JkZXItZGV0YWlsL29yZGVyLWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/views/order/order-detail/order-detail.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/order/order-detail/order-detail.component.ts ***!
  \********************************************************************/
/*! exports provided: OrderDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailComponent", function() { return OrderDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let OrderDetailComponent = class OrderDetailComponent {
    constructor() { }
    ngOnInit() {
    }
};
OrderDetailComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-order-detail',
        template: __importDefault(__webpack_require__(/*! raw-loader!./order-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-detail/order-detail.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./order-detail.component.scss */ "./src/app/views/order/order-detail/order-detail.component.scss")).default]
    }),
    __metadata("design:paramtypes", [])
], OrderDetailComponent);



/***/ }),

/***/ "./src/app/views/order/order-list/order-list.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/views/order/order-list/order-list.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvb3JkZXIvb3JkZXItbGlzdC9vcmRlci1saXN0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/order/order-list/order-list.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/views/order/order-list/order-list.component.ts ***!
  \****************************************************************/
/*! exports provided: OrderListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderListComponent", function() { return OrderListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let OrderListComponent = class OrderListComponent {
    constructor() { }
    ngOnInit() {
    }
};
OrderListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-order-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./order-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/order/order-list/order-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./order-list.component.scss */ "./src/app/views/order/order-list/order-list.component.scss")).default]
    }),
    __metadata("design:paramtypes", [])
], OrderListComponent);



/***/ }),

/***/ "./src/app/views/order/order-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/order/order-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: OrderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderRoutingModule", function() { return OrderRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _order_cost_list_order_cost_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order-cost-list/order-cost-list.component */ "./src/app/views/order/order-cost-list/order-cost-list.component.ts");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/views/order/order-detail/order-detail.component.ts");
/* harmony import */ var _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-list/order-list.component */ "./src/app/views/order/order-list/order-list.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





const routes = [
    {
        path: "",
        component: _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_4__["OrderListComponent"]
    },
    {
        path: ":id",
        data: { title: "Order Details", breadcrumb: "{{id}}" },
        children: [
            {
                path: "",
                component: _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_3__["OrderDetailComponent"]
            },
            {
                path: "costs",
                component: _order_cost_list_order_cost_list_component__WEBPACK_IMPORTED_MODULE_2__["OrderCostListComponent"],
                data: { title: "Costs", breadcrumb: "Costs" }
            }
        ]
    }
];
let OrderRoutingModule = class OrderRoutingModule {
};
OrderRoutingModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    })
], OrderRoutingModule);



/***/ }),

/***/ "./src/app/views/order/order.module.ts":
/*!*********************************************!*\
  !*** ./src/app/views/order/order.module.ts ***!
  \*********************************************/
/*! exports provided: OrderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _order_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order-routing.module */ "./src/app/views/order/order-routing.module.ts");
/* harmony import */ var _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-list/order-list.component */ "./src/app/views/order/order-list/order-list.component.ts");
/* harmony import */ var _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./order-detail/order-detail.component */ "./src/app/views/order/order-detail/order-detail.component.ts");
/* harmony import */ var _order_cost_list_order_cost_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-cost-list/order-cost-list.component */ "./src/app/views/order/order-cost-list/order-cost-list.component.ts");
/* harmony import */ var app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/shared-material.module */ "./src/app/shared/shared-material.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







let OrderModule = class OrderModule {
};
OrderModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        declarations: [
            _order_list_order_list_component__WEBPACK_IMPORTED_MODULE_3__["OrderListComponent"],
            _order_detail_order_detail_component__WEBPACK_IMPORTED_MODULE_4__["OrderDetailComponent"],
            _order_cost_list_order_cost_list_component__WEBPACK_IMPORTED_MODULE_5__["OrderCostListComponent"]
        ],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_6__["SharedMaterialModule"], _order_routing_module__WEBPACK_IMPORTED_MODULE_2__["OrderRoutingModule"]]
    })
], OrderModule);



/***/ })

}]);
//# sourceMappingURL=views-order-order-module-es2015.js.map