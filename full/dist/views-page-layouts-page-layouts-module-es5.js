function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-page-layouts-page-layouts-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.html":
  /*!*********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.html ***!
    \*********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsFullWidthCardTabFullWidthCardTabComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout overflow-hidden pb-8\">\n  <div class=\"header-bg blue\"></div>\n\n  <div class=\"content\">\n    <div class=\"content-header p-24\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <h1 class=\"text-white m-0\">Full width card tab</h1>\n    </div>\n\n    <div class=\"content-body\">\n      <div class=\"content-card default-bg\">\n          <mat-tab-group>\n              <mat-tab label=\"First\">\n                <div class=\"p-24 height-100vh-280px\" [perfectScrollbar]>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                </div>\n              </mat-tab>\n              <mat-tab label=\"Second\"> Content 2 </mat-tab>\n              <mat-tab label=\"Third\"> Content 3 </mat-tab>\n          </mat-tab-group>\n      </div>\n    </div>\n\n  </div>\n\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card/full-width-card.component.html":
  /*!*************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card/full-width-card.component.html ***!
    \*************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsFullWidthCardFullWidthCardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout overflow-hidden pb-8\">\n    <!-- header background -->\n    <div class=\"header-bg blue\"></div>\n    \n    <div class=\"content\">\n      <!-- Header content -->\n      <div class=\"content-header p-24\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n        <h1 class=\"text-white m-0\">Full width card</h1>\n      </div>\n      \n      <!-- Body card -->\n      <div class=\"content-body\">\n        <!-- body card toolbar -->\n        <div class=\"content-card default-bg\">\n          <div class=\"card-header px-24 border-bottom\">\n            <h4 class=\"m-0\">Full width card header</h4>\n          </div>\n\n          <!-- body content -->\n          <div class=\"card-body p-24\">\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, quibusdam at! Beatae dolorem, eos dolor vitae animi sit sint veniam recusandae natus magni impedit iure. Nemo rem sed id fuga!</p>\n          </div>\n        </div>\n      </div>\n  \n    </div>\n  \n  </div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-plain/full-width-plain.component.html":
  /*!***************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-plain/full-width-plain.component.html ***!
    \***************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsFullWidthPlainFullWidthPlainComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout plain overflow-hidden pb-8\">\n  <div class=\"header blue\"></div>\n  \n  <div class=\"content\">\n    <div class=\"content-header blue p-24\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <h5 class=\"text-white m-0\">Full width card</h5>\n    </div>\n\n    <div class=\"content-body\">\n          <mat-tab-group>\n              <mat-tab label=\"First\">\n                <div class=\"p-24 height-100vh-280px\">\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda accusamus ipsam est corrupti earum eum blanditiis suscipit eaque tempore neque doloremque facere quibusdam nihil excepturi, error dolor nam nemo! Nobis!</p>\n                </div>\n              </mat-tab>\n              <mat-tab label=\"Second\"> Content 2 </mat-tab>\n              <mat-tab label=\"Third\"> Content 3 </mat-tab>\n          </mat-tab-group>\n    </div>\n\n  </div>\n\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.html":
  /*!*****************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.html ***!
    \*****************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsLeftSidebarCardLeftSidebarCardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout overflow-hidden pb-8\">\n  <div class=\"header-bg dark-blue background-size-contain\" style=\"background-image: url('assets/images/home-bg-black.png')\"></div>\n\n  <!-- Left Sidebar -->\n  <egret-sidebar name=\"layout-left-sidebar-card\" class=\"default-bg-mobile\">\n    <div class=\"sidebar-header text-white py-24 px-16\">\n      <h5 class=\"text-white\">Sidebar header</h5>\n    </div>\n    <div>\n      <mat-list>\n        <mat-list-item>List item 1</mat-list-item>\n        <mat-list-item>List item 2</mat-list-item>\n        <mat-list-item>List item 3</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n      </mat-list>\n    </div>\n  </egret-sidebar>\n\n  <!-- body content -->\n  <div class=\"content\">\n    <!-- Body content header -->\n    <div class=\"content-header text-white p-24\">\n      <h4 class=\"text-white\">Left sidebar card</h4>\n    </div>\n\n    <!-- content card -->\n    <div class=\"content-body\">\n      <div class=\"content-card default-bg\">\n        <!-- card header -->\n        <div class=\"card-header px-24 py-8 border-bottom\">\n          <button\n            fxHide\n            fxShow.lt-md\n            mat-icon-button\n            egretSidebarToggler=\"layout-left-sidebar-card\"\n            class=\"mr-8\"\n          >\n            <mat-icon>menu</mat-icon>\n          </button>\n\n          Card toolbar\n        </div>\n        <!-- Card body -->\n        <div class=\"card-body p-24\">\n          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima\n          sapiente earum aspernatur quia officia eaque beatae rem molestiae fuga\n          tempora, architecto doloremque facilis, illum, soluta ducimus dolorum\n          tempore nemo inventore! Lorem ipsum dolor, sit amet consectetur\n          adipisicing elit. Minima sapiente earum aspernatur quia officia eaque\n          beatae rem molestiae fuga tempora, architecto doloremque facilis,\n          illum, soluta ducimus dolorum tempore nemo inventore! Lorem ipsum\n          dolor, sit amet consectetur adipisicing elit. Minima sapiente earum\n          aspernatur quia officia eaque beatae rem molestiae fuga tempora,\n          architecto doloremque facilis, illum, soluta ducimus dolorum tempore\n          nemo inventore! Lorem ipsum dolor, sit amet consectetur adipisicing\n          elit. Minima sapiente earum aspernatur quia officia eaque beatae rem\n          molestiae fuga tempora, architecto doloremque facilis, illum, soluta\n          ducimus dolorum tempore nemo inventore! Lorem ipsum dolor, sit amet\n          consectetur adipisicing elit. Minima sapiente earum aspernatur quia\n          officia eaque beatae rem molestiae fuga tempora, architecto doloremque\n          facilis, illum, soluta ducimus dolorum tempore nemo inventore!\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.html":
  /*!*******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.html ***!
    \*******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsLeftSidebarPlainLeftSidebarPlainComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout plain overflow-hidden pb-8\">\n  <!-- <div class=\"header-bg dark-blue background-size-contain\" style=\"background-image: url('assets/images/home-bg-black.png')\"></div> -->\n\n  <!-- Left Sidebar -->\n  <egret-sidebar name=\"layout-left-sidebar-card\" class=\"default-bg-mobile height-100vh-70px\" [perfectScrollbar]>\n    <div class=\"sidebar-header pt-24 px-16\">\n      <h5 class=\"mb-16\">Sidebar header</h5>\n    </div>\n    <div>\n      <mat-list>\n        <mat-list-item>List item 1</mat-list-item>\n        <mat-list-item>List item 2</mat-list-item>\n        <mat-list-item>List item 3</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n      </mat-list>\n    </div>\n  </egret-sidebar>\n\n  <!-- body content -->\n  <div class=\"content\">\n    <!-- Body content header -->\n    <div class=\"content-header blue text-white p-24\">\n      <h4 class=\"text-white\">Left sidebar card</h4>\n    </div>\n\n    <!-- content card -->\n    <div class=\"content-body\">\n      <div class=\"content-card default-bg\">\n        <!-- card header -->\n        <div class=\"card-header px-24 py-8 border-bottom\">\n          <button\n            fxHide\n            fxShow.lt-md\n            mat-icon-button\n            egretSidebarToggler=\"layout-left-sidebar-card\"\n            class=\"mr-8\"\n          >\n            <mat-icon>menu</mat-icon>\n          </button>\n\n          Card toolbar\n        </div>\n        <!-- Card body -->\n        <div class=\"card-body p-24 height-100vh-280px\" [perfectScrollbar]>\n          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima\n          sapiente earum aspernatur quia officia eaque beatae rem molestiae fuga\n          tempora, architecto doloremque facilis, illum, soluta ducimus dolorum\n          tempore nemo inventore! Lorem ipsum dolor, sit amet consectetur\n          adipisicing elit. Minima sapiente earum aspernatur quia officia eaque\n          beatae rem molestiae fuga tempora, architecto doloremque facilis,\n          illum, soluta ducimus dolorum tempore nemo inventore! Lorem ipsum\n          dolor, sit amet consectetur adipisicing elit. Minima sapiente earum\n          aspernatur quia officia eaque beatae rem molestiae fuga tempora,\n          architecto doloremque facilis, illum, soluta ducimus dolorum tempore\n          nemo inventore! Lorem ipsum dolor, sit amet consectetur adipisicing\n          elit. Minima sapiente earum aspernatur quia officia eaque beatae rem\n          molestiae fuga tempora, architecto doloremque facilis, illum, soluta\n          ducimus dolorum tempore nemo inventore! Lorem ipsum dolor, sit amet\n          consectetur adipisicing elit. Minima sapiente earum aspernatur quia\n          officia eaque beatae rem molestiae fuga tempora, architecto doloremque\n          facilis, illum, soluta ducimus dolorum tempore nemo inventore!\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.html":
  /*!*******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.html ***!
    \*******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPageLayoutsRightSidebarCardRightSidebarCardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"page-layout overflow-hidden pb-8\">\n  <div class=\"header-bg blue\"></div>\n\n  <!-- Left Sidebar -->\n  <egret-sidebar name=\"layout-right-sidebar-card\" [right]=\"true\" class=\"default-bg-mobile\">\n    <div class=\"sidebar-header text-white blue py-24 px-16\">\n      <h5 class=\"text-white\">Sidebar header</h5>\n    </div>\n    <div>\n      <mat-list>\n        <mat-list-item>List item 1</mat-list-item>\n        <mat-list-item>List item 2</mat-list-item>\n        <mat-list-item>List item 3</mat-list-item>\n        <mat-list-item>List item 4</mat-list-item>\n      </mat-list>\n    </div>\n  </egret-sidebar>\n\n  <!-- body content -->\n  <div class=\"content\">\n    <!-- Body content header -->\n    <div class=\"content-header text-white py-24\">\n      <h4 class=\"text-white\">Right sidebar card</h4>\n    </div>\n\n    <!-- content card -->\n    <div class=\"content-body\">\n      <div class=\"content-card default-bg\">\n        <!-- card header -->\n        <div class=\"card-header px-24 py-8 border-bottom\">\n          <button\n            fxHide\n            fxShow.lt-md\n            mat-button\n            egretSidebarToggler=\"layout-right-sidebar-card\"\n          >\n            toggle\n          </button>\n\n          Card toolbar\n        </div>\n        <!-- Card body -->\n        <div class=\"card-body p-24 scrollable\" [perfectScrollbar]>\n          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima\n          sapiente earum aspernatur quia officia eaque beatae rem molestiae fuga\n          tempora, architecto doloremque facilis, illum, soluta ducimus dolorum\n          tempore nemo inventore! Lorem ipsum dolor, sit amet consectetur\n          adipisicing elit. Minima sapiente earum aspernatur quia officia eaque\n          beatae rem molestiae fuga tempora, architecto doloremque facilis,\n          illum, soluta ducimus dolorum tempore nemo inventore! Lorem ipsum\n          dolor, sit amet consectetur adipisicing elit. Minima sapiente earum\n          aspernatur quia officia eaque beatae rem molestiae fuga tempora,\n          architecto doloremque facilis, illum, soluta ducimus dolorum tempore\n          nemo inventore! Lorem ipsum dolor, sit amet consectetur adipisicing\n          elit. Minima sapiente earum aspernatur quia officia eaque beatae rem\n          molestiae fuga tempora, architecto doloremque facilis, illum, soluta\n          ducimus dolorum tempore nemo inventore! Lorem ipsum dolor, sit amet\n          consectetur adipisicing elit. Minima sapiente earum aspernatur quia\n          officia eaque beatae rem molestiae fuga tempora, architecto doloremque\n          facilis, illum, soluta ducimus dolorum tempore nemo inventore!\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n          <p>\n            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam\n            commodi omnis consequuntur sint quos deleniti, accusantium iusto\n            earum quia pariatur, quasi ea expedita fuga libero! Porro nisi dicta\n            nemo laudantium.\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.scss":
  /*!*******************************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.scss ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsFullWidthCardTabFullWidthCardTabComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL2Z1bGwtd2lkdGgtY2FyZC10YWIvZnVsbC13aWR0aC1jYXJkLXRhYi5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.ts ***!
    \*****************************************************************************************/

  /*! exports provided: FullWidthCardTabComponent */

  /***/
  function srcAppViewsPageLayoutsFullWidthCardTabFullWidthCardTabComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FullWidthCardTabComponent", function () {
      return FullWidthCardTabComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FullWidthCardTabComponent =
    /*#__PURE__*/
    function () {
      function FullWidthCardTabComponent() {
        _classCallCheck(this, FullWidthCardTabComponent);
      }

      _createClass(FullWidthCardTabComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return FullWidthCardTabComponent;
    }();

    FullWidthCardTabComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-full-width-card-tab',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./full-width-card-tab.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./full-width-card-tab.component.scss */
      "./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.scss")).default]
    }), __metadata("design:paramtypes", [])], FullWidthCardTabComponent);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-card/full-width-card.component.scss":
  /*!***********************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-card/full-width-card.component.scss ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsFullWidthCardFullWidthCardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL2Z1bGwtd2lkdGgtY2FyZC9mdWxsLXdpZHRoLWNhcmQuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-card/full-width-card.component.ts":
  /*!*********************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-card/full-width-card.component.ts ***!
    \*********************************************************************************/

  /*! exports provided: FullWidthCardComponent */

  /***/
  function srcAppViewsPageLayoutsFullWidthCardFullWidthCardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FullWidthCardComponent", function () {
      return FullWidthCardComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FullWidthCardComponent =
    /*#__PURE__*/
    function () {
      function FullWidthCardComponent() {
        _classCallCheck(this, FullWidthCardComponent);
      }

      _createClass(FullWidthCardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return FullWidthCardComponent;
    }();

    FullWidthCardComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-full-width-card',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./full-width-card.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-card/full-width-card.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./full-width-card.component.scss */
      "./src/app/views/page-layouts/full-width-card/full-width-card.component.scss")).default]
    }), __metadata("design:paramtypes", [])], FullWidthCardComponent);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-plain/full-width-plain.component.scss":
  /*!*************************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-plain/full-width-plain.component.scss ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsFullWidthPlainFullWidthPlainComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL2Z1bGwtd2lkdGgtcGxhaW4vZnVsbC13aWR0aC1wbGFpbi5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/full-width-plain/full-width-plain.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/views/page-layouts/full-width-plain/full-width-plain.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: FullWidthPlainComponent */

  /***/
  function srcAppViewsPageLayoutsFullWidthPlainFullWidthPlainComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FullWidthPlainComponent", function () {
      return FullWidthPlainComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FullWidthPlainComponent =
    /*#__PURE__*/
    function () {
      function FullWidthPlainComponent() {
        _classCallCheck(this, FullWidthPlainComponent);
      }

      _createClass(FullWidthPlainComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return FullWidthPlainComponent;
    }();

    FullWidthPlainComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-full-width-plain',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./full-width-plain.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/full-width-plain/full-width-plain.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./full-width-plain.component.scss */
      "./src/app/views/page-layouts/full-width-plain/full-width-plain.component.scss")).default]
    }), __metadata("design:paramtypes", [])], FullWidthPlainComponent);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.scss":
  /*!***************************************************************************************!*\
    !*** ./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.scss ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsLeftSidebarCardLeftSidebarCardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL2xlZnQtc2lkZWJhci1jYXJkL2xlZnQtc2lkZWJhci1jYXJkLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.ts":
  /*!*************************************************************************************!*\
    !*** ./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.ts ***!
    \*************************************************************************************/

  /*! exports provided: LeftSidebarCardComponent */

  /***/
  function srcAppViewsPageLayoutsLeftSidebarCardLeftSidebarCardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LeftSidebarCardComponent", function () {
      return LeftSidebarCardComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var LeftSidebarCardComponent =
    /*#__PURE__*/
    function () {
      function LeftSidebarCardComponent() {
        _classCallCheck(this, LeftSidebarCardComponent);
      }

      _createClass(LeftSidebarCardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return LeftSidebarCardComponent;
    }();

    LeftSidebarCardComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-left-sidebar-card',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./left-sidebar-card.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./left-sidebar-card.component.scss */
      "./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.scss")).default]
    }), __metadata("design:paramtypes", [])], LeftSidebarCardComponent);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.scss":
  /*!*****************************************************************************************!*\
    !*** ./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.scss ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsLeftSidebarPlainLeftSidebarPlainComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL2xlZnQtc2lkZWJhci1wbGFpbi9sZWZ0LXNpZGViYXItcGxhaW4uY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.ts":
  /*!***************************************************************************************!*\
    !*** ./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.ts ***!
    \***************************************************************************************/

  /*! exports provided: LeftSidebarPlainComponent */

  /***/
  function srcAppViewsPageLayoutsLeftSidebarPlainLeftSidebarPlainComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LeftSidebarPlainComponent", function () {
      return LeftSidebarPlainComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var LeftSidebarPlainComponent =
    /*#__PURE__*/
    function () {
      function LeftSidebarPlainComponent() {
        _classCallCheck(this, LeftSidebarPlainComponent);
      }

      _createClass(LeftSidebarPlainComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return LeftSidebarPlainComponent;
    }();

    LeftSidebarPlainComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-left-sidebar-plain',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./left-sidebar-plain.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./left-sidebar-plain.component.scss */
      "./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.scss")).default]
    }), __metadata("design:paramtypes", [])], LeftSidebarPlainComponent);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/page-layouts-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/views/page-layouts/page-layouts-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: PageLayoutsRoutingModule */

  /***/
  function srcAppViewsPageLayoutsPageLayoutsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PageLayoutsRoutingModule", function () {
      return PageLayoutsRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _left_sidebar_card_left_sidebar_card_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./left-sidebar-card/left-sidebar-card.component */
    "./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.ts");
    /* harmony import */


    var _full_width_card_full_width_card_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./full-width-card/full-width-card.component */
    "./src/app/views/page-layouts/full-width-card/full-width-card.component.ts");
    /* harmony import */


    var _right_sidebar_card_right_sidebar_card_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./right-sidebar-card/right-sidebar-card.component */
    "./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.ts");
    /* harmony import */


    var _full_width_plain_full_width_plain_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./full-width-plain/full-width-plain.component */
    "./src/app/views/page-layouts/full-width-plain/full-width-plain.component.ts");
    /* harmony import */


    var _left_sidebar_plain_left_sidebar_plain_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./left-sidebar-plain/left-sidebar-plain.component */
    "./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.ts");
    /* harmony import */


    var _full_width_card_tab_full_width_card_tab_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./full-width-card-tab/full-width-card-tab.component */
    "./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var routes = [{
      path: 'full-width-card',
      component: _full_width_card_full_width_card_component__WEBPACK_IMPORTED_MODULE_3__["FullWidthCardComponent"]
    }, {
      path: 'full-width-card-tab',
      component: _full_width_card_tab_full_width_card_tab_component__WEBPACK_IMPORTED_MODULE_7__["FullWidthCardTabComponent"]
    }, {
      path: 'left-sidebar-card',
      component: _left_sidebar_card_left_sidebar_card_component__WEBPACK_IMPORTED_MODULE_2__["LeftSidebarCardComponent"]
    }, {
      path: 'right-sidebar-card',
      component: _right_sidebar_card_right_sidebar_card_component__WEBPACK_IMPORTED_MODULE_4__["RightSidebarCardComponent"]
    }, {
      path: 'full-width-plain',
      component: _full_width_plain_full_width_plain_component__WEBPACK_IMPORTED_MODULE_5__["FullWidthPlainComponent"]
    }, {
      path: 'left-sidebar-plain',
      component: _left_sidebar_plain_left_sidebar_plain_component__WEBPACK_IMPORTED_MODULE_6__["LeftSidebarPlainComponent"]
    }];

    var PageLayoutsRoutingModule = function PageLayoutsRoutingModule() {
      _classCallCheck(this, PageLayoutsRoutingModule);
    };

    PageLayoutsRoutingModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    })], PageLayoutsRoutingModule);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/page-layouts.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/views/page-layouts/page-layouts.module.ts ***!
    \***********************************************************/

  /*! exports provided: PageLayoutsModule */

  /***/
  function srcAppViewsPageLayoutsPageLayoutsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PageLayoutsModule", function () {
      return PageLayoutsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _page_layouts_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./page-layouts-routing.module */
    "./src/app/views/page-layouts/page-layouts-routing.module.ts");
    /* harmony import */


    var _left_sidebar_card_left_sidebar_card_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./left-sidebar-card/left-sidebar-card.component */
    "./src/app/views/page-layouts/left-sidebar-card/left-sidebar-card.component.ts");
    /* harmony import */


    var app_shared_directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! app/shared/directives/shared-directives.module */
    "./src/app/shared/directives/shared-directives.module.ts");
    /* harmony import */


    var app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! app/shared/shared-material.module */
    "./src/app/shared/shared-material.module.ts");
    /* harmony import */


    var app_shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! app/shared/components/shared-components.module */
    "./src/app/shared/components/shared-components.module.ts");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var _full_width_card_full_width_card_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./full-width-card/full-width-card.component */
    "./src/app/views/page-layouts/full-width-card/full-width-card.component.ts");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _right_sidebar_card_right_sidebar_card_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./right-sidebar-card/right-sidebar-card.component */
    "./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.ts");
    /* harmony import */


    var _full_width_plain_full_width_plain_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./full-width-plain/full-width-plain.component */
    "./src/app/views/page-layouts/full-width-plain/full-width-plain.component.ts");
    /* harmony import */


    var _left_sidebar_plain_left_sidebar_plain_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./left-sidebar-plain/left-sidebar-plain.component */
    "./src/app/views/page-layouts/left-sidebar-plain/left-sidebar-plain.component.ts");
    /* harmony import */


    var _full_width_card_tab_full_width_card_tab_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./full-width-card-tab/full-width-card-tab.component */
    "./src/app/views/page-layouts/full-width-card-tab/full-width-card-tab.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var PageLayoutsModule = function PageLayoutsModule() {
      _classCallCheck(this, PageLayoutsModule);
    };

    PageLayoutsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      declarations: [_left_sidebar_card_left_sidebar_card_component__WEBPACK_IMPORTED_MODULE_3__["LeftSidebarCardComponent"], _full_width_card_full_width_card_component__WEBPACK_IMPORTED_MODULE_8__["FullWidthCardComponent"], _right_sidebar_card_right_sidebar_card_component__WEBPACK_IMPORTED_MODULE_10__["RightSidebarCardComponent"], _full_width_plain_full_width_plain_component__WEBPACK_IMPORTED_MODULE_11__["FullWidthPlainComponent"], _left_sidebar_plain_left_sidebar_plain_component__WEBPACK_IMPORTED_MODULE_12__["LeftSidebarPlainComponent"], _full_width_card_tab_full_width_card_tab_component__WEBPACK_IMPORTED_MODULE_13__["FullWidthCardTabComponent"]],
      imports: [app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_5__["SharedMaterialModule"], app_shared_directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_4__["SharedDirectivesModule"], app_shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_6__["SharedComponentsModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_7__["FlexLayoutModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"], _page_layouts_routing_module__WEBPACK_IMPORTED_MODULE_2__["PageLayoutsRoutingModule"]]
    })], PageLayoutsModule);
    /***/
  },

  /***/
  "./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.scss":
  /*!*****************************************************************************************!*\
    !*** ./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.scss ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPageLayoutsRightSidebarCardRightSidebarCardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvcGFnZS1sYXlvdXRzL3JpZ2h0LXNpZGViYXItY2FyZC9yaWdodC1zaWRlYmFyLWNhcmQuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.ts":
  /*!***************************************************************************************!*\
    !*** ./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.ts ***!
    \***************************************************************************************/

  /*! exports provided: RightSidebarCardComponent */

  /***/
  function srcAppViewsPageLayoutsRightSidebarCardRightSidebarCardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RightSidebarCardComponent", function () {
      return RightSidebarCardComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var RightSidebarCardComponent =
    /*#__PURE__*/
    function () {
      function RightSidebarCardComponent() {
        _classCallCheck(this, RightSidebarCardComponent);
      }

      _createClass(RightSidebarCardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return RightSidebarCardComponent;
    }();

    RightSidebarCardComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-right-sidebar-card',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./right-sidebar-card.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./right-sidebar-card.component.scss */
      "./src/app/views/page-layouts/right-sidebar-card/right-sidebar-card.component.scss")).default]
    }), __metadata("design:paramtypes", [])], RightSidebarCardComponent);
    /***/
  }
}]);
//# sourceMappingURL=views-page-layouts-page-layouts-module-es5.js.map