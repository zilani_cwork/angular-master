function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-post-management-post-management-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-add-modal.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-add-modal.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPostManagementPostAddModalHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div *ngIf=\"data['action']=='create'\">\r\n        <div class=\"dialog-container\">\r\n                <h3>Schedule Post</h3>\r\n                <div class=\"buffer\">\r\n                    <textarea (input)=\"caption = $event.target.value;\" name=\"caption\" id=\"caption\" cols=\"72\" rows=\"10\"\r\n                        placeholder=\"What would you like to share?\"></textarea>\r\n                    <!-- <input type=\"text\" (input)=\"caption = $event.target.value;\" style=\"width: 100%; height: 100px;\"> -->\r\n            \r\n                    <div class=\"image-upload\">\r\n                        <label for=\"img\" id=\"upload-icon\">\r\n            \r\n                            <!-- <i style=\"margin-top: 20px;\" class=\"fas fa-image fa-3x\"></i> -->\r\n            \r\n                            <i class=\"material-icons icon-color\"  style=\"font-size: 40px;\">\r\n                                add_photo_alternate\r\n                            </i>\r\n            \r\n                        </label>\r\n                        <input type=\"file\" name=\"imgUrl2\"  (change)=\"test($event)\" style=\"display: none;\" />\r\n                        <div *ngIf=\"imgUrl2\">\r\n                            <mat-card><img src={{imgUrl2}} width=\"125px\" ></mat-card>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            \r\n                <br>\r\n                <div class=\"modal-buttons\">\r\n            \r\n                    {{data['time']}} \r\n                    <div class=\"input-field\">\r\n            \r\n\r\n            \r\n                        <button mat-raised-button color=\"primary\" (click)=\"Schedule()\">Schedule Post</button>\r\n                    </div>\r\n            \r\n            \r\n                </div>\r\n            \r\n            \r\n            \r\n            </div>\r\n</div>\r\n\r\n<div *ngIf=\"data['action']=='edit'\">\r\n        <div class=\"dialog-container\">\r\n                <h3>Schedule Post</h3>\r\n                <div class=\"buffer\">\r\n                    <textarea [value]=\"caption\" (input)=\"caption = $event.target.value;\" name=\"caption\" id=\"caption\" cols=\"72\" rows=\"10\"\r\n                        placeholder=\"What would you like to share?\"></textarea>\r\n                    <!-- <input type=\"text\" (input)=\"caption = $event.target.value;\" style=\"width: 100%; height: 100px;\"> -->\r\n            \r\n                    <div class=\"image-upload\">\r\n                        <label for=\"img2\" id=\"upload-icon\">\r\n            \r\n                            <!-- <i style=\"margin-top: 20px;\" class=\"fas fa-image fa-3x\"></i> -->\r\n            \r\n                            <i class=\"material-icons icon-color\"  style=\"font-size: 40px;\">\r\n                                add_photo_alternate\r\n                            </i>\r\n            \r\n                        </label>\r\n                        <input type=\"file\" name=\"imgUrl2\" id=\"img2\" (change)=\"preview_post($event)\" style=\"display: none;\" />\r\n                        <div *ngIf=\"imgUrl2\">\r\n                            <mat-card><img src={{imgUrl2}} width=\"125px\" ></mat-card>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            \r\n                <br>\r\n                <div class=\"modal-buttons\">\r\n            \r\n                    {{data['time']}}\r\n                    <div class=\"input-field\">\r\n            \r\n            \r\n            \r\n                        <button mat-raised-button color=\"primary\" (click)=\"Edit_post()\">Edit</button>\r\n                    </div>\r\n            \r\n            \r\n                </div>\r\n            \r\n            \r\n            \r\n            </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-management.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-management.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsPostManagementPostManagementComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n\n\n\n<mat-card>\n    <div class=\"dialog-container\">\n        <h3>Schedule Post</h3>\n        <div class=\"buffer\">\n            <textarea [value]=\"caption\" (input)=\"caption = $event.target.value;\" name=\"caption\" id=\"caption\" cols=\"144\" rows=\"5\"\n                placeholder=\"What would you like to share?\"></textarea>\n            <!-- <input type=\"text\" (input)=\"caption = $event.target.value;\" style=\"width: 100%; height: 100px;\"> -->\n    \n            <div class=\"image-upload\">\n                <label for=\"img\" id=\"upload-icon\">\n    \n                    <!-- <i style=\"margin-top: 20px;\" class=\"fas fa-image fa-3x\"></i> -->\n    \n                    <i class=\"material-icons icon-color\"  style=\"font-size: 40px;\">\n                        add_photo_alternate\n                    </i>\n    \n                </label>\n                <input type=\"file\" name=\"imgUrl\" id=\"img\" (change)=\"preview($event)\" style=\"display: none;\" />\n                <div *ngIf=\"imgUrl\">\n                    <mat-card><img src={{imgUrl}} width=\"125px\" ></mat-card>\n                </div>\n            </div>\n        </div>\n    \n        <br>\n        <div class=\"modal-buttons\">\n    \n            \n            <div class=\"input-field\">\n    \n    \n    \n                <button mat-raised-button color=\"default\" (click)=\"Schedule()\">Schedule Post</button>\n            </div>\n    \n    \n        </div>\n    \n    \n    \n    </div>\n</mat-card>\n\n\n\n\n<div class=\"timeline\" *ngFor=\"let p of posts\" (wheel)=\"generate()\">\n<mat-card class=\"default\" *ngIf=\"!p.finalize\">\n  <mat-card-title>Timeline</mat-card-title>\n  <mat-card-content>\n    \n\n\n      <div *ngIf=\"p.image\">\n\n\n        <div class=\"timeline-item\">\n          <div class=\"timeline-badge\">\n            <img src=\"assets/images/logo.png\" alt=\"\">\n          </div>\n          <div class=\"timeline-body\">\n            <div class=\"timeline-body-top\" fxLayout=\"row\">\n              <a href=\"#\" class=\"timeline-body-title mr-1\"><b> Owner : {{p.cuser.user.first_name}}</b>  Version : {{p.version_number}}</a>\n              \n              <span fxFlex></span>\n            </div>\n            <div class=\"timeline-body-content\">\n              \n              <div class=\"img-caption\">\n                  <p>{{p.caption}}</p>\n                  <img src=\"{{p.image}}\" style=\"width: 400px;\" alt=\"\">\n                  <br>\n              <mat-form-field class=\"example-full-width\" style=\"width:400px;\">\n                  <input matInput  placeholder=\"Comment\"  (change)=\"addComment($event,p.id)\">\n              </mat-form-field>\n              <mat-list role=\"list\" *ngFor=\"let item of version_comments\">\n                  <div *ngIf=\"p.id==item.version.post.id && p.version_number==item.version.version\">\n                    <mat-list-item role=\"listitem\">{{item.comment}}</mat-list-item>\n                </div>\n\n                </mat-list>\n              </div>\n              \n            </div>\n            \n            <div class=\"timeline-body-actions\">\n                <a class=\"mr-1 text-muted\" (click)=\"finalize(p.id)\" >Finalize</a>\n                \n              \n              <a class=\"mr-1 text-muted\" (click)=\"editDialog(p)\">Edit</a>\n              <!-- <a  class=\"mr-1 text-muted\" (click)=\"delete(p.id)\">Delete</a> -->\n            </div>\n          </div>\n        </div>\n        \n        \n        <div *ngFor=\"let v of post_versions\" >\n            <!-- addition -->\n            <div *ngIf=\"v.post.id==p.id && v.version==p.version_number\" style=\"text-align: center;\"><strong>Latest Edit by: {{v.latest_edit.user.first_name}}</strong></div>\n            <br>\n            <!-- addition -->\n\n            <div class=\"timeline-item\" *ngIf=\"v.post.id==p.id && v.version!=p.version_number\">\n                <div *ngIf=\"v.post.id==p.id\">\n                    <div class=\"timeline-badge\">\n                        <img src=\"assets/images/logo.png\" alt=\"\">\n                      </div>\n                      <div class=\"timeline-body\">\n                        <div class=\"timeline-body-top\" fxLayout=\"row\">\n                          <a href=\"#\" class=\"timeline-body-title mr-1\"><b>{{v.latest_edit.user.first_name}}</b> posted Version : {{v.version}}</a>\n                          \n                          <span fxFlex></span>\n                        </div>\n                        <div class=\"timeline-body-content\">\n                          \n                          <div class=\"img-caption\">\n                              <p>{{v.previous_caption}}</p>\n                              <img src=\"{{v.previous_image}}\" style=\"width: 400px;\" alt=\"\">\n                              <mat-list role=\"list\" *ngFor=\"let v_c of version_comments\">\n                                  <div *ngIf=\"v.version==v_c.version.version && v.post.id==v_c.version.post.id\">\n                                    <mat-list-item role=\"listitem\">{{v_c.comment}}</mat-list-item>\n                                </div>\n                                \n                                </mat-list>\n                            </div>\n                        </div>\n                        <!-- <div class=\"timeline-body-actions\">\n                            <a *ngIf=\"!p.like\" class=\"mr-1 text-muted\" (click)=\"like(p.id)\" >Like</a>\n                            <a *ngIf=\"p.like\"  class=\"mr-1\" style=\"color: lightblue;\"(click)=\"like(p.id)\" >Like</a>\n                          <a class=\"mr-1 text-muted\">Comment</a>\n                          <a class=\"mr-1 text-muted\" (click)=\"editDialog(p)\">Edit</a>\n                          <a  class=\"mr-1 text-muted\" (click)=\"delete(p.id)\">Delete</a>\n                        </div> -->\n                      </div>\n                </div>\n              </div>\n  \n  \n  \n        \n        \n        </div>\n      \n      \n      \n      </div>\n      \n\n      <div *ngIf=\"!p.image\">\n          <div class=\"timeline-item\">\n              <div class=\"timeline-badge\">\n                <img src=\"assets/images/logo.png\" alt=\"\">\n              </div>\n              <div class=\"timeline-body\">\n                <div class=\"timeline-body-top\" fxLayout=\"row\">\n                  <a href=\"#\" class=\"timeline-body-title mr-1\"><b>Owner : {{p.cuser.user.first_name}}</b> Version : {{p.version_number}}</a>\n                  <!-- <span class=\"text-muted\">1 hour ago</span> -->\n                  <span fxFlex></span>\n                </div>\n                <div class=\"timeline-body-content\">\n                  <p>{{p.caption}}.</p>\n                  <br>\n              <mat-form-field class=\"example-full-width\" style=\"width:400px;\">\n                  <input matInput  placeholder=\"Comment\"  (change)=\"addComment($event,p.id)\">\n              </mat-form-field>\n\n                <mat-list role=\"list\" *ngFor=\"let item of version_comments\">\n                  <div *ngIf=\"p.id==item.version.post.id && p.version_number==item.version.version\">\n                    <mat-list-item role=\"listitem\">{{item.comment}}</mat-list-item>\n                </div>\n\n                </mat-list>\n                </div>\n                <div class=\"timeline-body-actions\">\n                    <a class=\"mr-1 text-muted\" (click)=\"finalize(p.id)\" >Finalize</a>\n                    \n                  \n                  <a class=\"mr-1 text-muted\" (click)=\"editDialog(p)\">Edit</a>\n                  <!-- <a  class=\"mr-1 text-muted\" (click)=\"delete(p.id)\">Delete</a> -->\n                </div>\n              </div>\n            </div>\n\n            <div *ngFor=\"let v of post_versions\" >\n\n                <!-- addition -->\n            <div *ngIf=\"v.post.id==p.id && v.version==p.version_number\" style=\"text-align: center;\"><strong>Latest Edit by: {{v.latest_edit.user.first_name}}</strong></div>\n            <br>\n            <!-- addition -->\n\n                <div class=\"timeline-item\" *ngIf=\"v.post.id==p.id && v.version!=p.version_number\">\n                    <div *ngIf=\"v.post.id==p.id\">\n                        <div class=\"timeline-badge\">\n                            <img src=\"assets/images/logo.png\" alt=\"\">\n                          </div>\n                          <div class=\"timeline-body\">\n                            <div class=\"timeline-body-top\" fxLayout=\"row\">\n                              <a href=\"#\" class=\"timeline-body-title mr-1\"><b>{{v.latest_edit.user.first_name}}</b> posted Version : {{v.version}}</a>\n                              \n                              <span fxFlex></span>\n                            </div>\n                            <div class=\"timeline-body-content\">\n                              \n                              <div class=\"img-caption\">\n                                  <p>{{v.previous_caption}}</p>\n                                  <!-- <img src=\"{{v.previous_image}}\" style=\"width: 400px;\" alt=\"\"> -->\n                                  <mat-list role=\"list\" *ngFor=\"let v_c of version_comments\">\n                                      <div *ngIf=\"v.version==v_c.version.version && v.post.id==v_c.version.post.id\">\n                                        <mat-list-item role=\"listitem\">{{v_c.comment}}</mat-list-item>\n                                    </div>\n                                    \n                                    </mat-list>\n                                </div>\n                            </div>\n                            \n                          </div>\n                    </div>\n                  </div>\n      \n      \n      \n            \n            \n            </div>\n      </div>\n\n      \n\n\n    \n  </mat-card-content>\n</mat-card>\n</div>";
    /***/
  },

  /***/
  "./src/app/views/post-management/post-management.component.scss":
  /*!**********************************************************************!*\
    !*** ./src/app/views/post-management/post-management.component.scss ***!
    \**********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsPostManagementPostManagementComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".img-caption {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column; }\n\n.time-select {\n  list-style-type: none;\n  text-align: center; }\n\n.time-select :hover::after {\n  content: \" Schedule a post\";\n  cursor: pointer; }\n\n.modal-buttons {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n#upload-icon:hover::after {\n  cursor: pointer; }\n\n.input-field {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: space-evenly;\n          justify-content: space-evenly; }\n\ntextarea {\n  border: none;\n  border-style: none; }\n\ntextarea:focus {\n  border: 0;\n  outline: none; }\n\n.buffer {\n  border: 1px solid gainsboro; }\n\n.image-upload {\n  display: -webkit-box;\n  display: flex; }\n\n.icon-color:hover {\n  color: lightskyblue; }\n\n.full-post {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n.image-date {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n.caption-buttons {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n.edit-b {\n  margin-right: 5px; }\n\n.d-e-buttons {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n          flex-direction: row-reverse; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvcG9zdC1tYW5hZ2VtZW50L3Bvc3QtbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCLEVBQUE7O0FBSzFCO0VBQ0kscUJBQXFCO0VBQ3JCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLDJCQUEyQjtFQUMzQixlQUFlLEVBQUE7O0FBR25CO0VBQ0csb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQThCO1VBQTlCLDhCQUE4QixFQUFBOztBQUdqQztFQUNJLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qiw4QkFBNkI7VUFBN0IsNkJBQTZCLEVBQUE7O0FBRWpDO0VBQ0ksWUFBWTtFQUNaLGtCQUNKLEVBQUE7O0FBRUE7RUFDSSxTQUFTO0VBQ1QsYUFBYSxFQUFBOztBQUdqQjtFQUNJLDJCQUEyQixFQUFBOztBQUUvQjtFQUNJLG9CQUFhO0VBQWIsYUFBYSxFQUFBOztBQUVqQjtFQUNJLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUE4QjtVQUE5Qiw4QkFBOEIsRUFBQTs7QUFFbEM7RUFDSSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBOEI7VUFBOUIsOEJBQThCLEVBQUE7O0FBRWxDO0VBQ0ksb0JBQWE7RUFBYixhQUFhO0VBQ2IsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIseUJBQThCO1VBQTlCLDhCQUE4QixFQUFBOztBQUlsQztFQUNJLGlCQUFpQixFQUFBOztBQUVyQjtFQUNJLG9CQUFZO0VBQVosYUFBWTtFQUNaLDhCQUEyQjtFQUEzQiw4QkFBMkI7VUFBM0IsMkJBQTJCLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL3Bvc3QtbWFuYWdlbWVudC9wb3N0LW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1nLWNhcHRpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuXHJcblxyXG4udGltZS1zZWxlY3R7XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi50aW1lLXNlbGVjdCA6aG92ZXI6OmFmdGVye1xyXG4gICAgY29udGVudDogXCIgU2NoZWR1bGUgYSBwb3N0XCI7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5tb2RhbC1idXR0b25ze1xyXG4gICBkaXNwbGF5OiBmbGV4O1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbiN1cGxvYWQtaWNvbjpob3Zlcjo6YWZ0ZXJ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuLmlucHV0LWZpZWxke1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcclxufVxyXG50ZXh0YXJlYXtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1zdHlsZTpub25lXHJcbn1cclxuXHJcbnRleHRhcmVhOmZvY3Vze1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLmJ1ZmZlcntcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdhaW5zYm9ybztcclxufVxyXG4uaW1hZ2UtdXBsb2Fke1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG4uaWNvbi1jb2xvcjpob3ZlcntcclxuICAgIGNvbG9yOiBsaWdodHNreWJsdWU7XHJcbn1cclxuXHJcbi5mdWxsLXBvc3R7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5pbWFnZS1kYXRle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLmNhcHRpb24tYnV0dG9uc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG5cclxuLmVkaXQtYntcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcbi5kLWUtYnV0dG9uc3tcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcclxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/views/post-management/post-management.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/views/post-management/post-management.component.ts ***!
    \********************************************************************/

  /*! exports provided: PostManagementComponent, PostAddModal */

  /***/
  function srcAppViewsPostManagementPostManagementComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostManagementComponent", function () {
      return PostManagementComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostAddModal", function () {
      return PostAddModal;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var PostManagementComponent =
    /*#__PURE__*/
    function () {
      function PostManagementComponent(client, dialog, fireStorage) {
        _classCallCheck(this, PostManagementComponent);

        this.client = client;
        this.dialog = dialog;
        this.fireStorage = fireStorage;
        this.caption = "";
      }

      _createClass(PostManagementComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])(localStorage.getItem('Token'));
          this.getPosts();
          this.getVersions();
          console.log(localStorage);
          this.getComments();
          this.getVersionComments();
          console.log(this.fireStorage);
        }
      }, {
        key: "preview",
        value: function preview(event) {
          var _this = this;

          // console.log(event.target.files)
          // console.log(event.target.files[0].name)
          // this.imgUrl = event.target.files[0].name
          this.file = event.target.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]);

          reader.onload = function (_event) {
            _this.imgUrl = reader.result; // console.log(this.imgUrl)
          }; // this.fireStorage.upload('',this.file)
          // ref.getDownloadURL().subscribe((res)=>console.log(res))
          // task.then()

        }
      }, {
        key: "Schedule",
        value: function Schedule() {
          var _this2 = this;

          // console.log("Schedule button was clicked")
          if (this.file) {
            // console.log(new_body)
            var randomId = Math.random().toString(36).substring(2);
            this.ref = this.fireStorage.ref(randomId);
            this.task = this.ref.put(this.file).then(function (res) {
              _this2.ref.getDownloadURL().subscribe(function (res) {
                console.log("This is the url");
                console.log(res);
                var body = {
                  image: res,
                  caption: _this2.caption,
                  post_type: "GRAPHICS"
                };
                var new_body = JSON.stringify(body);

                _this2.client.post('https://agile-cove-96115.herokuapp.com/version/post/', new_body, _this2.header).subscribe(function (r) {
                  // console.log(r)
                  _this2.getPosts();
                });
              });
            });
          } else {
            // console.log("No Image here")
            // console.log("Content Writing")
            var body = {
              image: "",
              caption: this.caption,
              post_type: "CONTENT_WRITING"
            };
            var new_body = JSON.stringify(body); // console.log(new_body)

            this.client.post('https://agile-cove-96115.herokuapp.com/version/post/', new_body, this.header).subscribe(function (r) {
              console.log(r);
              _this2.caption = "";

              _this2.getPosts();
            });
          }
        }
      }, {
        key: "getPosts",
        value: function getPosts() {
          var _this3 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/post/', this.header).subscribe(function (res) {
            console.log(res);
            _this3.posts = res;
          });
        }
      }, {
        key: "like",
        value: function like(id) {
          var _this4 = this;

          console.log(id);
          var body = {
            id: id
          };
          var new_body = JSON.stringify(body);
          this.client.patch('https://agile-cove-96115.herokuapp.com/version/likePost', new_body, this.header).subscribe(function (res) {
            console.log(res);

            _this4.getPosts();
          });
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          var _this5 = this;

          this.client.delete('https://agile-cove-96115.herokuapp.com/version/post/' + id, this.header).subscribe(function (res) {
            console.log(res);

            _this5.getPosts();
          });
        }
      }, {
        key: "editDialog",
        value: function editDialog(obj) {
          var _this6 = this;

          var dialogRef = this.dialog.open(PostAddModal, {
            width: '600px',
            data: {
              obj: obj,
              action: "edit"
            }
          });
          dialogRef.afterClosed().subscribe(function (result) {
            // console.log('Here I will call all the events again');
            console.log("calling the events");

            _this6.getPosts();

            _this6.getVersions(); // setTimeout(()=>{this.getEvents()}, 2000)
            // this.getEvents()
            // this.getEvents()
            // this.getEvents()

          });
          this.getPosts();
          this.getVersions();
        }
      }, {
        key: "getVersions",
        value: function getVersions() {
          var _this7 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/version/', this.header).subscribe(function (res) {
            console.log(res);
            _this7.post_versions = res;
          });
        }
      }, {
        key: "generate",
        value: function generate() {
          this.getPosts();
          this.getVersions();
          this.getVersionComments();
        }
      }, {
        key: "addComment",
        value: function addComment(e, id) {
          var _this8 = this;

          console.log(e.target.value);
          console.log(id);
          var body = {
            post_id: id,
            comment: e.target.value
          }; // console.log(body)

          var new_body = JSON.stringify(body);
          console.log(new_body);
          this.client.post('https://agile-cove-96115.herokuapp.com/version/commentcreate/', new_body, this.header).subscribe(function (res) {
            // console.log(res)
            // console.log("Comment Working")
            _this8.getComments();

            _this8.getVersionComments();

            _this8.getVersions();

            _this8.getPosts();
          });
          this.getPosts();
          this.getComments();
        }
      }, {
        key: "getComments",
        value: function getComments() {
          var _this9 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/commentcreate/', this.header).subscribe(function (res) {
            // console.log(res)
            _this9.comments = res;
          });
        }
      }, {
        key: "getVersionComments",
        value: function getVersionComments() {
          var _this10 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/versioncomment/', this.header).subscribe(function (res) {
            console.log("THese are version comments");
            console.log(res);
            _this10.version_comments = res;
          });
        }
      }, {
        key: "finalize",
        value: function finalize(id) {
          var _this11 = this;

          console.log(id);
          console.log(localStorage.getItem('Token'));
          var body = {
            id: id
          };
          var new_body = JSON.stringify(body);
          this.client.patch('https://agile-cove-96115.herokuapp.com/version/finalizePost', new_body, this.header).subscribe(function (res) {
            console.log(res);

            _this11.getPosts();
          });
        }
      }]);

      return PostManagementComponent;
    }();

    PostManagementComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"]
      }];
    };

    PostManagementComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-post-management',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./post-management.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-management.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./post-management.component.scss */
      "./src/app/views/post-management/post-management.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"]])], PostManagementComponent);

    var PostAddModal =
    /*#__PURE__*/
    function () {
      function PostAddModal(dialogRef, data, client, fireStorage) {
        _classCallCheck(this, PostAddModal);

        this.dialogRef = dialogRef;
        this.data = data;
        this.client = client;
        this.fireStorage = fireStorage;
        this.caption = "";
      }

      _createClass(PostAddModal, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])(localStorage.getItem('Token'));
          console.log(this.data);

          if (this.data['obj']['image']) {
            console.log("There is an image");
            this.caption = this.data['obj']['caption'];
            this.imgUrl2 = this.data['obj']['image'];
          } else {
            console.log("There is no image");
            this.caption = this.data['obj']['caption'];
          }
        }
      }, {
        key: "onNoClick",
        value: function onNoClick() {
          this.dialogRef.close();
        }
      }, {
        key: "changeCaption",
        value: function changeCaption() {
          this.caption = document.getElementById('caption').value;
          console.log(this.caption);
        }
      }, {
        key: "preview_post",
        value: function preview_post(event) {
          var _this12 = this;

          // console.log(event.target.files)
          // console.log(event.target.files[0].name)
          // this.imgUrl = event.target.files[0].name
          this.file2 = event.target.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]);

          reader.onload = function (_event) {
            _this12.imgUrl2 = reader.result; // console.log(this.imgUrl2)
          }; // console.log(this.imgUrl2)

        }
      }, {
        key: "Edit_post",
        value: function Edit_post() {
          var _this13 = this;

          this.dialogRef.close();

          if (this.file2) {
            console.log("A file was changed");
            var f = document.getElementById('img2').files[0];
            console.log(f);
            var formData = new FormData();
            var id = this.data['obj']['id'];
            var version = this.data['obj']['version_number'] + 1;
            formData.append('image', f);
            formData.append('id', id);
            formData.append('caption', this.caption);
            formData.append('version', version);
            var randomId = Math.random().toString(36).substring(2);
            this.ref = this.fireStorage.ref(randomId);
            this.task = this.ref.put(f).then(function (res) {
              _this13.ref.getDownloadURL().subscribe(function (res) {
                console.log("This is the url");
                console.log(res);
                formData.append('image', res);
                formData.append('id', id);
                formData.append('caption', _this13.caption);
                formData.append('version', version);
                var body = {
                  image: res,
                  id: id,
                  caption: _this13.caption,
                  version: version
                };
                var test_body = JSON.stringify(body);
                console.log(test_body);

                _this13.client.put('https://agile-cove-96115.herokuapp.com/version/UpdatePostVersion', test_body, _this13.header).subscribe(function (res) {
                  console.log(res);
                });
              });
            });
          } else {
            console.log("Only Caption");
            var body = {
              id: this.data['obj']['id'],
              caption: this.caption,
              version: this.data['obj']['version_number'] + 1
            };
            var new_body = JSON.stringify(body);
            this.client.patch('https://agile-cove-96115.herokuapp.com/version/UpdateCaptionVersion', new_body, this.header).subscribe(function (res) {
              console.log(res);
            });
          }
        }
      }]);

      return PostAddModal;
    }();

    PostAddModal.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]]
        }]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"]
      }];
    };

    PostAddModal = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'post-add-modal',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./post-add-modal.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/post-management/post-add-modal.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./post-management.component.scss */
      "./src/app/views/post-management/post-management.component.scss")).default]
    }), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object, _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"]])], PostAddModal);
    /***/
  },

  /***/
  "./src/app/views/post-management/post-management.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/views/post-management/post-management.module.ts ***!
    \*****************************************************************/

  /*! exports provided: PostManagementModule */

  /***/
  function srcAppViewsPostManagementPostManagementModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostManagementModule", function () {
      return PostManagementModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var angular_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-calendar */
    "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-calendar/date-adapters/date-fns */
    "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-color-picker */
    "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js");
    /* harmony import */


    var _post_management_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./post-management.component */
    "./src/app/views/post-management/post-management.component.ts");
    /* harmony import */


    var _post_management_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./post-management.routing */
    "./src/app/views/post-management/post-management.routing.ts");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var app_environment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! app/environment */
    "./src/app/environment.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var PostManagementModule = function PostManagementModule() {
      _classCallCheck(this, PostManagementModule);
    };

    PostManagementModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__["ColorPickerModule"], angular_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"].forRoot({
        provide: angular_calendar__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"],
        useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__["adapterFactory"]
      }), _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_post_management_routing__WEBPACK_IMPORTED_MODULE_11__["PostManagementRoutes"]), _angular_fire__WEBPACK_IMPORTED_MODULE_12__["AngularFireModule"].initializeApp({
        apiKey: "AIzaSyCpENRtd-xU_iRD3N8u6mPt_SyvUGY1Qak",
        authDomain: "cworkva.firebaseapp.com",
        databaseURL: "https://cworkva.firebaseio.com",
        projectId: "cworkva",
        storageBucket: "cworkva.appspot.com",
        messagingSenderId: "944036100209",
        appId: "1:944036100209:web:2ba19f39dfa1fda85983d0"
      }), _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorageModule"]],
      providers: [{
        provide: _angular_fire__WEBPACK_IMPORTED_MODULE_12__["FirebaseOptionsToken"],
        useValue: app_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].firebase
      }],
      entryComponents: [_post_management_component__WEBPACK_IMPORTED_MODULE_10__["PostAddModal"]],
      declarations: [_post_management_component__WEBPACK_IMPORTED_MODULE_10__["PostManagementComponent"], _post_management_component__WEBPACK_IMPORTED_MODULE_10__["PostAddModal"]]
    })], PostManagementModule);
    /***/
  },

  /***/
  "./src/app/views/post-management/post-management.routing.ts":
  /*!******************************************************************!*\
    !*** ./src/app/views/post-management/post-management.routing.ts ***!
    \******************************************************************/

  /*! exports provided: PostManagementRoutes */

  /***/
  function srcAppViewsPostManagementPostManagementRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostManagementRoutes", function () {
      return PostManagementRoutes;
    });
    /* harmony import */


    var _post_management_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./post-management.component */
    "./src/app/views/post-management/post-management.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var PostManagementRoutes = [{
      path: '',
      component: _post_management_component__WEBPACK_IMPORTED_MODULE_0__["PostManagementComponent"],
      data: {
        title: 'PostManagement'
      }
    }];
    /***/
  }
}]);
//# sourceMappingURL=views-post-management-post-management-module-es5.js.map