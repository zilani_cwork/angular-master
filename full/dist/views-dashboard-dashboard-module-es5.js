function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-dashboard-dashboard-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/analytics/analytics.component.html":
  /*!**********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/analytics/analytics.component.html ***!
    \**********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardAnalyticsAnalyticsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- First row -->\n<mat-card\n  class=\"p-0\"\n  [@animate]=\"{ value: '*', params: { y: '50px', delay: '300ms' } }\"\n>\n  <div fxLayout=\"row wrap\">\n    <div\n      fxFlex=\"50\"\n      fxFlex.gt-sm=\"20\"\n      fxFlex.sm=\"50\"\n    >\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-green\">show_chart</mat-icon>\n        <h4 class=\"m-0 \">5040</h4>\n        <small class=\"m-0 \">Page views</small>\n      </div>\n    </div>\n    <div\n      fxFlex=\"50\"\n      fxFlex.gt-sm=\"20\"\n      fxFlex.sm=\"50\"\n    >\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-blue\">cloud_download</mat-icon>\n        <h4 class=\"m-0 \">1200</h4>\n        <small class=\"m-0 \">Downloads</small>\n      </div>\n    </div>\n    <div\n      fxFlex=\"50\"\n      fxFlex.gt-sm=\"20\"\n      fxFlex.sm=\"50\"\n    >\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-green\">comment</mat-icon>\n        <h4 class=\"m-0 \">16420</h4>\n        <small class=\"m-0 \">Comments</small>\n      </div>\n    </div>\n    <div\n      fxFlex=\"50\"\n      fxFlex.gt-sm=\"20\"\n      fxFlex.sm=\"50\"\n    >\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-blue\">person</mat-icon>\n        <h4 class=\"m-0 \">3010</h4>\n        <small class=\"m-0 \">Profile views</small>\n      </div>\n    </div>\n    <div\n      fxFlex=\"50\"\n      fxFlex.gt-sm=\"20\"\n      fxFlex.sm=\"50\"\n    >\n      <div class=\"text-center pt-1 pb-1\">\n        <mat-icon class=\"text-red\">favorite</mat-icon>\n        <h4 class=\"m-0 \">4070</h4>\n        <small class=\"m-0 \">Likes</small>\n      </div>\n    </div>\n  </div>\n</mat-card>\n<!--/ End first row -->\n\n<!-- 2nd, 3rd row -->\n<div fxLayout=\"row wrap\">\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"66.67\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { y: '50px', delay: '600ms' } }\"\n  >\n    <mat-card class=\"p-0 \">\n      <mat-tab-group>\n        <mat-tab label=\"Traffic Vs Sales\">\n          <div\n            echarts\n            [options]=\"trafficVsSaleOptions\"\n            [merge]=\"trafficVsSale\"\n            [autoResize]=\"true\"\n            style=\"height: 255px;\"\n          ></div>\n        </mat-tab>\n        <mat-tab label=\"Sessions\">\n          <div\n            echarts\n            [options]=\"sessionOptions\"\n            [merge]=\"sessions\"\n            [autoResize]=\"true\"\n            style=\"height: 255px;\"\n          ></div>\n        </mat-tab>\n      </mat-tab-group>\n    </mat-card>\n  </div>\n  <!--/ End tab -->\n\n  <!-- Donut chart -->\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"33.33\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { y: '50px', delay: '400ms' } }\"\n  >\n    <mat-card class=\"p-0 \">\n      <p class=\" ml-1 mt-1\">TRAFFIC SOURCES</p>\n      <div\n        echarts\n        [options]=\"trafficSourcesChart\"\n        [autoResize]=\"true\"\n        style=\"height: 250px;\"\n      ></div>\n    </mat-card>\n  </div>\n  <!--/ End donut chart -->\n  <!--/ End 2nd row -->\n\n  <!-- 3rd row, 1st column -->\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"33.33\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { y: '50px', delay: '800ms' } }\"\n  >\n    <mat-card class=\"\">\n      <p class=\"\">7 DAY TRAFFIC</p>\n      <div\n        echarts\n        [options]=\"dailyTrafficChartBar\"\n        [autoResize]=\"true\"\n        style=\"height: 250px;\"\n      ></div>\n    </mat-card>\n\n    <mat-card\n      class=\"p-0\"\n    >\n      <mat-card-title class=\"mb-1\">\n        <div class=\"card-title-text\">\n          <span class=\"font-weight-normal\">Top Campaign Performance</span>\n\n          <span fxFlex></span>\n          <button\n            class=\"card-control\"\n            mat-icon-button\n            [matMenuTriggerFor]=\"menu2\"\n          >\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu2=\"matMenu\">\n            <button mat-menu-item>\n              <mat-icon>settings</mat-icon>\n              <span>Campaign Settings</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>do_not_disturb</mat-icon>\n              <span>Disable All Campaigns</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>close</mat-icon>\n              <span>Remove panel</span>\n            </button>\n          </mat-menu>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <div class=\"mb-1\">\n          <p class=\"m-0\">\n            Facebook <span class=\"text-muted\">| 9.8 | 90%</span>\n          </p>\n          <mat-progress-bar\n            color=\"warn\"\n            mode=\"determinate\"\n            [value]=\"90\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"mb-1\">\n          <p class=\"m-0 text-13\">\n            Google AdSense <span class=\"text-muted\">| 8.3 | 80%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"primary\"\n            mode=\"determinate\"\n            [value]=\"80\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"mb-1\">\n          <p class=\"m-0 text-13\">\n            Twitter <span class=\"text-muted\">| 5.8 | 60%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"accent\"\n            mode=\"determinate\"\n            [value]=\"60\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"\">\n          <p class=\"m-0 text-13\">\n            LinkedIn <span class=\"text-muted\">| 4.8 | 40%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"warn\"\n            mode=\"determinate\"\n            [value]=\"40\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <!--/ End 3rd row, 1st column -->\n\n  <!-- 3rd row, 2nd column -->\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"66.67\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { y: '50px', delay: '1000ms' } }\"\n  >\n    <div fxLayout=\"row wrap\">\n      <div fxFlex=\"100\"\n      fxFlex.gt-sm=\"50\">\n        <mat-card class=\"p-0\">\n          <div fxLayout=\"row wrap\">\n            <div fxFlex=\"40\" class=\"p-1\">\n              <p class=\"\">TRAFFIC</p>\n              <p class=\"text-24 text-green font-weight-bold mb-0\">\n                27% <span class=\"material-icons\">arrow_drop_up</span>\n              </p>\n              <p class=\"text-muted m-0\">This Week</p>\n            </div>\n            <div fxFlex=\"60\">\n              <div\n                echarts\n                [options]=\"trafficGrowthChart\"\n                [autoResize]=\"true\"\n                style=\"height: 150px; right: -1px\"\n              ></div>\n            </div>\n          </div>\n        </mat-card>\n      </div>\n      <div fxFlex=\"100\"\n      fxFlex.gt-sm=\"50\">\n        <mat-card class=\"p-0\">\n          <div fxLayout=\"row wrap\">\n            <div fxFlex=\"40\" class=\"p-1\">\n              <p class=\"\">Bounce Rate</p>\n              <p class=\"text-24 text-red font-weight-bold mb-0\">\n                20% <span class=\"material-icons\">arrow_drop_up</span>\n              </p>\n              <p class=\"text-muted m-0\">This Week</p>\n            </div>\n            <div fxFlex=\"60\">\n              <div\n                echarts\n                [options]=\"bounceRateGrowthChart\"\n                [autoResize]=\"true\"\n                style=\"height: 150px; right: -1px\"\n              ></div>\n            </div>\n          </div>\n        </mat-card>\n      </div>\n    </div>\n\n    <mat-card\n      class=\"p-0 \"\n      [@animate]=\"{ value: '*', params: { scale: '.9', delay: '300ms' } }\"\n    >\n      <mat-table [dataSource]=\"countryTrafficStats\" matSort>\n        <ng-container matColumnDef=\"country\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Country\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"id\">\n            <span class=\"flag-icon {{ row.flag }}\"></span>\n            {{ row.country }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"visitor\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Visitor\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"progress\">\n            {{ row.visitor }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"pageView\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Page View\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"name\">\n            {{ row.pageView }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"download\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Download\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"color\">\n            {{ row.download }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"bounceRate\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Bounce Rate\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"color\">\n            {{ row.bounceRate }}\n          </mat-cell>\n        </ng-container>\n\n        <!-- <ng-container matColumnDef=\"action\">\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Action </mat-header-cell>\n            <mat-cell *matCellDef=\"let row\"> \n              <button mat-button><mat-icon>keyboard_arrow_right</mat-icon></button>\n            </mat-cell>\n          </ng-container> -->\n\n        <mat-header-row\n          *matHeaderRowDef=\"[\n            'country',\n            'visitor',\n            'pageView',\n            'download',\n            'bounceRate'\n          ]\"\n        ></mat-header-row>\n        <mat-row\n          *matRowDef=\"\n            let row;\n            columns: [\n              'country',\n              'visitor',\n              'pageView',\n              'download',\n              'bounceRate'\n            ]\n          \"\n        >\n        </mat-row>\n      </mat-table>\n    </mat-card>\n  </div>\n  <!--/ End 3rd row, 2nd column -->\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardCryptocurrencyCryptocurrencyComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"p-0\" [@animate]=\"{ value: '*', params: { scale: '.9', delay: '200ms' } }\">\n  <div fxLayout=\"row wrap\">\n      <div\n          fxFlex=\"100\"\n          fxFlex.gt-sm=\"20\"\n          fxFlex.sm=\"50\"\n      >\n          <div class=\"text-center pt-1 pb-1 border-right-light\">\n              <mat-icon class=\"text-blue\">account_balance_wallet</mat-icon>\n              <h4 class=\"m-0\">5040</h4>\n              <small class=\"m-0 text-muted\">Bitcoin Balance</small>\n          </div>\n      </div>\n      <div\n          fxFlex=\"100\"\n          fxFlex.gt-sm=\"20\"\n          fxFlex.sm=\"50\"\n      >\n          <div class=\"text-center pt-1 pb-1 border-right-light\">\n              <mat-icon class=\"text-orange\">attach_money</mat-icon>\n              <h4 class=\"m-0 \">1200</h4>\n              <small class=\"m-0 text-muted\">USD Balance</small>\n          </div>\n      </div>\n      <div\n          fxFlex=\"100\"\n          fxFlex.gt-sm=\"20\"\n          fxFlex.sm=\"50\"\n      >\n          <div class=\"text-center pt-1 pb-1 border-right-light\">\n              <mat-icon class=\"text-blue\">business_center</mat-icon>\n              <h4 class=\"m-0 \">16420</h4>\n              <small class=\"m-0 text-muted\">Invesment</small>\n          </div>\n      </div>\n      <div\n          fxFlex=\"100\"\n          fxFlex.gt-sm=\"20\"\n          fxFlex.sm=\"50\"\n      >\n          <div class=\"text-center pt-1 pb-1 border-right-light\">\n              <mat-icon class=\"text-blue\">add</mat-icon>\n              <h4 class=\"m-0 \">3010</h4>\n              <small class=\"m-0 text-muted\">Profit</small>\n          </div>\n      </div>\n      <div\n          fxFlex=\"100\"\n          fxFlex.gt-sm=\"20\"\n          fxFlex.sm=\"50\"\n      >\n          <div class=\"text-center pt-1 pb-1\">\n              <mat-icon class=\"text-orange\">compare_arrows</mat-icon>\n              <h4 class=\"m-0 \">270</h4>\n              <small class=\"m-0 text-muted\">Transactions</small>\n          </div>\n      </div>\n  </div>\n</mat-card>\n\n\n\n\n<div fxLayout=\"row wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"66.67\" fxFlex.sm=\"50\" \n  [@animate]=\"{value:'*',params:{delay:'400ms',scale:'.9'}}\">\n  <mat-card>\n    <div fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <h6 class=\"m-0\">Bitcoin vs Ethereum </h6>\n      <span fxFlex></span>\n      <button mat-icon-button [matMenuTriggerFor]=\"chartMenu\">\n        <mat-icon>more_vert</mat-icon>\n      </button>\n      <mat-menu #chartMenu=\"matMenu\">\n        <button mat-menu-item>\n          <span>Last 7 days</span>\n        </button>\n        <button mat-menu-item>\n          <span>Last 30 days</span>\n        </button>\n      </mat-menu>\n    </div>\n\n    <div echarts [options]=\"cryptoChart\" [autoResize]=\"true\" style=\"height: 340px;\"></div>\n  </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33.3\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'400ms',scale:'.9'}}\">\n    <div class=\"m-333\">\n        <mat-accordion multi=\"false\">\n            <mat-expansion-panel expanded=\"true\">\n              <mat-expansion-panel-header>\n                <mat-panel-title>\n                  BTC\n                </mat-panel-title>\n              </mat-expansion-panel-header>\n              <div class=\"border-bottom pb-1 mb-1\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>BTC vs USD</span>\n                      <span fxFlex></span>\n                      <span>8897.45</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-green\">530(+3.73%)</small>\n                    </div>\n              </div>\n              <div class=\"border-bottom pb-1 mb-1\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>BTC vs EUR</span>\n                      <span fxFlex></span>\n                      <span>3829.23</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-red\">430(-1.43%)</small>\n                    </div>\n              </div>\n              <div class=\"\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>BTC vs GBP</span>\n                      <span fxFlex></span>\n                      <span>5836.79</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-green\">210(+2.23%)</small>\n                    </div>\n              </div>\n\n            </mat-expansion-panel>\n            <mat-expansion-panel>\n              <mat-expansion-panel-header>\n                <mat-panel-title>\n                  ETH\n                </mat-panel-title>\n              </mat-expansion-panel-header>\n\n              <div class=\"border-bottom pb-1 mb-1\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>ETH vs USD</span>\n                      <span fxFlex></span>\n                      <span>8897.45</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-green\">530(+3.73%)</small>\n                    </div>\n              </div>\n              <div class=\"border-bottom pb-1 mb-1\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>ETH vs EUR</span>\n                      <span fxFlex></span>\n                      <span>3829.23</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-red\">430(-1.43%)</small>\n                    </div>\n              </div>\n              <div class=\"\">\n                  <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                      <span>ETH vs GBP</span>\n                      <span fxFlex></span>\n                      <span>5836.79</span>\n                    </div>\n                    <div fxLyout=\"row wrap\" class=\"text-muted\">\n                      <small>24 hour change</small>\n                      <span fxFlex></span>\n                      <small class=\"text-green\">210(+2.23%)</small>\n                    </div>\n              </div>\n            </mat-expansion-panel>\n            <mat-expansion-panel>\n                <mat-expansion-panel-header>\n                  <mat-panel-title>\n                    LTC\n                  </mat-panel-title>\n                </mat-expansion-panel-header>\n  \n                <div class=\"border-bottom pb-1 mb-1\">\n                    <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                        <span>LTC vs USD</span>\n                        <span fxFlex></span>\n                        <span>8897.45</span>\n                      </div>\n                      <div fxLyout=\"row wrap\" class=\"text-muted\">\n                        <small>24 hour change</small>\n                        <span fxFlex></span>\n                        <small class=\"text-green\">530(+3.73%)</small>\n                      </div>\n                </div>\n                <div class=\"border-bottom pb-1 mb-1\">\n                    <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                        <span>LTC vs EUR</span>\n                        <span fxFlex></span>\n                        <span>3829.23</span>\n                      </div>\n                      <div fxLyout=\"row wrap\" class=\"text-muted\">\n                        <small>24 hour change</small>\n                        <span fxFlex></span>\n                        <small class=\"text-red\">430(-1.43%)</small>\n                      </div>\n                </div>\n                <div class=\"\">\n                    <div fxLyout=\"row wrap\" class=\"text-blue font-weight-bold\">\n                        <span>LTC vs GBP</span>\n                        <span fxFlex></span>\n                        <span>5836.79</span>\n                      </div>\n                      <div fxLyout=\"row wrap\" class=\"text-muted\">\n                        <small>24 hour change</small>\n                        <span fxFlex></span>\n                        <small class=\"text-green\">210(+2.23%)</small>\n                      </div>\n                </div>\n              </mat-expansion-panel>\n          </mat-accordion>\n    </div>\n \n    <!-- <mat-card>\n        <div fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n            <h6 class=\"m-0\">Balance </h6>\n          </div>\n        <div echarts [options]=\"cryptoDonutChart\" [autoResize]=\"true\" style=\"height: 318px;\"></div>\n    </mat-card> -->\n  </div>\n</div>\n\n<!-- COIN ROW -->\n\n<div fxLayout=\"row wrap\" [@animate]=\"{value:'*',params:{delay:'600ms',scale:'.9'}}\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\" >\n    <mat-card class=\"rounded-circle\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <img class=\"avatar-md rounded-circle\" src=\"assets/images/cryptocurrencies/BTC.png\" alt=\"\">\n      <span style=\"width: 20px\"></span>\n      <div>\n        <p class=\"m-0 text-14 text-muted line-height-1\">Bitcoin</p>\n        <span class=\"text-18 font-weight-bold text-green\">$254 <span class=\"material-icons mat-icon-18\">arrow_drop_up</span></span>\n      </div>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\" >\n    <mat-card class=\"rounded-circle\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <img class=\"avatar-md rounded-circle\" src=\"assets/images/cryptocurrencies/ETH.png\" alt=\"\">\n      <span style=\"width: 20px\"></span>\n      <div>\n        <p class=\"m-0 text-14 text-muted line-height-1\">Ethereum</p>\n        <span class=\"text-18 font-weight-bold text-red\">$64 <span class=\"material-icons mat-icon-18\">arrow_drop_down</span></span>\n      </div>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\" >\n    <mat-card class=\"rounded-circle\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <img class=\"avatar-md rounded-circle\" src=\"assets/images/cryptocurrencies/LTC.png\" alt=\"\">\n      <span style=\"width: 20px\"></span>\n      <div>\n        <p class=\"m-0 text-14 text-muted line-height-1\">Litecoin</p>\n        <span class=\"text-18 font-weight-bold text-green\">$45 <span class=\"material-icons mat-icon-18\">arrow_drop_up</span></span>\n      </div>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\" >\n    <mat-card class=\"rounded-circle\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <img class=\"avatar-md rounded-circle\" src=\"assets/images/cryptocurrencies/BTS.png\" alt=\"\">\n      <span style=\"width: 20px\"></span>\n      <div>\n        <p class=\"m-0 text-14 text-muted line-height-1\">BTS</p>\n        <span class=\"text-18 font-weight-bold text-red\">$10 <span class=\"material-icons mat-icon-18\">arrow_drop_down</span></span>\n      </div>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\" >\n    <mat-card class=\"rounded-circle\" fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n      <img class=\"avatar-md rounded-circle\" src=\"assets/images/cryptocurrencies/CNX.png\" alt=\"\">\n      <span style=\"width: 20px\"></span>\n      <div>\n        <p class=\"m-0 text-14 text-muted line-height-1\">CNX</p>\n        <span class=\"text-18 font-weight-bold text-green\">$80 <span class=\"material-icons mat-icon-18\">arrow_drop_up</span></span>\n      </div>\n    </mat-card>\n  </div>\n</div>\n<!--/ COIN ROW -->\n\n<!-- Table row -->\n<div fxLayout=\"row wrap\" [@animate]=\"{value:'*',params:{delay:'800ms',scale:'.9'}}\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"66.67\">\n      <mat-card class=\"p-0 \">\n          <h6 class=\"pl-1 pt-1 m-0 \">Active Trades</h6>\n          <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"activeTrades\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"0\"\n          [rowHeight]=\"'auto'\">\n          \n          <ngx-datatable-column name=\"Currency\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <div fxLayout=\"row wrap\" fxLayoutAlign=\"start center\">\n                  <img [src]=\"row.icon\" alt=\"\" class=\"avatar-xs mr-05\">\n                  {{ row?.currency }}\n              </div>\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Balance\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{row?.balance}}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Buying Rate\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                    {{row?.buyingRate}}\n              </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Current Rate\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                        {{row?.currentRate}}\n                  </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Action\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                        <button mat-stroked-button color=\"primary\">Close</button>\n                  </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>    \n      </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33.33\">\n      <mat-card class=\"p-0 \">\n          <h6 class=\"ml-1 mt-1\">Trending Currencies</h6>\n          <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"trendingCurrencies\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"0\"\n          [rowHeight]=\"'auto'\">\n          \n          <ngx-datatable-column name=\"Currency\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.currency }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Balance\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{row?.rate}}\n            </ng-template>\n          </ngx-datatable-column>\n          \n          <ngx-datatable-column name=\"Action\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                        <button mat-stroked-button color=\"warn\">Buy</button>\n                  </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>    \n      </mat-card>\n  </div>\n\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardDashboardDarkDashboardDarkComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- Dashboard card row -->\n<mat-card\n  class=\"p-0 dark-blue\"\n  [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '200ms' } }\"\n>\n  <div fxLayout=\"row wrap\">\n    <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-pink\">show_chart</mat-icon>\n        <h4 class=\"m-0 text-white\">5040</h4>\n        <small class=\"m-0 text-white\">Page views</small>\n      </div>\n    </div>\n    <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-yellow\">cloud_download</mat-icon>\n        <h4 class=\"m-0 text-white\">1200</h4>\n        <small class=\"m-0 text-white\">Downloads</small>\n      </div>\n    </div>\n    <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-pink\">comment</mat-icon>\n        <h4 class=\"m-0 text-white\">16420</h4>\n        <small class=\"m-0 text-white\">Comments</small>\n      </div>\n    </div>\n    <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\n      <div class=\"text-center pt-1 pb-1 border-right-light\">\n        <mat-icon class=\"text-white\">person</mat-icon>\n        <h4 class=\"m-0 text-white\">3010</h4>\n        <small class=\"m-0 text-white\">Profile views</small>\n      </div>\n    </div>\n    <div fxFlex=\"100\" fxFlex.gt-sm=\"20\" fxFlex.sm=\"50\">\n      <div class=\"text-center pt-1 pb-1\">\n        <mat-icon class=\"text-pink\">favorite</mat-icon>\n        <h4 class=\"m-0 text-white\">4070</h4>\n        <small class=\"m-0 text-white\">Likes</small>\n      </div>\n    </div>\n  </div>\n</mat-card>\n\n<div fxLayout=\"row wrap\">\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"33.33\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '250ms' } }\"\n  >\n    <mat-card class=\"p-0 dark-blue\">\n      <p class=\"text-white ml-1 mt-1\">TRAFFIC VS SALES</p>\n      <div\n        echarts\n        [options]=\"monthlyTrafficChartBar\"\n        [autoResize]=\"true\"\n        style=\"height: 216px;\"\n      ></div>\n    </mat-card>\n  </div>\n\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"33.33\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '300ms' } }\"\n  >\n    <mat-card class=\"dark-blue\">\n      <p class=\"text-white\">7 DAY TRAFFIC</p>\n      <div\n        echarts\n        [options]=\"dailyTrafficChartBar\"\n        [autoResize]=\"true\"\n        style=\"height: 200px;\"\n      ></div>\n    </mat-card>\n  </div>\n\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"33.33\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '350ms' } }\"\n  >\n    <mat-card class=\"p-0 dark-blue\">\n      <p class=\"text-white ml-1 mt-1\">TRAFFIC SOURCES</p>\n      <div\n        echarts\n        [options]=\"dailyBandwithUsage\"\n        [autoResize]=\"true\"\n        style=\"height: 216px;\"\n      ></div>\n    </mat-card>\n  </div>\n\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"60\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '400ms' } }\"\n  >\n    <mat-card\n      class=\"p-0 dark-blue\"\n    >\n      <mat-table [dataSource]=\"countryTrafficStats\" matSort>\n        <ng-container matColumnDef=\"country\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Country\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"id\">\n            <span class=\"flag-icon {{ row.flag }}\"></span>\n            {{ row.country }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"visitor\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Visitor\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"progress\">\n            {{ row.visitor }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"pageView\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Page View\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"name\">\n            {{ row.pageView }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"download\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Download\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"color\">\n            {{ row.download }}\n          </mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"bounceRate\">\n          <mat-header-cell *matHeaderCellDef mat-sort-header>\n            Bounce Rate\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\" data-label=\"color\">\n            {{ row.bounceRate }}\n          </mat-cell>\n        </ng-container>\n\n        <mat-header-row\n          *matHeaderRowDef=\"[\n            'country',\n            'visitor',\n            'pageView',\n            'download',\n            'bounceRate'\n          ]\"\n        ></mat-header-row>\n        <mat-row\n          *matRowDef=\"\n            let row;\n            columns: [\n              'country',\n              'visitor',\n              'pageView',\n              'download',\n              'bounceRate'\n            ]\n          \"\n        >\n        </mat-row>\n      </mat-table>\n    </mat-card>\n  </div>\n  <div\n    fxFlex=\"100\"\n    fxFlex.gt-sm=\"40\"\n    fxFlex.sm=\"50\"\n    [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '450ms' } }\"\n  >\n    <mat-card class=\"p-0 dark-blue\">\n      <div fxLayout=\"row wrap\">\n        <div fxFlex=\"40\" class=\"p-1\">\n          <p class=\"text-white\">TRAFFIC</p>\n          <p class=\"text-24 text-green font-weight-bold mb-0\">\n            27% <span class=\"material-icons\">arrow_drop_up</span>\n          </p>\n          <p class=\"text-white m-0\">This Week</p>\n        </div>\n        <div fxFlex=\"60\">\n          <div\n            echarts\n            [options]=\"trafficGrowthChart\"\n            [autoResize]=\"true\"\n            style=\"height: 150px; right: -1px\"\n          ></div>\n        </div>\n      </div>\n    </mat-card>\n    <mat-card\n      class=\"p-0 dark-blue\"\n      [@animate]=\"{ value: '*', params: { opacity: '0', y: '40px', delay: '500ms' } }\"\n    >\n      <mat-card-title class=\"mb-1\">\n        <div class=\"card-title-text\">\n          <span class=\"font-weight-normal text-white\"\n            >Top Campaign Performance</span\n          >\n\n          <span fxFlex></span>\n          <button\n            class=\"card-control\"\n            mat-icon-button\n            [matMenuTriggerFor]=\"menu2\"\n          >\n            <mat-icon class=\"text-white\">more_vert</mat-icon>\n          </button>\n          <mat-menu #menu2=\"matMenu\">\n            <button mat-menu-item>\n              <mat-icon>settings</mat-icon>\n              <span>Campaign Settings</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>do_not_disturb</mat-icon>\n              <span>Disable All Campaigns</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>close</mat-icon>\n              <span>Remove panel</span>\n            </button>\n          </mat-menu>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <div class=\"mb-1\">\n          <p class=\"m-0 text-white\">\n            Facebook <span class=\"\">| 9.8 | 90%</span>\n          </p>\n          <mat-progress-bar\n            color=\"accent\"\n            mode=\"determinate\"\n            [value]=\"90\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"mb-1\">\n          <p class=\"m-0 text-13 text-white\">\n            Google AdSense <span class=\"text-white\">| 8.3 | 80%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"warn\"\n            mode=\"determinate\"\n            [value]=\"80\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"mb-1\">\n          <p class=\"m-0 text-13 text-white\">\n            Twitter <span class=\"text-white\">| 5.8 | 60%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"accent\"\n            mode=\"determinate\"\n            [value]=\"60\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n        <div class=\"\">\n          <p class=\"m-0 text-13 text-white\">\n            LinkedIn <span class=\"text-white\">| 4.8 | 40%</span>\n          </p>\n          <mat-progress-bar\n            class=\"\"\n            color=\"warn\"\n            mode=\"determinate\"\n            [value]=\"40\"\n            class=\"rounded\"\n            style=\"height: 6px\"\n          >\n          </mat-progress-bar>\n        </div>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardDashboardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container\">\r\n  <div *ngIf=\"organization\">\r\n    <div class=\"numbers\">\r\n        <mat-card class=\"number\">\r\n          <h2 style=\"text-align: center;\"><strong>Number of chats Today</strong></h2>\r\n          <p style=\"text-align: center;\"><strong>{{number_of_chats_today}}</strong></p>\r\n        </mat-card>\r\n        \r\n        <mat-card class=\"number\">\r\n          <h2 style=\"text-align: center;\"><strong>Number of Unique Users</strong></h2>\r\n          <p style=\"text-align: center;\"><strong>{{daily_users}}</strong></p>\r\n        </mat-card>\r\n      \r\n        <mat-card class=\"number\">\r\n          <h2 style=\"text-align: center;\"><strong>Number of Unhandled Chats</strong></h2>\r\n          <p style=\"text-align: center;\"><strong>{{unhandled}}</strong></p>\r\n        </mat-card>\r\n        <mat-card class=\"number\">\r\n          <h2 style=\"text-align: center;\"><strong>Number of CheckIns</strong></h2>\r\n          <p style=\"text-align: center;\"><strong>{{checkin}}</strong></p>\r\n        </mat-card>\r\n        <mat-card class=\"number\">\r\n          <h2 style=\"text-align: center;\"><strong>Number of Reservations</strong></h2>\r\n          <p style=\"text-align: center;\"><strong>{{reservation}}</strong></p>\r\n        </mat-card>\r\n      </div>\r\n  <mat-card>\r\n    <h2 style=\"text-align: center;\"><strong>Chatbot Data</strong></h2>\r\n    \r\n    <table class=\"table\">\r\n  <thead>\r\n    <tr>\r\n      <th scope=\"col\">ID</th>\r\n      <th scope=\"col\">Organization Name</th>\r\n      <th scope=\"col\">Sender ID</th>\r\n      <th scope=\"col\">Sender Message</th>\r\n      <th scope=\"col\">Bot Message</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr *ngFor=\"let item of log\">\r\n      <th scope=\"row\">{{item.id}}</th>\r\n      <td>{{item.organization_bot.user_organization.organization_name}}</td>\r\n      <td>{{item.sender_id}}</td>\r\n      <td>{{item.sender_message}}</td>\r\n      <td>{{item.chatbot_message}}</td>\r\n      \r\n    </tr>\r\n    \r\n  </tbody>\r\n</table>\r\n\r\n<div  style=\"text-align: center; display: flex; justify-content: space-evenly;\">\r\n        <a (click)=\"prev()\">Prev</a>\r\n        <a (click)=\"next()\">Next</a>\r\n</div>\r\n\r\n<br>\r\n    <button mat-raised-button (click)=\"downloadAction()\">Download Data</button>\r\n    \r\n\r\n\r\n    <table [hidden]=\"true\" class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">ID</th>\r\n                <th scope=\"col\">Organization Name</th>\r\n                <th scope=\"col\">Sender ID</th>\r\n                <th scope=\"col\">Sender Message</th>\r\n                <th scope=\"col\">Bot Message</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let item of total\">\r\n                <th scope=\"row\">{{item.id}}</th>\r\n                <td>{{item.organization_bot.user_organization.organization_name}}</td>\r\n                <td>{{item.sender_id}}</td>\r\n                <td>{{item.sender_message}}</td>\r\n                <td>{{item.chatbot_message}}</td>\r\n                \r\n              </tr>\r\n              \r\n            </tbody>\r\n          </table>\r\n</mat-card>\r\n\r\n</div>\r\n\r\n\r\n  <mat-card id=\"card\">\r\n\r\n\r\n    <div class=\"content-post\">\r\n        <p style=\"color:grey; margin-left:10px;\"> Content Generation</p>\r\n        <canvas id=\"post-chart\"  ></canvas>\r\n    </div>\r\n\r\n\r\n\r\n\r\n    \r\n\r\n\r\n\r\n</mat-card>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</div>\r\n\r\n<div style=\"margin-left: 21px;\" *ngIf=\"!organization\"><button mat-raised-button  data-toggle=\"modal\" data-target=\"#myModal\">Create Organization</button></div>\r\n\r\n\r\n<div class=\"modal fade\" id=\"myModal\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"false\">\r\n  <div class=\"modal-dialog\">\r\n  \r\n    <!-- Modal content-->\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">Create Organization</h4>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n        \r\n      </div>\r\n      <div class=\"modal-body\">\r\n\r\n        <mat-form-field class=\"example-full-width\">\r\n          <input (input)=\"org_name=$event.target.value\"  matInput placeholder=\"Organization Name\">\r\n        </mat-form-field>\r\n        \r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button  mat-raised-button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" (click)=\"createOrg()\">Create Organization</button>\r\n      </div>\r\n    </div>\r\n    \r\n  </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/default-dashboard/default-dashboard.component.html":
  /*!**************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/default-dashboard/default-dashboard.component.html ***!
    \**************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardDefaultDashboardDefaultDashboardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- Dashboard card row -->\n<div fxLayout=\"row wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{scale:'.9',delay:'300ms'}}\">\n    <mat-card>\n      <mat-card-title fxLayoutAlign=\"start center\">\n        <small class=\"text-muted\">Total Sales</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"primary\" \n        selected=\"true\"><mat-icon>trending_up</mat-icon>20%</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">22,450</h3>\n        <small class=\"text-muted\">Monthly</small>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">Revenue</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"accent\" \n        selected=\"true\"><mat-icon>trending_up</mat-icon>10%</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">377,670</h3>\n        <small class=\"text-muted\">Monthly</small>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">Traffic</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"accent\" \n        selected=\"true\"><mat-icon>trending_up</mat-icon>9%</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">54,845</h3>\n        <small class=\"text-muted\">Monthly</small>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">New User</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"warn\" \n        selected=\"true\"><mat-icon>trending_down</mat-icon>2%</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">245</h3>\n        <small class=\"text-muted\">Monthly</small>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n<!-- Fullwidth chart -->\n<div fxLayout=\"row wrap\" [@animate]=\"{value:'*',params:{y:'25px',delay:'300ms'}}\">\n  <div fxFlex=\"100\">\n    <mat-card class=\"default\">\n      <mat-card-title class=\"\">\n        <span>Orders</span>\n        <span fxFlex></span>\n      </mat-card-title>\n      <mat-card-subtitle>Orders vs New user registrations</mat-card-subtitle>\n      <mat-card-content class=\"p-0\" [style.height]=\"'240px'\">\n        <canvas \n          height=\"240\"\n          baseChart \n          class=\"chart m-0\"\n          [datasets]=\"lineChartSteppedData\"\n          [labels]=\"lineChartLabels\"\n          [options]=\"lineChartOptions\"\n          [colors]=\"lineChartColors\"\n          [legend]=\"lineChartLegend\"\n          [chartType]=\"lineChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n<div fxLayout=\"row wrap\">\n  <!-- Gallery and chart column -->\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33.33\" fxLayout=\"column\">\n    <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n      <div fxLayout=\"row wrap\" class=\"doughnut-grid text-center\">\n          <div fxFlex=\"50\" class=\"doughnut-grid-item light-mat-gray\">\n            <canvas \n            height=\"120\"\n            baseChart\n            class=\"chart\"\n            [data]=\"doughnutChartData1\"\n            [options]=\"doughnutOptions\"\n            [labels]=\"doughnutLabels\"\n            [colors]=\"doughnutChartColors1\"\n            [chartType]=\"doughnutChartType\"></canvas>\n            <small>Space: {{data1}}/{{total1}} GB</small>\n        </div>\n        <div fxFlex=\"50\" class=\"doughnut-grid-item\">\n          <canvas \n            height=\"120\" \n            baseChart \n            class=\"chart\"\n            [data]=\"doughnutChartData2\"\n            [options]=\"doughnutOptions\"\n            [labels]=\"doughnutLabels\"\n            [colors]=\"doughnutChartColors2\"\n            [chartType]=\"doughnutChartType\"></canvas>\n            <small>Tasks: 8/12</small>\n      </div>\n      <div fxFlex=\"50\" class=\"doughnut-grid-item\">\n        <canvas \n          height=\"120\" \n          baseChart \n          class=\"chart\"\n          [data]=\"doughnutChartData1\"\n          [options]=\"doughnutOptions\"\n          [labels]=\"doughnutLabels\"\n          [colors]=\"doughnutChartColors2\"\n          [chartType]=\"doughnutChartType\"></canvas>\n          <small>Tickets: 15/40</small>\n      </div>\n      <div fxFlex=\"50\" class=\"doughnut-grid-item light-mat-gray\">\n        <canvas \n        height=\"120\" \n        baseChart \n        class=\"chart\"\n        [data]=\"doughnutChartData2\"\n        [options]=\"doughnutOptions\"\n        [labels]=\"doughnutLabels\"\n        [colors]=\"doughnutChartColors1\"\n        [chartType]=\"doughnutChartType\"></canvas>\n        <small>Stock: 1600/2000</small>\n      </div>\n    </div>\n    </mat-card>\n\n    <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{scale:'.9',delay:'300ms'}}\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">\n          <span>Top Campaign Performance</span>\n          <span fxFlex></span>\n          <button class=\"card-control\" mat-icon-button [matMenuTriggerFor]=\"menu2\">\n            <mat-icon>more_vert</mat-icon>\n          </button>\n          <mat-menu #menu2=\"matMenu\">\n            <button mat-menu-item>\n              <mat-icon>settings</mat-icon>\n              <span>Campaign Settings</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>do_not_disturb</mat-icon>\n              <span>Disable All Campaigns</span>\n            </button>\n            <button mat-menu-item>\n              <mat-icon>close</mat-icon>\n              <span>Remove panel</span>\n            </button>\n          </mat-menu>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n       <div class=\"mb-1\">\n        <p class=\"mb-05 text-muted\">Facebook Campaign</p>\n        <mat-progress-bar\n        class=\"\"\n        color=\"primary\"\n        mode=\"determinate\"\n        [value]=\"90\">\n        </mat-progress-bar>\n       </div>\n       <div class=\"mb-1\">\n        <p class=\"mb-05 text-muted\">Google AdSense</p>\n        <mat-progress-bar\n        class=\"\"\n        color=\"primary\"\n        mode=\"determinate\"\n        [value]=\"80\">\n        </mat-progress-bar>\n       </div>\n       <div class=\"mb-1\">\n        <p class=\"mb-05 text-muted\">Twitter Campaign</p>\n        <mat-progress-bar\n        class=\"\"\n        color=\"accent\"\n        mode=\"determinate\"\n        [value]=\"60\">\n        </mat-progress-bar>\n       </div>\n       <div class=\"mb-1\">\n        <p class=\"mb-05 text-muted\">LinkedIn Campaign</p>\n        <mat-progress-bar\n        class=\"\"\n        color=\"warn\"\n        mode=\"determinate\"\n        [value]=\"40\">\n        </mat-progress-bar>\n       </div>\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"default\">\n      <mat-card-content class=\"p-0\">\n        <mat-grid-list cols=\"3\" rowHeight=\"1:1\" class=\"app-gallery\">\n          <!-- Gallery item -->\n          <mat-grid-tile *ngFor=\"let photo of photos\">\n            <img [src]=\"photo.url\" alt=\"\">\n            <!-- item detail, show on hover -->\n            <div class=\"gallery-control-wrap\">\n              <div class=\"gallery-control\">\n                <h4 class=\"photo-detail fz-1\" [fxHide.lt-sm]=\"true\">{{photo.name}}</h4>\n                <span fxFlex></span>\n                <button mat-icon-button [matMenuTriggerFor]=\"photoMenu\" class=\"\">\n                  <mat-icon>more_vert</mat-icon>\n                </button>\n                <mat-menu #photoMenu=\"matMenu\">\n                  <button mat-menu-item><mat-icon>send</mat-icon>Send as attachment</button>\n                  <button mat-menu-item><mat-icon>favorite</mat-icon>Favorite</button>\n                  <button mat-menu-item><mat-icon>delete</mat-icon>Delete</button>\n                </mat-menu>\n              </div>\n            </div>\n          </mat-grid-tile>\n        </mat-grid-list>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <!-- ticket and project table column -->\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"66.66\" fxLayout=\"column wrap\">\n    <mat-card class=\"default\" [@animate]=\"{value:'*',params:{delay:'300ms',y:'50px'}}\">\n      <mat-card-title>Recent Tickets</mat-card-title>\n      <mat-card-content class=\"p-0\">\n        <mat-list class=\"compact-list mb-1\">\n          <mat-list-item class=\"\" *ngFor=\"let t of tickets\">\n            <img mat-list-avatar class=\"mr-1\" [src]=\"t.img\" alt=\"\">\n            <div fxLayout=\"row\" fxFlex=\"100\">\n              <h6 class=\"m-0 mr-1\">{{t.name}}</h6>\n              <span fxFlex></span>\n              <div fxFlex=\"40\">{{t.text | excerpt:20 }}</div>\n              <span fxFlex></span>\n              <small class=\"text-muted mr-1\">{{ t.date | relativeTime}}</small>\n            </div>\n            <mat-chip mat-sm-chip [color]=\"'warn'\" [selected]=\"t.isOpen\">{{t.isOpen ? 'active' : 'closed'}}</mat-chip>\n          </mat-list-item>\n        </mat-list>\n        <div class=\"text-center\">\n          <button mat-button class=\"full-width\">View all</button>\n        </div>\n      </mat-card-content>\n    </mat-card>\n\n    <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n      <mat-card-content class=\"p-0\">\n        <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"projects\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"0\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Sprints\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.name }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Manager\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.user }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Progress\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <mat-progress-bar\n                class=\"\"\n                color=\"primary\"\n                mode=\"determinate\"\n                [value]=\"row.progress\">\n              </mat-progress-bar>\n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n        <div class=\"text-center\">\n          <button mat-button class=\"full-width\">View all</button>\n        </div>\n      </mat-card-content>\n    </mat-card>\n\n    <!-- Users Row -->\n    <div fxLayout=\"row wrap\">\n      <div\n      *ngFor=\"let user of users\" \n      fxFlex=\"100\"\n      fxFlex.gt-sm=\"50\">\n        <mat-card class=\"user-card p-0\">\n          <mat-card-title class=\"mat-bg-primary\">\n            <div class=\"card-title-text\">\n              <a href=\"\" class=\"toolbar-avatar md mr-1\"><img [src]=\"user.photo\" alt=\"\"></a>\n              <span>{{user.name}}</span>\n              <span fxFlex></span>\n              <button mat-icon-button [matMenuTriggerFor]=\"userMenu\" class=\"\">\n                  <mat-icon class=\"\">more_vert</mat-icon>\n              </button>\n              <mat-menu #userMenu=\"matMenu\">\n                  <button mat-menu-item>Follow</button>\n                  <button mat-menu-item>Message</button>\n                  <button mat-menu-item>Block</button>\n                  <button mat-menu-item>Delete</button>\n              </mat-menu>\n            </div>\n            <mat-divider></mat-divider>\n          </mat-card-title>\n          <mat-card-content>\n            <!-- user detail lines-->\n            <div class=\"user-details\">\n              <p><mat-icon class=\"text-muted\">card_membership</mat-icon>{{user.membership}}</p>\n              <p><mat-icon class=\"text-muted\">date_range</mat-icon>Member since {{user.registered | date}}</p>\n              <p><mat-icon class=\"text-muted\">location_on</mat-icon>{{user.address}}</p>\n            </div>\n          </mat-card-content>\n        </mat-card>\n      </div>\n    </div>\n    <!-- End Users Row -->\n  </div>\n  <!-- End tables and users column -->\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/manage-team/manage-team.component.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/manage-team/manage-team.component.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsDashboardManageTeamManageTeamComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card>\n    <div id=\"mynav\">\n        <button mat-button data-toggle=\"modal\" data-target=\"#exampleModal\">Add People</button>\n        <button mat-button data-toggle=\"modal\" data-target=\"#exampleModal2\">Invite People</button>\n    </div>\n</mat-card>\n<mat-card class=\"mycon\">\n\n\n    \n\n\n    <div *ngIf=\"team\">\n        <h3>My Team:</h3>\n    </div>\n    <div class=\"team\">\n        <mat-card class=\"member\" *ngFor=\"let item of team\">\n            \n            <img src=\"https://banner2.cleanpng.com/20180402/ojw/kisspng-united-states-avatar-organization-information-user-avatar-5ac20804a62b58.8673620215226654766806.jpg\" alt=\"\">\n            {{item.user.user.email}}\n            <button mat-raised-button class=\"btn btn-dark\" style=\"margin-top: 5px;\"\n                (click)=\"removeMember(item.id)\">Remove</button>\n        </mat-card>\n    </div>\n\n    <div *ngIf=\"c_teams\">\n        <h3>Current Teams:</h3>\n    </div>\n    <div class=\"team\">\n        <mat-card class=\"member_c\" *ngFor=\"let item of c_teams\">\n            <strong>Team Leader</strong>\n            <img src=\"https://banner2.cleanpng.com/20180402/ojw/kisspng-united-states-avatar-organization-information-user-avatar-5ac20804a62b58.8673620215226654766806.jpg\" alt=\"\">\n\n            {{item.owner.user.email}}\n            <br>\n            <button mat-raised-button class=\"btn btn-dark btn-sm\" (click)=\"quitTeam(item.id)\">Quit Team</button>\n        </mat-card>\n    </div>\n\n\n\n\n</mat-card>\n\n\n<!-- Button trigger modal -->\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\" >\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h3 class=\"modal-title\" id=\"exampleModalLabel\">Add User to your team</h3>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n\n                <input class=\"form-control\" id=\"exampleInputEmail1\" [value]=\"active_email\" (input)=\"searchUser($event)\"\n                    placeholder=\"Enter search term...\">\n                <ul class=\"list-group\">\n                    <li *ngFor=\"let item of search_results\" (click)=\"setActiveUser(item.user.email)\"\n                        class=\"list-group-item\">{{item.user.email}}</li>\n                </ul>\n                <br>\n            </div>\n            <div class=\"modal-footer\">\n\n                <button mat-raised-button type=\"button\" class=\"btn btn-primary\" (click)=\"invite('active')\" data-dismiss=\"modal\">Add\n                    People</button>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n\n<div class=\"modal fade\" id=\"exampleModal2\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\" >\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h3 class=\"modal-title\" id=\"exampleModalLabel\">Invite People</h3>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n\n                    <input class=\"form-control\" id=\"exampleInputEmail1\" (input)=\"email = $event.target.value\"\n                    placeholder=\"Enter an email address...\">\n            </div>\n            <div class=\"modal-footer\">\n\n                <button mat-raised-button type=\"button\"  class=\"btn btn-primary\" (click)=\"invite('cWork')\" data-dismiss=\"modal\">Invite\n                    People</button>\n            </div>\n        </div>\n    </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/tinycolor2/tinycolor.js":
  /*!**********************************************!*\
    !*** ./node_modules/tinycolor2/tinycolor.js ***!
    \**********************************************/

  /*! no static exports found */

  /***/
  function node_modulesTinycolor2TinycolorJs(module, exports, __webpack_require__) {
    var __WEBPACK_AMD_DEFINE_RESULT__; // TinyColor v1.4.1
    // https://github.com/bgrins/TinyColor
    // Brian Grinstead, MIT License


    (function (Math) {
      var trimLeft = /^\s+/,
          trimRight = /\s+$/,
          tinyCounter = 0,
          mathRound = Math.round,
          mathMin = Math.min,
          mathMax = Math.max,
          mathRandom = Math.random;

      function tinycolor(color, opts) {
        color = color ? color : '';
        opts = opts || {}; // If input is already a tinycolor, return itself

        if (color instanceof tinycolor) {
          return color;
        } // If we are called as a function, call using new instead


        if (!(this instanceof tinycolor)) {
          return new tinycolor(color, opts);
        }

        var rgb = inputToRGB(color);
        this._originalInput = color, this._r = rgb.r, this._g = rgb.g, this._b = rgb.b, this._a = rgb.a, this._roundA = mathRound(100 * this._a) / 100, this._format = opts.format || rgb.format;
        this._gradientType = opts.gradientType; // Don't let the range of [0,255] come back in [0,1].
        // Potentially lose a little bit of precision here, but will fix issues where
        // .5 gets interpreted as half of the total, instead of half of 1
        // If it was supposed to be 128, this was already taken care of by `inputToRgb`

        if (this._r < 1) {
          this._r = mathRound(this._r);
        }

        if (this._g < 1) {
          this._g = mathRound(this._g);
        }

        if (this._b < 1) {
          this._b = mathRound(this._b);
        }

        this._ok = rgb.ok;
        this._tc_id = tinyCounter++;
      }

      tinycolor.prototype = {
        isDark: function isDark() {
          return this.getBrightness() < 128;
        },
        isLight: function isLight() {
          return !this.isDark();
        },
        isValid: function isValid() {
          return this._ok;
        },
        getOriginalInput: function getOriginalInput() {
          return this._originalInput;
        },
        getFormat: function getFormat() {
          return this._format;
        },
        getAlpha: function getAlpha() {
          return this._a;
        },
        getBrightness: function getBrightness() {
          //http://www.w3.org/TR/AERT#color-contrast
          var rgb = this.toRgb();
          return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
        },
        getLuminance: function getLuminance() {
          //http://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
          var rgb = this.toRgb();
          var RsRGB, GsRGB, BsRGB, R, G, B;
          RsRGB = rgb.r / 255;
          GsRGB = rgb.g / 255;
          BsRGB = rgb.b / 255;

          if (RsRGB <= 0.03928) {
            R = RsRGB / 12.92;
          } else {
            R = Math.pow((RsRGB + 0.055) / 1.055, 2.4);
          }

          if (GsRGB <= 0.03928) {
            G = GsRGB / 12.92;
          } else {
            G = Math.pow((GsRGB + 0.055) / 1.055, 2.4);
          }

          if (BsRGB <= 0.03928) {
            B = BsRGB / 12.92;
          } else {
            B = Math.pow((BsRGB + 0.055) / 1.055, 2.4);
          }

          return 0.2126 * R + 0.7152 * G + 0.0722 * B;
        },
        setAlpha: function setAlpha(value) {
          this._a = boundAlpha(value);
          this._roundA = mathRound(100 * this._a) / 100;
          return this;
        },
        toHsv: function toHsv() {
          var hsv = rgbToHsv(this._r, this._g, this._b);
          return {
            h: hsv.h * 360,
            s: hsv.s,
            v: hsv.v,
            a: this._a
          };
        },
        toHsvString: function toHsvString() {
          var hsv = rgbToHsv(this._r, this._g, this._b);
          var h = mathRound(hsv.h * 360),
              s = mathRound(hsv.s * 100),
              v = mathRound(hsv.v * 100);
          return this._a == 1 ? "hsv(" + h + ", " + s + "%, " + v + "%)" : "hsva(" + h + ", " + s + "%, " + v + "%, " + this._roundA + ")";
        },
        toHsl: function toHsl() {
          var hsl = rgbToHsl(this._r, this._g, this._b);
          return {
            h: hsl.h * 360,
            s: hsl.s,
            l: hsl.l,
            a: this._a
          };
        },
        toHslString: function toHslString() {
          var hsl = rgbToHsl(this._r, this._g, this._b);
          var h = mathRound(hsl.h * 360),
              s = mathRound(hsl.s * 100),
              l = mathRound(hsl.l * 100);
          return this._a == 1 ? "hsl(" + h + ", " + s + "%, " + l + "%)" : "hsla(" + h + ", " + s + "%, " + l + "%, " + this._roundA + ")";
        },
        toHex: function toHex(allow3Char) {
          return rgbToHex(this._r, this._g, this._b, allow3Char);
        },
        toHexString: function toHexString(allow3Char) {
          return '#' + this.toHex(allow3Char);
        },
        toHex8: function toHex8(allow4Char) {
          return rgbaToHex(this._r, this._g, this._b, this._a, allow4Char);
        },
        toHex8String: function toHex8String(allow4Char) {
          return '#' + this.toHex8(allow4Char);
        },
        toRgb: function toRgb() {
          return {
            r: mathRound(this._r),
            g: mathRound(this._g),
            b: mathRound(this._b),
            a: this._a
          };
        },
        toRgbString: function toRgbString() {
          return this._a == 1 ? "rgb(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ")" : "rgba(" + mathRound(this._r) + ", " + mathRound(this._g) + ", " + mathRound(this._b) + ", " + this._roundA + ")";
        },
        toPercentageRgb: function toPercentageRgb() {
          return {
            r: mathRound(bound01(this._r, 255) * 100) + "%",
            g: mathRound(bound01(this._g, 255) * 100) + "%",
            b: mathRound(bound01(this._b, 255) * 100) + "%",
            a: this._a
          };
        },
        toPercentageRgbString: function toPercentageRgbString() {
          return this._a == 1 ? "rgb(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%)" : "rgba(" + mathRound(bound01(this._r, 255) * 100) + "%, " + mathRound(bound01(this._g, 255) * 100) + "%, " + mathRound(bound01(this._b, 255) * 100) + "%, " + this._roundA + ")";
        },
        toName: function toName() {
          if (this._a === 0) {
            return "transparent";
          }

          if (this._a < 1) {
            return false;
          }

          return hexNames[rgbToHex(this._r, this._g, this._b, true)] || false;
        },
        toFilter: function toFilter(secondColor) {
          var hex8String = '#' + rgbaToArgbHex(this._r, this._g, this._b, this._a);
          var secondHex8String = hex8String;
          var gradientType = this._gradientType ? "GradientType = 1, " : "";

          if (secondColor) {
            var s = tinycolor(secondColor);
            secondHex8String = '#' + rgbaToArgbHex(s._r, s._g, s._b, s._a);
          }

          return "progid:DXImageTransform.Microsoft.gradient(" + gradientType + "startColorstr=" + hex8String + ",endColorstr=" + secondHex8String + ")";
        },
        toString: function toString(format) {
          var formatSet = !!format;
          format = format || this._format;
          var formattedString = false;
          var hasAlpha = this._a < 1 && this._a >= 0;
          var needsAlphaFormat = !formatSet && hasAlpha && (format === "hex" || format === "hex6" || format === "hex3" || format === "hex4" || format === "hex8" || format === "name");

          if (needsAlphaFormat) {
            // Special case for "transparent", all other non-alpha formats
            // will return rgba when there is transparency.
            if (format === "name" && this._a === 0) {
              return this.toName();
            }

            return this.toRgbString();
          }

          if (format === "rgb") {
            formattedString = this.toRgbString();
          }

          if (format === "prgb") {
            formattedString = this.toPercentageRgbString();
          }

          if (format === "hex" || format === "hex6") {
            formattedString = this.toHexString();
          }

          if (format === "hex3") {
            formattedString = this.toHexString(true);
          }

          if (format === "hex4") {
            formattedString = this.toHex8String(true);
          }

          if (format === "hex8") {
            formattedString = this.toHex8String();
          }

          if (format === "name") {
            formattedString = this.toName();
          }

          if (format === "hsl") {
            formattedString = this.toHslString();
          }

          if (format === "hsv") {
            formattedString = this.toHsvString();
          }

          return formattedString || this.toHexString();
        },
        clone: function clone() {
          return tinycolor(this.toString());
        },
        _applyModification: function _applyModification(fn, args) {
          var color = fn.apply(null, [this].concat([].slice.call(args)));
          this._r = color._r;
          this._g = color._g;
          this._b = color._b;
          this.setAlpha(color._a);
          return this;
        },
        lighten: function lighten() {
          return this._applyModification(_lighten, arguments);
        },
        brighten: function brighten() {
          return this._applyModification(_brighten, arguments);
        },
        darken: function darken() {
          return this._applyModification(_darken, arguments);
        },
        desaturate: function desaturate() {
          return this._applyModification(_desaturate, arguments);
        },
        saturate: function saturate() {
          return this._applyModification(_saturate, arguments);
        },
        greyscale: function greyscale() {
          return this._applyModification(_greyscale, arguments);
        },
        spin: function spin() {
          return this._applyModification(_spin, arguments);
        },
        _applyCombination: function _applyCombination(fn, args) {
          return fn.apply(null, [this].concat([].slice.call(args)));
        },
        analogous: function analogous() {
          return this._applyCombination(_analogous, arguments);
        },
        complement: function complement() {
          return this._applyCombination(_complement, arguments);
        },
        monochromatic: function monochromatic() {
          return this._applyCombination(_monochromatic, arguments);
        },
        splitcomplement: function splitcomplement() {
          return this._applyCombination(_splitcomplement, arguments);
        },
        triad: function triad() {
          return this._applyCombination(_triad, arguments);
        },
        tetrad: function tetrad() {
          return this._applyCombination(_tetrad, arguments);
        }
      }; // If input is an object, force 1 into "1.0" to handle ratios properly
      // String input requires "1.0" as input, so 1 will be treated as 1

      tinycolor.fromRatio = function (color, opts) {
        if (typeof color == "object") {
          var newColor = {};

          for (var i in color) {
            if (color.hasOwnProperty(i)) {
              if (i === "a") {
                newColor[i] = color[i];
              } else {
                newColor[i] = convertToPercentage(color[i]);
              }
            }
          }

          color = newColor;
        }

        return tinycolor(color, opts);
      }; // Given a string or object, convert that input to RGB
      // Possible string inputs:
      //
      //     "red"
      //     "#f00" or "f00"
      //     "#ff0000" or "ff0000"
      //     "#ff000000" or "ff000000"
      //     "rgb 255 0 0" or "rgb (255, 0, 0)"
      //     "rgb 1.0 0 0" or "rgb (1, 0, 0)"
      //     "rgba (255, 0, 0, 1)" or "rgba 255, 0, 0, 1"
      //     "rgba (1.0, 0, 0, 1)" or "rgba 1.0, 0, 0, 1"
      //     "hsl(0, 100%, 50%)" or "hsl 0 100% 50%"
      //     "hsla(0, 100%, 50%, 1)" or "hsla 0 100% 50%, 1"
      //     "hsv(0, 100%, 100%)" or "hsv 0 100% 100%"
      //


      function inputToRGB(color) {
        var rgb = {
          r: 0,
          g: 0,
          b: 0
        };
        var a = 1;
        var s = null;
        var v = null;
        var l = null;
        var ok = false;
        var format = false;

        if (typeof color == "string") {
          color = stringInputToObject(color);
        }

        if (typeof color == "object") {
          if (isValidCSSUnit(color.r) && isValidCSSUnit(color.g) && isValidCSSUnit(color.b)) {
            rgb = rgbToRgb(color.r, color.g, color.b);
            ok = true;
            format = String(color.r).substr(-1) === "%" ? "prgb" : "rgb";
          } else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.v)) {
            s = convertToPercentage(color.s);
            v = convertToPercentage(color.v);
            rgb = hsvToRgb(color.h, s, v);
            ok = true;
            format = "hsv";
          } else if (isValidCSSUnit(color.h) && isValidCSSUnit(color.s) && isValidCSSUnit(color.l)) {
            s = convertToPercentage(color.s);
            l = convertToPercentage(color.l);
            rgb = hslToRgb(color.h, s, l);
            ok = true;
            format = "hsl";
          }

          if (color.hasOwnProperty("a")) {
            a = color.a;
          }
        }

        a = boundAlpha(a);
        return {
          ok: ok,
          format: color.format || format,
          r: mathMin(255, mathMax(rgb.r, 0)),
          g: mathMin(255, mathMax(rgb.g, 0)),
          b: mathMin(255, mathMax(rgb.b, 0)),
          a: a
        };
      } // Conversion Functions
      // --------------------
      // `rgbToHsl`, `rgbToHsv`, `hslToRgb`, `hsvToRgb` modified from:
      // <http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript>
      // `rgbToRgb`
      // Handle bounds / percentage checking to conform to CSS color spec
      // <http://www.w3.org/TR/css3-color/>
      // *Assumes:* r, g, b in [0, 255] or [0, 1]
      // *Returns:* { r, g, b } in [0, 255]


      function rgbToRgb(r, g, b) {
        return {
          r: bound01(r, 255) * 255,
          g: bound01(g, 255) * 255,
          b: bound01(b, 255) * 255
        };
      } // `rgbToHsl`
      // Converts an RGB color value to HSL.
      // *Assumes:* r, g, and b are contained in [0, 255] or [0, 1]
      // *Returns:* { h, s, l } in [0,1]


      function rgbToHsl(r, g, b) {
        r = bound01(r, 255);
        g = bound01(g, 255);
        b = bound01(b, 255);
        var max = mathMax(r, g, b),
            min = mathMin(r, g, b);
        var h,
            s,
            l = (max + min) / 2;

        if (max == min) {
          h = s = 0; // achromatic
        } else {
          var d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

          switch (max) {
            case r:
              h = (g - b) / d + (g < b ? 6 : 0);
              break;

            case g:
              h = (b - r) / d + 2;
              break;

            case b:
              h = (r - g) / d + 4;
              break;
          }

          h /= 6;
        }

        return {
          h: h,
          s: s,
          l: l
        };
      } // `hslToRgb`
      // Converts an HSL color value to RGB.
      // *Assumes:* h is contained in [0, 1] or [0, 360] and s and l are contained [0, 1] or [0, 100]
      // *Returns:* { r, g, b } in the set [0, 255]


      function hslToRgb(h, s, l) {
        var r, g, b;
        h = bound01(h, 360);
        s = bound01(s, 100);
        l = bound01(l, 100);

        function hue2rgb(p, q, t) {
          if (t < 0) t += 1;
          if (t > 1) t -= 1;
          if (t < 1 / 6) return p + (q - p) * 6 * t;
          if (t < 1 / 2) return q;
          if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
          return p;
        }

        if (s === 0) {
          r = g = b = l; // achromatic
        } else {
          var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
          var p = 2 * l - q;
          r = hue2rgb(p, q, h + 1 / 3);
          g = hue2rgb(p, q, h);
          b = hue2rgb(p, q, h - 1 / 3);
        }

        return {
          r: r * 255,
          g: g * 255,
          b: b * 255
        };
      } // `rgbToHsv`
      // Converts an RGB color value to HSV
      // *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
      // *Returns:* { h, s, v } in [0,1]


      function rgbToHsv(r, g, b) {
        r = bound01(r, 255);
        g = bound01(g, 255);
        b = bound01(b, 255);
        var max = mathMax(r, g, b),
            min = mathMin(r, g, b);
        var h,
            s,
            v = max;
        var d = max - min;
        s = max === 0 ? 0 : d / max;

        if (max == min) {
          h = 0; // achromatic
        } else {
          switch (max) {
            case r:
              h = (g - b) / d + (g < b ? 6 : 0);
              break;

            case g:
              h = (b - r) / d + 2;
              break;

            case b:
              h = (r - g) / d + 4;
              break;
          }

          h /= 6;
        }

        return {
          h: h,
          s: s,
          v: v
        };
      } // `hsvToRgb`
      // Converts an HSV color value to RGB.
      // *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
      // *Returns:* { r, g, b } in the set [0, 255]


      function hsvToRgb(h, s, v) {
        h = bound01(h, 360) * 6;
        s = bound01(s, 100);
        v = bound01(v, 100);
        var i = Math.floor(h),
            f = h - i,
            p = v * (1 - s),
            q = v * (1 - f * s),
            t = v * (1 - (1 - f) * s),
            mod = i % 6,
            r = [v, q, p, p, t, v][mod],
            g = [t, v, v, q, p, p][mod],
            b = [p, p, t, v, v, q][mod];
        return {
          r: r * 255,
          g: g * 255,
          b: b * 255
        };
      } // `rgbToHex`
      // Converts an RGB color to hex
      // Assumes r, g, and b are contained in the set [0, 255]
      // Returns a 3 or 6 character hex


      function rgbToHex(r, g, b, allow3Char) {
        var hex = [pad2(mathRound(r).toString(16)), pad2(mathRound(g).toString(16)), pad2(mathRound(b).toString(16))]; // Return a 3 character hex if possible

        if (allow3Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1)) {
          return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0);
        }

        return hex.join("");
      } // `rgbaToHex`
      // Converts an RGBA color plus alpha transparency to hex
      // Assumes r, g, b are contained in the set [0, 255] and
      // a in [0, 1]. Returns a 4 or 8 character rgba hex


      function rgbaToHex(r, g, b, a, allow4Char) {
        var hex = [pad2(mathRound(r).toString(16)), pad2(mathRound(g).toString(16)), pad2(mathRound(b).toString(16)), pad2(convertDecimalToHex(a))]; // Return a 4 character hex if possible

        if (allow4Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1) && hex[3].charAt(0) == hex[3].charAt(1)) {
          return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0) + hex[3].charAt(0);
        }

        return hex.join("");
      } // `rgbaToArgbHex`
      // Converts an RGBA color to an ARGB Hex8 string
      // Rarely used, but required for "toFilter()"


      function rgbaToArgbHex(r, g, b, a) {
        var hex = [pad2(convertDecimalToHex(a)), pad2(mathRound(r).toString(16)), pad2(mathRound(g).toString(16)), pad2(mathRound(b).toString(16))];
        return hex.join("");
      } // `equals`
      // Can be called with any tinycolor input


      tinycolor.equals = function (color1, color2) {
        if (!color1 || !color2) {
          return false;
        }

        return tinycolor(color1).toRgbString() == tinycolor(color2).toRgbString();
      };

      tinycolor.random = function () {
        return tinycolor.fromRatio({
          r: mathRandom(),
          g: mathRandom(),
          b: mathRandom()
        });
      }; // Modification Functions
      // ----------------------
      // Thanks to less.js for some of the basics here
      // <https://github.com/cloudhead/less.js/blob/master/lib/less/functions.js>


      function _desaturate(color, amount) {
        amount = amount === 0 ? 0 : amount || 10;
        var hsl = tinycolor(color).toHsl();
        hsl.s -= amount / 100;
        hsl.s = clamp01(hsl.s);
        return tinycolor(hsl);
      }

      function _saturate(color, amount) {
        amount = amount === 0 ? 0 : amount || 10;
        var hsl = tinycolor(color).toHsl();
        hsl.s += amount / 100;
        hsl.s = clamp01(hsl.s);
        return tinycolor(hsl);
      }

      function _greyscale(color) {
        return tinycolor(color).desaturate(100);
      }

      function _lighten(color, amount) {
        amount = amount === 0 ? 0 : amount || 10;
        var hsl = tinycolor(color).toHsl();
        hsl.l += amount / 100;
        hsl.l = clamp01(hsl.l);
        return tinycolor(hsl);
      }

      function _brighten(color, amount) {
        amount = amount === 0 ? 0 : amount || 10;
        var rgb = tinycolor(color).toRgb();
        rgb.r = mathMax(0, mathMin(255, rgb.r - mathRound(255 * -(amount / 100))));
        rgb.g = mathMax(0, mathMin(255, rgb.g - mathRound(255 * -(amount / 100))));
        rgb.b = mathMax(0, mathMin(255, rgb.b - mathRound(255 * -(amount / 100))));
        return tinycolor(rgb);
      }

      function _darken(color, amount) {
        amount = amount === 0 ? 0 : amount || 10;
        var hsl = tinycolor(color).toHsl();
        hsl.l -= amount / 100;
        hsl.l = clamp01(hsl.l);
        return tinycolor(hsl);
      } // Spin takes a positive or negative amount within [-360, 360] indicating the change of hue.
      // Values outside of this range will be wrapped into this range.


      function _spin(color, amount) {
        var hsl = tinycolor(color).toHsl();
        var hue = (hsl.h + amount) % 360;
        hsl.h = hue < 0 ? 360 + hue : hue;
        return tinycolor(hsl);
      } // Combination Functions
      // ---------------------
      // Thanks to jQuery xColor for some of the ideas behind these
      // <https://github.com/infusion/jQuery-xcolor/blob/master/jquery.xcolor.js>


      function _complement(color) {
        var hsl = tinycolor(color).toHsl();
        hsl.h = (hsl.h + 180) % 360;
        return tinycolor(hsl);
      }

      function _triad(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [tinycolor(color), tinycolor({
          h: (h + 120) % 360,
          s: hsl.s,
          l: hsl.l
        }), tinycolor({
          h: (h + 240) % 360,
          s: hsl.s,
          l: hsl.l
        })];
      }

      function _tetrad(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [tinycolor(color), tinycolor({
          h: (h + 90) % 360,
          s: hsl.s,
          l: hsl.l
        }), tinycolor({
          h: (h + 180) % 360,
          s: hsl.s,
          l: hsl.l
        }), tinycolor({
          h: (h + 270) % 360,
          s: hsl.s,
          l: hsl.l
        })];
      }

      function _splitcomplement(color) {
        var hsl = tinycolor(color).toHsl();
        var h = hsl.h;
        return [tinycolor(color), tinycolor({
          h: (h + 72) % 360,
          s: hsl.s,
          l: hsl.l
        }), tinycolor({
          h: (h + 216) % 360,
          s: hsl.s,
          l: hsl.l
        })];
      }

      function _analogous(color, results, slices) {
        results = results || 6;
        slices = slices || 30;
        var hsl = tinycolor(color).toHsl();
        var part = 360 / slices;
        var ret = [tinycolor(color)];

        for (hsl.h = (hsl.h - (part * results >> 1) + 720) % 360; --results;) {
          hsl.h = (hsl.h + part) % 360;
          ret.push(tinycolor(hsl));
        }

        return ret;
      }

      function _monochromatic(color, results) {
        results = results || 6;
        var hsv = tinycolor(color).toHsv();
        var h = hsv.h,
            s = hsv.s,
            v = hsv.v;
        var ret = [];
        var modification = 1 / results;

        while (results--) {
          ret.push(tinycolor({
            h: h,
            s: s,
            v: v
          }));
          v = (v + modification) % 1;
        }

        return ret;
      } // Utility Functions
      // ---------------------


      tinycolor.mix = function (color1, color2, amount) {
        amount = amount === 0 ? 0 : amount || 50;
        var rgb1 = tinycolor(color1).toRgb();
        var rgb2 = tinycolor(color2).toRgb();
        var p = amount / 100;
        var rgba = {
          r: (rgb2.r - rgb1.r) * p + rgb1.r,
          g: (rgb2.g - rgb1.g) * p + rgb1.g,
          b: (rgb2.b - rgb1.b) * p + rgb1.b,
          a: (rgb2.a - rgb1.a) * p + rgb1.a
        };
        return tinycolor(rgba);
      }; // Readability Functions
      // ---------------------
      // <http://www.w3.org/TR/2008/REC-WCAG20-20081211/#contrast-ratiodef (WCAG Version 2)
      // `contrast`
      // Analyze the 2 colors and returns the color contrast defined by (WCAG Version 2)


      tinycolor.readability = function (color1, color2) {
        var c1 = tinycolor(color1);
        var c2 = tinycolor(color2);
        return (Math.max(c1.getLuminance(), c2.getLuminance()) + 0.05) / (Math.min(c1.getLuminance(), c2.getLuminance()) + 0.05);
      }; // `isReadable`
      // Ensure that foreground and background color combinations meet WCAG2 guidelines.
      // The third argument is an optional Object.
      //      the 'level' property states 'AA' or 'AAA' - if missing or invalid, it defaults to 'AA';
      //      the 'size' property states 'large' or 'small' - if missing or invalid, it defaults to 'small'.
      // If the entire object is absent, isReadable defaults to {level:"AA",size:"small"}.
      // *Example*
      //    tinycolor.isReadable("#000", "#111") => false
      //    tinycolor.isReadable("#000", "#111",{level:"AA",size:"large"}) => false


      tinycolor.isReadable = function (color1, color2, wcag2) {
        var readability = tinycolor.readability(color1, color2);
        var wcag2Parms, out;
        out = false;
        wcag2Parms = validateWCAG2Parms(wcag2);

        switch (wcag2Parms.level + wcag2Parms.size) {
          case "AAsmall":
          case "AAAlarge":
            out = readability >= 4.5;
            break;

          case "AAlarge":
            out = readability >= 3;
            break;

          case "AAAsmall":
            out = readability >= 7;
            break;
        }

        return out;
      }; // `mostReadable`
      // Given a base color and a list of possible foreground or background
      // colors for that base, returns the most readable color.
      // Optionally returns Black or White if the most readable color is unreadable.
      // *Example*
      //    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:false}).toHexString(); // "#112255"
      //    tinycolor.mostReadable(tinycolor.mostReadable("#123", ["#124", "#125"],{includeFallbackColors:true}).toHexString();  // "#ffffff"
      //    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"large"}).toHexString(); // "#faf3f3"
      //    tinycolor.mostReadable("#a8015a", ["#faf3f3"],{includeFallbackColors:true,level:"AAA",size:"small"}).toHexString(); // "#ffffff"


      tinycolor.mostReadable = function (baseColor, colorList, args) {
        var bestColor = null;
        var bestScore = 0;
        var readability;
        var includeFallbackColors, level, size;
        args = args || {};
        includeFallbackColors = args.includeFallbackColors;
        level = args.level;
        size = args.size;

        for (var i = 0; i < colorList.length; i++) {
          readability = tinycolor.readability(baseColor, colorList[i]);

          if (readability > bestScore) {
            bestScore = readability;
            bestColor = tinycolor(colorList[i]);
          }
        }

        if (tinycolor.isReadable(baseColor, bestColor, {
          "level": level,
          "size": size
        }) || !includeFallbackColors) {
          return bestColor;
        } else {
          args.includeFallbackColors = false;
          return tinycolor.mostReadable(baseColor, ["#fff", "#000"], args);
        }
      }; // Big List of Colors
      // ------------------
      // <http://www.w3.org/TR/css3-color/#svg-color>


      var names = tinycolor.names = {
        aliceblue: "f0f8ff",
        antiquewhite: "faebd7",
        aqua: "0ff",
        aquamarine: "7fffd4",
        azure: "f0ffff",
        beige: "f5f5dc",
        bisque: "ffe4c4",
        black: "000",
        blanchedalmond: "ffebcd",
        blue: "00f",
        blueviolet: "8a2be2",
        brown: "a52a2a",
        burlywood: "deb887",
        burntsienna: "ea7e5d",
        cadetblue: "5f9ea0",
        chartreuse: "7fff00",
        chocolate: "d2691e",
        coral: "ff7f50",
        cornflowerblue: "6495ed",
        cornsilk: "fff8dc",
        crimson: "dc143c",
        cyan: "0ff",
        darkblue: "00008b",
        darkcyan: "008b8b",
        darkgoldenrod: "b8860b",
        darkgray: "a9a9a9",
        darkgreen: "006400",
        darkgrey: "a9a9a9",
        darkkhaki: "bdb76b",
        darkmagenta: "8b008b",
        darkolivegreen: "556b2f",
        darkorange: "ff8c00",
        darkorchid: "9932cc",
        darkred: "8b0000",
        darksalmon: "e9967a",
        darkseagreen: "8fbc8f",
        darkslateblue: "483d8b",
        darkslategray: "2f4f4f",
        darkslategrey: "2f4f4f",
        darkturquoise: "00ced1",
        darkviolet: "9400d3",
        deeppink: "ff1493",
        deepskyblue: "00bfff",
        dimgray: "696969",
        dimgrey: "696969",
        dodgerblue: "1e90ff",
        firebrick: "b22222",
        floralwhite: "fffaf0",
        forestgreen: "228b22",
        fuchsia: "f0f",
        gainsboro: "dcdcdc",
        ghostwhite: "f8f8ff",
        gold: "ffd700",
        goldenrod: "daa520",
        gray: "808080",
        green: "008000",
        greenyellow: "adff2f",
        grey: "808080",
        honeydew: "f0fff0",
        hotpink: "ff69b4",
        indianred: "cd5c5c",
        indigo: "4b0082",
        ivory: "fffff0",
        khaki: "f0e68c",
        lavender: "e6e6fa",
        lavenderblush: "fff0f5",
        lawngreen: "7cfc00",
        lemonchiffon: "fffacd",
        lightblue: "add8e6",
        lightcoral: "f08080",
        lightcyan: "e0ffff",
        lightgoldenrodyellow: "fafad2",
        lightgray: "d3d3d3",
        lightgreen: "90ee90",
        lightgrey: "d3d3d3",
        lightpink: "ffb6c1",
        lightsalmon: "ffa07a",
        lightseagreen: "20b2aa",
        lightskyblue: "87cefa",
        lightslategray: "789",
        lightslategrey: "789",
        lightsteelblue: "b0c4de",
        lightyellow: "ffffe0",
        lime: "0f0",
        limegreen: "32cd32",
        linen: "faf0e6",
        magenta: "f0f",
        maroon: "800000",
        mediumaquamarine: "66cdaa",
        mediumblue: "0000cd",
        mediumorchid: "ba55d3",
        mediumpurple: "9370db",
        mediumseagreen: "3cb371",
        mediumslateblue: "7b68ee",
        mediumspringgreen: "00fa9a",
        mediumturquoise: "48d1cc",
        mediumvioletred: "c71585",
        midnightblue: "191970",
        mintcream: "f5fffa",
        mistyrose: "ffe4e1",
        moccasin: "ffe4b5",
        navajowhite: "ffdead",
        navy: "000080",
        oldlace: "fdf5e6",
        olive: "808000",
        olivedrab: "6b8e23",
        orange: "ffa500",
        orangered: "ff4500",
        orchid: "da70d6",
        palegoldenrod: "eee8aa",
        palegreen: "98fb98",
        paleturquoise: "afeeee",
        palevioletred: "db7093",
        papayawhip: "ffefd5",
        peachpuff: "ffdab9",
        peru: "cd853f",
        pink: "ffc0cb",
        plum: "dda0dd",
        powderblue: "b0e0e6",
        purple: "800080",
        rebeccapurple: "663399",
        red: "f00",
        rosybrown: "bc8f8f",
        royalblue: "4169e1",
        saddlebrown: "8b4513",
        salmon: "fa8072",
        sandybrown: "f4a460",
        seagreen: "2e8b57",
        seashell: "fff5ee",
        sienna: "a0522d",
        silver: "c0c0c0",
        skyblue: "87ceeb",
        slateblue: "6a5acd",
        slategray: "708090",
        slategrey: "708090",
        snow: "fffafa",
        springgreen: "00ff7f",
        steelblue: "4682b4",
        tan: "d2b48c",
        teal: "008080",
        thistle: "d8bfd8",
        tomato: "ff6347",
        turquoise: "40e0d0",
        violet: "ee82ee",
        wheat: "f5deb3",
        white: "fff",
        whitesmoke: "f5f5f5",
        yellow: "ff0",
        yellowgreen: "9acd32"
      }; // Make it easy to access colors via `hexNames[hex]`

      var hexNames = tinycolor.hexNames = flip(names); // Utilities
      // ---------
      // `{ 'name1': 'val1' }` becomes `{ 'val1': 'name1' }`

      function flip(o) {
        var flipped = {};

        for (var i in o) {
          if (o.hasOwnProperty(i)) {
            flipped[o[i]] = i;
          }
        }

        return flipped;
      } // Return a valid alpha value [0,1] with all invalid values being set to 1


      function boundAlpha(a) {
        a = parseFloat(a);

        if (isNaN(a) || a < 0 || a > 1) {
          a = 1;
        }

        return a;
      } // Take input from [0, n] and return it as [0, 1]


      function bound01(n, max) {
        if (isOnePointZero(n)) {
          n = "100%";
        }

        var processPercent = isPercentage(n);
        n = mathMin(max, mathMax(0, parseFloat(n))); // Automatically convert percentage into number

        if (processPercent) {
          n = parseInt(n * max, 10) / 100;
        } // Handle floating point rounding errors


        if (Math.abs(n - max) < 0.000001) {
          return 1;
        } // Convert into [0, 1] range if it isn't already


        return n % max / parseFloat(max);
      } // Force a number between 0 and 1


      function clamp01(val) {
        return mathMin(1, mathMax(0, val));
      } // Parse a base-16 hex value into a base-10 integer


      function parseIntFromHex(val) {
        return parseInt(val, 16);
      } // Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
      // <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>


      function isOnePointZero(n) {
        return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
      } // Check to see if string passed in is a percentage


      function isPercentage(n) {
        return typeof n === "string" && n.indexOf('%') != -1;
      } // Force a hex value to have 2 characters


      function pad2(c) {
        return c.length == 1 ? '0' + c : '' + c;
      } // Replace a decimal with it's percentage value


      function convertToPercentage(n) {
        if (n <= 1) {
          n = n * 100 + "%";
        }

        return n;
      } // Converts a decimal to a hex value


      function convertDecimalToHex(d) {
        return Math.round(parseFloat(d) * 255).toString(16);
      } // Converts a hex value to a decimal


      function convertHexToDecimal(h) {
        return parseIntFromHex(h) / 255;
      }

      var matchers = function () {
        // <http://www.w3.org/TR/css3-values/#integers>
        var CSS_INTEGER = "[-\\+]?\\d+%?"; // <http://www.w3.org/TR/css3-values/#number-value>

        var CSS_NUMBER = "[-\\+]?\\d*\\.\\d+%?"; // Allow positive/negative integer/number.  Don't capture the either/or, just the entire outcome.

        var CSS_UNIT = "(?:" + CSS_NUMBER + ")|(?:" + CSS_INTEGER + ")"; // Actual matching.
        // Parentheses and commas are optional, but not required.
        // Whitespace can take the place of commas or opening paren

        var PERMISSIVE_MATCH3 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
        var PERMISSIVE_MATCH4 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
        return {
          CSS_UNIT: new RegExp(CSS_UNIT),
          rgb: new RegExp("rgb" + PERMISSIVE_MATCH3),
          rgba: new RegExp("rgba" + PERMISSIVE_MATCH4),
          hsl: new RegExp("hsl" + PERMISSIVE_MATCH3),
          hsla: new RegExp("hsla" + PERMISSIVE_MATCH4),
          hsv: new RegExp("hsv" + PERMISSIVE_MATCH3),
          hsva: new RegExp("hsva" + PERMISSIVE_MATCH4),
          hex3: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
          hex6: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/,
          hex4: /^#?([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
          hex8: /^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/
        };
      }(); // `isValidCSSUnit`
      // Take in a single string / number and check to see if it looks like a CSS unit
      // (see `matchers` above for definition).


      function isValidCSSUnit(color) {
        return !!matchers.CSS_UNIT.exec(color);
      } // `stringInputToObject`
      // Permissive string parsing.  Take in a number of formats, and output an object
      // based on detected format.  Returns `{ r, g, b }` or `{ h, s, l }` or `{ h, s, v}`


      function stringInputToObject(color) {
        color = color.replace(trimLeft, '').replace(trimRight, '').toLowerCase();
        var named = false;

        if (names[color]) {
          color = names[color];
          named = true;
        } else if (color == 'transparent') {
          return {
            r: 0,
            g: 0,
            b: 0,
            a: 0,
            format: "name"
          };
        } // Try to match string input using regular expressions.
        // Keep most of the number bounding out of this function - don't worry about [0,1] or [0,100] or [0,360]
        // Just return an object and let the conversion functions handle that.
        // This way the result will be the same whether the tinycolor is initialized with string or object.


        var match;

        if (match = matchers.rgb.exec(color)) {
          return {
            r: match[1],
            g: match[2],
            b: match[3]
          };
        }

        if (match = matchers.rgba.exec(color)) {
          return {
            r: match[1],
            g: match[2],
            b: match[3],
            a: match[4]
          };
        }

        if (match = matchers.hsl.exec(color)) {
          return {
            h: match[1],
            s: match[2],
            l: match[3]
          };
        }

        if (match = matchers.hsla.exec(color)) {
          return {
            h: match[1],
            s: match[2],
            l: match[3],
            a: match[4]
          };
        }

        if (match = matchers.hsv.exec(color)) {
          return {
            h: match[1],
            s: match[2],
            v: match[3]
          };
        }

        if (match = matchers.hsva.exec(color)) {
          return {
            h: match[1],
            s: match[2],
            v: match[3],
            a: match[4]
          };
        }

        if (match = matchers.hex8.exec(color)) {
          return {
            r: parseIntFromHex(match[1]),
            g: parseIntFromHex(match[2]),
            b: parseIntFromHex(match[3]),
            a: convertHexToDecimal(match[4]),
            format: named ? "name" : "hex8"
          };
        }

        if (match = matchers.hex6.exec(color)) {
          return {
            r: parseIntFromHex(match[1]),
            g: parseIntFromHex(match[2]),
            b: parseIntFromHex(match[3]),
            format: named ? "name" : "hex"
          };
        }

        if (match = matchers.hex4.exec(color)) {
          return {
            r: parseIntFromHex(match[1] + '' + match[1]),
            g: parseIntFromHex(match[2] + '' + match[2]),
            b: parseIntFromHex(match[3] + '' + match[3]),
            a: convertHexToDecimal(match[4] + '' + match[4]),
            format: named ? "name" : "hex8"
          };
        }

        if (match = matchers.hex3.exec(color)) {
          return {
            r: parseIntFromHex(match[1] + '' + match[1]),
            g: parseIntFromHex(match[2] + '' + match[2]),
            b: parseIntFromHex(match[3] + '' + match[3]),
            format: named ? "name" : "hex"
          };
        }

        return false;
      }

      function validateWCAG2Parms(parms) {
        // return valid WCAG2 parms for isReadable.
        // If input parms are invalid, return {"level":"AA", "size":"small"}
        var level, size;
        parms = parms || {
          "level": "AA",
          "size": "small"
        };
        level = (parms.level || "AA").toUpperCase();
        size = (parms.size || "small").toLowerCase();

        if (level !== "AA" && level !== "AAA") {
          level = "AA";
        }

        if (size !== "small" && size !== "large") {
          size = "small";
        }

        return {
          "level": level,
          "size": size
        };
      } // Node: Export function


      if (true && module.exports) {
        module.exports = tinycolor;
      } // AMD/requirejs: Define the module
      else if (true) {
          !(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
            return tinycolor;
          }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
        } // Browser: Expose to window
        else {}
    })(Math);
    /***/

  },

  /***/
  "./src/app/views/dashboard/analytics/analytics.component.scss":
  /*!********************************************************************!*\
    !*** ./src/app/views/dashboard/analytics/analytics.component.scss ***!
    \********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardAnalyticsAnalyticsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvZGFzaGJvYXJkL2FuYWx5dGljcy9hbmFseXRpY3MuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/analytics/analytics.component.ts":
  /*!******************************************************************!*\
    !*** ./src/app/views/dashboard/analytics/analytics.component.ts ***!
    \******************************************************************/

  /*! exports provided: AnalyticsComponent */

  /***/
  function srcAppViewsDashboardAnalyticsAnalyticsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AnalyticsComponent", function () {
      return AnalyticsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! app/shared/animations/egret-animations */
    "./src/app/shared/animations/egret-animations.ts");
    /* harmony import */


    var app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! app/shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! tinycolor2 */
    "./node_modules/tinycolor2/tinycolor.js");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(tinycolor2__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AnalyticsComponent =
    /*#__PURE__*/
    function () {
      function AnalyticsComponent(themeService, router) {
        _classCallCheck(this, AnalyticsComponent);

        this.themeService = themeService;
        this.router = router;
      }

      _createClass(AnalyticsComponent, [{
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          if (!localStorage.getItem('isSignedIn')) {
            console.log("Not signed in");
            this.router.navigateByUrl('/dashboard');
            this.router.navigateByUrl('/sessions/signin');
          }

          console.log("IS THIS IT?!");
          this.themeService.onThemeChange.subscribe(function (activeTheme) {
            _this.initTrafficVsSaleChart(activeTheme);

            _this.initSessionsChart(activeTheme);

            _this.initTrafficSourcesChart(activeTheme);

            _this.initDailyTrafficChartBar(activeTheme);

            _this.initTrafficGrowthChart(activeTheme);
          });
          this.initTrafficVsSaleChart(this.themeService.activatedTheme);
          this.initSessionsChart(this.themeService.activatedTheme);
          this.initTrafficSourcesChart(this.themeService.activatedTheme);
          this.initDailyTrafficChartBar(this.themeService.activatedTheme);
          this.initTrafficGrowthChart(this.themeService.activatedTheme);
          this.countryTrafficStats = [{
            country: "US",
            visitor: 14040,
            pageView: 10000,
            download: 1000,
            bounceRate: 30,
            flag: "flag-icon-us"
          }, {
            country: "India",
            visitor: 12500,
            pageView: 10000,
            download: 1000,
            bounceRate: 45,
            flag: "flag-icon-in"
          }, {
            country: "UK",
            visitor: 11000,
            pageView: 10000,
            download: 1000,
            bounceRate: 50,
            flag: "flag-icon-gb"
          }, {
            country: "Brazil",
            visitor: 4000,
            pageView: 10000,
            download: 1000,
            bounceRate: 30,
            flag: "flag-icon-br"
          }, {
            country: "Spain",
            visitor: 4000,
            pageView: 10000,
            download: 1000,
            bounceRate: 45,
            flag: "flag-icon-es"
          }, {
            country: "Mexico",
            visitor: 4000,
            pageView: 10000,
            download: 1000,
            bounceRate: 70,
            flag: "flag-icon-mx"
          }, {
            country: "Russia",
            visitor: 4000,
            pageView: 10000,
            download: 1000,
            bounceRate: 40,
            flag: "flag-icon-ru"
          }];
          this.bounceRateGrowthChart = {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                animation: true
              }
            },
            grid: {
              left: "0",
              top: "0",
              right: "0",
              bottom: "0"
            },
            xAxis: {
              type: "category",
              boundaryGap: false,
              data: ["0", "1", "2", "3", "4"],
              axisLabel: {
                show: false
              },
              axisLine: {
                lineStyle: {
                  show: false
                }
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              min: 0,
              max: 200,
              interval: 50,
              axisLabel: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            series: [{
              name: "Bounce Rate",
              type: "line",
              smooth: false,
              data: [0, 20, 90, 120, 190],
              symbolSize: 8,
              showSymbol: false,
              lineStyle: {
                opacity: 0,
                width: 0
              },
              itemStyle: {
                borderColor: "rgba(233, 31, 99, 0.4)"
              },
              areaStyle: {
                normal: {
                  color: {
                    type: "linear",
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                      offset: 0,
                      color: "rgba(244, 67, 54, 1)"
                    }, {
                      offset: 1,
                      color: "rgba(244, 67, 54, .4)"
                    }]
                  }
                }
              }
            }]
          };
        }
      }, {
        key: "initTrafficVsSaleChart",
        value: function initTrafficVsSaleChart(theme) {
          this.trafficVsSaleOptions = {
            tooltip: {
              show: true,
              trigger: "axis",
              backgroundColor: "#fff",
              extraCssText: "box-shadow: 0 0 3px rgba(0, 0, 0, 0.3); color: #444",
              axisPointer: {
                type: "line",
                animation: true
              }
            },
            grid: {
              top: "10%",
              left: "80px",
              right: "30px",
              bottom: "60"
            },
            xAxis: {
              type: "category",
              boundaryGap: false,
              data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"],
              axisLabel: {
                show: true,
                margin: 20,
                color: "#888"
              },
              axisTick: {
                show: false
              },
              axisLine: {
                show: false,
                lineStyle: {
                  show: false
                }
              },
              splitLine: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              axisLine: {
                show: false
              },
              axisLabel: {
                show: true,
                margin: 30,
                color: "#888"
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: true,
                lineStyle: {
                  type: "dashed"
                }
              }
            },
            series: [{
              name: "Traffic",
              label: {
                show: false,
                color: "#0168c1"
              },
              type: "bar",
              barGap: 0,
              color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.4).toString(),
              smooth: true
            }, {
              name: "Sales",
              label: {
                show: false,
                color: "#639"
              },
              type: "bar",
              color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
              smooth: true
            }]
          };
          this.trafficData = [1400, 1350, 950, 1150, 950, 1260, 930, 1450, 1150, 1400, 1350, 950, 1150, 950, 1260];
          this.saleData = [500, 700, 350, 840, 750, 800, 700, 500, 700, 650, 104, 750, 800, 700, 500];
          this.trafficVsSale = {
            series: [{
              data: this.trafficData
            }, {
              data: this.saleData
            }]
          };
        }
      }, {
        key: "initSessionsChart",
        value: function initSessionsChart(theme) {
          this.sessionOptions = {
            tooltip: {
              show: true,
              trigger: "axis",
              backgroundColor: "#fff",
              extraCssText: "box-shadow: 0 0 3px rgba(0, 0, 0, 0.3); color: #444",
              axisPointer: {
                type: "line",
                animation: true
              }
            },
            grid: {
              top: "10%",
              left: "60",
              right: "15",
              bottom: "60"
            },
            xAxis: {
              type: "category",
              data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
              axisLine: {
                show: false
              },
              axisLabel: {
                show: true,
                margin: 30,
                color: "#888"
              },
              axisTick: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              axisLine: {
                show: false
              },
              axisLabel: {
                show: true,
                margin: 20,
                color: "#888"
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: true,
                lineStyle: {
                  type: "dashed"
                }
              }
            },
            series: [{
              data: [],
              type: "line",
              name: "User",
              smooth: true,
              color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
              lineStyle: {
                opacity: 1,
                width: 3
              },
              itemStyle: {
                opacity: 0
              },
              emphasis: {
                itemStyle: {
                  color: "rgba(16, 23, 76, 1)",
                  borderColor: "rgba(16, 23, 76, .4)",
                  opacity: 1,
                  borderWidth: 8
                },
                label: {
                  show: false,
                  backgroundColor: "#fff"
                }
              }
            }]
          };
          this.sessionsData = [140, 135, 95, 115, 95, 126, 93, 145, 115, 140, 135, 95, 115, 95, 126, 125, 145, 115, 140, 135, 95, 115, 95, 126, 93, 145, 115, 140, 135, 95];
          this.sessions = {
            series: [{
              data: this.sessionsData
            }]
          };
        }
      }, {
        key: "initTrafficSourcesChart",
        value: function initTrafficSourcesChart(theme) {
          this.trafficSourcesChart = {
            grid: {
              left: "3%",
              right: "4%",
              bottom: "3%",
              containLabel: true
            },
            color: [tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.6).toString(), tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.7).toString(), tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.8).toString()],
            tooltip: {
              show: false,
              trigger: "item",
              formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            xAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            yAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            series: [{
              name: "Sessions",
              type: "pie",
              radius: ["55%", "85%"],
              center: ["50%", "50%"],
              avoidLabelOverlap: false,
              hoverOffset: 5,
              stillShowZeroSum: false,
              label: {
                normal: {
                  show: false,
                  position: "center",
                  textStyle: {
                    fontSize: "13",
                    fontWeight: "normal"
                  },
                  formatter: "{a}"
                },
                emphasis: {
                  show: true,
                  textStyle: {
                    fontSize: "15",
                    fontWeight: "normal",
                    color: "rgba(15, 21, 77, 1)"
                  },
                  formatter: "{b} \n{c} ({d}%)"
                }
              },
              labelLine: {
                normal: {
                  show: false
                }
              },
              data: [{
                value: 335,
                name: "Direct"
              }, {
                value: 310,
                name: "Search Eng."
              }, {
                value: 148,
                name: "Social"
              }],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: "rgba(0, 0, 0, 0.5)"
                }
              }
            }]
          };
        }
      }, {
        key: "initDailyTrafficChartBar",
        value: function initDailyTrafficChartBar(theme) {
          this.dailyTrafficChartBar = {
            legend: {
              show: false
            },
            grid: {
              left: "8px",
              right: "8px",
              bottom: "0",
              top: "0",
              containLabel: true
            },
            tooltip: {
              show: true,
              backgroundColor: "rgba(0, 0, 0, .8)"
            },
            xAxis: [{
              type: "category",
              // data: ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
              data: ["1", "2", "3", "4", "5", "6", "7"],
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisLabel: {
                color: "#fff"
              }
            }],
            yAxis: [{
              type: "value",
              axisLabel: {
                show: false,
                formatter: "${value}"
              },
              min: 0,
              max: 100000,
              interval: 25000,
              axisTick: {
                show: false
              },
              axisLine: {
                show: false
              },
              splitLine: {
                show: false,
                interval: "auto"
              }
            }],
            series: [{
              name: "Online",
              data: [35000, 69000, 22500, 60000, 50000, 50000, 30000],
              label: {
                show: true,
                color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
                position: "top"
              },
              type: "bar",
              barWidth: "12",
              color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
              smooth: true,
              itemStyle: {
                barBorderRadius: 10
              }
            }]
          };
        }
      }, {
        key: "initTrafficGrowthChart",
        value: function initTrafficGrowthChart(theme) {
          this.trafficGrowthChart = {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                animation: true
              }
            },
            grid: {
              left: "0",
              top: "0",
              right: "0",
              bottom: "0"
            },
            xAxis: {
              type: "category",
              boundaryGap: false,
              data: ["0", "1", "2", "3", "4"],
              axisLabel: {
                show: false
              },
              axisLine: {
                lineStyle: {
                  show: false
                }
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              min: 0,
              max: 200,
              interval: 50,
              axisLabel: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            series: [{
              name: "Visit",
              type: "line",
              smooth: false,
              data: [0, 40, 140, 90, 160],
              symbolSize: 8,
              showSymbol: false,
              lineStyle: {
                opacity: 0,
                width: 0
              },
              itemStyle: {
                borderColor: "rgba(233, 31, 99, 0.4)"
              },
              areaStyle: {
                normal: {
                  color: {
                    type: "linear",
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                      offset: 0,
                      color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString()
                    }, {
                      offset: 1,
                      color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.6).toString()
                    }]
                  }
                }
              }
            }]
          };
        }
      }]);

      return AnalyticsComponent;
    }();

    AnalyticsComponent.ctorParameters = function () {
      return [{
        type: app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    AnalyticsComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "app-analytics",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./analytics.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/analytics/analytics.component.html")).default,
      animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"],
      styles: [__importDefault(__webpack_require__(
      /*! ./analytics.component.scss */
      "./src/app/views/dashboard/analytics/analytics.component.scss")).default]
    }), __metadata("design:paramtypes", [app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], AnalyticsComponent);
    /***/
  },

  /***/
  "./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardCryptocurrencyCryptocurrencyComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvZGFzaGJvYXJkL2NyeXB0b2N1cnJlbmN5L2NyeXB0b2N1cnJlbmN5LmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.ts ***!
    \****************************************************************************/

  /*! exports provided: CryptocurrencyComponent */

  /***/
  function srcAppViewsDashboardCryptocurrencyCryptocurrencyComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CryptocurrencyComponent", function () {
      return CryptocurrencyComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! app/shared/animations/egret-animations */
    "./src/app/shared/animations/egret-animations.ts");
    /* harmony import */


    var app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! app/shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! tinycolor2 */
    "./node_modules/tinycolor2/tinycolor.js");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(tinycolor2__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var CryptocurrencyComponent =
    /*#__PURE__*/
    function () {
      function CryptocurrencyComponent(themeService, router) {
        _classCallCheck(this, CryptocurrencyComponent);

        this.themeService = themeService;
        this.router = router;
      }

      _createClass(CryptocurrencyComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          if (!localStorage.getItem('isSignedIn')) {
            console.log("Not signed in");
            this.router.navigateByUrl('/dashboard');
            this.router.navigateByUrl('/sessions/signin');
          }

          this.themeService.onThemeChange.subscribe(function (activeTheme) {
            _this2.initCryptoChart(activeTheme);
          });
          this.initCryptoChart(this.themeService.activatedTheme);
          this.cryptoDonutChart = {
            grid: {
              left: "3%",
              right: "4%",
              bottom: "3%",
              containLabel: true
            },
            color: ["#03A9F4", "#039BE5", "#fcc02e"],
            tooltip: {
              show: false,
              trigger: "item",
              formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            xAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            yAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            series: [{
              name: "Sessions",
              type: "pie",
              radius: ["65%", "85%"],
              center: ["50%", "50%"],
              avoidLabelOverlap: false,
              hoverOffset: 5,
              stillShowZeroSum: false,
              label: {
                normal: {
                  show: false,
                  position: "center",
                  textStyle: {
                    fontSize: "13",
                    fontWeight: "normal"
                  },
                  formatter: "{a}"
                },
                emphasis: {
                  show: true,
                  textStyle: {
                    fontSize: "15",
                    fontWeight: "normal",
                    color: "rgba(0, 0, 0, 0.8)"
                  },
                  formatter: "{b} \n{c} ({d}%)"
                }
              },
              labelLine: {
                normal: {
                  show: false
                }
              },
              data: [{
                value: 335,
                name: "Direct"
              }, {
                value: 310,
                name: "Search Eng."
              }, {
                value: 148,
                name: "Social"
              }],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: "rgba(0, 0, 0, 0.5)"
                }
              }
            }]
          };
          this.activeTrades = [{
            icon: "assets/images/cryptocurrencies/BTC.png",
            currency: "Bitcoin",
            balance: 3000,
            buyingRate: 450,
            currentRate: 450,
            profitLoss: 400,
            lastPrice: 300
          }, {
            icon: "assets/images/cryptocurrencies/ADA.png",
            currency: "Bitcoin",
            balance: 3000,
            buyingRate: 450,
            currentRate: 450,
            profitLoss: 400,
            lastPrice: 300
          }, {
            icon: "assets/images/cryptocurrencies/LTC.png",
            currency: "Bitcoin",
            balance: 3000,
            buyingRate: 450,
            currentRate: 450,
            profitLoss: 400,
            lastPrice: 300
          }, {
            icon: "assets/images/cryptocurrencies/AE.png",
            currency: "Bitcoin",
            balance: 3000,
            buyingRate: 450,
            currentRate: 450,
            profitLoss: 400,
            lastPrice: 300
          }];
          this.trendingCurrencies = [{
            currency: "Bitcoin",
            rate: 3800
          }, {
            currency: "Bitcoin",
            rate: 3800
          }, {
            currency: "Bitcoin",
            rate: 3800
          }, {
            currency: "Bitcoin",
            rate: 3800
          }];
        }
      }, {
        key: "initCryptoChart",
        value: function initCryptoChart(theme) {
          this.cryptoChart = {
            tooltip: {
              show: true,
              trigger: "axis",
              backgroundColor: "#fff",
              extraCssText: "box-shadow: 0 0 3px rgba(0, 0, 0, 0.3); color: #444",
              axisPointer: {
                type: "line",
                animation: true
              }
            },
            grid: {
              top: "10%",
              left: "60",
              right: "20",
              bottom: "60"
            },
            xAxis: {
              type: "category",
              data: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
              axisLine: {
                show: false
              },
              axisLabel: {
                show: true,
                margin: 30,
                color: "#888"
              },
              axisTick: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              axisLine: {
                show: false
              },
              axisLabel: {
                show: true,
                margin: 20,
                color: "#888"
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: true,
                lineStyle: {
                  type: "dashed"
                }
              }
            },
            series: [{
              data: [640, 1040, 840, 1240, 1040, 1440, 1240, 1640, 1440, 1840, 1640, 2040, 1840, 2240, 2040, 2440, 2240, 2640, 2440, 2840, 2640, 3040, 2840, 3240, 3040, 3440, 3240, 3640, 3440, 3840],
              type: "line",
              name: "Bitcoin",
              smooth: true,
              color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
              lineStyle: {
                opacity: 1,
                width: 3
              },
              itemStyle: {
                opacity: 0
              },
              emphasis: {
                itemStyle: {
                  color: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).toString(),
                  borderColor: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.4).toString(),
                  opacity: 1,
                  borderWidth: 8
                },
                label: {
                  show: false,
                  backgroundColor: "#fff"
                }
              }
            }, {
              data: [240, 640, 440, 840, 640, 1040, 840, 1240, 1040, 1440, 1240, 1640, 1440, 1840, 1640, 2040, 1840, 2240, 2040, 2440, 2240, 2640, 2440, 2840, 2640, 3040, 2840, 3240, 3040, 3440],
              type: "line",
              name: "Ethereum (ETH)",
              smooth: true,
              color: "rgba(0, 0, 0, .3)",
              lineStyle: {
                opacity: 1,
                width: 3
              },
              itemStyle: {
                opacity: 0
              },
              emphasis: {
                itemStyle: {
                  color: "rgba(0, 0, 0, .5)",
                  borderColor: "rgba(0, 0, 0, .2)",
                  opacity: 1,
                  borderWidth: 8
                },
                label: {
                  show: false,
                  backgroundColor: "#fff"
                }
              }
            }]
          };
        }
      }]);

      return CryptocurrencyComponent;
    }();

    CryptocurrencyComponent.ctorParameters = function () {
      return [{
        type: app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    CryptocurrencyComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "app-cryptocurrency",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./cryptocurrency.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.html")).default,
      animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"],
      styles: [__importDefault(__webpack_require__(
      /*! ./cryptocurrency.component.scss */
      "./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.scss")).default]
    }), __metadata("design:paramtypes", [app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], CryptocurrencyComponent);
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardDashboardDarkDashboardDarkComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvZGFzaGJvYXJkL2Rhc2hib2FyZC1kYXJrL2Rhc2hib2FyZC1kYXJrLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.ts ***!
    \****************************************************************************/

  /*! exports provided: DashboardDarkComponent */

  /***/
  function srcAppViewsDashboardDashboardDarkDashboardDarkComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardDarkComponent", function () {
      return DashboardDarkComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! app/shared/animations/egret-animations */
    "./src/app/shared/animations/egret-animations.ts");
    /* harmony import */


    var app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! app/shared/services/layout.service */
    "./src/app/shared/services/layout.service.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../auth.service */
    "./src/app/auth.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DashboardDarkComponent =
    /*#__PURE__*/
    function () {
      function DashboardDarkComponent(layout, scack, authService, router) {
        _classCallCheck(this, DashboardDarkComponent);

        this.layout = layout;
        this.scack = scack;
        this.authService = authService;
        this.router = router;
        this.countryTrafficStats = [{
          country: "US",
          visitor: 14040,
          pageView: 10000,
          download: 1000,
          bounceRate: 30,
          flag: "flag-icon-us"
        }, {
          country: "India",
          visitor: 12500,
          pageView: 10000,
          download: 1000,
          bounceRate: 45,
          flag: "flag-icon-in"
        }, {
          country: "UK",
          visitor: 11000,
          pageView: 10000,
          download: 1000,
          bounceRate: 50,
          flag: "flag-icon-gb"
        }, {
          country: "Brazil",
          visitor: 4000,
          pageView: 10000,
          download: 1000,
          bounceRate: 30,
          flag: "flag-icon-br"
        }, {
          country: "Spain",
          visitor: 4000,
          pageView: 10000,
          download: 1000,
          bounceRate: 45,
          flag: "flag-icon-es"
        }, {
          country: "Mexico",
          visitor: 4000,
          pageView: 10000,
          download: 1000,
          bounceRate: 70,
          flag: "flag-icon-mx"
        }, {
          country: "Russia",
          visitor: 4000,
          pageView: 10000,
          download: 1000,
          bounceRate: 40,
          flag: "flag-icon-ru"
        }];
      }

      _createClass(DashboardDarkComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this3 = this;

          console.log('hopefully');

          if (!localStorage.getItem('isSignedIn')) {
            console.log("Not signed in");
            this.router.navigateByUrl('/dashboard');
            this.router.navigateByUrl('/sessions/signin');
          }

          setTimeout(function () {
            _this3.layout.publishLayoutChange({
              sidebarColor: 'dark-blue',
              topbarColor: 'dark-blue'
            });

            _this3.scack.open('Layout option changed to {sidebarColor: "dark-blue", topbarColor: "dark-blue"};', 'OK', {
              duration: 6000
            });
          });
          this.dailyTrafficChartBar = {
            legend: {
              show: false
            },
            grid: {
              left: "8px",
              right: "8px",
              bottom: "0",
              top: "0",
              containLabel: true
            },
            tooltip: {
              show: true,
              backgroundColor: "rgba(0, 0, 0, .8)"
            },
            xAxis: [{
              type: "category",
              // data: ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
              data: ["1", "2", "3", "4", "5", "6", "7"],
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisLabel: {
                color: "#fff"
              }
            }],
            yAxis: [{
              type: "value",
              axisLabel: {
                show: false,
                formatter: "${value}"
              },
              min: 0,
              max: 100000,
              interval: 25000,
              axisTick: {
                show: false
              },
              axisLine: {
                show: false
              },
              splitLine: {
                show: false,
                interval: "auto"
              }
            }],
            series: [{
              name: "Online",
              data: [35000, 69000, 22500, 60000, 50000, 50000, 30000],
              label: {
                show: false,
                color: "#0168c1"
              },
              type: "bar",
              barWidth: "8",
              color: "#f6be1a",
              smooth: true,
              itemStyle: {
                barBorderRadius: 10
              }
            }]
          };
          this.monthlyTrafficChartBar = {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                animation: true
              }
            },
            grid: {
              left: "0",
              top: "4%",
              right: "0",
              bottom: "0"
            },
            xAxis: {
              type: "category",
              boundaryGap: false,
              data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
              axisLabel: {
                show: false
              },
              axisLine: {
                lineStyle: {
                  show: false
                }
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              min: 0,
              max: 200,
              interval: 50,
              axisLabel: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            series: [{
              name: "Visit",
              type: "line",
              smooth: true,
              data: [140, 135, 95, 115, 95, 126, 93, 145, 115, 140, 135, 95, 115, 95, 126, 125, 145, 115, 140, 135, 95, 115, 95, 126, 93, 145, 115, 140, 135, 95],
              symbolSize: 8,
              showSymbol: false,
              lineStyle: {
                opacity: 0,
                width: 0
              },
              itemStyle: {
                borderColor: "#f6be1a"
              },
              areaStyle: {
                color: "#f6be1a",
                opacity: 1
              }
            }, {
              name: "Sales",
              type: "line",
              smooth: true,
              data: [50, 70, 65, 84, 75, 80, 70, 50, 70, 65, 104, 75, 80, 70, 50, 70, 65, 94, 75, 80, 70, 50, 70, 65, 86, 75, 80, 70, 50, 70],
              symbolSize: 8,
              showSymbol: false,
              lineStyle: {
                opacity: 0,
                width: 0
              },
              itemStyle: {
                borderColor: "#e91f63"
              },
              areaStyle: {
                color: "#e91f63",
                opacity: 1
              }
            }]
          };
          this.dailyBandwithUsage = {
            grid: {
              left: "3%",
              right: "4%",
              bottom: "3%",
              containLabel: true
            },
            color: ["#fcc02e", "#e91f63", "#f44336"],
            tooltip: {
              show: false,
              trigger: "item",
              formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            xAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            yAxis: [{
              axisLine: {
                show: false
              },
              splitLine: {
                show: false
              }
            }],
            series: [{
              name: "Sessions",
              type: "pie",
              radius: ["50%", "85%"],
              center: ["50%", "50%"],
              avoidLabelOverlap: false,
              hoverOffset: 5,
              stillShowZeroSum: false,
              label: {
                normal: {
                  show: false,
                  position: "center",
                  textStyle: {
                    fontSize: "13",
                    fontWeight: "normal"
                  },
                  formatter: "{a}"
                },
                emphasis: {
                  show: true,
                  textStyle: {
                    fontSize: "15",
                    fontWeight: "normal",
                    color: "white"
                  },
                  formatter: "{b} \n{c} ({d}%)"
                }
              },
              labelLine: {
                normal: {
                  show: false
                }
              },
              data: [{
                value: 335,
                name: "Direct"
              }, {
                value: 310,
                name: "Search Eng."
              }, {
                value: 148,
                name: "Social"
              }],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: "rgba(0, 0, 0, 0.5)"
                }
              }
            }]
          };
          this.trafficGrowthChart = {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                animation: true
              }
            },
            grid: {
              left: "0",
              top: "0",
              right: "0",
              bottom: "0"
            },
            xAxis: {
              type: "category",
              boundaryGap: false,
              data: ["0", "1", "2", "3", "4"],
              axisLabel: {
                show: false
              },
              axisLine: {
                lineStyle: {
                  show: false
                }
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            yAxis: {
              type: "value",
              min: 0,
              max: 200,
              interval: 50,
              axisLabel: {
                show: false
              },
              axisLine: {
                show: false
              },
              axisTick: {
                show: false
              },
              splitLine: {
                show: false
              }
            },
            series: [{
              name: "Visit",
              type: "line",
              smooth: false,
              data: [0, 40, 140, 90, 160],
              symbolSize: 8,
              showSymbol: false,
              lineStyle: {
                opacity: 0,
                width: 0
              },
              itemStyle: {
                borderColor: "#fcc02e"
              },
              areaStyle: {
                color: '#f44336',
                opacity: 1
              }
            }]
          };
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          var _this4 = this;

          setTimeout(function () {
            _this4.layout.publishLayoutChange({
              sidebarColor: 'white',
              topbarColor: 'white'
            });

            _this4.scack.open('Layout option changed {sidebarColor: "white", topbarColor: "white"};', 'OK', {
              duration: 6000
            });
          });
        }
      }]);

      return DashboardDarkComponent;
    }();

    DashboardDarkComponent.ctorParameters = function () {
      return [{
        type: app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    DashboardDarkComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-dashboard-dark',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./dashboard-dark.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.html")).default,
      animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"],
      styles: [__importDefault(__webpack_require__(
      /*! ./dashboard-dark.component.scss */
      "./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.scss")).default]
    }), __metadata("design:paramtypes", [app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], DashboardDarkComponent);
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/views/dashboard/dashboard.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardDashboardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".mycon {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row; }\n\n.content-post {\n  width: 300px;\n  height: 420px; }\n\n#card {\n  width: 333px; }\n\n#post-chart {\n  margin-right: auto; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n/* canvas{\r\n    width:250px;\r\n    height:350px;\r\n} */\n\n.container {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap; }\n\n.number {\n  height: 200px;\n  font-size: 100px; }\n\n.numbers {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsImFwcC92aWV3cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksb0JBQWE7RUFBYixhQUFhO0VBQ2IsOEJBQW1CO0VBQW5CLDZCQUFtQjtVQUFuQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFFSSxZQUFZO0VBQ1osYUFBYSxFQUFBOztBQUVqQjtFQUNJLFlBQVksRUFBQTs7QUFJaEI7RUFDSSxrQkFBa0IsRUFBQTs7QUFFdEI7RUFDSSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLFdBQVcsRUFBQTs7QUFHZjs7O0dDSkc7O0FEVUg7RUFDSSxvQkFBYTtFQUFiLGFBQWE7RUFDYixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksYUFBYTtFQUNiLGdCQUFnQixFQUFBOztBQUdwQjtFQUVJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGVBQWUsRUFBQSIsImZpbGUiOiJhcHAvdmlld3MvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teWNvbntcclxuICAgIC8vIG1hcmdpbjogMjBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uY29udGVudC1wb3N0e1xyXG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgZ2FpbnNib3JvO1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgaGVpZ2h0OiA0MjBweDtcclxufVxyXG4jY2FyZHtcclxuICAgIHdpZHRoOiAzMzNweDtcclxuICAgIFxyXG59XHJcblxyXG4jcG9zdC1jaGFydHtcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxufVxyXG4uZXhhbXBsZS1mb3JtIHtcclxuICAgIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuLyogY2FudmFze1xyXG4gICAgd2lkdGg6MjUwcHg7XHJcbiAgICBoZWlnaHQ6MzUwcHg7XHJcbn0gKi9cclxuXHJcblxyXG4uY29udGFpbmVye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxufVxyXG5cclxuLm51bWJlcntcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBmb250LXNpemU6IDEwMHB4O1xyXG59XHJcblxyXG4ubnVtYmVyc3tcclxuICAgIC8vIG1hcmdpbjogYXV0bztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBcclxuICAgIFxyXG59IiwiLm15Y29uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdzsgfVxuXG4uY29udGVudC1wb3N0IHtcbiAgd2lkdGg6IDMwMHB4O1xuICBoZWlnaHQ6IDQyMHB4OyB9XG5cbiNjYXJkIHtcbiAgd2lkdGg6IDMzM3B4OyB9XG5cbiNwb3N0LWNoYXJ0IHtcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvOyB9XG5cbi5leGFtcGxlLWZvcm0ge1xuICBtaW4td2lkdGg6IDE1MHB4O1xuICBtYXgtd2lkdGg6IDUwMHB4O1xuICB3aWR0aDogMTAwJTsgfVxuXG4uZXhhbXBsZS1mdWxsLXdpZHRoIHtcbiAgd2lkdGg6IDEwMCU7IH1cblxuLyogY2FudmFze1xyXG4gICAgd2lkdGg6MjUwcHg7XHJcbiAgICBoZWlnaHQ6MzUwcHg7XHJcbn0gKi9cbi5jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7IH1cblxuLm51bWJlciB7XG4gIGhlaWdodDogMjAwcHg7XG4gIGZvbnQtc2l6ZTogMTAwcHg7IH1cblxuLm51bWJlcnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7IH1cbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/views/dashboard/dashboard.component.ts ***!
    \********************************************************/

  /*! exports provided: DashboardComponent */

  /***/
  function srcAppViewsDashboardDashboardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
      return DashboardComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! chart.js */
    "./node_modules/chart.js/src/chart.js");
    /* harmony import */


    var chart_js__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_3__);

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DashboardComponent =
    /*#__PURE__*/
    function () {
      function DashboardComponent(client) {
        _classCallCheck(this, DashboardComponent);

        this.client = client;
        this.start = 0;
        this.end = this.start + 10;
        this.httpOptions2 = {
          headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': 'token ' + '	4b1e50f9ee7751b593ed4f7cd494af6f68013d22'
          })
        };
      }

      _createClass(DashboardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])(localStorage.getItem('Token')); // console.log('new new new')
          // console.log(this.header)

          this.getPostTypeCount(); // console.log(Date.prototype.getFullYear())
          // console.log(Date.prototype.getMonth())
          // console.log(Date.prototype.getDay())
          // console.log(Date.prototype.getUTCDate)
          // console.log("THESE ARE OPTIONS")

          this.getUser(); // console.log(httpOptions)
          // console.log(localStorage)

          this.getOrganization();
        }
      }, {
        key: "createOrg",
        value: function createOrg() {
          var _this5 = this;

          this.client.post('https://agile-cove-96115.herokuapp.com/organization/userOrganization/', {
            organization_name: this.org_name
          }, this.header).subscribe(function (res) {
            console.log(res);

            _this5.getOrganization();

            _this5.org_name = "";
          });
        }
      }, {
        key: "getOrganization",
        value: function getOrganization() {
          var _this6 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/organization/userOrganization', this.header).subscribe(function (res) {
            // console.log(res)
            _this6.organization = res[0]; //@ts-ignore

            if (res.length > 0) {
              _this6.getChatbotLog(); // // console.log(Date.now())


              _this6.dailyChat();

              _this6.dailyUser();

              _this6.getUnhandled();

              _this6.getCheckIn();

              _this6.getReservation();
            }
          });
        }
      }, {
        key: "getUser",
        value: function getUser() {
          this.client.get('https://agile-cove-96115.herokuapp.com/cuser/getUser', this.header).subscribe(function (res) {
            // console.log(res[0]['user']['first_name'])
            localStorage.setItem('name', res[0]['user']['first_name']);
          });
        }
      }, {
        key: "getPostTypeCount",
        value: function getPostTypeCount() {
          var _this7 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/count', this.header).subscribe(function (res) {
            // console.log(res)
            _this7.count = res; // console.log(this.count['content_writing'])
            // console.log(this.count['graphics'])

            _this7.CHART = document.getElementById('post-chart').getContext('2d');
            var chart = new chart_js__WEBPACK_IMPORTED_MODULE_3__["Chart"](_this7.CHART, {
              type: 'doughnut',
              data: {
                labels: ['GRAPHICS', 'CONTENT WRITING'],
                datasets: [{
                  label: 'Points',
                  backgroundColor: ['#e67e22', '#2980b9'],
                  data: [_this7.count['graphics'], _this7.count['content_writing']]
                }]
              },
              options: {
                responsive: true,
                maintainAspectRatio: false
              }
            });
          });
        }
      }, {
        key: "getChatbotLog",
        value: function getChatbotLog() {
          var _this8 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/organization/botConversations/', this.header).subscribe(function (res) {
            // console.log(res)
            // console.log("LOG LOG LOG")
            _this8.total = res; // console.log(typeof res)
            // @ts-ignore

            res = res.reverse();
            _this8.log = Array.prototype.slice.call(res, _this8.start, _this8.end);
          });
        }
      }, {
        key: "download_csv",
        value: function download_csv(csv, filename) {
          var csvFile;
          var downloadLink; // CSV FILE

          csvFile = new Blob([csv], {
            type: "text/csv"
          }); // Download link

          downloadLink = document.createElement("a"); // File name

          downloadLink.download = filename; // We have to create a link to the file

          downloadLink.href = window.URL.createObjectURL(csvFile); // Make sure that the link is not displayed

          downloadLink.style.display = "none"; // Add the link to your DOM

          document.body.appendChild(downloadLink); // Lanzamos

          downloadLink.click();
        }
      }, {
        key: "export_table_to_csv",
        value: function export_table_to_csv(html, filename) {
          var csv = [];
          var rows = document.querySelectorAll("table tr");

          for (var i = 0; i < rows.length; i++) {
            var row = [],
                cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++) {
              row.push(cols[j].innerHTML);
            }

            csv.push(row.join(","));
          } // Download CSV


          this.download_csv(csv.join("\n"), filename);
        }
      }, {
        key: "downloadAction",
        value: function downloadAction() {
          var html = document.querySelector("table").outerHTML;
          this.export_table_to_csv(html, "data.csv");
        }
      }, {
        key: "next",
        value: function next() {
          // this.end = this.start+10
          // if(this.end+10>this.total.length){
          //   this.start = this.end
          //   this.end = this.total.length
          // }
          // else{
          //   this.start+= 10
          //   this.end = this.end+10
          // }
          if (this.total.length - this.end > 10) {
            this.start = this.end;
            this.end = this.end + 10;
          } else {
            this.start = this.end;
            this.end = this.total.length;
          }

          this.getChatbotLog();
        }
      }, {
        key: "prev",
        value: function prev() {
          if (this.start - 10 >= 0) {
            this.start -= 10;
            this.end -= 10;
            this.getChatbotLog();
          }
        }
      }, {
        key: "dailyChat",
        value: function dailyChat() {
          var _this9 = this;

          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          var day = date.getDate();
          var new_day = "";
          var new_month = "";

          if (day < 10) {
            new_day = "0" + day.toString();
          } else {
            new_day = day.toString();
          }

          if (month < 10) {
            new_month = "0" + month.toString();
          } else {
            new_month = month.toString();
          }

          var req_day = year.toString() + '-' + new_month + '-' + new_day; // console.log("daily chats")
          // console.log(req_day)

          var body = {
            date: req_day
          }; // console.log("This is body")
          // console.log(body)

          var new_body = JSON.stringify(body); // console.log(new_body)

          this.client.post('https://agile-cove-96115.herokuapp.com/organization/dailyChats', new_body, this.header).subscribe(function (res) {
            // console.log("THESE ARE THE DAILY CHATS")
            _this9.number_of_chats_today = res['DailyChats']; // console.log(this.number_of_chats_today)
          });
        }
      }, {
        key: "dailyUser",
        value: function dailyUser() {
          var _this10 = this;

          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          var day = date.getDate();
          var new_day = "";
          var new_month = "";

          if (day < 10) {
            new_day = "0" + day.toString();
          } else {
            new_day = day.toString();
          }

          if (month < 10) {
            new_month = "0" + month.toString();
          } else {
            new_month = month.toString();
          }

          var req_day = year.toString() + '-' + new_month + '-' + new_day; // console.log(req_day)

          var body = {
            date: req_day
          }; // console.log("This is body")
          // console.log(body)

          var new_body = JSON.stringify(body); // console.log(new_body)

          this.client.post('https://agile-cove-96115.herokuapp.com/organization/dailyUsers', new_body, this.header).subscribe(function (res) {
            // console.log("THESE ARE THE DAILY CHATS")
            _this10.daily_users = res['users']; // console.log(this.number_of_chats_today)
          });
        }
      }, {
        key: "getUnhandled",
        value: function getUnhandled() {
          var _this11 = this;

          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          var day = date.getDate();
          var new_day = "";
          var new_month = "";

          if (day < 10) {
            new_day = "0" + day.toString();
          } else {
            new_day = day.toString();
          }

          if (month < 10) {
            new_month = "0" + month.toString();
          } else {
            new_month = month.toString();
          }

          var req_day = year.toString() + '-' + new_month + '-' + new_day; // console.log(req_day)

          var body = {
            date: req_day
          }; // console.log("This is body")
          // console.log(body)

          var new_body = JSON.stringify(body); // console.log(new_body)

          this.client.post('https://agile-cove-96115.herokuapp.com/organization/unhandled', new_body, this.header).subscribe(function (res) {
            // console.log("THESE ARE THE DAILY CHATS")
            _this11.unhandled = res['unhandled']; // console.log(this.number_of_chats_today)
          });
        }
      }, {
        key: "getCheckIn",
        value: function getCheckIn() {
          var _this12 = this;

          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          var day = date.getDate();
          var new_day = "";
          var new_month = "";

          if (day < 10) {
            new_day = "0" + day.toString();
          } else {
            new_day = day.toString();
          }

          if (month < 10) {
            new_month = "0" + month.toString();
          } else {
            new_month = month.toString();
          }

          var req_day = year.toString() + '-' + new_month + '-' + new_day; // console.log(req_day)

          var body = {
            date: req_day
          };
          var new_body = JSON.stringify(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/organization/countcheckin', body).subscribe(function (res) {
            _this12.checkin = res['check_in']; // console.log("THIS IS THE CHECKIN")
            // console.log(this.checkin)
          });
        }
      }, {
        key: "getReservation",
        value: function getReservation() {
          var _this13 = this;

          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          var day = date.getDate();
          var new_day = "";
          var new_month = "";

          if (day < 10) {
            new_day = "0" + day.toString();
          } else {
            new_day = day.toString();
          }

          if (month < 10) {
            new_month = "0" + month.toString();
          } else {
            new_month = month.toString();
          }

          var req_day = year.toString() + '-' + new_month + '-' + new_day; // console.log(req_day)

          var body = {
            date: req_day
          };
          var new_body = JSON.stringify(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/services/countreserve', body).subscribe(function (res) {
            _this13.reservation = res['ReservationCount']; // console.log("THIS IS THE CHECKIN")
            // console.log(this.checkin)
          });
        }
      }]);

      return DashboardComponent;
    }();

    DashboardComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }];
    };

    DashboardComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-dashboard',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./dashboard.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/dashboard.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./dashboard.component.scss */
      "./src/app/views/dashboard/dashboard.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])], DashboardComponent);
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/views/dashboard/dashboard.module.ts ***!
    \*****************************************************/

  /*! exports provided: DashboardModule */

  /***/
  function srcAppViewsDashboardDashboardModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardModule", function () {
      return DashboardModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ng2-charts */
    "./node_modules/ng2-charts/fesm2015/ng2-charts.js");
    /* harmony import */


    var ngx_echarts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ngx-echarts */
    "./node_modules/ngx-echarts/fesm2015/ngx-echarts.js");
    /* harmony import */


    var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @swimlane/ngx-datatable */
    "./node_modules/@swimlane/ngx-datatable/release/index.js");
    /* harmony import */


    var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7___default =
    /*#__PURE__*/
    __webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__);
    /* harmony import */


    var app_shared_pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! app/shared/pipes/shared-pipes.module */
    "./src/app/shared/pipes/shared-pipes.module.ts");
    /* harmony import */


    var _dashboard_routing__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./dashboard.routing */
    "./src/app/views/dashboard/dashboard.routing.ts");
    /* harmony import */


    var _analytics_analytics_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./analytics/analytics.component */
    "./src/app/views/dashboard/analytics/analytics.component.ts");
    /* harmony import */


    var _dashboard_dark_dashboard_dark_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./dashboard-dark/dashboard-dark.component */
    "./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.ts");
    /* harmony import */


    var _cryptocurrency_cryptocurrency_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./cryptocurrency/cryptocurrency.component */
    "./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.ts");
    /* harmony import */


    var _default_dashboard_default_dashboard_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./default-dashboard/default-dashboard.component */
    "./src/app/views/dashboard/default-dashboard/default-dashboard.component.ts");
    /* harmony import */


    var _dashboard_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./dashboard.component */
    "./src/app/views/dashboard/dashboard.component.ts");
    /* harmony import */


    var _manage_team_manage_team_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./manage-team/manage-team.component */
    "./src/app/views/dashboard/manage-team/manage-team.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DashboardModule = function DashboardModule() {
      _classCallCheck(this, DashboardModule);
    };

    DashboardModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_5__["ChartsModule"], ngx_echarts__WEBPACK_IMPORTED_MODULE_6__["NgxEchartsModule"], _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_7__["NgxDatatableModule"], app_shared_pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_8__["SharedPipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(_dashboard_routing__WEBPACK_IMPORTED_MODULE_9__["DashboardRoutes"])],
      declarations: [_analytics_analytics_component__WEBPACK_IMPORTED_MODULE_10__["AnalyticsComponent"], _dashboard_dark_dashboard_dark_component__WEBPACK_IMPORTED_MODULE_11__["DashboardDarkComponent"], _cryptocurrency_cryptocurrency_component__WEBPACK_IMPORTED_MODULE_12__["CryptocurrencyComponent"], _default_dashboard_default_dashboard_component__WEBPACK_IMPORTED_MODULE_13__["DefaultDashboardComponent"], _dashboard_component__WEBPACK_IMPORTED_MODULE_14__["DashboardComponent"], _manage_team_manage_team_component__WEBPACK_IMPORTED_MODULE_15__["ManageTeamComponent"]],
      exports: []
    })], DashboardModule);
    /***/
  },

  /***/
  "./src/app/views/dashboard/dashboard.routing.ts":
  /*!******************************************************!*\
    !*** ./src/app/views/dashboard/dashboard.routing.ts ***!
    \******************************************************/

  /*! exports provided: DashboardRoutes */

  /***/
  function srcAppViewsDashboardDashboardRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardRoutes", function () {
      return DashboardRoutes;
    });
    /* harmony import */


    var _analytics_analytics_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./analytics/analytics.component */
    "./src/app/views/dashboard/analytics/analytics.component.ts");
    /* harmony import */


    var _dashboard_dark_dashboard_dark_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./dashboard-dark/dashboard-dark.component */
    "./src/app/views/dashboard/dashboard-dark/dashboard-dark.component.ts");
    /* harmony import */


    var _cryptocurrency_cryptocurrency_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./cryptocurrency/cryptocurrency.component */
    "./src/app/views/dashboard/cryptocurrency/cryptocurrency.component.ts");
    /* harmony import */


    var _default_dashboard_default_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./default-dashboard/default-dashboard.component */
    "./src/app/views/dashboard/default-dashboard/default-dashboard.component.ts");
    /* harmony import */


    var _dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./dashboard.component */
    "./src/app/views/dashboard/dashboard.component.ts");
    /* harmony import */


    var _manage_team_manage_team_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./manage-team/manage-team.component */
    "./src/app/views/dashboard/manage-team/manage-team.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DashboardRoutes = [{
      path: "default",
      component: _default_dashboard_default_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DefaultDashboardComponent"],
      data: {
        title: "Default",
        breadcrumb: "Default"
      }
    }, {
      path: "team",
      component: _manage_team_manage_team_component__WEBPACK_IMPORTED_MODULE_5__["ManageTeamComponent"],
      data: {
        title: "Team Management",
        breadcrumb: "Manage"
      }
    }, {
      path: "analytics",
      component: _analytics_analytics_component__WEBPACK_IMPORTED_MODULE_0__["AnalyticsComponent"],
      data: {
        title: "Analytics",
        breadcrumb: "Analytics"
      }
    }, {
      path: "crypto",
      component: _cryptocurrency_cryptocurrency_component__WEBPACK_IMPORTED_MODULE_2__["CryptocurrencyComponent"],
      data: {
        title: "Cryptocurrency",
        breadcrumb: "Cryptocurrency"
      }
    }, {
      path: "dark",
      component: _dashboard_dark_dashboard_dark_component__WEBPACK_IMPORTED_MODULE_1__["DashboardDarkComponent"],
      data: {
        title: "Dark Cards",
        breadcrumb: "Dark Cards"
      }
    }, {
      path: "",
      component: _dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"],
      data: {
        title: "Dashboard",
        breadcrumb: "Dashboard"
      }
    }];
    /***/
  },

  /***/
  "./src/app/views/dashboard/default-dashboard/default-dashboard.component.scss":
  /*!************************************************************************************!*\
    !*** ./src/app/views/dashboard/default-dashboard/default-dashboard.component.scss ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardDefaultDashboardDefaultDashboardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvZGFzaGJvYXJkL2RlZmF1bHQtZGFzaGJvYXJkL2RlZmF1bHQtZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/default-dashboard/default-dashboard.component.ts":
  /*!**********************************************************************************!*\
    !*** ./src/app/views/dashboard/default-dashboard/default-dashboard.component.ts ***!
    \**********************************************************************************/

  /*! exports provided: DefaultDashboardComponent */

  /***/
  function srcAppViewsDashboardDefaultDashboardDefaultDashboardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DefaultDashboardComponent", function () {
      return DefaultDashboardComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! app/shared/animations/egret-animations */
    "./src/app/shared/animations/egret-animations.ts");
    /* harmony import */


    var app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! app/shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! tinycolor2 */
    "./node_modules/tinycolor2/tinycolor.js");
    /* harmony import */


    var tinycolor2__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(tinycolor2__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DefaultDashboardComponent =
    /*#__PURE__*/
    function () {
      function DefaultDashboardComponent(themeService, router) {
        _classCallCheck(this, DefaultDashboardComponent);

        this.themeService = themeService;
        this.router = router;
        this.lineChartSteppedData = [{
          data: [1, 8, 4, 8, 2, 2, 9],
          label: 'Order',
          borderWidth: 0,
          fill: true
        }, {
          data: [6, 2, 9, 3, 8, 2, 1],
          label: 'New client',
          borderWidth: 1,
          fill: true
        }];
        this.lineChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July'];
        /*
        * Full width Chart Options
        */

        this.lineChartOptions = {
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            display: false,
            position: 'bottom'
          },
          scales: {
            xAxes: [{
              display: false,
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }],
            yAxes: [{
              display: false,
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }]
          }
        };
        this.lineChartColors = [];
        this.lineChartLegend = false;
        this.lineChartType = 'line'; // Chart grid options

        this.doughnutChartColors1 = [{
          backgroundColor: ['#fff', 'rgba(0, 0, 0, .24)']
        }];
        this.doughnutChartColors2 = [{
          backgroundColor: ['rgba(0, 0, 0, .5)', 'rgba(0, 0, 0, .15)']
        }];
        this.total1 = 500;
        this.data1 = 200;
        this.doughnutChartData1 = [this.data1, this.total1 - this.data1];
        this.total2 = 600;
        this.data2 = 400;
        this.doughnutChartData2 = [this.data2, this.total2 - this.data2];
        this.doughnutLabels = ['Spent', 'Remaining'];
        this.doughnutChartType = 'doughnut';
        this.doughnutOptions = {
          cutoutPercentage: 85,
          responsive: true,
          legend: {
            display: false,
            position: 'bottom'
          },
          elements: {
            arc: {
              borderWidth: 0
            }
          },
          tooltips: {
            enabled: true
          }
        };
        this.photos = [{
          name: 'Photo 1',
          url: 'assets/images/sq-15.jpg'
        }, {
          name: 'Photo 2',
          url: 'assets/images/sq-8.jpg'
        }, {
          name: 'Photo 3',
          url: 'assets/images/sq-9.jpg'
        }, {
          name: 'Photo 4',
          url: 'assets/images/sq-10.jpg'
        }, {
          name: 'Photo 5',
          url: 'assets/images/sq-11.jpg'
        }, {
          name: 'Photo 6',
          url: 'assets/images/sq-12.jpg'
        }];
        this.tickets = [{
          img: 'assets/images/face-1.jpg',
          name: 'Mike Dake',
          text: 'Excerpt pipe is used.',
          date: new Date('07/12/2017'),
          isOpen: true
        }, {
          img: 'assets/images/face-5.jpg',
          name: 'Jhone Doe',
          text: 'My dashboard is not working.',
          date: new Date('07/7/2017'),
          isOpen: false
        }, {
          img: 'assets/images/face-3.jpg',
          name: 'Jhonson lee',
          text: 'Fix stock issue',
          date: new Date('04/10/2017'),
          isOpen: false
        }, {
          img: 'assets/images/face-4.jpg',
          name: 'Mikie Jyni',
          text: 'Renew my subscription.',
          date: new Date('07/7/2017'),
          isOpen: false
        }]; // users

        this.users = [{
          "name": "Snow Benton",
          "membership": "Paid Member",
          "phone": "+1 (956) 486-2327",
          "photo": "assets/images/face-4.jpg",
          "address": "329 Dictum Court, Minnesota",
          "registered": "2016-07-09"
        }, {
          "name": "Kay Sellers",
          "membership": "Paid Member",
          "phone": "+1 (929) 406-3172",
          "photo": "assets/images/face-2.jpg",
          "address": "893 Garden Place, American Samoa",
          "registered": "2017-02-16"
        }];
        this.projects = [{
          name: 'User Story',
          user: 'Watson Joyce',
          progress: 100,
          leader: 'Snow Benton'
        }, {
          name: 'Design Data Model',
          user: 'Morris Adams',
          progress: 30,
          leader: 'Watson Joyce'
        }, {
          name: 'Develop CR Algorithm',
          user: 'Jhone Doe',
          progress: 70,
          leader: 'Ada Kidd'
        }, {
          name: 'Payment Module',
          user: 'Ada Kidd',
          progress: 50,
          leader: 'Snow Benton'
        }, {
          name: 'Discount Module',
          user: 'Workman Floyd',
          progress: 50,
          leader: 'Robert Middleton'
        }];
      }

      _createClass(DefaultDashboardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this14 = this;

          if (!localStorage.getItem('isSignedIn')) {
            console.log("Not signed in");
            this.router.navigateByUrl('/dashboard');
            this.router.navigateByUrl('/sessions/signin');
          }

          console.log("Hello");
          this.themeService.onThemeChange.subscribe(function (activeTheme) {
            _this14.setChartColor(activeTheme);
          });
          this.setChartColor(this.themeService.activatedTheme);
        }
      }, {
        key: "setChartColor",
        value: function setChartColor(theme) {
          this.lineChartColors = [{
            backgroundColor: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.6),
            borderColor: 'rgba(0,0,0,0)',
            pointBackgroundColor: tinycolor2__WEBPACK_IMPORTED_MODULE_3___default()(theme.baseColor).setAlpha(.4),
            pointBorderColor: 'rgba(0, 0, 0, 0)',
            pointHoverBackgroundColor: theme.baseColor,
            pointHoverBorderColor: theme.baseColor
          }, {
            backgroundColor: 'rgba(0, 0, 0, .08)',
            borderColor: 'rgba(0,0,0,0)',
            pointBackgroundColor: 'rgba(0, 0, 0, 0.06)',
            pointBorderColor: 'rgba(0, 0, 0, 0)',
            pointHoverBackgroundColor: 'rgba(0, 0, 0, 0.1)',
            pointHoverBorderColor: 'rgba(0, 0, 0, 0)'
          }];
        }
      }]);

      return DefaultDashboardComponent;
    }();

    DefaultDashboardComponent.ctorParameters = function () {
      return [{
        type: app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    DefaultDashboardComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-default-dashboard',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./default-dashboard.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/default-dashboard/default-dashboard.component.html")).default,
      animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"],
      styles: [__importDefault(__webpack_require__(
      /*! ./default-dashboard.component.scss */
      "./src/app/views/dashboard/default-dashboard/default-dashboard.component.scss")).default]
    }), __metadata("design:paramtypes", [app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], DefaultDashboardComponent);
    /***/
  },

  /***/
  "./src/app/views/dashboard/manage-team/manage-team.component.scss":
  /*!************************************************************************!*\
    !*** ./src/app/views/dashboard/manage-team/manage-team.component.scss ***!
    \************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsDashboardManageTeamManageTeamComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".team {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  padding: 10px;\n  text-align: center; }\n\n.member {\n  height: auto;\n  width: 250px;\n  padding: 5px;\n  margin: 5px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  text-align: center; }\n\n.member_c {\n  border: 1px solid grey;\n  height: auto;\n  width: 250px;\n  padding: 5px;\n  margin: 5px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  text-align: center; }\n\n#mynav {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n          flex-direction: row-reverse; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvZGFzaGJvYXJkL21hbmFnZS10ZWFtL21hbmFnZS10ZWFtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0JBQWE7RUFBYixhQUFhO0VBQ2IsOEJBQW1CO0VBQW5CLDZCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGtCQUFrQixFQUFBOztBQUd0QjtFQUVJLFlBQVk7RUFDWixZQUFZO0VBQ1osWUFBVztFQUNYLFdBQVU7RUFDVixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0QixrQkFBa0IsRUFBQTs7QUFJdEI7RUFFSSxzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLFlBQVk7RUFDWixZQUFXO0VBQ1gsV0FBVTtFQUNWLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLGtCQUFrQixFQUFBOztBQUd0QjtFQUVJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDhCQUEyQjtFQUEzQiw4QkFBMkI7VUFBM0IsMkJBQTJCLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL2Rhc2hib2FyZC9tYW5hZ2UtdGVhbS9tYW5hZ2UtdGVhbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZWFte1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4ubWVtYmVye1xyXG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgZ3JleTtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIHdpZHRoOiAyNTBweDtcclxuICAgIHBhZGRpbmc6NXB4O1xyXG4gICAgbWFyZ2luOjVweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgXHJcbn1cclxuXHJcbi5tZW1iZXJfY3tcclxuXHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG4gICAgcGFkZGluZzo1cHg7XHJcbiAgICBtYXJnaW46NXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG59XHJcbiNteW5hdntcclxuICAgIFxyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcclxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/views/dashboard/manage-team/manage-team.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/views/dashboard/manage-team/manage-team.component.ts ***!
    \**********************************************************************/

  /*! exports provided: ManageTeamComponent */

  /***/
  function srcAppViewsDashboardManageTeamManageTeamComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ManageTeamComponent", function () {
      return ManageTeamComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../httpOptions */
    "./src/app/httpOptions.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ManageTeamComponent =
    /*#__PURE__*/
    function () {
      function ManageTeamComponent(client) {
        _classCallCheck(this, ManageTeamComponent);

        this.client = client;
        this.email = "";
        this.active_email = "";
      }

      _createClass(ManageTeamComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])(localStorage.getItem('Token'));
          this.getTeam();
          this.currentTeams();
        }
      }, {
        key: "invite",
        value: function invite(i_type) {
          var _this15 = this;

          if (i_type == 'cWork') {
            console.log(i_type);
            console.log(this.email);
            var body = {
              "recipient": this.email,
              "role": 'R'
            };
            var new_body = JSON.stringify(body);
            this.client.post('https://agile-cove-96115.herokuapp.com/invitation/invitation/', new_body, this.header).subscribe(function (res) {
              console.log(res);
              _this15.email = "";

              _this15.getTeam();
            });
          } else {
            console.log(i_type);
            console.log(this.active_email);
            var _body = {
              "email": this.active_email
            };

            var _new_body = JSON.stringify(_body);

            this.client.post('https://agile-cove-96115.herokuapp.com/invitation/addTeam/', _new_body, this.header).subscribe(function (res) {
              console.log(res);
              _this15.active_email = "";

              _this15.getTeam();
            });
          }
        }
      }, {
        key: "searchUser",
        value: function searchUser(e) {
          var _this16 = this;

          this.active_email = e.target.value;

          if (this.active_email == "") {
            this.search_results = [];
          } else {
            this.client.get('https://agile-cove-96115.herokuapp.com/invitation/searchUser?search=' + this.active_email).subscribe(function (res) {
              console.log(res);
              _this16.search_results = res;
              console.log(_this16.search_results);
            });
          }
        }
      }, {
        key: "setActiveUser",
        value: function setActiveUser(a_email) {
          this.search_results = [];
          this.active_email = a_email;
          console.log("clicked");
        }
      }, {
        key: "getTeam",
        value: function getTeam() {
          var _this17 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/invitation/teams/', this.header).subscribe(function (res) {
            console.log("This is team"); // console.log(res)

            _this17.team = res;
            console.log(_this17.team);
          });
        }
      }, {
        key: "removeMember",
        value: function removeMember(id) {
          var _this18 = this;

          console.log(id);
          this.client.delete('https://agile-cove-96115.herokuapp.com/invitation/teams/' + id, this.header).subscribe(function (res) {
            _this18.getTeam();
          });
        }
      }, {
        key: "quitTeam",
        value: function quitTeam(id) {
          var _this19 = this;

          this.client.delete('https://agile-cove-96115.herokuapp.com/invitation/deleteTeam/' + id + '/', this.header).subscribe(function (res) {
            _this19.currentTeams();
          });
        }
      }, {
        key: "currentTeams",
        value: function currentTeams() {
          var _this20 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/invitation/currentTeams', this.header).subscribe(function (res) {
            console.log("These are the curent teams the user belongs to.");
            console.log(res);
            _this20.c_teams = res;
          });
        }
      }]);

      return ManageTeamComponent;
    }();

    ManageTeamComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }];
    };

    ManageTeamComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-manage-team',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./manage-team.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/dashboard/manage-team/manage-team.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./manage-team.component.scss */
      "./src/app/views/dashboard/manage-team/manage-team.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])], ManageTeamComponent);
    /***/
  }
}]);
//# sourceMappingURL=views-dashboard-dashboard-module-es5.js.map