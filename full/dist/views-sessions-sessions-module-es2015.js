(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-sessions-sessions-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/error/error.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/error/error.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 default-bg\">\n  <div class=\"app-error\">\n    <div class=\"fix\">\n      <mat-icon class=\"error-icon\" color=\"warn\">warning</mat-icon>\n      <div class=\"error-text\">\n        <h1 class=\"error-title\">500</h1>\n        <div class=\"error-subtitle\">Server Error!</div>\n      </div>\n    </div>\n    \n    <div class=\"error-actions\">\n      <button \n      mat-raised-button \n      color=\"primary\"\n      class=\"mb-1 mr-05\"\n      [routerLink]=\"['/']\">Back to Dashboard</button>\n      <button \n      mat-raised-button \n      color=\"warn\">Report this Problem</button>\n    </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/forgot-password/forgot-password.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/forgot-password/forgot-password.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 black\">\n  <div class=\"session-form-hold\">\n    <mat-progress-bar mode=\"determinate\" class=\"session-progress\"></mat-progress-bar>\n    <mat-card>\n      <mat-card-content>\n        <div class=\"text-center pt-8 pb-16\">\n          <img width=\"60px\" src=\"assets/images/logo.png\" alt=\"\" class=\"mb-05\">\n          <p class=\"text-muted m-0\">New password will be sent to your email address</p>\n        </div>\n        <form #fpForm=\"ngForm\" (ngSubmit)=\"submitEmail()\">\n\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                matInput\n                name=\"email\"\n                required\n                [(ngModel)]=\"userEmail\"\n                #email=\"ngModel\"\n                placeholder=\"Email\"\n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"email.errors && (email.dirty || email.touched) && (email?.errors?.required)\" \n              class=\"form-error-msg\"> Email is required </small>\n          </div>\n\n          <button mat-raised-button class=\"mat-primary full-width mb-1\" [disabled]=\"fpForm.invalid\">Submit</button>\n          <div class=\"text-center\" style=\"color:green\">\n              <div class=\"text-center full-width\" [hidden]=\"!showMessage\">Please check your email and follow the link to reset your password</div>\n          </div>\n          \n          <div class=\"text-center\">\n            <a [routerLink]=\"'/sessions/signin'\" class=\"mat-primary text-center full-width\">Sign in</a>\n            <span fxFlex></span>\n            <a [routerLink]=\"'/sessions/signup'\" class=\"mat-primary text-center full-width\">Create a new account</a>\n          </div>\n        </form>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/lockscreen/lockscreen.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/lockscreen/lockscreen.component.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 black\">\n  <div class=\"session-form-hold session-lockscreen\">\n    <mat-progress-bar mode=\"determinate\" class=\"session-progress\"></mat-progress-bar>\n    <mat-card>\n      <mat-card-content>\n        <div fxFlex=\"column\" fxFlexWrap=\"wrap\">\n          <div fxFlexWrap=\"wrap\" class=\"lockscreen-user\">\n            <img class=\"lockscreen-face\" src=\"assets/images/face-3.jpg\" alt=\"\">\n            <h5 class=\"m-0 font-normal\">John Doe</h5>\n            <small class=\"text-muted\">Last seen 1 hour ago</small>\n          </div>\n          <form #lockscreenForm=\"ngForm\" (ngSubmit)=\"unlock()\">\n            <div class=\"\">\n              <mat-form-field class=\"full-width\">\n                <input \n                  type=\"password\"\n                  name=\"password\"\n                  required\n                  matInput\n                  [(ngModel)]=\"lockscreenData.password\"\n                  #password=\"ngModel\"\n                  placeholder=\"Password\">\n              </mat-form-field>\n              <small \n                *ngIf=\"password.errors && (password.dirty || password.touched) && (password?.errors?.required)\" \n                class=\"form-error-msg\"> Password is required </small>\n            </div>\n            \n            <button mat-raised-button class=\"mat-primary full-width mb-05\" [disabled]=\"lockscreenForm.invalid\">Unlock</button>\n            <button mat-raised-button [routerLink]=\"'/sessions/signin'\" color=\"accent\" class=\"mat-primary full-width\">It's not me!</button>\n          </form>\n        </div>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/not-found/not-found.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/not-found/not-found.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 default-bg\">\n  <div class=\"app-error\">\n    <div class=\"fix\">\n      <mat-icon class=\"error-icon\" color=\"warn\">error</mat-icon>\n      <div class=\"error-text\">\n        <h1 class=\"error-title\">404</h1>\n        <div class=\"error-subtitle\">Page Not Found!</div>\n      </div>\n    </div>\n    \n    <div class=\"error-actions\">\n      <button \n      mat-raised-button \n      color=\"primary\"\n      class=\"mb-1 mr-05\"\n      [routerLink]=\"['/']\">Back to Dashboard</button>\n      \n      <button \n      mat-raised-button \n      color=\"warn\">Report this Problem</button>\n    </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin/signin.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin/signin.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 black\">\n  <div class=\"session-form-hold\">\n    <mat-progress-bar mode=\"determinate\" class=\"session-progress\"></mat-progress-bar>\n    <mat-card>\n      <mat-card-content>\n        <div class=\"text-center pt-8 pb-16\">\n          <img width=\"60px\" src=\"assets/images/logo.png\" alt=\"\" class=\"mb-05\">\n          <p class=\"text-muted m-0\">Sign in to your account</p>\n        </div>\n        <form [formGroup]=\"signinForm\" (ngSubmit)=\"signin()\">\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                matInput\n                name=\"email\"\n                [formControl]=\"signinForm.controls['username']\"\n                placeholder=\"Email Address\"\n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signinForm.controls['username'].hasError('required') && signinForm.controls['username'].touched\" \n              class=\"form-error-msg\"> Email is required </small>\n          </div>\n\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input \n                type=\"password\"\n                name=\"password\"\n                matInput\n                [formControl]=\"signinForm.controls['password']\"\n                placeholder=\"Password\" \n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signinForm.controls['password'].hasError('required') && signinForm.controls['password'].touched\" \n              class=\"form-error-msg\"> Password is required </small>\n          </div>\n          \n          <div class=\"pb-1\">\n            <mat-checkbox\n              name=\"rememberMe\"\n              [formControl]=\"signinForm.controls['rememberMe']\"\n              class=\"pb-1\">Remember this computer</mat-checkbox>\n          </div>\n          <div [hidden]=\"!authService.wrong_pass\" style=\"color:red; text-align:center\">Email or Password is incorrect</div>\n          <br>\n          <button mat-raised-button class=\"mat-primary full-width mb-1\" [disabled]=\"signinForm.invalid\">Sign in</button>\n          <div class=\"text-center\">\n            <a [routerLink]=\"'/sessions/forgot-password'\" class=\"mat-primary text-center full-width\">Forgot password</a>\n            <span fxFlex></span>\n            <a [routerLink]=\"'/sessions/signup'\" class=\"mat-primary text-center full-width\">Create a new account</a>\n          </div>\n        </form>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin2/signin2.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin2/signin2.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"seciton-left\">\n  <div class=\"section-left-content\">\n    <h1 class=\"text-36 font-weight-light\">Don't have an account?</h1>\n    <p class=\"mb-24 text-small\">Stop wasting time and money. It's free!</p>\n    <button mat-flat-button color=\"accent\" [routerLink]=\"'/sessions/signup2'\">Sign Up</button>\n  </div>\n</div>\n\n\n<div class=\"form-holder height-100vh mat-elevation-z4\" [perfectScrollbar]=\"{}\">\n  <form\n    [formGroup]=\"signupForm\"\n    class=\"signup-form\"\n    fxLayout=\"column\"\n    (ngSubmit)=\"onSubmit()\"\n  >\n    <!-- headline -->\n    <div class=\"form-headline text-center mb-32\">\n      <h1 class=\"font-weight-light\">Sign in to your account</h1>\n    </div>\n    <div class=\"mb-48\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n      <img\n        width=\"200px\"\n        src=\"assets/images/illustrations/breaking_barriers.svg\"\n        alt=\"\"\n      />\n    </div>\n\n    <mat-form-field class=\"full-width\" appearance=\"outline\">\n      <mat-label>Email</mat-label>\n      <input\n        matInput\n        formControlName=\"email\"\n        type=\"email\"\n        name=\"email\"\n        placeholder=\"Email\"\n      />\n    </mat-form-field>\n\n    <mat-form-field class=\"full-width\" appearance=\"outline\">\n      <mat-label>Password</mat-label>\n      <input\n        matInput\n        formControlName=\"password\"\n        type=\"password\"\n        name=\"password\"\n        placeholder=\"********\"\n      />\n    </mat-form-field>\n\n    <mat-checkbox class=\"mb-24\" formControlName=\"agreed\"\n      >I agree with terms and condtions</mat-checkbox\n    >\n    <button  mat-raised-button color=\"primary\">Signin</button>\n    <mat-divider class=\"my-32\"></mat-divider>\n\n    <div class=\"mb-24\" fxLayout=\"column\" fxLayoutGap=\"16px\">\n      <button mat-raised-button color=\"warn\" type=\"button\">\n        Signin with Auth0\n      </button>\n      <button mat-raised-button color=\"warn\" type=\"button\">Firebase Signin</button>\n    </div>\n\n    <!-- <div class=\"text-center\">\n      <a\n        [routerLink]=\"'/sessions/signup2'\"\n        class=\"mat-color-primary text-center full-width\"\n        >Sign up</a\n      >\n    </div> -->\n  </form>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin3/signin3.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin3/signin3.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"signup3-wrap\">\n  <div\n    class=\"signup3-form-holder mat-elevation-z4 py-48\"\n    [perfectScrollbar]\n    [@animate]=\"{value:'*',params:{y: '40px',opacity:'0',delay:'100ms', duration: '400ms'}}\"\n  >\n    <!-- headline -->\n    <div class=\"form-headline text-center mb-32\">\n      <h1 class=\"font-weight-light\">Sign in to your account</h1>\n    </div>\n    <div class=\"mb-32\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n      <img\n        width=\"200px\"\n        src=\"assets/images/illustrations/breaking_barriers.svg\"\n        alt=\"\"\n      />\n    </div>\n\n    <form [formGroup]=\"signupForm\" class=\"signup3-form\" (ngSubmit)=\"onSubmit()\">\n      <div>\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Username</mat-label>\n          <input\n            matInput\n            formControlName=\"username\"\n            type=\"text\"\n            name=\"username\"\n            placeholder=\"Username\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Password</mat-label>\n          <input\n            matInput\n            formControlName=\"password\"\n            type=\"password\"\n            name=\"password\"\n            placeholder=\"********\"\n          />\n        </mat-form-field>\n      </div>\n      <div class=\"mb-24\">\n        <button fxFill mat-raised-button color=\"primary\">Sign In</button>\n      </div>\n      <p>\n        Don't have an account?\n        <a\n          class=\"mat-color-primary font-weight-bold mx-8\"\n          routerLink=\"/sessions/signup3\"\n          >Sign Up</a\n        >\n      </p>\n    </form>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin4/signin4.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin4/signin4.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div\n  [perfectScrollbar]=\"{}\"\n  class=\"height-100vh signup4-wrap\"\n  fxLayout=\"row wrap\"\n  fxLayoutAlign=\"center center\"\n>\n  <div\n    class=\"signup4-container mat-elevation-z4 white\"\n    fxLayout=\"row wrap\"\n    fxLayout.xs=\"column\"\n    fxLayoutAlign=\"start stretch\"\n    fxFlex=\"60\"\n    fxFlex.xs=\"94\"\n    fxFlex.sm=\"80\"\n    [@animate]=\"{value:'*',params:{y:'40px',opacity:'0',delay:'100ms', duration: '400ms'}}\"\n  >\n\n  <!-- Left Side content -->\n      <div fxLayout=\"column\" fxLayoutAlign=\"center center\" \n      class=\"signup4-header\" fxFlex=\"40\">\n\n        <div class=\"\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n            <img width=\"200px\" src=\"assets/images/illustrations/lighthouse.svg\" alt=\"\">\n        </div>\n\n      </div>\n\n    <!-- Right side content -->\n   <div fxFlex=\"60\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n    <form  [formGroup]=\"signupForm\" class=\"signup4-form grey-100\" (ngSubmit)=\"onSubmit()\">\n      \n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Email</mat-label>\n          <input\n            matInput\n            formControlName=\"email\"\n            type=\"email\"\n            name=\"email\"\n            placeholder=\"Email\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Password</mat-label>\n          <input\n            matInput\n            formControlName=\"password\"\n            type=\"password\"\n            name=\"password\"\n            placeholder=\"********\"\n          />\n        </mat-form-field>\n\n        <mat-checkbox formControlName=\"agreed\"\n        >I agree with terms and condtions</mat-checkbox\n      >\n      \n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start center\" style=\"margin-top: 20px;\">\n        <button mat-flat-button color=\"primary\">Sign In</button>\n        <span class=\"px-16\">or</span>\n        <a class=\"font-weight-bold mat-color-primary\" routerLink=\"/sessions/signup4\">Sign Up</a>\n      </div>\n\n      <!-- <div fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\" style=\"margin-top: 20px\">\n          <span>or connect with </span>\n          <div>\n            icons goes here\n          </div>\n        </div> -->\n\n    </form>\n   </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup/signup.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup/signup.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"page-wrap height-100 black\">\n  <div class=\"session-form-hold\">\n    <mat-progress-bar mode=\"determinate\" class=\"session-progress\"></mat-progress-bar>\n    <mat-card>\n      <mat-card-content>\n        <div class=\"text-center pt-8 pb-16\">\n          <img width=\"60px\" src=\"assets/images/logo.png\" alt=\"\" class=\"mb-05\">\n           <h2>Lets get started</h2>\n          <p class=\"text-muted m-0\">Sign up to use our service</p>\n        </div>\n        <form [formGroup]=\"signupForm\"  (ngSubmit)=\"signup()\">\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                matInput\n                type=\"text\"\n                name=\"firstName\"\n                [formControl]=\"signupForm.controls['firstName']\"\n                placeholder=\"Your Firstname\"\n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signupForm.controls['firstName'].hasError('required') && signupForm.controls['firstName'].touched\" \n              class=\"form-error-msg\"> Firstname is required </small>\n\n              \n          </div>\n\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                matInput\n                type=\"text\"\n                name=\"lastName\"\n                [formControl]=\"signupForm.controls['lastName']\"\n                placeholder=\"Your Lastname\"\n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signupForm.controls['lastName'].hasError('required') && signupForm.controls['lastName'].touched\" \n              class=\"form-error-msg\"> Lastname is required </small>\n\n              \n          </div>\n          \n          \n          \n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                matInput\n                type=\"email\"\n                name=\"email\"\n                [formControl]=\"signupForm.controls['email']\"\n                placeholder=\"Your Email\"\n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signupForm.controls['email'].hasError('required') && signupForm.controls['email'].touched\" \n              class=\"form-error-msg\"> Email is required </small>\n\n              <small \n                *ngIf=\"signupForm.controls['email'].hasError('email') && signupForm.controls['email'].touched\" \n                class=\"form-error-msg\"> Invaild email address </small>\n          </div>\n\n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input \n                type=\"password\"\n                name=\"password\"\n                matInput\n                [formControl]=\"signupForm.controls['password']\"\n                placeholder=\"Password\" \n                value=\"\">\n            </mat-form-field>\n            <small \n              *ngIf=\"signupForm.controls['password'].hasError('required') && signupForm.controls['password'].touched\" \n              class=\"form-error-msg\"> Password is required </small>\n          </div>\n          \n          <div class=\"\">\n            <mat-form-field class=\"full-width\">\n              <input\n                type=\"password\"\n                name=\"confirmPassword\"\n                matInput\n                [formControl]=\"signupForm.controls['confirmPassword']\"\n                placeholder=\"Confirm Password\"\n                value=\"\">\n            </mat-form-field>\n            <small *ngIf=\"signupForm.controls['confirmPassword'].hasError('required') && signupForm.controls['confirmPassword'].touched\" class=\"form-error-msg\">Confirm password is required.</small>\n            <small *ngIf=\"signupForm.controls['confirmPassword'].hasError('equalTo')\" class=\"form-error-msg\">Passwords do not math.</small>\n          </div>\n          \n          <div class=\"pb-1\">\n            <mat-checkbox\n              name=\"agreed\"\n              [formControl]=\"signupForm.controls['agreed']\"\n              class=\"pb-1\">I have read and agree to the terms of service.</mat-checkbox>\n\n              <small \n                *ngIf=\"signupForm.controls['agreed'].hasError('agreed') && signupForm.controls['agreed'].touched\" \n                class=\"form-error-msg\"> You must agree to the terms and conditions </small>\n          </div>\n\n          <button mat-raised-button class=\"mat-primary full-width mb-1\" [disabled]=\"signupForm.invalid\">Sign up</button>\n          <div class=\"text-center\" style=\"color:green\">\n\n            <div class=\"text-center full-width\" [hidden]=\"!showMessage\">Please check your email to verify your account.</div>\n\n\n          </div>\n          <div class=\"text-center\">\n            <a [routerLink]=\"'/sessions/signin'\" class=\"text-center full-width\">Sign in to existing account</a>\n          </div>\n        </form>\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup2/signup2.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup2/signup2.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"seciton-left\">\n</div>\n  <div\n    class=\"form-holder height-100vh mat-elevation-z4\"\n    [perfectScrollbar]=\"{}\"\n  >\n    <form\n      [formGroup]=\"signupForm\"\n      class=\"signup-form\"\n      fxLayout=\"column\"\n      fxLayoutAlign=\"start\"\n      (ngSubmit)=\"onSubmit()\"\n    >\n      <!-- headline -->\n      <div class=\"form-headline text-center mb-16\">\n        <h1 class=\"font-weight-bold\">Lets get started!</h1>\n        <p class=\"text-muted\">Create an account get unlimited access</p>\n      </div>\n      <div class=\"mb-32\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n          <img width=\"200px\" src=\"assets/images/illustrations/posting_photo.svg\" alt=\"\">\n      </div>\n      <div fxLayout=\"row wrap\" fxLayoutGap.md=\"16px\" fxLayoutGap.lg=\"16px\">\n        <mat-form-field fxFlex=\"1 1 0%\" appearance=\"outline\">\n          <mat-label>First name</mat-label>\n          <input\n            formControlName=\"firstName\"\n            matInput\n            type=\"text\"\n            name=\"first_name\"\n            placeholder=\"First name\"\n          />\n        </mat-form-field>\n        <mat-form-field fxFlex=\"1 1 0%\" appearance=\"outline\">\n          <mat-label>Last name</mat-label>\n          <input\n            formControlName=\"lastName\"\n            matInput\n            type=\"text\"\n            name=\"Last_name\"\n            placeholder=\"Last name\"\n          />\n        </mat-form-field>\n      </div>\n\n      <mat-form-field class=\"full-width\" appearance=\"outline\">\n        <mat-label>Username</mat-label>\n        <input\n          matInput\n          formControlName=\"username\"\n          type=\"text\"\n          name=\"username\"\n          placeholder=\"Username\"\n        />\n      </mat-form-field>\n\n      <mat-form-field class=\"full-width\" appearance=\"outline\">\n        <mat-label>Email</mat-label>\n        <input\n          matInput\n          formControlName=\"email\"\n          type=\"email\"\n          name=\"email\"\n          placeholder=\"Email\"\n        />\n      </mat-form-field>\n\n      <mat-form-field class=\"full-width\" appearance=\"outline\">\n        <mat-label>Password</mat-label>\n        <input\n          matInput\n          formControlName=\"password\"\n          type=\"password\"\n          name=\"password\"\n          placeholder=\"********\"\n        />\n      </mat-form-field>\n\n      <mat-form-field class=\"full-width\" appearance=\"outline\">\n        <mat-label>Confirm password</mat-label>\n        <input\n          matInput\n          type=\"password\"\n          name=\"confirm_password\"\n          placeholder=\"********\"\n          formControlName=\"confirmPassword\"\n        />\n        <mat-error>\n          Password didn't match\n        </mat-error>\n      </mat-form-field>\n\n      <mat-checkbox class=\"mb-24\" formControlName=\"agreed\"\n        >I agree with terms and condtions</mat-checkbox\n      >\n      <button  mat-raised-button color=\"primary\">Signup</button>\n      <mat-divider class=\"my-32\"></mat-divider>\n\n      <div fxLayout=\"column\" fxLayoutGap=\"16px\" class=\"mb-24\">\n        <button mat-raised-button color=\"warn\" type=\"button\">\n          Signup with Auth0\n        </button>\n        <button mat-raised-button color=\"warn\" type=\"button\">Firebase Signup</button>\n      </div>\n\n      <div class=\"text-center\">\n          <a\n            [routerLink]=\"'/sessions/signin2'\"\n            class=\"mat-color-primary text-center full-width\"\n            >Sign in with an existing account</a\n          >\n        </div>\n    </form>\n  </div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup3/signup3.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup3/signup3.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"signup3-wrap\">\n  <div\n    class=\"signup3-form-holder mat-elevation-z4 pt-48 pb-16\"\n    [perfectScrollbar]=\"{}\"\n    [@animate]=\"{value:'*',params:{y: '40px', opacity:'0',delay:'100ms', duration: '400ms'}}\"\n  >\n    <!-- headline -->\n    <div class=\"form-headline text-center mb-32\">\n      <h1 class=\"font-weight-bold\">Lets get started!</h1>\n      <p class=\"text-muted\">Create an account get unlimited access</p>\n    </div>\n    <div class=\"mb-32\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n      <img\n        width=\"200px\"\n        src=\"assets/images/illustrations/posting_photo.svg\"\n        alt=\"\"\n      />\n    </div>\n\n    <form [formGroup]=\"signupForm\" class=\"signup3-form\" (ngSubmit)=\"onSubmit()\">\n      <div>\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Username</mat-label>\n          <input\n            matInput\n            formControlName=\"username\"\n            type=\"text\"\n            name=\"username\"\n            placeholder=\"Username\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Email</mat-label>\n          <input\n            matInput\n            formControlName=\"email\"\n            type=\"email\"\n            name=\"email\"\n            placeholder=\"Email\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Password</mat-label>\n          <input\n            matInput\n            formControlName=\"password\"\n            type=\"password\"\n            name=\"password\"\n            placeholder=\"********\"\n          />\n        </mat-form-field>\n      </div>\n      <div class=\"mb-24\">\n        <button fxFill mat-raised-button color=\"primary\">Signup</button>\n      </div>\n      <p>\n        Don't have an account?\n        <a\n          class=\"mat-color-primary font-weight-bold mx-8\"\n          routerLink=\"/sessions/signin3\"\n          >Sign In</a\n        >\n      </p>\n    </form>\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup4/signup4.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup4/signup4.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div\n  [perfectScrollbar]=\"{}\"\n  class=\"height-100vh signup4-wrap\"\n  fxLayout=\"row wrap\"\n  fxLayoutAlign=\"center center\"\n>\n  <div\n    class=\"signup4-container mat-elevation-z4\"\n    fxLayout=\"row wrap\"\n    fxLayout.xs=\"column\"\n    fxLayoutAlign=\"start stretch\"\n    fxFlex=\"60\"\n    fxFlex.xs=\"94\"\n    fxFlex.sm=\"80\"\n    [@animate]=\"{value:'*',params:{y:'120px',opacity:'0',delay:'100ms', duration: '400ms'}}\"\n  >\n\n  <!-- Left Side content -->\n  <div fxLayout=\"column\" fxLayoutAlign=\"center center\" \n  class=\"signup4-header grey-200\" fxFlex=\"40\">\n\n    <div class=\"\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n        <img width=\"200px\" src=\"assets/images/illustrations/posting_photo.svg\" alt=\"\">\n    </div>\n\n  </div>\n\n    <!-- Right side content -->\n   <div fxFlex=\"60\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n    <form  [formGroup]=\"signupForm\" class=\"signup4-form white\" (ngSubmit)=\"onSubmit()\">\n        \n      <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Username</mat-label>\n          <input\n            matInput\n            formControlName=\"username\"\n            type=\"text\"\n            name=\"username\"\n            placeholder=\"Username\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Email</mat-label>\n          <input\n            matInput\n            formControlName=\"email\"\n            type=\"email\"\n            name=\"email\"\n            placeholder=\"Email\"\n          />\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\" appearance=\"outline\">\n          <mat-label>Password</mat-label>\n          <input\n            matInput\n            formControlName=\"password\"\n            type=\"password\"\n            name=\"password\"\n            placeholder=\"********\"\n          />\n        </mat-form-field>\n\n        <mat-checkbox formControlName=\"agreed\"\n        >I agree with terms and condtions</mat-checkbox\n      >\n      \n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start center\" style=\"margin-top: 20px;\">\n        <button mat-flat-button color=\"primary\">Sign Up</button>\n        <span style=\"padding: 0px 8px 0px 16px;\">or</span>\n        <a class=\"font-weight-bold mat-color-primary\" routerLink=\"/sessions/signin4\">Sign In</a>\n      </div>\n\n      <!-- <div fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\" style=\"margin-top: 20px\">\n          <span>or connect with </span>\n          <div>\n            icons goes here\n          </div>\n        </div> -->\n\n    </form>\n   </div>\n  </div>\n</div>\n");

/***/ }),

/***/ "./src/app/views/sessions/error/error.component.css":
/*!**********************************************************!*\
  !*** ./src/app/views/sessions/error/error.component.css ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvZXJyb3IvZXJyb3IuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/views/sessions/error/error.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/sessions/error/error.component.ts ***!
  \*********************************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let ErrorComponent = class ErrorComponent {
    constructor() { }
    ngOnInit() {
    }
};
ErrorComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-error',
        template: __importDefault(__webpack_require__(/*! raw-loader!./error.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/error/error.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./error.component.css */ "./src/app/views/sessions/error/error.component.css")).default]
    }),
    __metadata("design:paramtypes", [])
], ErrorComponent);



/***/ }),

/***/ "./src/app/views/sessions/forgot-password/forgot-password.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/views/sessions/forgot-password/forgot-password.component.css ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/forgot-password/forgot-password.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/views/sessions/forgot-password/forgot-password.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let ForgotPasswordComponent = class ForgotPasswordComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
        this.showMessage = false;
    }
    submitEmail() {
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        console.log(this.userEmail);
        this.authService.ResetPassword(this.userEmail);
        this.userEmail = "  ";
        this.showMessage = true;
    }
};
ForgotPasswordComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"])
], ForgotPasswordComponent.prototype, "progressBar", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
], ForgotPasswordComponent.prototype, "submitButton", void 0);
ForgotPasswordComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-forgot-password',
        template: __importDefault(__webpack_require__(/*! raw-loader!./forgot-password.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/forgot-password/forgot-password.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./forgot-password.component.css */ "./src/app/views/sessions/forgot-password/forgot-password.component.css")).default]
    }),
    __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
], ForgotPasswordComponent);



/***/ }),

/***/ "./src/app/views/sessions/lockscreen/lockscreen.component.css":
/*!********************************************************************!*\
  !*** ./src/app/views/sessions/lockscreen/lockscreen.component.css ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvbG9ja3NjcmVlbi9sb2Nrc2NyZWVuLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/views/sessions/lockscreen/lockscreen.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/sessions/lockscreen/lockscreen.component.ts ***!
  \*******************************************************************/
/*! exports provided: LockscreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LockscreenComponent", function() { return LockscreenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


let LockscreenComponent = class LockscreenComponent {
    constructor() {
        this.lockscreenData = {
            password: ''
        };
    }
    ngOnInit() {
    }
    unlock() {
        console.log(this.lockscreenData);
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
    }
};
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"])
], LockscreenComponent.prototype, "progressBar", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
], LockscreenComponent.prototype, "submitButton", void 0);
LockscreenComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-lockscreen',
        template: __importDefault(__webpack_require__(/*! raw-loader!./lockscreen.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/lockscreen/lockscreen.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./lockscreen.component.css */ "./src/app/views/sessions/lockscreen/lockscreen.component.css")).default]
    }),
    __metadata("design:paramtypes", [])
], LockscreenComponent);



/***/ }),

/***/ "./src/app/views/sessions/not-found/not-found.component.css":
/*!******************************************************************!*\
  !*** ./src/app/views/sessions/not-found/not-found.component.css ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/not-found/not-found.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/sessions/not-found/not-found.component.ts ***!
  \*****************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let NotFoundComponent = class NotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
};
NotFoundComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-not-found',
        template: __importDefault(__webpack_require__(/*! raw-loader!./not-found.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/not-found/not-found.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./not-found.component.css */ "./src/app/views/sessions/not-found/not-found.component.css")).default]
    }),
    __metadata("design:paramtypes", [])
], NotFoundComponent);



/***/ }),

/***/ "./src/app/views/sessions/sessions.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/sessions/sessions.module.ts ***!
  \***************************************************/
/*! exports provided: SessionsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionsModule", function() { return SessionsModule; });
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/shared-material.module */ "./src/app/shared/shared-material.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/views/sessions/forgot-password/forgot-password.component.ts");
/* harmony import */ var _lockscreen_lockscreen_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lockscreen/lockscreen.component */ "./src/app/views/sessions/lockscreen/lockscreen.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/views/sessions/signin/signin.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/views/sessions/signup/signup.component.ts");
/* harmony import */ var _sessions_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sessions.routing */ "./src/app/views/sessions/sessions.routing.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/views/sessions/not-found/not-found.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./error/error.component */ "./src/app/views/sessions/error/error.component.ts");
/* harmony import */ var _signup2_signup2_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./signup2/signup2.component */ "./src/app/views/sessions/signup2/signup2.component.ts");
/* harmony import */ var _signup3_signup3_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./signup3/signup3.component */ "./src/app/views/sessions/signup3/signup3.component.ts");
/* harmony import */ var _signup4_signup4_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./signup4/signup4.component */ "./src/app/views/sessions/signup4/signup4.component.ts");
/* harmony import */ var _signin3_signin3_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./signin3/signin3.component */ "./src/app/views/sessions/signin3/signin3.component.ts");
/* harmony import */ var _signin4_signin4_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./signin4/signin4.component */ "./src/app/views/sessions/signin4/signin4.component.ts");
/* harmony import */ var _signin2_signin2_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./signin2/signin2.component */ "./src/app/views/sessions/signin2/signin2.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};







// import { CommonDirectivesModule } from './sdirectives/common/common-directives.module';













let SessionsModule = class SessionsModule {
};
SessionsModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            app_shared_shared_material_module__WEBPACK_IMPORTED_MODULE_5__["SharedMaterialModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
            ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__["PerfectScrollbarModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(_sessions_routing__WEBPACK_IMPORTED_MODULE_11__["SessionsRoutes"])
        ],
        declarations: [_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_7__["ForgotPasswordComponent"], _lockscreen_lockscreen_component__WEBPACK_IMPORTED_MODULE_8__["LockscreenComponent"], _signin_signin_component__WEBPACK_IMPORTED_MODULE_9__["SigninComponent"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_10__["SignupComponent"], _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_12__["NotFoundComponent"], _error_error_component__WEBPACK_IMPORTED_MODULE_13__["ErrorComponent"], _signup2_signup2_component__WEBPACK_IMPORTED_MODULE_14__["Signup2Component"], _signup3_signup3_component__WEBPACK_IMPORTED_MODULE_15__["Signup3Component"], _signup4_signup4_component__WEBPACK_IMPORTED_MODULE_16__["Signup4Component"], _signin3_signin3_component__WEBPACK_IMPORTED_MODULE_17__["Signin3Component"], _signin4_signin4_component__WEBPACK_IMPORTED_MODULE_18__["Signin4Component"], _signin2_signin2_component__WEBPACK_IMPORTED_MODULE_19__["Signin2Component"]]
    })
], SessionsModule);



/***/ }),

/***/ "./src/app/views/sessions/sessions.routing.ts":
/*!****************************************************!*\
  !*** ./src/app/views/sessions/sessions.routing.ts ***!
  \****************************************************/
/*! exports provided: SessionsRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionsRoutes", function() { return SessionsRoutes; });
/* harmony import */ var _signup4_signup4_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup4/signup4.component */ "./src/app/views/sessions/signup4/signup4.component.ts");
/* harmony import */ var _signup3_signup3_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup3/signup3.component */ "./src/app/views/sessions/signup3/signup3.component.ts");
/* harmony import */ var _signup2_signup2_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signup2/signup2.component */ "./src/app/views/sessions/signup2/signup2.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/views/sessions/forgot-password/forgot-password.component.ts");
/* harmony import */ var _lockscreen_lockscreen_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./lockscreen/lockscreen.component */ "./src/app/views/sessions/lockscreen/lockscreen.component.ts");
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signin/signin.component */ "./src/app/views/sessions/signin/signin.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/views/sessions/signup/signup.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/views/sessions/not-found/not-found.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./error/error.component */ "./src/app/views/sessions/error/error.component.ts");
/* harmony import */ var _signin3_signin3_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./signin3/signin3.component */ "./src/app/views/sessions/signin3/signin3.component.ts");
/* harmony import */ var _signin4_signin4_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./signin4/signin4.component */ "./src/app/views/sessions/signin4/signin4.component.ts");
/* harmony import */ var _signin2_signin2_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./signin2/signin2.component */ "./src/app/views/sessions/signin2/signin2.component.ts");
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};












const SessionsRoutes = [
    {
        path: "",
        children: [
            {
                path: "signup",
                component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_6__["SignupComponent"],
                data: { title: "Signup" }
            },
            {
                path: "signup2",
                component: _signup2_signup2_component__WEBPACK_IMPORTED_MODULE_2__["Signup2Component"],
                data: { title: "Signup2" }
            },
            {
                path: "signin2",
                component: _signin2_signin2_component__WEBPACK_IMPORTED_MODULE_11__["Signin2Component"],
                data: { title: "Signin2" }
            },
            {
                path: "signup3",
                component: _signup3_signup3_component__WEBPACK_IMPORTED_MODULE_1__["Signup3Component"],
                data: { title: "Signup3" }
            },
            {
                path: "signin3",
                component: _signin3_signin3_component__WEBPACK_IMPORTED_MODULE_9__["Signin3Component"],
                data: { title: "sign-in-3" }
            },
            {
                path: "signup4",
                component: _signup4_signup4_component__WEBPACK_IMPORTED_MODULE_0__["Signup4Component"],
                data: { title: "Signup4" }
            },
            {
                path: "signin4",
                component: _signin4_signin4_component__WEBPACK_IMPORTED_MODULE_10__["Signin4Component"],
                data: { title: "Signin4" }
            },
            {
                path: "signin",
                component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_5__["SigninComponent"],
                data: { title: "Signin" }
            },
            {
                path: "forgot-password",
                component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_3__["ForgotPasswordComponent"],
                data: { title: "Forgot password" }
            },
            {
                path: "lockscreen",
                component: _lockscreen_lockscreen_component__WEBPACK_IMPORTED_MODULE_4__["LockscreenComponent"],
                data: { title: "Lockscreen" }
            },
            {
                path: "404",
                component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_7__["NotFoundComponent"],
                data: { title: "Not Found" }
            },
            {
                path: "error",
                component: _error_error_component__WEBPACK_IMPORTED_MODULE_8__["ErrorComponent"],
                data: { title: "Error" }
            }
        ]
    }
];


/***/ }),

/***/ "./src/app/views/sessions/signin/signin.component.css":
/*!************************************************************!*\
  !*** ./src/app/views/sessions/signin/signin.component.css ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbmluL3NpZ25pbi5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signin/signin.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/views/sessions/signin/signin.component.ts ***!
  \***********************************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





let SigninComponent = class SigninComponent {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
        this.wrongPass = true;
    }
    ngOnInit() {
        let is_sign_in = localStorage.getItem('isSignedIn');
        if (is_sign_in == "true") {
            //   // window.location.href='http://localhost:4200/dashboard'
            this.router.navigate(['/dashboard']);
            //   window.location.reload()
        }
        // window.location.href = window.location.href
        this.signinForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            rememberMe: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](false)
        });
    }
    signin() {
        const signinData = this.signinForm.value;
        console.log(signinData);
        const email = signinData['username'];
        const password = signinData['password'];
        console.log(email);
        console.log(password);
        this.authService.SignIn(email, password);
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        this.wrongPass = this.authService.wrong_pass;
        console.log("This is wrong pass ", this.wrongPass);
        console.log(localStorage);
        // window.location.reload()
    }
};
SigninComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"])
], SigninComponent.prototype, "progressBar", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
], SigninComponent.prototype, "submitButton", void 0);
SigninComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signin',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signin.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin/signin.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./signin.component.css */ "./src/app/views/sessions/signin/signin.component.css")).default]
    }),
    __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], SigninComponent);



/***/ }),

/***/ "./src/app/views/sessions/signin2/signin2.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signin2/signin2.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbmluMi9zaWduaW4yLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signin2/signin2.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signin2/signin2.component.ts ***!
  \*************************************************************/
/*! exports provided: Signin2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin2Component", function() { return Signin2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let Signin2Component = class Signin2Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        const password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
        const confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].equalTo(password));
        this.signupForm = this.fb.group({
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: password,
            agreed: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    }
    onSubmit() {
        if (!this.signupForm.invalid) {
            // do what you wnat with your data
            console.log(this.signupForm.value);
        }
    }
};
Signin2Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
];
Signin2Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signin2',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signin2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin2/signin2.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./signin2.component.scss */ "./src/app/views/sessions/signin2/signin2.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
], Signin2Component);



/***/ }),

/***/ "./src/app/views/sessions/signin3/signin3.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signin3/signin3.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbmluMy9zaWduaW4zLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signin3/signin3.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signin3/signin3.component.ts ***!
  \*************************************************************/
/*! exports provided: Signin3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin3Component", function() { return Signin3Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let Signin3Component = class Signin3Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.signupForm = this.fb.group({
            username: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    }
    onSubmit() {
        if (this.signupForm.valid) {
            // do what you want to do with your data
            console.log(this.signupForm.value);
        }
    }
};
Signin3Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
];
Signin3Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signin3',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signin3.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin3/signin3.component.html")).default,
        animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_2__["egretAnimations"],
        styles: [__importDefault(__webpack_require__(/*! ./signin3.component.scss */ "./src/app/views/sessions/signin3/signin3.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
], Signin3Component);



/***/ }),

/***/ "./src/app/views/sessions/signin4/signin4.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signin4/signin4.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbmluNC9zaWduaW40LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signin4/signin4.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signin4/signin4.component.ts ***!
  \*************************************************************/
/*! exports provided: Signin4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin4Component", function() { return Signin4Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let Signin4Component = class Signin4Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        const password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
        const confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].equalTo(password));
        this.signupForm = this.fb.group({
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: password,
            agreed: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    }
    onSubmit() {
        if (!this.signupForm.invalid) {
            // do what you wnat with your data
            console.log(this.signupForm.value);
        }
    }
};
Signin4Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
];
Signin4Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signin4',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signin4.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signin4/signin4.component.html")).default,
        animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__["egretAnimations"],
        styles: [__importDefault(__webpack_require__(/*! ./signin4.component.scss */ "./src/app/views/sessions/signin4/signin4.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
], Signin4Component);



/***/ }),

/***/ "./src/app/views/sessions/signup/signup.component.css":
/*!************************************************************!*\
  !*** ./src/app/views/sessions/signup/signup.component.css ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signup/signup.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/views/sessions/signup/signup.component.ts ***!
  \***********************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../auth.service */ "./src/app/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





let SignupComponent = class SignupComponent {
    constructor(authService) {
        this.authService = authService;
    }
    ngOnInit() {
        this.showMessage = false;
        const password = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required);
        const confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_3__["CustomValidators"].equalTo(password));
        this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,]),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
            password: password,
            confirmPassword: confirmPassword,
            agreed: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', (control) => {
                const agreed = control.value;
                if (!agreed) {
                    return { agreed: true };
                }
                return null;
            })
        });
    }
    signup() {
        const signupData = this.signupForm.value;
        console.log(signupData);
        console.log(signupData['email']);
        console.log(signupData['password']);
        console.log(signupData['firstName']);
        console.log(signupData['lastName']);
        let body = {
            "user": {
                "first_name": signupData['firstName'],
                "last_name": signupData['lastName'],
                "email": signupData['email']
            }
        };
        let cuser_id = this.authService.CuserSignUp(signupData['email'], signupData['password'], body);
        this.showMessage = true;
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
    }
};
SignupComponent.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBar"])
], SignupComponent.prototype, "progressBar", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"], { static: false }),
    __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButton"])
], SignupComponent.prototype, "submitButton", void 0);
SignupComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signup',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signup.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup/signup.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./signup.component.css */ "./src/app/views/sessions/signup/signup.component.css")).default]
    }),
    __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
], SignupComponent);



/***/ }),

/***/ "./src/app/views/sessions/signup2/signup2.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signup2/signup2.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbnVwMi9zaWdudXAyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signup2/signup2.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signup2/signup2.component.ts ***!
  \*************************************************************/
/*! exports provided: Signup2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signup2Component", function() { return Signup2Component; });
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let Signup2Component = class Signup2Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        const password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
        const confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_0__["CustomValidators"].equalTo(password));
        this.signupForm = this.fb.group({
            firstName: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastName: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            username: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: password,
            confirmPassword: confirmPassword,
            agreed: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    }
    onSubmit() {
        if (!this.signupForm.invalid) {
            // do what you wnat with your data
            console.log(this.signupForm.value);
        }
    }
};
Signup2Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
];
Signup2Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-signup2",
        template: __importDefault(__webpack_require__(/*! raw-loader!./signup2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup2/signup2.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./signup2.component.scss */ "./src/app/views/sessions/signup2/signup2.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
], Signup2Component);



/***/ }),

/***/ "./src/app/views/sessions/signup3/signup3.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signup3/signup3.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbnVwMy9zaWdudXAzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signup3/signup3.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signup3/signup3.component.ts ***!
  \*************************************************************/
/*! exports provided: Signup3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signup3Component", function() { return Signup3Component; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let Signup3Component = class Signup3Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.signupForm = this.fb.group({
            username: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].email]],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]
        });
    }
    onSubmit() {
        if (this.signupForm.valid) {
            // do what you want to do with your data
            console.log(this.signupForm.value);
        }
    }
};
Signup3Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"] }
];
Signup3Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-signup3",
        template: __importDefault(__webpack_require__(/*! raw-loader!./signup3.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup3/signup3.component.html")).default,
        animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_2__["egretAnimations"],
        styles: [__importDefault(__webpack_require__(/*! ./signup3.component.scss */ "./src/app/views/sessions/signup3/signup3.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]])
], Signup3Component);



/***/ }),

/***/ "./src/app/views/sessions/signup4/signup4.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/views/sessions/signup4/signup4.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2Vzc2lvbnMvc2lnbnVwNC9zaWdudXA0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/views/sessions/signup4/signup4.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/sessions/signup4/signup4.component.ts ***!
  \*************************************************************/
/*! exports provided: Signup4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signup4Component", function() { return Signup4Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let Signup4Component = class Signup4Component {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        const password = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
        const confirmPassword = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', ng2_validation__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].equalTo(password));
        this.signupForm = this.fb.group({
            username: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: password,
            agreed: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    }
    onSubmit() {
        if (!this.signupForm.invalid) {
            // do what you wnat with your data
            console.log(this.signupForm.value);
        }
    }
};
Signup4Component.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }
];
Signup4Component = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-signup4',
        template: __importDefault(__webpack_require__(/*! raw-loader!./signup4.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/sessions/signup4/signup4.component.html")).default,
        animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__["egretAnimations"],
        styles: [__importDefault(__webpack_require__(/*! ./signup4.component.scss */ "./src/app/views/sessions/signup4/signup4.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
], Signup4Component);



/***/ })

}]);
//# sourceMappingURL=views-sessions-sessions-module-es2015.js.map