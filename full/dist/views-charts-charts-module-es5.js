function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-charts-charts-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/charts/charts.component.html":
  /*!******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/charts/charts.component.html ***!
    \******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsChartsChartsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div fxLayout=\"row wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Vertical Bar chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas  \n          baseChart \n          class=\"chart\"\n          [datasets]=\"barChartData\"\n          [labels]=\"barChartLabels\"\n          [options]=\"barChartOptions\"\n          [colors]=\"chartColors\"\n          [legend]=\"barChartLegend\"\n          [chartType]=\"barChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Horizontal Bar chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas  \n          baseChart \n          class=\"chart\"\n          [datasets]=\"barChartData\"\n          [labels]=\"barChartLabels\"\n          [options]=\"barChartHorizontalOptions\"\n          [colors]=\"chartColors\"\n          [legend]=\"barChartLegend\"\n          [chartType]=\"barChartHorizontalType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Stacked Bar chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas  \n          baseChart class=\"chart\"\n          [datasets]=\"barChartData\"\n          [labels]=\"barChartLabels\"\n          [options]=\"barChartStackedOptions\"\n          [colors]=\"chartColors\"\n          [legend]=\"barChartLegend\"\n          [chartType]=\"barChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Basic Line chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas \n          baseChart \n          class=\"chart\"\n          [datasets]=\"lineChartData\"\n          [labels]=\"lineChartLabels\"\n          [options]=\"lineChartOptions\"\n          [colors]=\"chartColors\"\n          [legend]=\"lineChartLegend\"\n          [chartType]=\"lineChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Point Line chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas \n          baseChart \n          class=\"chart\"\n          [datasets]=\"lineChartPointsData\"\n          [labels]=\"lineChartLabels\"\n          [options]=\"lineChartPointsOptions\"\n          [colors]=\"chartColors\"\n          [legend]=\"lineChartLegend\"\n          [chartType]=\"lineChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Bubble chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas \n          baseChart \n          class=\"chart\"\n          [datasets]=\"bubbleChartData\"\n          [labels]=\"bubbleChartLabels\"\n          [options]=\"bubbleChartOptions\"\n          [legend]=\"bubbleChartLegend\"\n          [chartType]=\"bubbleChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  \n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Doughnut chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas \n          baseChart \n          class=\"chart\"\n          [data]=\"doughnutChartData\"\n          [labels]=\"doughnutChartLabels\"\n          [options]=\"doughnutOptions\"\n          [colors]=\"doughnutChartColors\"\n          [chartType]=\"doughnutChartType\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Pie chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas \n        class=\"chart\"\n        baseChart\n        [data]=\"pieChartData\"\n        [labels]=\"pieChartLabels\"\n        [options]=\"doughnutOptions\"\n        [colors]=\"doughnutChartColors\"\n        [chartType]=\"pieChartType\"\n        (chartHover)=\"pieChartHovered($event)\"\n        (chartClick)=\"pieChartClicked($event)\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"33\">\n    <mat-card class=\"p-0\">\n      <mat-card-title class=\"\">\n        <div class=\"card-title-text\">Radar chart</div>\n        <mat-divider></mat-divider>\n      </mat-card-title>\n      <mat-card-content>\n        <canvas\n        class=\"chart\" \n        baseChart\n        [datasets]=\"radarChartData\"\n        [labels]=\"radarChartLabels\"\n        [legend]=\"false\"\n        [colors]=\"chartColors\"\n        [chartType]=\"radarChartType\"\n        (chartHover)=\"radarChartHovered($event)\"\n        (chartClick)=\"radarChartClicked($event)\"></canvas>\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./src/app/views/charts/charts.component.css":
  /*!***************************************************!*\
    !*** ./src/app/views/charts/charts.component.css ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsChartsChartsComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvY2hhcnRzL2NoYXJ0cy5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/charts/charts.component.ts":
  /*!**************************************************!*\
    !*** ./src/app/views/charts/charts.component.ts ***!
    \**************************************************/

  /*! exports provided: ChartsComponent */

  /***/
  function srcAppViewsChartsChartsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChartsComponent", function () {
      return ChartsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ChartsComponent =
    /*#__PURE__*/
    function () {
      function ChartsComponent() {
        _classCallCheck(this, ChartsComponent);

        this.sharedChartOptions = {
          responsive: true,
          // maintainAspectRatio: false,
          legend: {
            display: false,
            position: 'bottom'
          }
        };
        this.chartColors = [{
          backgroundColor: '#3f51b5',
          borderColor: '#3f51b5',
          pointBackgroundColor: '#3f51b5',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }, {
          backgroundColor: '#eeeeee',
          borderColor: '#e0e0e0',
          pointBackgroundColor: '#e0e0e0',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(77,83,96,1)'
        }, {
          backgroundColor: 'rgba(148,159,177,0.2)',
          borderColor: 'rgba(148,159,177,1)',
          pointBackgroundColor: 'rgba(148,159,177,1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }];
        /*
        * Bar Chart
        */

        this.barChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [{
          data: [5, 6, 7, 8, 4, 5, 5],
          label: 'Series A',
          borderWidth: 0
        }, {
          data: [5, 4, 4, 3, 6, 2, 5],
          label: 'Series B',
          borderWidth: 0
        }];
        this.barChartOptions = Object.assign({
          scaleShowVerticalLines: false,
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              position: 'left',
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }]
          }
        }, this.sharedChartOptions); // Horizontal Bar Chart

        this.barChartHorizontalType = 'horizontalBar';
        this.barChartHorizontalOptions = Object.assign({
          scaleShowVerticalLines: false,
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }]
          }
        }, this.sharedChartOptions); // Bar Chart Stacked

        this.barChartStackedOptions = Object.assign({
          scaleShowVerticalLines: false,
          tooltips: {
            mode: 'index',
            intersect: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              stacked: true,
              ticks: {
                beginAtZero: true
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              stacked: true
            }]
          }
        }, this.sharedChartOptions);
        /*
        * Line Chart Options
        */

        this.lineChartData = [{
          data: [5, 5, 7, 8, 4, 5, 5],
          label: 'Series A',
          borderWidth: 1
        }, {
          data: [5, 4, 4, 3, 6, 2, 5],
          label: 'Series B',
          borderWidth: 1
        }];
        this.lineChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
        this.lineChartOptions = Object.assign({
          animation: false,
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }]
          }
        }, this.sharedChartOptions);
        this.lineChartLegend = false;
        this.lineChartType = 'line';
        this.lineChartPointsData = [{
          data: [6, 5, 8, 8, 5, 5, 4],
          label: 'Series A',
          borderWidth: 1,
          fill: false,
          pointRadius: 10,
          pointHoverRadius: 15,
          showLine: false
        }, {
          data: [5, 4, 4, 2, 6, 2, 5],
          label: 'Series B',
          borderWidth: 1,
          fill: false,
          pointRadius: 10,
          pointHoverRadius: 15,
          showLine: false
        }];
        this.lineChartPointsOptions = Object.assign({
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }]
          },
          elements: {
            point: {
              pointStyle: 'rectRot'
            }
          }
        }, this.sharedChartOptions); // Bubble Chart

        this.bubbleChartData = [{
          data: [{
            x: 4,
            y: 4,
            r: 15
          }, {
            x: 6,
            y: 12,
            r: 30
          }, {
            x: 5,
            y: 4,
            r: 10
          }, {
            x: 8,
            y: 4,
            r: 6
          }, {
            x: 7,
            y: 8,
            r: 4
          }, {
            x: 3,
            y: 13,
            r: 14
          }, {
            x: 5,
            y: 6,
            r: 8
          }, {
            x: 7,
            y: 2,
            r: 10
          }],
          label: 'Series A',
          borderWidth: 1
        }];
        this.bubbleChartType = 'bubble';
        this.bubbleChartLabels = ['1', '2', '3', '4', '5', '6', '7'];
        this.bubbleChartLegend = true;
        this.bubbleChartOptions = Object.assign({
          animation: false,
          scales: {
            xAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              }
            }],
            yAxes: [{
              gridLines: {
                color: 'rgba(0,0,0,0.02)',
                zeroLineColor: 'rgba(0,0,0,0.02)'
              },
              ticks: {
                beginAtZero: true,
                suggestedMax: 9
              }
            }]
          }
        }, this.sharedChartOptions); // Doughnut

        this.doughnutChartColors = [{
          backgroundColor: ['#f44336', '#3f51b5', '#ffeb3b', '#4caf50', '#2196f']
        }];
        this.doughnutChartLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
        this.doughnutChartData = [350, 450, 100];
        this.doughnutChartType = 'doughnut';
        this.doughnutOptions = Object.assign({
          elements: {
            arc: {
              borderWidth: 0
            }
          }
        }, this.sharedChartOptions);
        /*
        * Radar Chart Options
        */

        this.radarChartLabels = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];
        this.radarChartData = [{
          data: [65, 59, 90, 81, 56, 55, 40],
          label: 'Series A',
          borderWidth: 1
        }, {
          data: [28, 48, 40, 19, 96, 27, 100],
          label: 'Series B',
          borderWidth: 1
        }];
        this.radarChartType = 'radar';
        this.radarChartColors = [{
          backgroundColor: 'rgba(36, 123, 160, 0.2)',
          borderColor: 'rgba(36, 123, 160, 0.6)',
          pointBackgroundColor: 'rgba(36, 123, 160, 0.8)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(36, 123, 160, 0.8)'
        }, {
          backgroundColor: 'rgba(244, 67, 54, 0.2)',
          borderColor: 'rgba(244, 67, 54, .8)',
          pointBackgroundColor: 'rgba(244, 67, 54, .8)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(244, 67, 54, 1)'
        }];
        /*
        * Pie Chart Options
        */

        this.pieChartLabels = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
        this.pieChartData = [300, 500, 100];
        this.pieChartType = 'pie';
        this.pieChartColors = [{
          backgroundColor: ['rgba(255, 217, 125, 0.8)', 'rgba(36, 123, 160, 0.8)', 'rgba(244, 67, 54, 0.8)']
        }];
      }

      _createClass(ChartsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
        /*
        * Bar Chart Event Handler
        */

      }, {
        key: "barChartClicked",
        value: function barChartClicked(e) {}
      }, {
        key: "barChartHovered",
        value: function barChartHovered(e) {}
        /*
        * Line Chart Event Handler
        */

      }, {
        key: "lineChartClicked",
        value: function lineChartClicked(e) {}
      }, {
        key: "lineChartHovered",
        value: function lineChartHovered(e) {}
        /*
        * Doughnut Chart Event Handler
        */

      }, {
        key: "doughnutChartClicked",
        value: function doughnutChartClicked(e) {}
      }, {
        key: "doughnutChartHovered",
        value: function doughnutChartHovered(e) {}
        /*
        * Rader Chart Event Handler
        */

      }, {
        key: "radarChartClicked",
        value: function radarChartClicked(e) {}
      }, {
        key: "radarChartHovered",
        value: function radarChartHovered(e) {}
        /*
        * Pie Chart Event Handler
        */

      }, {
        key: "pieChartClicked",
        value: function pieChartClicked(e) {}
      }, {
        key: "pieChartHovered",
        value: function pieChartHovered(e) {}
      }]);

      return ChartsComponent;
    }();

    ChartsComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-charts',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./charts.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/charts/charts.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./charts.component.css */
      "./src/app/views/charts/charts.component.css")).default]
    }), __metadata("design:paramtypes", [])], ChartsComponent);
    /***/
  },

  /***/
  "./src/app/views/charts/charts.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/views/charts/charts.module.ts ***!
    \***********************************************/

  /*! exports provided: AppChartsModule */

  /***/
  function srcAppViewsChartsChartsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppChartsModule", function () {
      return AppChartsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ng2-charts */
    "./node_modules/ng2-charts/fesm2015/ng2-charts.js");
    /* harmony import */


    var _charts_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./charts.component */
    "./src/app/views/charts/charts.component.ts");
    /* harmony import */


    var _charts_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./charts.routing */
    "./src/app/views/charts/charts.routing.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppChartsModule = function AppChartsModule() {
      _classCallCheck(this, AppChartsModule);
    };

    AppChartsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ng2_charts__WEBPACK_IMPORTED_MODULE_5__["ChartsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_charts_routing__WEBPACK_IMPORTED_MODULE_7__["ChartsRoutes"])],
      declarations: [_charts_component__WEBPACK_IMPORTED_MODULE_6__["ChartsComponent"]]
    })], AppChartsModule);
    /***/
  },

  /***/
  "./src/app/views/charts/charts.routing.ts":
  /*!************************************************!*\
    !*** ./src/app/views/charts/charts.routing.ts ***!
    \************************************************/

  /*! exports provided: ChartsRoutes */

  /***/
  function srcAppViewsChartsChartsRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChartsRoutes", function () {
      return ChartsRoutes;
    });
    /* harmony import */


    var _charts_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./charts.component */
    "./src/app/views/charts/charts.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ChartsRoutes = [{
      path: '',
      component: _charts_component__WEBPACK_IMPORTED_MODULE_0__["ChartsComponent"],
      data: {
        title: 'Charts'
      }
    }];
    /***/
  }
}]);
//# sourceMappingURL=views-charts-charts-module-es5.js.map