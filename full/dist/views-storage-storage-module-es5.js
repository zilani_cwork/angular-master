function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-storage-storage-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/storage/storage.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/storage/storage.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsStorageStorageComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card>\n  <h3>Finalized Content</h3>\n  <ul>\n    <div *ngFor=\"let item of files\">\n\n      <mat-card *ngIf=\"item.image!=null\"><li>\n        <a href={{item.image}}> <img src={{item.image}} alt=\"No image here\"> </a>\n      </li>\n    </mat-card>\n    <mat-card style=\"width:200px;\" *ngIf=\"item.image==null\" (click)=\"card_data=item.caption\"  data-toggle=\"modal\" data-target=\"#exampleModal\"><li>\n        <a > {{fixString(item.caption)}} </a>\n      </li>\n    </mat-card>\n    </div>\n\n    \n  </ul>\n\n\n\n\n  <!-- modal -->\n\n\n\n  <!-- Modal -->\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\" data-backdrop=\"false\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Finalized Post</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          {{card_data}}\n        </div>\n        <div class=\"modal-footer\">\n          <button mat-raised-button type=\"button\" data-dismiss=\"modal\">Close</button>\n          \n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n  <!-- modal -->\n</mat-card>";
    /***/
  },

  /***/
  "./src/app/views/storage/storage.component.scss":
  /*!******************************************************!*\
    !*** ./src/app/views/storage/storage.component.scss ***!
    \******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsStorageStorageComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ul {\n  list-style: none;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: initial;\n          justify-content: initial;\n  flex-wrap: wrap; }\n\nul img {\n  width: 200px; }\n\nli {\n  margin: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3Mvc3RvcmFnZS9zdG9yYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQWdCO0VBQ2hCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUF3QjtVQUF4Qix3QkFBd0I7RUFDeEIsZUFBZSxFQUFBOztBQUduQjtFQUNJLFlBQVksRUFBQTs7QUFLaEI7RUFDSSxXQUFXLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL3N0b3JhZ2Uvc3RvcmFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInVse1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGluaXRpYWw7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbnVsIGltZ3tcclxuICAgIHdpZHRoOiAyMDBweDtcclxuICAgIFxyXG5cclxufVxyXG5cclxubGl7XHJcbiAgICBtYXJnaW46IDVweDtcclxufVxyXG5cclxuLy8gdWwgaW1nOmhvdmVye1xyXG4vLyAgICAgd2lkdGg6IDI1MHB4O1xyXG4vLyB9Il19 */";
    /***/
  },

  /***/
  "./src/app/views/storage/storage.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/views/storage/storage.component.ts ***!
    \****************************************************/

  /*! exports provided: StorageComponent */

  /***/
  function srcAppViewsStorageStorageComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StorageComponent", function () {
      return StorageComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var StorageComponent =
    /*#__PURE__*/
    function () {
      function StorageComponent(client) {
        _classCallCheck(this, StorageComponent);

        this.client = client;
        this.card_data = "";
      }

      _createClass(StorageComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])(localStorage.getItem('Token'));
          this.getFiles();
        }
      }, {
        key: "getFiles",
        value: function getFiles() {
          var _this = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/version/finalizedList', this.header).subscribe(function (res) {
            console.log(res);
            _this.files = res;
          });
        }
      }, {
        key: "fix",
        value: function fix(url_name) {
          return url_name.slice(45, url_name.length);
        }
      }, {
        key: "fixString",
        value: function fixString(s) {
          if (s.length < 40) {
            return s;
          } else {
            return s.substring(0, 40) + "....";
          }
        }
      }]);

      return StorageComponent;
    }();

    StorageComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }];
    };

    StorageComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-storage',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./storage.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/storage/storage.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./storage.component.scss */
      "./src/app/views/storage/storage.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])], StorageComponent);
    /***/
  },

  /***/
  "./src/app/views/storage/storage.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/views/storage/storage.module.ts ***!
    \*************************************************/

  /*! exports provided: StorageModule */

  /***/
  function srcAppViewsStorageStorageModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StorageModule", function () {
      return StorageModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var angular_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-calendar */
    "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-calendar/date-adapters/date-fns */
    "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-color-picker */
    "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js");
    /* harmony import */


    var _storage_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./storage.component */
    "./src/app/views/storage/storage.component.ts");
    /* harmony import */


    var _storage_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./storage.routing */
    "./src/app/views/storage/storage.routing.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var StorageModule = function StorageModule() {
      _classCallCheck(this, StorageModule);
    };

    StorageModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__["ColorPickerModule"], angular_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"].forRoot({
        provide: angular_calendar__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"],
        useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__["adapterFactory"]
      }), _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_storage_routing__WEBPACK_IMPORTED_MODULE_11__["StorageRoutes"])],
      providers: [],
      entryComponents: [],
      declarations: [_storage_component__WEBPACK_IMPORTED_MODULE_10__["StorageComponent"]]
    })], StorageModule);
    /***/
  },

  /***/
  "./src/app/views/storage/storage.routing.ts":
  /*!**************************************************!*\
    !*** ./src/app/views/storage/storage.routing.ts ***!
    \**************************************************/

  /*! exports provided: StorageRoutes */

  /***/
  function srcAppViewsStorageStorageRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StorageRoutes", function () {
      return StorageRoutes;
    });
    /* harmony import */


    var _storage_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./storage.component */
    "./src/app/views/storage/storage.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var StorageRoutes = [{
      path: '',
      component: _storage_component__WEBPACK_IMPORTED_MODULE_0__["StorageComponent"],
      data: {
        title: 'Storage'
      }
    }];
    /***/
  }
}]);
//# sourceMappingURL=views-storage-storage-module-es5.js.map