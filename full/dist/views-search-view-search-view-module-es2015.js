(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-search-view-search-view-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/search-view/result-page/result-page.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/search-view/result-page/result-page.component.html ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"m-333\">\n    <h6 class=\"mt-0\">Search result for \"{{ searchService.searchTerm$ | async }}\"</h6>\n</div>\n\n<div class=\"mat-box-shadow margin-333\">\n  <ngx-datatable\n    class=\"material bg-white\"\n    [columnMode]=\"'force'\"\n    [headerHeight]=\"50\"\n    [footerHeight]=\"50\"\n    [rowHeight]=\"50\"\n    [limit]=\"8\"\n    [rows]=\"countries$ | async\"\n    [scrollbarH]=\"true\"\n  >\n    <ngx-datatable-column name=\"flag\" [maxWidth]=\"160\">\n      <ng-template ngx-datatable-header-template>\n        Flag\n      </ng-template>\n      <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n        <img\n          height=\"32px\"\n          width=\"auto\"\n          style=\"border-radius: 4px\"\n          [src]=\"value\"\n          alt=\"\"\n        />\n      </ng-template>\n    </ngx-datatable-column>\n\n    <ngx-datatable-column name=\"name\" [maxWidth]=\"250\">\n      <ng-template ngx-datatable-header-template>\n        Name\n      </ng-template>\n      <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n        {{ value }}\n      </ng-template>\n    </ngx-datatable-column>\n\n    <ngx-datatable-column name=\"region\" [maxWidth]=\"250\">\n      <ng-template ngx-datatable-header-template>\n        Region\n      </ng-template>\n      <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n        {{ value }}\n      </ng-template>\n    </ngx-datatable-column>\n\n    <ngx-datatable-column name=\"capital\" [maxWidth]=\"250\">\n      <ng-template ngx-datatable-header-template>\n        Capital\n      </ng-template>\n      <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n        {{ value }}\n      </ng-template>\n    </ngx-datatable-column>\n\n    <ngx-datatable-column name=\"population\" [maxWidth]=\"250\">\n      <ng-template ngx-datatable-header-template>\n        Population\n      </ng-template>\n      <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n        {{ value }}\n      </ng-template>\n    </ngx-datatable-column>\n  </ngx-datatable>\n</div>\n");

/***/ }),

/***/ "./src/app/views/search-view/country.service.ts":
/*!******************************************************!*\
  !*** ./src/app/views/search-view/country.service.ts ***!
  \******************************************************/
/*! exports provided: CountryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryService", function() { return CountryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


let CountryService = class CountryService {
    constructor(http) {
        this.http = http;
    }
    getCountries(name) {
        return this.http.get('https://restcountries.eu/rest/v2/name/' + name);
    }
};
CountryService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
CountryService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], CountryService);



/***/ }),

/***/ "./src/app/views/search-view/result-page/result-page.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/search-view/result-page/result-page.component.scss ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3Mvc2VhcmNoLXZpZXcvcmVzdWx0LXBhZ2UvcmVzdWx0LXBhZ2UuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/views/search-view/result-page/result-page.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/search-view/result-page/result-page.component.ts ***!
  \************************************************************************/
/*! exports provided: ResultPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultPageComponent", function() { return ResultPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var app_shared_search_search_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/search/search.service */ "./src/app/shared/search/search.service.ts");
/* harmony import */ var _country_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../country.service */ "./src/app/views/search-view/country.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let ResultPageComponent = class ResultPageComponent {
    constructor(searchService, countryService) {
        this.searchService = searchService;
        this.countryService = countryService;
    }
    ngOnInit() {
        this.searchTermSub = this.searchService.searchTerm$.subscribe(term => {
            this.countries$ = this.countryService.getCountries(term);
        });
    }
    ngOnDestroy() {
        if (this.searchTermSub) {
            this.searchTermSub.unsubscribe();
        }
    }
};
ResultPageComponent.ctorParameters = () => [
    { type: app_shared_search_search_service__WEBPACK_IMPORTED_MODULE_1__["SearchService"] },
    { type: _country_service__WEBPACK_IMPORTED_MODULE_2__["CountryService"] }
];
ResultPageComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: "app-result-page",
        template: __importDefault(__webpack_require__(/*! raw-loader!./result-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/search-view/result-page/result-page.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./result-page.component.scss */ "./src/app/views/search-view/result-page/result-page.component.scss")).default]
    }),
    __metadata("design:paramtypes", [app_shared_search_search_service__WEBPACK_IMPORTED_MODULE_1__["SearchService"],
        _country_service__WEBPACK_IMPORTED_MODULE_2__["CountryService"]])
], ResultPageComponent);



/***/ }),

/***/ "./src/app/views/search-view/search-view-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/search-view/search-view-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: SearchViewRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchViewRoutingModule", function() { return SearchViewRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _result_page_result_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./result-page/result-page.component */ "./src/app/views/search-view/result-page/result-page.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



const routes = [
    {
        path: '',
        component: _result_page_result_page_component__WEBPACK_IMPORTED_MODULE_2__["ResultPageComponent"]
    }
];
let SearchViewRoutingModule = class SearchViewRoutingModule {
};
SearchViewRoutingModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    })
], SearchViewRoutingModule);



/***/ }),

/***/ "./src/app/views/search-view/search-view.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/search-view/search-view.module.ts ***!
  \*********************************************************/
/*! exports provided: SearchViewModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchViewModule", function() { return SearchViewModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _search_view_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-view-routing.module */ "./src/app/views/search-view/search-view-routing.module.ts");
/* harmony import */ var _result_page_result_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./result-page/result-page.component */ "./src/app/views/search-view/result-page/result-page.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};






let SearchViewModule = class SearchViewModule {
};
SearchViewModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        declarations: [_result_page_result_page_component__WEBPACK_IMPORTED_MODULE_3__["ResultPageComponent"]],
        imports: [_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__["NgxDatatableModule"], _search_view_routing_module__WEBPACK_IMPORTED_MODULE_2__["SearchViewRoutingModule"]]
    })
], SearchViewModule);



/***/ })

}]);
//# sourceMappingURL=views-search-view-search-view-module-es2015.js.map