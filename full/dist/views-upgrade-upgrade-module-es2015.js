(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-upgrade-upgrade-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/upgrade/upgrade.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/upgrade/upgrade.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container\">\n\n\n\n<br><br>\n  <div class=\"custom-recharge\">\n    <h4>Custom Recharge:</h4>\n    <div class=\"input-group mb-3\">\n      <div class=\"input-group-prepend\">\n        <span class=\"input-group-text\">৳</span>\n      </div>\n      <input type=\"text\" class=\"form-control\"  (input)=\"custom=$event.target.value\"  aria-label=\"Amount (to the nearest taka)\">\n      <div class=\"input-group-append\">\n        <span class=\"input-group-text\">.00</span>\n      </div>\n    </div>\n\n    <button mat-raised-button type=\"button\" class=\"btn btn-dark btn-sm\" data-toggle=\"modal\" data-target=\"#customModal\">Recharge</button>\n    \n  </div>\n\n\n  <br>\n  <h4>Packages:</h4>\n  <div class=\"mycon\">\n    \n    <mat-card class=\"box\" >\n      <br>\n      <br>\n      <h2>SME</h2>\n      <br>\n      <br>\n      <div class=\"taka\">৳15,000</div>\n      <p>PER USER, MONTH</p>\n      <div class=\"pro\">\n        <p>Professional features for your business to grow</p>\n      </div> <br>\n      <button mat-raised-button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#firstModal\">\n        Buy Package\n      </button>\n    </mat-card>\n\n\n    <mat-card class=\"box\">\n      <br>\n      <br>\n      <h2>STARTUP</h2>\n      <br>\n      <br>\n      <div class=\"taka\">৳7500</div>\n      <p>PER USER, MONTH</p>\n      <div class=\"pro\">\n        <p>Professional features for your business to grow</p>\n      </div> <br>\n      <button mat-raised-button type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#secondModal\">\n        Buy Package\n      </button>\n    </mat-card>\n\n    <mat-card class=\"box\">\n      <br>\n      <br>\n      <h2>SME</h2>\n      <br>\n      <br>\n      <div class=\"taka\">৳45,000</div>\n      <p>PER USER, MONTH</p>\n      <div class=\"pro\">\n        <p>Professional features for your business to grow</p>\n      </div>\n      <br>\n      <button mat-raised-button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#thirdModal\">\n        Buy Package\n      </button>\n    </mat-card>\n\n\n\n\n\n  </div>\n  <!-- Button trigger modal -->\n\n\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"firstModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"firstModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"firstModalLabel\">৳ 15000 Package</h5>\n          <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"form-group\">\n\n            <input matInput type=\"text\" class=\"form-control\"  (input)=\"phone=$event.target.value\" placeholder=\"Enter your phone number\" required>\n            <br>\n            <input matInput type=\"text\" class=\"form-control\" (input)=\"street=$event.target.value\" placeholder=\"Enter your street\" required>\n            <br>\n            <input matInput type=\"text\" class=\"form-control\" (input)=\"city=$event.target.value\" placeholder=\"Enter your city\" required>\n            <br>\n            <input matInput type=\"text\" class=\"form-control\" (input)=\"state=$event.target.value\" placeholder=\"Enter your state\" required>\n            <br>\n            <input matInput type=\"text\" class=\"form-control\" (input)=\"zipcode=$event.target.value\" placeholder=\"Enter your zipcode\" required>\n\n\n\n\n\n\n\n\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button mat-raised-button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button mat-raised-button type=\"button\" class=\"btn btn-primary\"\n            (click)=\"getInvoice(15000, 'Tk 15000', 'Add Tk 15000 to balance')\" data-dismiss=\"modal\">Submit</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"secondModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"secondModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"secondModalLabel\">৳ 7500 Package</h5>\n          <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n\n\n          <div class=\"form-group\">\n\n            <input type=\"text\" class=\"form-control\" (input)=\"phone=$event.target.value\" placeholder=\"Enter your phone number\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"street=$event.target.value\" placeholder=\"Enter your street\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"city=$event.target.value\" placeholder=\"Enter your city\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"state=$event.target.value\" placeholder=\"Enter your state\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"zipcode=$event.target.value\" placeholder=\"Enter your zipcode\" required>\n\n\n\n\n\n\n\n\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button mat-raised-button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button mat-raised-button type=\"button\" class=\"btn btn-primary\" (click)=\"getInvoice(7500, 'Tk 7500', 'Add Tk 7500 to balance')\"\n            data-dismiss=\"modal\">Submit</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n\n\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"thirdModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"thirdModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"thirdModalLabel\">৳ 45000 Package</h5>\n          <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"form-group\">\n\n            <input type=\"text\" class=\"form-control\" (input)=\"phone=$event.target.value\" placeholder=\"Enter your phone number\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"street=$event.target.value\" placeholder=\"Enter your street\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"city=$event.target.value\" placeholder=\"Enter your city\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"state=$event.target.value\" placeholder=\"Enter your state\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"zipcode=$event.target.value\" placeholder=\"Enter your zipcode\" required>\n\n\n\n\n\n\n\n\n          </div>\n\n\n\n        </div>\n        <div class=\"modal-footer\">\n          <button mat-raised-button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button mat-raised-button type=\"button\" class=\"btn btn-primary\"\n            (click)=\"getInvoice(45000, 'Tk 45000', 'Add Tk 45000 to balance')\" data-dismiss=\"modal\">Submit</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"customModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"customModalLabel\"\n    aria-hidden=\"true\" data-backdrop=\"false\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"customModalLabel\">Tk {{custom}} Recharge</h5>\n          <button  type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n\n          <div class=\"form-group\">\n\n            <input type=\"text\" class=\"form-control\" (input)=\"phone=$event.target.value\" placeholder=\"Enter your phone number\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"street=$event.target.value\" placeholder=\"Enter your street\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"city=$event.target.value\" placeholder=\"Enter your city\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"state=$event.target.value\" placeholder=\"Enter your state\" required>\n            <br>\n            <input type=\"text\" class=\"form-control\" (input)=\"zipcode=$event.target.value\" placeholder=\"Enter your zipcode\" required>\n\n\n\n\n\n\n\n\n          </div>\n\n\n\n        </div>\n        <div class=\"modal-footer\">\n          <button mat-raised-button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n          <button mat-raised-button type=\"button\" class=\"btn btn-primary\"\n            (click)=\"getInvoice(custom, 'Custom amount', 'Custom Recharge')\" data-dismiss=\"modal\">Submit</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <br>\n  <br>\n  <br>\n\n\n\n</div>\n\n");

/***/ }),

/***/ "./src/app/views/upgrade/upgrade.component.scss":
/*!******************************************************!*\
  !*** ./src/app/views/upgrade/upgrade.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".box {\n  height: 450px;\n  width: 280px;\n  text-align: center; }\n\n.taka {\n  font-size: 60px; }\n\n.mycon {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: space-evenly;\n          justify-content: space-evenly; }\n\n.pro {\n  width: 140px;\n  margin: auto; }\n\n#blue {\n  background-color: #6FCEFF; }\n\n#red {\n  background-color: #FF6F6F; }\n\n#purple {\n  background-color: #6F8DFF; }\n\n/* \r\n.custom-recharge{\r\n    display:flex;\r\n    flex-direction: column;\r\n} */\n\n.modal-backdrop {\n  z-index: 1050; }\n\n.modal-content {\n  z-index: 1060; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvdXBncmFkZS91cGdyYWRlLmNvbXBvbmVudC5zY3NzIiwiYXBwL3ZpZXdzL3VwZ3JhZGUvdXBncmFkZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQVk7RUFDWixZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksZUFBZSxFQUFBOztBQUluQjtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDhCQUFtQjtFQUFuQiw2QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLDhCQUNKO1VBREksNkJBQ0osRUFBQTs7QUFDQTtFQUNJLFlBQVk7RUFDWixZQUFZLEVBQUE7O0FBSWhCO0VBQ0kseUJBQXlCLEVBQUE7O0FBRzdCO0VBQ0kseUJBQXlCLEVBQUE7O0FBRzdCO0VBQ0kseUJBQXlCLEVBQUE7O0FBRTdCOzs7O0dDSEc7O0FEUUg7RUFBaUIsYUFBYSxFQUFBOztBQUM5QjtFQUFnQixhQUFhLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL3VwZ3JhZGUvdXBncmFkZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib3h7XHJcbiAgICBoZWlnaHQ6NDUwcHg7XHJcbiAgICB3aWR0aDogMjgwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi50YWthe1xyXG4gICAgZm9udC1zaXplOiA2MHB4O1xyXG4gICAgXHJcbn1cclxuXHJcbi5teWNvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHlcclxufVxyXG4ucHJve1xyXG4gICAgd2lkdGg6IDE0MHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG5cclxufVxyXG5cclxuI2JsdWV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNkZDRUZGO1xyXG59XHJcblxyXG4jcmVke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGNkY2RjtcclxufVxyXG5cclxuI3B1cnBsZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM2RjhERkY7XHJcbn1cclxuLyogXHJcbi5jdXN0b20tcmVjaGFyZ2V7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59ICovXHJcbi5tb2RhbC1iYWNrZHJvcCB7ei1pbmRleDogMTA1MDt9XHJcbi5tb2RhbC1jb250ZW50IHt6LWluZGV4OiAxMDYwO30iLCIuYm94IHtcbiAgaGVpZ2h0OiA0NTBweDtcbiAgd2lkdGg6IDI4MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLnRha2Ege1xuICBmb250LXNpemU6IDYwcHg7IH1cblxuLm15Y29uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7IH1cblxuLnBybyB7XG4gIHdpZHRoOiAxNDBweDtcbiAgbWFyZ2luOiBhdXRvOyB9XG5cbiNibHVlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzZGQ0VGRjsgfVxuXG4jcmVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0ZGNkY2RjsgfVxuXG4jcHVycGxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzZGOERGRjsgfVxuXG4vKiBcclxuLmN1c3RvbS1yZWNoYXJnZXtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn0gKi9cbi5tb2RhbC1iYWNrZHJvcCB7XG4gIHotaW5kZXg6IDEwNTA7IH1cblxuLm1vZGFsLWNvbnRlbnQge1xuICB6LWluZGV4OiAxMDYwOyB9XG4iXX0= */");

/***/ }),

/***/ "./src/app/views/upgrade/upgrade.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/upgrade/upgrade.component.ts ***!
  \****************************************************/
/*! exports provided: UpgradeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpgradeComponent", function() { return UpgradeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../httpOptions */ "./src/app/httpOptions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let UpgradeComponent = class UpgradeComponent {
    constructor(client) {
        this.client = client;
        this.url = "https://api-sandbox.portwallet.com/api/v1/";
        this.params = {};
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'token ' + localStorage.getItem('Token')
            })
        };
        this.name = localStorage.getItem('UserFirstName') + " " + localStorage.getItem('UserLastName');
        this.email = localStorage.getItem('UserEmail');
        this.phone = "";
        this.street = "";
        this.city = "";
        this.state = "";
        this.zipcode = "";
        this.country = "BGD";
        this.custom = "";
    }
    ngOnInit() {
        this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])(localStorage.getItem('Token'));
    }
    getInvoice(amount, name, description) {
        console.log(amount);
        console.log(localStorage);
        let body = {
            amount: amount,
            name: this.name,
            email: this.email,
            phone: this.phone,
            street: this.street,
            city: this.city,
            state: this.state,
            zipcode: this.zipcode,
            country: this.country,
            productName: name,
            productDescription: description
        };
        let new_body = JSON.stringify(body);
        this.client.post('https://agile-cove-96115.herokuapp.com/wallet/createInvoice', new_body, this.header).subscribe((res) => {
            console.log(res);
            let url = res['data']['action']['url'];
            console.log(typeof url);
            console.log(url);
            window.open(url);
        });
    }
};
UpgradeComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
UpgradeComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-upgrade',
        template: __importDefault(__webpack_require__(/*! raw-loader!./upgrade.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/views/upgrade/upgrade.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./upgrade.component.scss */ "./src/app/views/upgrade/upgrade.component.scss")).default]
    }),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], UpgradeComponent);



/***/ }),

/***/ "./src/app/views/upgrade/upgrade.module.ts":
/*!*************************************************!*\
  !*** ./src/app/views/upgrade/upgrade.module.ts ***!
  \*************************************************/
/*! exports provided: UpgradeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpgradeModule", function() { return UpgradeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var angular_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-calendar */ "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-calendar/date-adapters/date-fns */ "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
/* harmony import */ var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-color-picker */ "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js");
/* harmony import */ var _upgrade_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./upgrade.component */ "./src/app/views/upgrade/upgrade.component.ts");
/* harmony import */ var _upgrade_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./upgrade.routing */ "./src/app/views/upgrade/upgrade.routing.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};












let UpgradeModule = class UpgradeModule {
};
UpgradeModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
            ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__["ColorPickerModule"],
            angular_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"].forRoot({
                provide: angular_calendar__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"],
                useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__["adapterFactory"]
            }),
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_upgrade_routing__WEBPACK_IMPORTED_MODULE_11__["UpgradeRoutes"])
        ],
        providers: [],
        entryComponents: [],
        declarations: [
            _upgrade_component__WEBPACK_IMPORTED_MODULE_10__["UpgradeComponent"],
        ]
    })
], UpgradeModule);



/***/ }),

/***/ "./src/app/views/upgrade/upgrade.routing.ts":
/*!**************************************************!*\
  !*** ./src/app/views/upgrade/upgrade.routing.ts ***!
  \**************************************************/
/*! exports provided: UpgradeRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpgradeRoutes", function() { return UpgradeRoutes; });
/* harmony import */ var _upgrade_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./upgrade.component */ "./src/app/views/upgrade/upgrade.component.ts");
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

const UpgradeRoutes = [
    { path: '', component: _upgrade_component__WEBPACK_IMPORTED_MODULE_0__["UpgradeComponent"], data: { title: 'Upgrade' } }
];


/***/ })

}]);
//# sourceMappingURL=views-upgrade-upgrade-module-es2015.js.map