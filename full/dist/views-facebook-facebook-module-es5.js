function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-facebook-facebook-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/facebook/facebook.component.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/facebook/facebook.component.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsFacebookFacebookComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"content\">\r\n    <mat-card class=\"social-card\">\r\n\r\n        <!-- <div class=\"content\"> -->\r\n\r\n        <i style=\"color:#3b5998\" class=\"fab fa-facebook fa-4x\"></i>\r\n        <br>\r\n        <button mat-raised-button (click)=\"submitLogin()\">Authenticate</button>\r\n\r\n        <!-- </div> -->\r\n\r\n    </mat-card>\r\n\r\n\r\n\r\n    <mat-card class=\"social-card\">\r\n\r\n        <!-- <div class=\"content\"> -->\r\n\r\n        <i style=\"color:#0e76a8\" class=\"fab fa-linkedin fa-4x\"></i>\r\n        <br>\r\n        <button mat-raised-button>Authenticate</button>\r\n\r\n        <!-- </div> -->\r\n\r\n    </mat-card>\r\n\r\n   \r\n</div>\r\n\r\n\r\n";
    /***/
  },

  /***/
  "./src/app/views/facebook/facebook.component.scss":
  /*!********************************************************!*\
    !*** ./src/app/views/facebook/facebook.component.scss ***!
    \********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsFacebookFacebookComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".social-card {\n  width: 150px;\n  height: 150px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center; }\n\n.content {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvZmFjZWJvb2svZmFjZWJvb2suY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZO0VBQ1osYUFBYTtFQUNiLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIseUJBQW1CO1VBQW5CLG1CQUFtQixFQUFBOztBQUl2QjtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDhCQUFtQjtFQUFuQiw2QkFBbUI7VUFBbkIsbUJBQW1CLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL2ZhY2Vib29rL2ZhY2Vib29rLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNvY2lhbC1jYXJke1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgXHJcbn1cclxuXHJcbi5jb250ZW50e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/views/facebook/facebook.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/views/facebook/facebook.component.ts ***!
    \******************************************************/

  /*! exports provided: FacebookComponent */

  /***/
  function srcAppViewsFacebookFacebookComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FacebookComponent", function () {
      return FacebookComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FacebookComponent =
    /*#__PURE__*/
    function () {
      function FacebookComponent(client, router, route) {
        _classCallCheck(this, FacebookComponent);

        this.client = client;
        this.router = router;
        this.route = route;
        this.mail_chimp_httpOptions = {
          headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        };
        this.mailchimp_options = {
          clientId: '471281846639',
          clientSecret: 'b9ca5027afbf1032194f65f87340f1da73656a1801ceaf00fa',
          redirectUri: 'https://cworkva.firebaseapp.com',
          ownServer: true,
          addPort: true,
          finalUri: 'https://cworkva.firebaseapp.com',
          code: 'f76ba9aa66572120ca387fa8bc05bfb2'
        };
      }

      _createClass(FacebookComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])('token');
          this.facebookinit(); // let oauth = MailChimpOAuth(this.mailchimp_options)
          //@ts-ignore

          var param1 = this.route.snapshot.paramMap.get('code');
          console.log("THIS IS THE CODE");
          console.log(param1); // console.log(SessionStorage)

          this.route.queryParamMap.subscribe(function (params) {
            console.log(params.get('code'));
            _this.code = params.get('code');

            if (_this.code) {
              console.log("there is a code");
              var body = {
                code: _this.code
              };

              _this.client.post("https://agile-cove-96115.herokuapp.com/schedule/chimp/", body, _this.header).subscribe(function (res) {
                console.log(res);
              });
            } else {
              console.log("there is no code");
            }
          });
        }
      }, {
        key: "facebookinit",
        value: function facebookinit() {
          window.fbAsyncInit = function () {
            //@ts-ignore
            FB.init({
              appId: '3004517459564703',
              cookie: true,
              xfbml: true,
              version: 'v3.1'
            }); //@ts-ignore

            FB.AppEvents.logPageView(); // this.submitLogin()
          };

          (function (d, s, id) {
            var js,
                fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) {
              return;
            }

            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          })(document, 'script', 'facebook-jssdk'); //  console.log(this.FB)
          //@ts-ignore

        }
      }, {
        key: "submitLogin",
        value: function submitLogin() {
          var _this2 = this;

          console.log("submit login to facebook"); // FB.login();
          //@ts-ignore

          FB.login(function (response) {
            console.log('submitLogin', response);

            if (response.authResponse) {
              //login success
              //login success code here
              //redirect to home page
              console.log(response.status);
              _this2.fb_id = response.authResponse['userID'];

              _this2.client.post('https://agile-cove-96115.herokuapp.com/schedule/fbuser/', {
                id: _this2.fb_id,
                access_token: response.authResponse['accessToken']
              }, _this2.header).subscribe(function (res) {
                console.log(res);
              });

              _this2.fb_accesstoken = response.authResponse['accessToken'];
              console.log(_this2.fb_id);
              console.log(_this2.fb_accesstoken);

              _this2.getdata(_this2.fb_id);
            } else {
              console.log('User login failed');

              _this2.router.navigateByUrl('/calendar');
            }
          }); //@ts-ignore
        }
      }, {
        key: "getdata",
        value: function getdata(user_id) {
          var _this3 = this;

          //@ts-ignore
          FB.api("/" + user_id + "/accounts?type=page", function (response) {
            if (response && !response.error) {
              (function () {
                /* handle the result */
                console.log("User data");
                console.log(response);
                _this3.data_array = response.data;
                var count = 0; //@ts-ignore

                for (var i = 0; i < _this3.data_array.length; i++) {
                  var body = {
                    user_id: _this3.data_array[i]['id'],
                    access_token: _this3.data_array[i]['access_token'],
                    page_name: _this3.data_array[i]['name']
                  };

                  _this3.client.post('https://agile-cove-96115.herokuapp.com/schedule/ptcreate', body, _this3.header).subscribe(function (res) {
                    console.log(res);
                    count++;
                  });

                  console.log(body);
                } //@ts-ignore


                _this3.router.navigateByUrl('/calendar');

                console.log(_this3.data_array);
                console.log(_this3.data_array);
              })();
            } else {
              console.log(response);
            }
          });
        }
      }, {
        key: "mail_oauth",
        value: function mail_oauth() {
          // body = {
          //   grant_type=authorization_code&client_id={client_id}&client_secret={client_secret}&redirect_uri={encoded_url}&code={code}
          // }
          // this.client.get('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639&redirect_uri=https://www.cworkva.firebaseapp.com', this.mail_chimp_httpOptions).subscribe((res)=>{
          //   console.log(res)
          // })
          var mailchimp = window.open('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639');
        }
      }]);

      return FacebookComponent;
    }();

    FacebookComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    FacebookComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-facebook',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./facebook.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/facebook/facebook.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./facebook.component.scss */
      "./src/app/views/facebook/facebook.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])], FacebookComponent);
    /***/
  },

  /***/
  "./src/app/views/facebook/facebook.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/views/facebook/facebook.module.ts ***!
    \***************************************************/

  /*! exports provided: FacebookModule */

  /***/
  function srcAppViewsFacebookFacebookModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FacebookModule", function () {
      return FacebookModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _facebook_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./facebook.component */
    "./src/app/views/facebook/facebook.component.ts");
    /* harmony import */


    var _facebook_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./facebook.routing */
    "./src/app/views/facebook/facebook.routing.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FacebookModule = function FacebookModule() {
      _classCallCheck(this, FacebookModule);
    };

    FacebookModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [// MailchimpModule,
      _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_facebook_routing__WEBPACK_IMPORTED_MODULE_7__["FacebookRoutes"])],
      declarations: [_facebook_component__WEBPACK_IMPORTED_MODULE_6__["FacebookComponent"]]
    })], FacebookModule);
    /***/
  },

  /***/
  "./src/app/views/facebook/facebook.routing.ts":
  /*!****************************************************!*\
    !*** ./src/app/views/facebook/facebook.routing.ts ***!
    \****************************************************/

  /*! exports provided: FacebookRoutes */

  /***/
  function srcAppViewsFacebookFacebookRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FacebookRoutes", function () {
      return FacebookRoutes;
    });
    /* harmony import */


    var _facebook_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./facebook.component */
    "./src/app/views/facebook/facebook.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FacebookRoutes = [{
      path: '',
      component: _facebook_component__WEBPACK_IMPORTED_MODULE_0__["FacebookComponent"]
    }];
    /***/
  }
}]);
//# sourceMappingURL=views-facebook-facebook-module-es5.js.map