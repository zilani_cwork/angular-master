function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
  /*!**************************************************!*\
    !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
    \**************************************************/

  /*! no static exports found */

  /***/
  function node_modulesMomentLocaleSyncRecursive$(module, exports, __webpack_require__) {
    var map = {
      "./af": "./node_modules/moment/locale/af.js",
      "./af.js": "./node_modules/moment/locale/af.js",
      "./ar": "./node_modules/moment/locale/ar.js",
      "./ar-dz": "./node_modules/moment/locale/ar-dz.js",
      "./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
      "./ar-kw": "./node_modules/moment/locale/ar-kw.js",
      "./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
      "./ar-ly": "./node_modules/moment/locale/ar-ly.js",
      "./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
      "./ar-ma": "./node_modules/moment/locale/ar-ma.js",
      "./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
      "./ar-sa": "./node_modules/moment/locale/ar-sa.js",
      "./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
      "./ar-tn": "./node_modules/moment/locale/ar-tn.js",
      "./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
      "./ar.js": "./node_modules/moment/locale/ar.js",
      "./az": "./node_modules/moment/locale/az.js",
      "./az.js": "./node_modules/moment/locale/az.js",
      "./be": "./node_modules/moment/locale/be.js",
      "./be.js": "./node_modules/moment/locale/be.js",
      "./bg": "./node_modules/moment/locale/bg.js",
      "./bg.js": "./node_modules/moment/locale/bg.js",
      "./bm": "./node_modules/moment/locale/bm.js",
      "./bm.js": "./node_modules/moment/locale/bm.js",
      "./bn": "./node_modules/moment/locale/bn.js",
      "./bn.js": "./node_modules/moment/locale/bn.js",
      "./bo": "./node_modules/moment/locale/bo.js",
      "./bo.js": "./node_modules/moment/locale/bo.js",
      "./br": "./node_modules/moment/locale/br.js",
      "./br.js": "./node_modules/moment/locale/br.js",
      "./bs": "./node_modules/moment/locale/bs.js",
      "./bs.js": "./node_modules/moment/locale/bs.js",
      "./ca": "./node_modules/moment/locale/ca.js",
      "./ca.js": "./node_modules/moment/locale/ca.js",
      "./cs": "./node_modules/moment/locale/cs.js",
      "./cs.js": "./node_modules/moment/locale/cs.js",
      "./cv": "./node_modules/moment/locale/cv.js",
      "./cv.js": "./node_modules/moment/locale/cv.js",
      "./cy": "./node_modules/moment/locale/cy.js",
      "./cy.js": "./node_modules/moment/locale/cy.js",
      "./da": "./node_modules/moment/locale/da.js",
      "./da.js": "./node_modules/moment/locale/da.js",
      "./de": "./node_modules/moment/locale/de.js",
      "./de-at": "./node_modules/moment/locale/de-at.js",
      "./de-at.js": "./node_modules/moment/locale/de-at.js",
      "./de-ch": "./node_modules/moment/locale/de-ch.js",
      "./de-ch.js": "./node_modules/moment/locale/de-ch.js",
      "./de.js": "./node_modules/moment/locale/de.js",
      "./dv": "./node_modules/moment/locale/dv.js",
      "./dv.js": "./node_modules/moment/locale/dv.js",
      "./el": "./node_modules/moment/locale/el.js",
      "./el.js": "./node_modules/moment/locale/el.js",
      "./en-SG": "./node_modules/moment/locale/en-SG.js",
      "./en-SG.js": "./node_modules/moment/locale/en-SG.js",
      "./en-au": "./node_modules/moment/locale/en-au.js",
      "./en-au.js": "./node_modules/moment/locale/en-au.js",
      "./en-ca": "./node_modules/moment/locale/en-ca.js",
      "./en-ca.js": "./node_modules/moment/locale/en-ca.js",
      "./en-gb": "./node_modules/moment/locale/en-gb.js",
      "./en-gb.js": "./node_modules/moment/locale/en-gb.js",
      "./en-ie": "./node_modules/moment/locale/en-ie.js",
      "./en-ie.js": "./node_modules/moment/locale/en-ie.js",
      "./en-il": "./node_modules/moment/locale/en-il.js",
      "./en-il.js": "./node_modules/moment/locale/en-il.js",
      "./en-nz": "./node_modules/moment/locale/en-nz.js",
      "./en-nz.js": "./node_modules/moment/locale/en-nz.js",
      "./eo": "./node_modules/moment/locale/eo.js",
      "./eo.js": "./node_modules/moment/locale/eo.js",
      "./es": "./node_modules/moment/locale/es.js",
      "./es-do": "./node_modules/moment/locale/es-do.js",
      "./es-do.js": "./node_modules/moment/locale/es-do.js",
      "./es-us": "./node_modules/moment/locale/es-us.js",
      "./es-us.js": "./node_modules/moment/locale/es-us.js",
      "./es.js": "./node_modules/moment/locale/es.js",
      "./et": "./node_modules/moment/locale/et.js",
      "./et.js": "./node_modules/moment/locale/et.js",
      "./eu": "./node_modules/moment/locale/eu.js",
      "./eu.js": "./node_modules/moment/locale/eu.js",
      "./fa": "./node_modules/moment/locale/fa.js",
      "./fa.js": "./node_modules/moment/locale/fa.js",
      "./fi": "./node_modules/moment/locale/fi.js",
      "./fi.js": "./node_modules/moment/locale/fi.js",
      "./fo": "./node_modules/moment/locale/fo.js",
      "./fo.js": "./node_modules/moment/locale/fo.js",
      "./fr": "./node_modules/moment/locale/fr.js",
      "./fr-ca": "./node_modules/moment/locale/fr-ca.js",
      "./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
      "./fr-ch": "./node_modules/moment/locale/fr-ch.js",
      "./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
      "./fr.js": "./node_modules/moment/locale/fr.js",
      "./fy": "./node_modules/moment/locale/fy.js",
      "./fy.js": "./node_modules/moment/locale/fy.js",
      "./ga": "./node_modules/moment/locale/ga.js",
      "./ga.js": "./node_modules/moment/locale/ga.js",
      "./gd": "./node_modules/moment/locale/gd.js",
      "./gd.js": "./node_modules/moment/locale/gd.js",
      "./gl": "./node_modules/moment/locale/gl.js",
      "./gl.js": "./node_modules/moment/locale/gl.js",
      "./gom-latn": "./node_modules/moment/locale/gom-latn.js",
      "./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
      "./gu": "./node_modules/moment/locale/gu.js",
      "./gu.js": "./node_modules/moment/locale/gu.js",
      "./he": "./node_modules/moment/locale/he.js",
      "./he.js": "./node_modules/moment/locale/he.js",
      "./hi": "./node_modules/moment/locale/hi.js",
      "./hi.js": "./node_modules/moment/locale/hi.js",
      "./hr": "./node_modules/moment/locale/hr.js",
      "./hr.js": "./node_modules/moment/locale/hr.js",
      "./hu": "./node_modules/moment/locale/hu.js",
      "./hu.js": "./node_modules/moment/locale/hu.js",
      "./hy-am": "./node_modules/moment/locale/hy-am.js",
      "./hy-am.js": "./node_modules/moment/locale/hy-am.js",
      "./id": "./node_modules/moment/locale/id.js",
      "./id.js": "./node_modules/moment/locale/id.js",
      "./is": "./node_modules/moment/locale/is.js",
      "./is.js": "./node_modules/moment/locale/is.js",
      "./it": "./node_modules/moment/locale/it.js",
      "./it-ch": "./node_modules/moment/locale/it-ch.js",
      "./it-ch.js": "./node_modules/moment/locale/it-ch.js",
      "./it.js": "./node_modules/moment/locale/it.js",
      "./ja": "./node_modules/moment/locale/ja.js",
      "./ja.js": "./node_modules/moment/locale/ja.js",
      "./jv": "./node_modules/moment/locale/jv.js",
      "./jv.js": "./node_modules/moment/locale/jv.js",
      "./ka": "./node_modules/moment/locale/ka.js",
      "./ka.js": "./node_modules/moment/locale/ka.js",
      "./kk": "./node_modules/moment/locale/kk.js",
      "./kk.js": "./node_modules/moment/locale/kk.js",
      "./km": "./node_modules/moment/locale/km.js",
      "./km.js": "./node_modules/moment/locale/km.js",
      "./kn": "./node_modules/moment/locale/kn.js",
      "./kn.js": "./node_modules/moment/locale/kn.js",
      "./ko": "./node_modules/moment/locale/ko.js",
      "./ko.js": "./node_modules/moment/locale/ko.js",
      "./ku": "./node_modules/moment/locale/ku.js",
      "./ku.js": "./node_modules/moment/locale/ku.js",
      "./ky": "./node_modules/moment/locale/ky.js",
      "./ky.js": "./node_modules/moment/locale/ky.js",
      "./lb": "./node_modules/moment/locale/lb.js",
      "./lb.js": "./node_modules/moment/locale/lb.js",
      "./lo": "./node_modules/moment/locale/lo.js",
      "./lo.js": "./node_modules/moment/locale/lo.js",
      "./lt": "./node_modules/moment/locale/lt.js",
      "./lt.js": "./node_modules/moment/locale/lt.js",
      "./lv": "./node_modules/moment/locale/lv.js",
      "./lv.js": "./node_modules/moment/locale/lv.js",
      "./me": "./node_modules/moment/locale/me.js",
      "./me.js": "./node_modules/moment/locale/me.js",
      "./mi": "./node_modules/moment/locale/mi.js",
      "./mi.js": "./node_modules/moment/locale/mi.js",
      "./mk": "./node_modules/moment/locale/mk.js",
      "./mk.js": "./node_modules/moment/locale/mk.js",
      "./ml": "./node_modules/moment/locale/ml.js",
      "./ml.js": "./node_modules/moment/locale/ml.js",
      "./mn": "./node_modules/moment/locale/mn.js",
      "./mn.js": "./node_modules/moment/locale/mn.js",
      "./mr": "./node_modules/moment/locale/mr.js",
      "./mr.js": "./node_modules/moment/locale/mr.js",
      "./ms": "./node_modules/moment/locale/ms.js",
      "./ms-my": "./node_modules/moment/locale/ms-my.js",
      "./ms-my.js": "./node_modules/moment/locale/ms-my.js",
      "./ms.js": "./node_modules/moment/locale/ms.js",
      "./mt": "./node_modules/moment/locale/mt.js",
      "./mt.js": "./node_modules/moment/locale/mt.js",
      "./my": "./node_modules/moment/locale/my.js",
      "./my.js": "./node_modules/moment/locale/my.js",
      "./nb": "./node_modules/moment/locale/nb.js",
      "./nb.js": "./node_modules/moment/locale/nb.js",
      "./ne": "./node_modules/moment/locale/ne.js",
      "./ne.js": "./node_modules/moment/locale/ne.js",
      "./nl": "./node_modules/moment/locale/nl.js",
      "./nl-be": "./node_modules/moment/locale/nl-be.js",
      "./nl-be.js": "./node_modules/moment/locale/nl-be.js",
      "./nl.js": "./node_modules/moment/locale/nl.js",
      "./nn": "./node_modules/moment/locale/nn.js",
      "./nn.js": "./node_modules/moment/locale/nn.js",
      "./pa-in": "./node_modules/moment/locale/pa-in.js",
      "./pa-in.js": "./node_modules/moment/locale/pa-in.js",
      "./pl": "./node_modules/moment/locale/pl.js",
      "./pl.js": "./node_modules/moment/locale/pl.js",
      "./pt": "./node_modules/moment/locale/pt.js",
      "./pt-br": "./node_modules/moment/locale/pt-br.js",
      "./pt-br.js": "./node_modules/moment/locale/pt-br.js",
      "./pt.js": "./node_modules/moment/locale/pt.js",
      "./ro": "./node_modules/moment/locale/ro.js",
      "./ro.js": "./node_modules/moment/locale/ro.js",
      "./ru": "./node_modules/moment/locale/ru.js",
      "./ru.js": "./node_modules/moment/locale/ru.js",
      "./sd": "./node_modules/moment/locale/sd.js",
      "./sd.js": "./node_modules/moment/locale/sd.js",
      "./se": "./node_modules/moment/locale/se.js",
      "./se.js": "./node_modules/moment/locale/se.js",
      "./si": "./node_modules/moment/locale/si.js",
      "./si.js": "./node_modules/moment/locale/si.js",
      "./sk": "./node_modules/moment/locale/sk.js",
      "./sk.js": "./node_modules/moment/locale/sk.js",
      "./sl": "./node_modules/moment/locale/sl.js",
      "./sl.js": "./node_modules/moment/locale/sl.js",
      "./sq": "./node_modules/moment/locale/sq.js",
      "./sq.js": "./node_modules/moment/locale/sq.js",
      "./sr": "./node_modules/moment/locale/sr.js",
      "./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
      "./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
      "./sr.js": "./node_modules/moment/locale/sr.js",
      "./ss": "./node_modules/moment/locale/ss.js",
      "./ss.js": "./node_modules/moment/locale/ss.js",
      "./sv": "./node_modules/moment/locale/sv.js",
      "./sv.js": "./node_modules/moment/locale/sv.js",
      "./sw": "./node_modules/moment/locale/sw.js",
      "./sw.js": "./node_modules/moment/locale/sw.js",
      "./ta": "./node_modules/moment/locale/ta.js",
      "./ta.js": "./node_modules/moment/locale/ta.js",
      "./te": "./node_modules/moment/locale/te.js",
      "./te.js": "./node_modules/moment/locale/te.js",
      "./tet": "./node_modules/moment/locale/tet.js",
      "./tet.js": "./node_modules/moment/locale/tet.js",
      "./tg": "./node_modules/moment/locale/tg.js",
      "./tg.js": "./node_modules/moment/locale/tg.js",
      "./th": "./node_modules/moment/locale/th.js",
      "./th.js": "./node_modules/moment/locale/th.js",
      "./tl-ph": "./node_modules/moment/locale/tl-ph.js",
      "./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
      "./tlh": "./node_modules/moment/locale/tlh.js",
      "./tlh.js": "./node_modules/moment/locale/tlh.js",
      "./tr": "./node_modules/moment/locale/tr.js",
      "./tr.js": "./node_modules/moment/locale/tr.js",
      "./tzl": "./node_modules/moment/locale/tzl.js",
      "./tzl.js": "./node_modules/moment/locale/tzl.js",
      "./tzm": "./node_modules/moment/locale/tzm.js",
      "./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
      "./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
      "./tzm.js": "./node_modules/moment/locale/tzm.js",
      "./ug-cn": "./node_modules/moment/locale/ug-cn.js",
      "./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
      "./uk": "./node_modules/moment/locale/uk.js",
      "./uk.js": "./node_modules/moment/locale/uk.js",
      "./ur": "./node_modules/moment/locale/ur.js",
      "./ur.js": "./node_modules/moment/locale/ur.js",
      "./uz": "./node_modules/moment/locale/uz.js",
      "./uz-latn": "./node_modules/moment/locale/uz-latn.js",
      "./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
      "./uz.js": "./node_modules/moment/locale/uz.js",
      "./vi": "./node_modules/moment/locale/vi.js",
      "./vi.js": "./node_modules/moment/locale/vi.js",
      "./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
      "./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
      "./yo": "./node_modules/moment/locale/yo.js",
      "./yo.js": "./node_modules/moment/locale/yo.js",
      "./zh-cn": "./node_modules/moment/locale/zh-cn.js",
      "./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
      "./zh-hk": "./node_modules/moment/locale/zh-hk.js",
      "./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
      "./zh-tw": "./node_modules/moment/locale/zh-tw.js",
      "./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
    };

    function webpackContext(req) {
      var id = webpackContextResolve(req);
      return __webpack_require__(id);
    }

    function webpackContextResolve(req) {
      if (!__webpack_require__.o(map, req)) {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      }

      return map[req];
    }

    webpackContext.keys = function webpackContextKeys() {
      return Object.keys(map);
    };

    webpackContext.resolve = webpackContextResolve;
    module.exports = webpackContext;
    webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<router-outlet></router-outlet>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/facebook/facebook.component.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/facebook/facebook.component.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppFacebookFacebookComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<p>\n  facebook works!\n</p>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.html":
  /*!******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.html ***!
    \******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsBottomSheetShareBottomSheetShareComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-nav-list>\n  <mat-list-item>\n      <svg class=\"icon-bottom-sheet icon-facebook\">\n          <use xlink:href=\"#icon-facebook\" />\n      </svg>\n      <a mat-list-item href=\"https://www.facebook.com/sharer.php?u=https://themeforest.net/item/angular-landing-material-design-angular-app-landing-page/21198258\">\n        <span mat-line>Share on Facebook</span>\n      </a>\n  </mat-list-item>\n  <mat-list-item>\n      <svg class=\"icon-bottom-sheet icon-facebook\">\n          <use xlink:href=\"#icon-twitter\" />\n      </svg>\n      <a mat-list-item href=\"https://twitter.com/intent/tweet?url=https://themeforest.net/item/angular-landing-material-design-angular-app-landing-page/21198258&hashtags=angular,template,landing\">\n        <span mat-line>Tweet About Us!</span>\n      </a>\n  </mat-list-item>\n  <mat-list-item>\n      <svg class=\"icon-bottom-sheet icon-linkedin\">\n        <use xlink:href=\"#icon-linkedin\" />\n      </svg>\n      <a mat-list-item href=\"https://www.linkedin.com/shareArticle?mini=true&url=https://themeforest.net/item/angular-landing-material-design-angular-app-landing-page/21198258\">\n        <span mat-line>Share on LinkedIn</span>\n      </a>\n  </mat-list-item>\n</mat-nav-list>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/breadcrumb/breadcrumb.component.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/breadcrumb/breadcrumb.component.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsBreadcrumbBreadcrumbComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"breadcrumb-bar\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'simple'\">\n  <ul class=\"breadcrumb\">\n    <li *ngFor=\"let part of routeParts\"><a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a></li>\n  </ul>\n</div>\n\n<div class=\"breadcrumb-title\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'title'\">\n  <h1 class=\"bc-title\">{{routeParts[routeParts.length -1]?.breadcrumb | translate}}</h1>\n  <ul class=\"breadcrumb\" *ngIf=\"routeParts.length > 1\">\n    <li *ngFor=\"let part of routeParts\"><a routerLink=\"/{{part.url}}\" class=\"text-muted\">{{part.breadcrumb | translate}}</a></li>\n  </ul>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/button-loading/button-loading.component.html":
  /*!**********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/button-loading/button-loading.component.html ***!
    \**********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsButtonLoadingButtonLoadingComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<button mat-button [color]=\"color\" class=\"button-loading {{btnClass}}\" [type]=\"type\" [disabled]=\"loading\" \n[ngClass]=\"{\n    loading: loading,\n    'mat-button': !raised,\n    'mat-raised-button': raised\n  }\">\n    <div class=\"btn-spinner\" *ngIf=\"loading\"></div>\n    <span *ngIf=\"!loading\">\n        <ng-content></ng-content>\n    </span>\n    <span *ngIf=\"loading\">{{loadingText}}</span>\n</button>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/customizer/customizer.component.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/customizer/customizer.component.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsCustomizerCustomizerComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"handle\" *ngIf=\"!isCustomizerOpen\">\n    <button \n    mat-fab\n    color=\"primary\" \n    (click)=\"open()\">\n      <mat-icon class=\"spin text-white size\">add_circle_outline</mat-icon>\n    </button>\n  </div>\n<div id=\"app-customizer\" *ngIf=\"isCustomizerOpen\">\n  <mat-card class=\"p-0\">\n    <mat-card-title class=\"m-0 light-gray\">\n      <mat-card class=\"card-title-text\" fxLayout=\"row wrap\" fxLayoutAlign=\"center center\">\n        <!-- <button mat-flat-button [color]=\"viewMode === 'options' ? 'primary':''\" (click)=\"viewMode = 'options'\">Options</button>\n        <span fxFlex=\"15px\"></span>\n        <button mat-flat-button [color]=\"viewMode === 'json' ? 'primary':''\" (click)=\"viewMode = 'json'\">Json</button>\n        <span fxFlex></span>\n        <button \n        class=\"card-control\" \n        mat-icon-button\n        (click)=\"isCustomizerOpen = false\">\n          <mat-icon>close</mat-icon>\n        </button> -->\n        <div style=\"display: flex; flex-direction: row; justify-content: space-between;\">\n         <div style=\"margin-right: 20px;\">TO DO LIST</div>\n        <button  \n        class=\"card-control\" \n        mat-icon-button\n        (click)=\"isCustomizerOpen = false\">\n          <mat-icon>close</mat-icon>\n        </button>\n      </div>\n      </mat-card>\n    </mat-card-title>\n    <mat-card-content *ngIf=\"viewMode === 'json'\" style=\"min-height: 100vh\">\n        \n        <pre><code [egretHighlight]=\"this.layoutConf | json\"></code></pre>\n        <div>\n          <span fxFlex></span>\n          <a href=\"http://demos.ui-lib.com/egret-doc/#layout\" target=\"_blank\" mat-mini-fab><mat-icon>help</mat-icon></a>\n        </div>\n    </mat-card-content>\n\n    <mat-card-content [perfectScrollbar] *ngIf=\"viewMode === 'options'\">\n      <!-- <p><small>Customize the template then copy configuration json.</small></p>\n      <div class=\"pb-1 mb-1 border-bottom\">\n        <h6 class=\"title text-muted\">Layouts</h6>\n        <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\n            <mat-radio-button [value]=\"'top'\"> Top Navigation </mat-radio-button>\n            <mat-radio-button [value]=\"'side'\"> Side Navigation </mat-radio-button>\n        </mat-radio-group>\n      </div>\n\n     \n      <div class=\"pb-1 mb-1 border-bottom\">\n          <h6 class=\"title text-muted\">Header Colors</h6>\n          <div class=\"mb-1\">\n            <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\" [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Header</mat-checkbox>\n          </div>\n\n          <div class=\"colors\">\n              <div \n              class=\"color {{c.class}}\" \n              *ngFor=\"let c of customizer.topbarColors\"\n              (click)=\"customizer.changeTopbarColor(c)\">\n              <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\n            </div>\n          </div>  \n      </div>\n\n      <div class=\"pb-1 mb-1 border-bottom\">\n        <h6 class=\"title text-muted\">Sidebar colors</h6>\n        <div class=\"colors\">\n            <div \n            class=\"color {{c.class}}\" \n            *ngFor=\"let c of customizer.sidebarColors\"\n            (click)=\"customizer.changeSidebarColor(c)\">\n            <mat-icon class=\"active-icon\" *ngIf=\"c.active\">check</mat-icon>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"pb-1 mb-1 border-bottom\">\n        <h6 class=\"title text-muted\">Material Themes</h6>\n        <div class=\"colors\">\n            <div class=\"color\" *ngFor=\"let theme of egretThemes\"\n            (click)=\"changeTheme(theme)\" [style.background]=\"theme.baseColor\">\n              <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\n            </div>\n        </div>\n      </div>\n\n      <div class=\"pb-1 mb-1 border-bottom\">\n          <h6 class=\"title text-muted\">Breadcrumb</h6>\n          <div class=\"mb-1\">\n              <mat-checkbox [(ngModel)]=\"layoutConf.useBreadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use breadcrumb</mat-checkbox>\n          </div>\n          <small class=\"text-muted\">Breadcrumb types</small>\n          <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\" [disabled]=\"!layoutConf.useBreadcrumb\">\n              <mat-radio-button [value]=\"'simple'\"> Simple </mat-radio-button>\n              <mat-radio-button [value]=\"'title'\"> Simple with title </mat-radio-button>\n          </mat-radio-group>\n        </div>\n\n      <div class=\"pb-1 pos-rel mb-1 border-bottom\">\n        <h6 class=\"title text-muted\">Navigation</h6>\n        <mat-radio-group \n        fxLayout=\"column\" \n        [(ngModel)]=\"selectedMenu\" \n        (change)=\"changeSidenav($event)\" \n        [disabled]=\"selectedLayout === 'top'\">\n          <mat-radio-button \n          *ngFor=\"let type of sidenavTypes\" \n          [value]=\"type.value\">\n            {{type.name}}\n          </mat-radio-button>\n        </mat-radio-group>\n      </div>\n\n      <div class=\"pb-1 pos-rel mb-1 border-bottom\">\n        <mat-checkbox [(ngModel)]=\"perfectScrollbarEnabled\" (change)=\"tooglePerfectScrollbar($event)\">Custom scrollbar</mat-checkbox>\n      </div>\n      \n      <div class=\"pb-1 \">\n        <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\n      </div> -->\n\n      <!-- <form class=\"example-form\"> -->\n        <mat-form-field class=\"example-full-width\">\n          <textarea matInput [value]=\"task\" (input)=\"task=$event.target.value\" placeholder=\"Task to be done...\"></textarea>\n        </mat-form-field>\n      <!-- </form> -->\n\n      <button mat-raised-button (click)=\"addTask()\">Add Task</button>\n      <br>\n      <br>\n    \n      <mat-card-title class=\"card-title-text\">Tasks:</mat-card-title>\n      <mat-list role=\"list\">\n        <mat-list-item role=\"listitem\" *ngFor=\"let item of tasks\">\n\n          <div *ngIf=\"item.done\" class=\"done item\">\n            {{item.task}}\n            <i class=\"fas fa-times\" (click)=\"delete(item.id)\"></i>\n\n\n          </div>\n          <div *ngIf=\"!item.done\" class=\"item\">\n            {{item.task}}\n            <i class=\"fas fa-times\" (click)=\"delete(item.id)\"></i>\n\n\n          \n          </div>\n\n        </mat-list-item>\n        \n      </mat-list>\n    \n    </mat-card-content>\n  </mat-card>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/egret-sidebar/egret-sidebar.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/egret-sidebar/egret-sidebar.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsEgretSidebarEgretSidebarComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div>\n  <ng-content></ng-content>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer-template/example-viewer-template.component.html":
  /*!****************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer-template/example-viewer-template.component.html ***!
    \****************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsExampleViewerTemplateExampleViewerTemplateComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"m-333\">\n  <div *ngFor=\"let example of examples\" class=\"mb-24\">\n    <egret-example-viewer [exampleId]=\"example\" [path]=\"componentDirPath\" [data]=\"exampleComponents[example]\"></egret-example-viewer>\n  </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer/example-viewer.component.html":
  /*!**********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer/example-viewer.component.html ***!
    \**********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsExampleViewerExampleViewerComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"mat-elevation-z1 rounded overflow-hidden\">\n  <mat-accordion multi=\"true\" displayMode=\"flat\" class=\"egret-example-viewer-accordion\">\n    <mat-expansion-panel>\n      <mat-expansion-panel-header collapsedHeight=\"48px\" expandedHeight=\"48px\" class=\"light-gray egret-example-viewer-header\">\n        <mat-panel-title>\n          {{data?.title}} \n        </mat-panel-title>\n        <button mat-button color=\"warn\" ><mat-icon>code</mat-icon> Code</button>\n      </mat-expansion-panel-header>\n  \n      <div class=\"example-viewer-tab-wrap\">\n        <mat-tab-group class=\"mb-24\">\n          <mat-tab label=\"HTML\">\n            <div class=\"code-wrap\" id=\"html\">\n                <pre><code egretHighlight [languages]=\"['xml']\" [path]=\"componentPath +'.html'\"></code></pre>\n            </div>\n          </mat-tab>\n          <mat-tab label=\"TS\">\n              <div class=\"code-wrap\" id=\"ts\">\n                <pre><code egretHighlight [path]=\"componentPath+'.ts'\"></code></pre>\n              </div>\n          </mat-tab>\n          <mat-tab label=\"SCSS\">\n            <div class=\"code-wrap\" id=\"scss\">\n                <pre><code egretHighlight [path]=\"componentPath+'.scss'\"></code></pre>\n            </div>\n          </mat-tab>\n        </mat-tab-group>\n      </div>\n    </mat-expansion-panel>\n  \n    <mat-expansion-panel expanded style=\"position: relative;\">\n      <div class=\"example-component pt-16\">\n        <div #exampleContainer></div>\n      </div>\n    </mat-expansion-panel>\n  </mat-accordion>\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-side/header-side.template.html":
  /*!***************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-side/header-side.template.html ***!
    \***************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsHeaderSideHeaderSideTemplateHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-toolbar class=\"topbar\">\n  <!-- Sidenav toggle button -->\n  <button *ngIf=\"layoutConf.sidebarStyle !== 'compact'\" mat-icon-button id=\"sidenavToggle\" (click)=\"toggleSidenav()\"\n    matTooltip=\"Toggle Hide/Open\">\n    <mat-icon>menu</mat-icon>\n  </button>\n\n  <!-- Search form -->\n  <!-- <div fxFlex fxHide.lt-sm=\"true\" class=\"search-bar\">\n    <form class=\"top-search-form\">\n      <mat-icon role=\"img\">search</mat-icon>\n      <input autofocus=\"true\" placeholder=\"Search\" type=\"text\" />\n    </form>\n  </div> -->\n\n  <span fxFlex></span>\n  <!-- Language Switcher -->\n\n  <!-- <button mat-button [matMenuTriggerFor]=\"menu\">\n    <span class=\"flag-icon {{currentLang.flag}} mr-05\"></span>\n    <span>{{currentLang.name}}</span>\n\n  </button>\n  <mat-menu #menu=\"matMenu\">\n    <button mat-menu-item *ngFor=\"let lang of availableLangs\" (click)=\"setLang(lang)\">\n      <span class=\"flag-icon mr-05 {{lang.flag}}\"></span>\n      <span>{{lang.name}}</span>\n    </button>\n  </mat-menu>\n\n\n  <egret-search-input-over placeholder=\"Country (e.g. US)\" resultPage=\"/search\">\n  </egret-search-input-over> -->\n\n  <!-- Open \"views/search-view/result-page.component\" to understand how to subscribe to input field value -->\n\n  <!-- Notification toggle button -->\n \n  <!-- <button mat-icon-button matTooltip=\"Notifications\" (click)=\"toggleNotific()\" [style.overflow]=\"'visible'\"\n    class=\"topbar-button-right\">\n    <mat-icon>notifications</mat-icon>\n    <span class=\"notification-number mat-bg-warn\">3</span>\n  </button>\n   -->\n  <!-- Top left user menu -->\n  <a mat-button color=\"primary\" [routerLink]=\"'/upgrade'\"> Upgrade </a>\n  <a mat-button [disabled]=\"true\"  style=\"color: black;\"><strong> {{name}} </strong></a>\n  <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right img-button\">\n    <img src=\"assets/images/logo.png\" alt=\"\" />\n  </button>\n\n  <mat-menu #accountMenu=\"matMenu\">\n    <!-- <button mat-menu-item [routerLink]=\"['/profile/overview']\">\n      <mat-icon>account_box</mat-icon>\n      <span>Profile</span>\n    </button>\n    <button mat-menu-item [routerLink]=\"['/profile/settings']\">\n      <mat-icon>settings</mat-icon>\n      <span>Account Settings</span>\n    </button>\n    <button mat-menu-item>\n      <mat-icon>notifications_off</mat-icon>\n      <span>Disable alerts</span>\n    </button> -->\n    <button mat-menu-item [routerLink]=\"['/sessions/signin']\" (click)=\"SignOut()\">\n      <mat-icon>exit_to_app</mat-icon>\n      <span>Sign out</span>\n    </button>\n  </mat-menu>\n</mat-toolbar>\n<!-- <mat-toolbar class=\"topbar\"  style=\"display:flex; justify-content:space-between;\"> -->\n  \n    <!-- <mat-toolbar-row> -->\n\n    \n      <!-- <div>\n        <a mat-button [routerLink]=\"'/calendar'\"> Calendar </a>\n        <a mat-button [routerLink]=\"'/postmanagement'\"> Communication </a>\n        <a mat-button [routerLink]=\"'/storage'\"> Storage </a>\n      </div> -->\n    <!-- </mat-toolbar-row> -->\n      <!-- <div>\n        <a mat-button color=\"primary\" [routerLink]=\"'/upgrade'\"> Upgrade </a>\n      </div>\n     -->\n<!-- </mat-toolbar> -->\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-top/header-top.component.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-top/header-top.component.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsHeaderTopHeaderTopComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"header-topnav mat-elevation-z2\">\n  <div class=\"container\">\n    <div class=\"topnav\">\n      <!-- App Logo -->\n      <div class=\"topbar-branding\">\n        <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\n      </div>\n\n      <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\n        <li *ngFor=\"let item of menuItems; let i = index;\">\n          <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\n            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\n              <mat-icon>{{item.icon}}</mat-icon> \n              {{item.name | translate}}\n            </a>\n            <div *ngIf=\"item.type === 'dropDown'\">\n              <label matRipple for=\"drop-{{i}}\" class=\"toggle\"><mat-icon>{{item.icon}}</mat-icon> {{item.name | translate}}</label>\n              <a matRipple><mat-icon>{{item.icon}}</mat-icon> {{item.name | translate}}</a>\n              <input type=\"checkbox\" id=\"drop-{{i}}\" />\n              <ul>\n                <li *ngFor=\"let itemLvL2 of item.sub; let j = index;\" routerLinkActive=\"open\">\n                  <a matRipple routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" \n                  *ngIf=\"itemLvL2.type !== 'dropDown'\">\n                    <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>  \n                    {{itemLvL2.name | translate}}\n                  </a>\n                  \n                  <div *ngIf=\"itemLvL2.type === 'dropDown'\">\n                    <label matRipple for=\"drop-{{i}}{{j}}\" class=\"toggle\">{{itemLvL2.name | translate}}</label>\n                    <a matRipple><mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>  {{itemLvL2.name | translate}}</a>\n                    <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\" />\n                    <!-- Level 3 -->\n                    <ul>\n                      <li *ngFor=\"let itemLvL3 of itemLvL2.sub\" routerLinkActive=\"open\">\n                        <a matRipple routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\n                          <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\n                          {{itemLvL3.name | translate}}\n                        </a>\n                      </li>\n                    </ul>\n                  </div>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </li>\n      </ul>\n      <span fxFlex></span>\n      <!-- End Navigation -->\n      \n      <!-- Language Switcher -->\n      <mat-select \n      *ngIf=\"!layoutConf.isMobile\"\n      placeholder=\"\"\n      id=\"langToggle\"\n      [style.width]=\"'auto'\"\n      name=\"currentLang\"\n      [(ngModel)]=\"currentLang\" \n      (selectionChange)=\"setLang()\"\n      class=\"topbar-button-right\">\n        <mat-option \n        *ngFor=\"let lang of availableLangs\" \n        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\n      </mat-select>\n      <!-- Theme Switcher -->\n      <button \n      mat-icon-button\n      id=\"schemeToggle\" \n      [style.overflow]=\"'visible'\"\n      matTooltip=\"Color Schemes\"\n      [matMenuTriggerFor]=\"themeMenu\"\n      class=\"topbar-button-right\">\n        <mat-icon>format_color_fill</mat-icon>\n      </button>\n      <mat-menu #themeMenu=\"matMenu\">\n        <mat-grid-list\n        class=\"theme-list\" \n        cols=\"2\" \n        rowHeight=\"48px\">\n          <mat-grid-tile \n          *ngFor=\"let theme of egretThemes\"\n          (click)=\"changeTheme(theme)\">\n            <div mat-menu-item [title]=\"theme.name\">\n              <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\n              <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\n            </div>\n          </mat-grid-tile>\n        </mat-grid-list>\n      </mat-menu>\n      <!-- Notification toggle button -->\n      <button \n      mat-icon-button\n      matTooltip=\"Notifications\" \n      (click)=\"toggleNotific()\"\n      [style.overflow]=\"'visible'\" \n      class=\"topbar-button-right\">\n        <mat-icon>notifications</mat-icon>\n        <span class=\"notification-number mat-bg-warn\">3</span>\n      </button>\n      <!-- Top left user menu -->\n      <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\n        <img src=\"assets/images/face-7.jpg\" alt=\"\">\n      </button>\n      <mat-menu #accountMenu=\"matMenu\">\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\n          <mat-icon>account_box</mat-icon>\n          <span>Profile</span>\n        </button>\n        <button mat-menu-item [routerLink]=\"['/profile/settings']\">\n          <mat-icon>settings</mat-icon>\n          <span>Account Settings</span>\n        </button>\n        <button mat-menu-item>\n          <mat-icon>notifications_off</mat-icon>\n          <span>Disable alerts</span>\n        </button>\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\n          <mat-icon>exit_to_app</mat-icon>\n          <span>Sign out</span>\n        </button>\n      </mat-menu>\n      <!-- Mobile screen menu toggle -->\n      <button \n      mat-icon-button \n      class=\"mr-1\" \n      (click)=\"toggleSidenav()\" \n      *ngIf=\"layoutConf.isMobile\">\n        <mat-icon>menu</mat-icon>\n      </button>\n\n    </div>\n  </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/admin-layout/admin-layout.template.html":
  /*!*************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/admin-layout/admin-layout.template.html ***!
    \*************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsLayoutsAdminLayoutAdminLayoutTemplateHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"app-admin-wrap\" [dir]='layoutConf?.dir'>\n  <!-- Header for top navigation layout -->\n  <!-- ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT -->\n  <app-header-top \n    *ngIf=\"layoutConf.navigationPos === 'top'\" \n    [notificPanel]=\"notificationPanel\">\n  </app-header-top>\n  <!-- Main Container -->\n  <mat-sidenav-container \n  [dir]='layoutConf.dir'\n  class=\"app-admin-container app-side-nav-container mat-drawer-transition sidebar-{{layoutConf?.sidebarColor}} topbar-{{layoutConf?.topbarColor}}\"\n  [ngClass]=\"adminContainerClasses\">\n  <!-- SIDEBAR -->\n  <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\n  <app-sidebar-side \n  *ngIf=\"layoutConf.navigationPos === 'side'\" \n  (mouseenter)=\"sidebarMouseenter($event)\"\n  (mouseleave)=\"sidebarMouseleave($event)\"\n  ></app-sidebar-side>\n  \n  <!-- Top navigation layout (navigation for mobile screen) -->\n  <!-- ONLY REQUIRED FOR **TOP** NAVIGATION MOBILE LAYOUT -->\n  <app-sidebar-top *ngIf=\"layoutConf.navigationPos === 'top' && layoutConf.isMobile\"></app-sidebar-top>\n\n    <!-- App content -->\n    <div class=\"main-content-wrap\" id=\"main-content-wrap\" [perfectScrollbar]=\"\" [disabled]=\"layoutConf.topbarFixed || !layoutConf.perfectScrollbar\">\n      <!-- Header for side navigation layout -->\n      <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\n      <app-header-side \n        *ngIf=\"layoutConf.navigationPos === 'side'\"\n        [notificPanel]=\"notificationPanel\">\n      </app-header-side>\n\n      <div class=\"rightside-content-hold\" id=\"rightside-content-hold\" [perfectScrollbar]=\"scrollConfig\" [disabled]=\"!layoutConf.topbarFixed || !layoutConf.perfectScrollbar\">\n        <!-- View Loader -->\n        <div class=\"view-loader\" *ngIf=\"isModuleLoading\">\n          <div class=\"spinner\">\n            <div class=\"double-bounce1 mat-bg-accent\"></div>\n            <div class=\"double-bounce2 mat-bg-primary\"></div>\n          </div>\n        </div>\n        <!-- Breadcrumb -->\n        <app-breadcrumb></app-breadcrumb>\n        <!-- View outlet -->\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n    <!-- View overlay for mobile navigation -->\n    <div class=\"sidebar-backdrop\"\n    [ngClass]=\"{'visible': layoutConf.sidebarStyle !== 'closed' && layoutConf.isMobile}\"\n    (click)=\"closeSidebar()\"></div>\n    \n    <!-- Notificaation bar -->\n    <mat-sidenav #notificationPanel mode=\"over\" class=\"\" position=\"end\">\n      <div class=\"nofication-panel\" fxLayout=\"column\">\n        <app-notifications [notificPanel]=\"notificationPanel\"></app-notifications>\n      </div>\n    </mat-sidenav>\n  </mat-sidenav-container>\n</div>\n\n\n<!-- Only for demo purpose -->\n<!-- Remove this from your production version -->\n<app-customizer></app-customizer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/auth-layout/auth-layout.component.html":
  /*!************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/auth-layout/auth-layout.component.html ***!
    \************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsLayoutsAuthLayoutAuthLayoutComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<router-outlet></router-outlet>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/notifications/notifications.component.html":
  /*!********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/notifications/notifications.component.html ***!
    \********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsNotificationsNotificationsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\n  <h6 class=\"m-0\">Notifications</h6>\n</div>\n<mat-nav-list class=\"notification-list\" role=\"list\">\n  <!-- Notification item -->\n  <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\n    <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\n    <a [routerLink]=\"[n.route || '/dashboard']\">\n      <div class=\"mat-list-text\">\n        <h4 class=\"message\">{{n.message}}</h4>\n        <small class=\"time text-muted\">{{n.time}}</small>\n      </div>\n    </a>\n  </mat-list-item>\n</mat-nav-list>\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\n  <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-side/sidebar-side.component.html":
  /*!******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-side/sidebar-side.component.html ***!
    \******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsSidebarSideSidebarSideComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"sidebar-panel\">\n    <div id=\"scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\n        <div class=\"sidebar-hold\">\n            \n            <!-- App Logo -->\n            <div class=\"branding\">\n                <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\n                <span class=\"app-logo-text\">cWork</span>\n                \n                <span style=\"margin: auto\" *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"></span>\n                <div \n                class=\"sidebar-compact-switch\"\n                [ngClass]=\"{active: layoutConf.sidebarCompactToggle}\"\n                (click)=\"toggleCollapse()\" \n                *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"><span></span></div>\n            </div>\n        \n            <!-- Sidebar user -->\n            <!-- <div class=\"app-user\">\n                <div class=\"app-user-photo\">\n                <img src=\"assets/images/face-7.jpg\" class=\"mat-elevation-z1\" alt=\"\">\n                </div>\n                <span class=\"app-user-name mb-05\">\n                <mat-icon class=\"icon-xs text-muted\">lock</mat-icon> \n                Watson Joyce\n                </span>\n               \n                <div class=\"app-user-controls\">\n                <button \n                class=\"text-muted\"\n                mat-icon-button \n                mat-xs-button\n                [matMenuTriggerFor]=\"appUserMenu\">\n                    <mat-icon>settings</mat-icon>\n                </button>\n                <button \n                class=\"text-muted\"\n                mat-icon-button \n                mat-xs-button\n                matTooltip=\"Inbox\"\n                routerLink=\"/inbox\">\n                  <mat-icon>email</mat-icon>\n                </button>\n                <button \n                class=\"text-muted\"\n                mat-icon-button \n                mat-xs-button\n                matTooltip=\"Sign Out\"\n                routerLink=\"/sessions/signin\">\n                    <mat-icon>exit_to_app</mat-icon>\n                </button>\n                <mat-menu #appUserMenu=\"matMenu\">\n                    <button mat-menu-item routerLink=\"/profile/overview\">\n                        <mat-icon>account_box</mat-icon>\n                        <span>Profile</span>\n                    </button>\n                    <button mat-menu-item routerLink=\"/profile/settings\">\n                        <mat-icon>settings</mat-icon>\n                        <span>Account Settings</span>\n                    </button>\n                    <button mat-menu-item routerLink=\"/calendar\">\n                        <mat-icon>date_range</mat-icon>\n                        <span>Calendar</span>\n                    </button>\n                    <button mat-menu-item routerLink=\"/sessions/signin\">\n                        <mat-icon>exit_to_app</mat-icon>\n                        <span>Sign out</span>\n                    </button>\n                    </mat-menu>\n                </div>\n            </div> -->\n            <!-- Navigation -->\n            <app-sidenav [items]=\"menuItems\" [hasIconMenu]=\"hasIconTypeMenuItem\" [iconMenuTitle]=\"iconTypeMenuTitle\"></app-sidenav>\n        </div>\n    </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-top/sidebar-top.component.html":
  /*!****************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-top/sidebar-top.component.html ***!
    \****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsSidebarTopSidebarTopComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"sidebar-panel\">\n  <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\n    <app-sidenav [items]=\"menuItems\"></app-sidenav>\n  </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidenav/sidenav.template.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidenav/sidenav.template.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsSidenavSidenavTemplateHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"sidenav-hold\">\n  <div class=\"icon-menu mb-1\" *ngIf=\"hasIconTypeMenuItem\">\n    <!-- Icon menu separator -->\n    <div class=\"mb-1 nav-item-sep\">\n      <mat-divider [ngStyle]=\"{margin: '0 -24px'}\"></mat-divider>\n      <span class=\"text-muted icon-menu-title\">{{iconTypeMenuTitle}}</span>\n    </div>\n    <!-- Icon menu items -->\n    <div class=\"icon-menu-item\" *ngFor=\"let item of menuItems\">\n      <button *ngIf=\"!item.disabled && item.type === 'icon'\" mat-raised-button [matTooltip]=\"item.tooltip\" routerLink=\"/{{item.state}}\"\n        routerLinkActive=\"mat-bg-primary\">\n        <mat-icon>{{item.icon}}</mat-icon>\n      </button>\n    </div>\n  </div>\n\n  <ul appDropdown class=\"sidenav\">\n    <li *ngFor=\"let item of menuItems\" appDropdownLink routerLinkActive=\"open\">\n      \n      <div class=\"nav-item-sep\" *ngIf=\"item.type === 'separator'\">\n        <mat-divider></mat-divider>\n        <span class=\"text-muted\">{{item.name | translate}}</span>\n      </div>\n      <div *ngIf=\"!item.disabled && item.type !== 'separator' && item.type !== 'icon'\" class=\"lvl1\">\n        <a routerLink=\"/{{item.state}}\" appDropdownToggle matRipple *ngIf=\"item.type === 'link'\">\n          <mat-icon>{{item.icon}}</mat-icon>\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\n          <span fxFlex></span>\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\n        </a>\n        <a [href]=\"item.state\" appDropdownToggle matRipple *ngIf=\"item.type === 'extLink'\" target=\"_blank\">\n          <mat-icon>{{item.icon}}</mat-icon>\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\n          <span fxFlex></span>\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\n        </a>\n\n        <!-- DropDown -->\n        <a *ngIf=\"item.type === 'dropDown'\" appDropdownToggle matRipple>\n          <mat-icon>{{item.icon}}</mat-icon>\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\n          <span fxFlex></span>\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\n          <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\n        </a>\n        <!-- LEVEL 2 -->\n        <ul class=\"submenu lvl2\" appDropdown *ngIf=\"item.type === 'dropDown'\">\n          <li *ngFor=\"let itemLvL2 of item.sub\" appDropdownLink routerLinkActive=\"open\">\n\n            <a routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" appDropdownToggle *ngIf=\"itemLvL2.type !== 'dropDown'\"\n              matRipple>\n              <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\n              <span fxFlex></span>\n            </a>\n\n            <a *ngIf=\"itemLvL2.type === 'dropDown'\" appDropdownToggle matRipple>\n              <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\n              <span fxFlex></span>\n              <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\n            </a>\n\n            <!-- LEVEL 3 -->\n            <ul class=\"submenu lvl3\" appDropdown *ngIf=\"itemLvL2.type === 'dropDown'\">\n              <li *ngFor=\"let itemLvL3 of itemLvL2.sub\" appDropdownLink routerLinkActive=\"open\">\n                <a routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\" appDropdownToggle\n                  matRipple>\n                  <span class=\"item-name lvl3\">{{itemLvL3.name | translate}}</span>\n                </a>\n              </li>\n            </ul>\n\n          </li>\n        </ul>\n      </div>\n    </li>\n  </ul>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search/search-input-over/search-input-over.component.html":
  /*!************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search/search-input-over/search-input-over.component.html ***!
    \************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedSearchSearchInputOverSearchInputOverComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"search-bar-wide \" [ngClass]=\"{ open: isOpen }\">\n  <button mat-icon-button class=\"search-icon-btn ml-05 mr-05\" (click)=\"open()\">\n    <mat-icon>search</mat-icon>\n  </button>\n  <div>\n    <input [formControl]=\"searchCtrl\" class=\"default-bg\" type=\"text\" [placeholder]=\"placeholder\" />\n  </div>\n  <mat-icon class=\"search-close text-muted\" (click)=\"close()\">close</mat-icon>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/services/app-loader/app-loader.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/services/app-loader/app-loader.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedServicesAppLoaderAppLoaderComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"text-center\">\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\n    <div mat-dialog-content>\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\n    </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/comingsoon/comingsoon.component.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/comingsoon/comingsoon.component.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsComingsoonComingsoonComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<h1>Feature coming soon</h1>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/contact/contact.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/contact/contact.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsContactContactComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<table class=\"table\">\n  <thead>\n    <tr>\n      <th scope=\"col\">Name</th>\n      <th scope=\"col\">Email</th>\n      <th scope=\"col\">Phone Number</th>\n      <th scope=\"col\">Address</th>\n      <th scope=\"col\">Company Name</th>\n      <th scope=\"col\">Designation</th>\n      <th scope=\"col\" >Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let item of contacts\">\n      <th scope=\"row\">{{item.contact.name}}</th>\n      <td>{{item.contact.email}}</td>\n      <td>{{item.contact.phone_number}}</td>\n      <td>{{item.contact.address}}</td>\n      <td>{{item.contact.company_name}}</td>\n      <td>{{item.contact.designation}}</td>\n      <td><button mat-raised-button color=\"red\" (click)=\"delete(item.id)\">Delete</button></td>\n      \n    </tr>\n  </tbody>\n</table>\n\n<button mat-raised-button color=\"light\" data-toggle=\"modal\" data-target=\"#exampleModal\">Add Contact</button>\n\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\" data-backdrop=\"false\" >\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add Contact</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"fix\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Name\" [(ngModel)]=\"name\">\n          </mat-form-field>\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Email\" [(ngModel)]=\"email\">\n          </mat-form-field>\n        </div>\n        <div class=\"fix\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Phone Number\" [(ngModel)]=\"phone_number\">\n          </mat-form-field>\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Address\" [(ngModel)]=\"address\">\n          </mat-form-field>\n        </div>\n        <div class=\"fix\">\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Company Name\" [(ngModel)]=\"company_name\">\n          </mat-form-field>\n          <mat-form-field class=\"example-full-width\">\n            <input matInput placeholder=\"Designation\" [(ngModel)]=\"designation\">\n          </mat-form-field>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <!-- <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button> -->\n        <button mat-raised-button data-dismiss=\"modal\" (click)=\"addContact()\">Add Contact</button>\n      </div>\n    </div>\n  </div>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/email/email.component.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/email/email.component.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsEmailEmailComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\r\n\r\n<div class=\"member-section\">\r\n<div>\r\n<mat-form-field class=\"example-full-width\">\r\n<input type=\"text\" matInput [(ngModel)]=\"add_email\" placeholder=\"Email Address\">\r\n</mat-form-field>\r\n<br>\r\n\r\n<button mat-raised-button color=\"light\" (click)=\"addListMember()\" [disabled]=\"!add_email\">Add Member</button>\r\n</div>\r\n<mat-form-field *ngIf=\"members\">\r\n    <mat-label>List</mat-label>\r\n    <mat-select>\r\n      <mat-option *ngFor=\"let element of members.members\">\r\n        <div style=\"display: flex; justify-content: space-between;\">\r\n            {{element.email_address}}\r\n\r\n            <button mat-button style=\"color: red;\" (click)=\"removeEmail(element.email_address)\" >Delete</button>\r\n        </div>\r\n      </mat-option>\r\n    </mat-select>\r\n  </mat-form-field>\r\n</div>\r\n\r\n<br>\r\n<div class=\"email-header\">\r\n<mat-form-field class=\"example-full-width el\" >\r\n<input matInput type=\"text\" placeholder=\"Subject\" [(ngModel)]=\"subject_line\">\r\n</mat-form-field>\r\n\r\n<mat-form-field class=\"example-full-width el\">\r\n<input matInput type=\"text\" placeholder=\"Email Preview\" [(ngModel)]=\"preview_text\">\r\n</mat-form-field>\r\n\r\n<mat-form-field class=\"example-full-width el\">\r\n<input matInput type=\"text\" placeholder=\"Email Title\" [(ngModel)]=\"title\">\r\n</mat-form-field>\r\n\r\n<mat-form-field class=\"example-full-width el\">\r\n<input matInput type=\"text\" placeholder=\"From Name\" [(ngModel)]=\"from_name\">\r\n</mat-form-field>\r\n\r\n<mat-form-field class=\"example-full-width el\">\r\n<input matInput type=\"text\" placeholder=\"Your Email\" [(ngModel)]=\"reply_to\">\r\n</mat-form-field>\r\n</div>\r\n\r\n<mat-form-field class=\"textareaContainer  \">\r\n<textarea matInput class=\"boxsizingBorder\" (input)=\"plain_text=$event.target.value\" name=\"email body\" id=\"\" cols=\"30\" rows=\"10\"></textarea>\r\n</mat-form-field>\r\n\r\n<br>\r\n<button mat-raised-button color=\"light\" (click)=\"runCampaign()\" [disabled]=\"isDisabled()\">Run Campaign</button>\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/emailsettings/emailsettings.component.html":
  /*!********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/emailsettings/emailsettings.component.html ***!
    \********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsEmailsettingsEmailsettingsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"content\">\n<mat-card class=\"social-card\" style=\"background-color:#f8da1a\">\n\n        <!-- <div class=\"content\"> -->\n\n        <i style=\"color:black\" class=\"fab fa-mailchimp fa-4x\"></i>\n        <br>\n        <button mat-raised-button (click)=\"mail_oauth()\">Authenticate</button>\n\n        <!-- </div> -->\n\n    </mat-card>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/task/task.component.html":
  /*!**************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/task/task.component.html ***!
    \**************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsTaskTaskComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div>\n  <h1>Tasks:</h1>\n</div>\n\n<mat-selection-list role=\"list\">\n        <mat-list-item role=\"listitem\" *ngFor=\"let item of teamtasks\">\n\n          <div class=\"item\">\n\n          \n\n          <mat-checkbox (change)=\"completeTask(item.id)\">{{item.task}}</mat-checkbox>\n            \n\n          \n          </div>\n\n        </mat-list-item>\n\n\n\n        \n</mat-selection-list>\n\n<div *ngIf=\"!defaultShow\">\n  <h3>No task remaining</h3>\n</div>\n<br>\n<br>\n<br>\n<div>\n  <h1>Completed Tasks:</h1>\n</div>\n\n<div *ngIf=\"!completeShow\">\n  <h3 style=\"margin-right: 10px;\">No tasks completed</h3>\n</div>\n\n<mat-selection-list role=\"list\">\n  <mat-list-item role=\"listitem\" *ngFor=\"let item of completed\">\n\n    <div class=\"item\">\n\n    \n\n    {{item.task.task}}\n      \n\n    \n    </div>\n\n  </mat-list-item>\n\n\n\n  \n</mat-selection-list>";
    /***/
  },

  /***/
  "./src/$$_lazy_route_resource lazy recursive":
  /*!**********************************************************!*\
    !*** ./src/$$_lazy_route_resource lazy namespace object ***!
    \**********************************************************/

  /*! no static exports found */

  /***/
  function src$$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/app.component.css":
  /*!***********************************!*\
    !*** ./src/app/app.component.css ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvYXBwLmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./shared/services/route-parts.service */
    "./src/app/shared/services/route-parts.service.ts");
    /* harmony import */


    var _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./shared/services/layout.service */
    "./src/app/shared/services/layout.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppComponent =
    /*#__PURE__*/
    function () {
      function AppComponent(title, router, activeRoute, routePartsService, themeService, layout, renderer) {
        _classCallCheck(this, AppComponent);

        this.title = title;
        this.router = router;
        this.activeRoute = activeRoute;
        this.routePartsService = routePartsService;
        this.themeService = themeService;
        this.layout = layout;
        this.renderer = renderer;
        this.appTitle = 'Egret';
        this.pageTitle = '';
      }

      _createClass(AppComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.changePageTitle(); // this.layout.applyMatTheme(this.renderer)
          // console.log('app');
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "changePageTitle",
        value: function changePageTitle() {
          var _this = this;

          this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (event) {
            return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"];
          })).subscribe(function (routeChange) {
            var routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);

            if (!routeParts.length) return _this.title.setTitle(_this.appTitle); // Extract title from parts;

            _this.pageTitle = routeParts.reverse().map(function (part) {
              return part.title;
            }).reduce(function (partA, partI) {
              return "".concat(partA, " > ").concat(partI);
            });
            _this.pageTitle += " | ".concat(_this.appTitle);

            _this.title.setTitle(_this.pageTitle);
          });
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_3__["RoutePartsService"]
      }, {
        type: _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"]
      }, {
        type: _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_6__["LayoutService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }];
    };

    AppComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-root',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./app.component.css */
      "./src/app/app.component.css")).default]
    }), __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_3__["RoutePartsService"], _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"], _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_6__["LayoutService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: HttpLoaderFactory, AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function () {
      return HttpLoaderFactory;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/fesm2015/animations.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var angular_in_memory_web_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! angular-in-memory-web-api */
    "./node_modules/angular-in-memory-web-api/index.js");
    /* harmony import */


    var _shared_inmemory_db_inmemory_db_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./shared/inmemory-db/inmemory-db.service */
    "./src/app/shared/inmemory-db/inmemory-db.service.ts");
    /* harmony import */


    var _app_routing__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./app.routing */
    "./src/app/app.routing.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./shared/shared.module */
    "./src/app/shared/shared.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/esm2015/ngx-translate-core.js");
    /* harmony import */


    var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @ngx-translate/http-loader */
    "./node_modules/@ngx-translate/http-loader/esm2015/ngx-translate-http-loader.js");
    /* harmony import */


    var _shared_services_error_handler_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./shared/services/error-handler.service */
    "./src/app/shared/services/error-handler.service.ts");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _environment__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./environment */
    "./src/app/environment.ts");
    /* harmony import */


    var _facebook_facebook_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./facebook/facebook.component */
    "./src/app/facebook/facebook.component.ts");
    /* harmony import */


    var _views_comingsoon_comingsoon_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./views/comingsoon/comingsoon.module */
    "./src/app/views/comingsoon/comingsoon.module.ts");
    /* harmony import */


    var _views_task_task_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./views/task/task.module */
    "./src/app/views/task/task.module.ts");
    /* harmony import */


    var _views_contact_contact_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./views/contact/contact.module */
    "./src/app/views/contact/contact.module.ts");
    /* harmony import */


    var _views_emailsettings_emailsettings_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ./views/emailsettings/emailsettings.module */
    "./src/app/views/emailsettings/emailsettings.module.ts");
    /* harmony import */


    var _views_email_email_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ./views/email/email.module */
    "./src/app/views/email/email.module.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }; // import { AngularFireAuthModule } from '@angular/fire/auth';
    // import { AngularFireModule } from "@angular/fire/";
    // import {AngularFireModule} from '@angular/fire'
    // AoT requires an exported function for factories


    function HttpLoaderFactory(httpClient) {
      return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_13__["TranslateHttpLoader"](httpClient);
    }

    var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
      suppressScrollX: true
    };

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_views_email_email_module__WEBPACK_IMPORTED_MODULE_23__["EmailModule"], _views_emailsettings_emailsettings_module__WEBPACK_IMPORTED_MODULE_22__["EmailsettingsModule"], _views_contact_contact_module__WEBPACK_IMPORTED_MODULE_21__["ContactModule"], _views_task_task_module__WEBPACK_IMPORTED_MODULE_20__["TaskModule"], _views_comingsoon_comingsoon_module__WEBPACK_IMPORTED_MODULE_19__["comingsoon"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_16__["AngularFireAuthModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"].forRoot({
        loader: {
          provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateLoader"],
          useFactory: HttpLoaderFactory,
          deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClient"]]
        }
      }), angular_in_memory_web_api__WEBPACK_IMPORTED_MODULE_6__["InMemoryWebApiModule"].forRoot(_shared_inmemory_db_inmemory_db_service__WEBPACK_IMPORTED_MODULE_7__["InMemoryDataService"], {
        passThruUnknownUrl: true
      }), _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(_app_routing__WEBPACK_IMPORTED_MODULE_8__["rootRouterConfig"], {
        useHash: false
      })],
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"], _facebook_facebook_component__WEBPACK_IMPORTED_MODULE_18__["FacebookComponent"]],
      providers: [{
        provide: _angular_fire__WEBPACK_IMPORTED_MODULE_15__["FirebaseOptionsToken"],
        useValue: _environment__WEBPACK_IMPORTED_MODULE_17__["environment"].firebase
      }, {
        provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"],
        useClass: _shared_services_error_handler_service__WEBPACK_IMPORTED_MODULE_14__["ErrorHandlerService"]
      }, {
        provide: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["HAMMER_GESTURE_CONFIG"],
        useClass: _angular_material__WEBPACK_IMPORTED_MODULE_4__["GestureConfig"]
      }, {
        provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PERFECT_SCROLLBAR_CONFIG"],
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/app.routing.ts":
  /*!********************************!*\
    !*** ./src/app/app.routing.ts ***!
    \********************************/

  /*! exports provided: rootRouterConfig */

  /***/
  function srcAppAppRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "rootRouterConfig", function () {
      return rootRouterConfig;
    });
    /* harmony import */


    var _shared_components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./shared/components/layouts/admin-layout/admin-layout.component */
    "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts");
    /* harmony import */


    var _shared_components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./shared/components/layouts/auth-layout/auth-layout.component */
    "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts");
    /* harmony import */


    var _shared_services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./shared/services/auth/auth.guard */
    "./src/app/shared/services/auth/auth.guard.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var rootRouterConfig = [{
      path: '',
      redirectTo: 'sessions/signin',
      pathMatch: 'full'
    }, {
      path: 'home',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | views-home-home-module */
        "views-home-home-module").then(__webpack_require__.bind(null,
        /*! ./views/home/home.module */
        "./src/app/views/home/home.module.ts")).then(function (m) {
          return m.HomeModule;
        });
      },
      data: {
        title: 'Choose A Demo'
      }
    }, {
      path: '',
      component: _shared_components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_1__["AuthLayoutComponent"],
      children: [{
        path: 'sessions',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-sessions-sessions-module */
          [__webpack_require__.e("default~views-forms-forms-module~views-sessions-sessions-module"), __webpack_require__.e("common"), __webpack_require__.e("views-sessions-sessions-module")]).then(__webpack_require__.bind(null,
          /*! ./views/sessions/sessions.module */
          "./src/app/views/sessions/sessions.module.ts")).then(function (m) {
            return m.SessionsModule;
          });
        },
        data: {
          title: 'Session'
        }
      }]
    }, {
      path: '',
      component: _shared_components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_0__["AdminLayoutComponent"],
      canActivate: [_shared_services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_2__["AuthGuard"]],
      children: [{
        path: 'email',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./views/email/email.module */
          "./src/app/views/email/email.module.ts")).then(function (m) {
            return m.EmailModule;
          });
        },
        data: {
          title: 'Email Marketing',
          breadcrumb: 'Email Marketing'
        }
      }, {
        path: 'emailsettings',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./views/emailsettings/emailsettings.module */
          "./src/app/views/emailsettings/emailsettings.module.ts")).then(function (m) {
            return m.EmailsettingsModule;
          });
        },
        data: {
          title: 'Email Settings',
          breadcrumb: 'Email Settings'
        }
      }, {
        path: 'inventory',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-inventory-inventory-module */
          "views-inventory-inventory-module").then(__webpack_require__.bind(null,
          /*! ./views/inventory/inventory.module */
          "./src/app/views/inventory/inventory.module.ts")).then(function (m) {
            return m.InventoryModule;
          });
        },
        data: {
          title: 'Inventory',
          breadcrumb: 'Service'
        }
      }, {
        path: 'facebook',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-facebook-facebook-module */
          "views-facebook-facebook-module").then(__webpack_require__.bind(null,
          /*! ./views/facebook/facebook.module */
          "./src/app/views/facebook/facebook.module.ts")).then(function (m) {
            return m.FacebookModule;
          });
        },
        data: {
          title: 'Facebook',
          breadcrumb: 'Administration'
        }
      }, {
        path: 'contact',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./views/contact/contact.module */
          "./src/app/views/contact/contact.module.ts")).then(function (m) {
            return m.ContactModule;
          });
        },
        data: {
          title: 'Contact',
          breadcrumb: 'Contact'
        }
      }, {
        path: 'tasks',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./views/task/task.module */
          "./src/app/views/task/task.module.ts")).then(function (m) {
            return m.TaskModule;
          });
        },
        data: {
          title: '',
          breadcrumb: 'Tasks'
        }
      }, {
        path: 'comingsoon',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./views/comingsoon/comingsoon.module */
          "./src/app/views/comingsoon/comingsoon.module.ts")).then(function (m) {
            return m.comingsoon;
          });
        },
        data: {
          title: '',
          breadcrumb: 'comingsoon'
        }
      }, {
        path: 'reservation',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-reservation-reservation-module */
          "views-reservation-reservation-module").then(__webpack_require__.bind(null,
          /*! ./views/reservation/reservation.module */
          "./src/app/views/reservation/reservation.module.ts")).then(function (m) {
            return m.ReservationModule;
          });
        },
        data: {
          title: 'Reservation',
          breadcrumb: 'Service'
        }
      }, {
        path: 'postmanagement',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-post-management-post-management-module */
          "views-post-management-post-management-module").then(__webpack_require__.bind(null,
          /*! ./views/post-management/post-management.module */
          "./src/app/views/post-management/post-management.module.ts")).then(function (m) {
            return m.PostManagementModule;
          });
        },
        data: {
          title: 'PostManagement',
          breadcrumb: 'Communication'
        }
      }, {
        path: 'storage',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-storage-storage-module */
          "views-storage-storage-module").then(__webpack_require__.bind(null,
          /*! ./views/storage/storage.module */
          "./src/app/views/storage/storage.module.ts")).then(function (m) {
            return m.StorageModule;
          });
        },
        data: {
          title: 'Storage',
          breadcrumb: 'Storage'
        }
      }, {
        path: 'upgrade',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-upgrade-upgrade-module */
          "views-upgrade-upgrade-module").then(__webpack_require__.bind(null,
          /*! ./views/upgrade/upgrade.module */
          "./src/app/views/upgrade/upgrade.module.ts")).then(function (m) {
            return m.UpgradeModule;
          });
        },
        data: {
          title: 'Upgrade',
          breadcrumb: 'Upgrade'
        }
      }, {
        path: 'dashboard',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-dashboard-dashboard-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("default~views-charts-charts-module~views-dashboard-dashboard-module~views-others-others-module~views~8ea793c5"), __webpack_require__.e("default~views-chart-example-view-chart-example-view-module~views-dashboard-dashboard-module"), __webpack_require__.e("common"), __webpack_require__.e("views-dashboard-dashboard-module")]).then(__webpack_require__.bind(null,
          /*! ./views/dashboard/dashboard.module */
          "./src/app/views/dashboard/dashboard.module.ts")).then(function (m) {
            return m.DashboardModule;
          });
        },
        data: {
          title: 'Dashboard',
          breadcrumb: 'DASHBOARD'
        }
      }, {
        path: 'material',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-material-example-view-material-example-view-module */
          "views-material-example-view-material-example-view-module").then(__webpack_require__.bind(null,
          /*! ./views/material-example-view/material-example-view.module */
          "./src/app/views/material-example-view/material-example-view.module.ts")).then(function (m) {
            return m.MaterialExampleViewModule;
          });
        },
        data: {
          title: 'Material',
          breadcrumb: 'MATERIAL'
        }
      }, {
        path: 'dialogs',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-app-dialogs-app-dialogs-module */
          "views-app-dialogs-app-dialogs-module").then(__webpack_require__.bind(null,
          /*! ./views/app-dialogs/app-dialogs.module */
          "./src/app/views/app-dialogs/app-dialogs.module.ts")).then(function (m) {
            return m.AppDialogsModule;
          });
        },
        data: {
          title: 'Dialogs',
          breadcrumb: 'DIALOGS'
        }
      }, {
        path: 'profile',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-profile-profile-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("default~views-charts-charts-module~views-dashboard-dashboard-module~views-others-others-module~views~8ea793c5"), __webpack_require__.e("default~views-forms-forms-module~views-others-others-module~views-profile-profile-module"), __webpack_require__.e("views-profile-profile-module")]).then(__webpack_require__.bind(null,
          /*! ./views/profile/profile.module */
          "./src/app/views/profile/profile.module.ts")).then(function (m) {
            return m.ProfileModule;
          });
        },
        data: {
          title: 'Profile',
          breadcrumb: 'PROFILE'
        }
      }, {
        path: 'others',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-others-others-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("default~views-charts-charts-module~views-dashboard-dashboard-module~views-others-others-module~views~8ea793c5"), __webpack_require__.e("default~views-forms-forms-module~views-others-others-module~views-profile-profile-module"), __webpack_require__.e("views-others-others-module")]).then(__webpack_require__.bind(null,
          /*! ./views/others/others.module */
          "./src/app/views/others/others.module.ts")).then(function (m) {
            return m.OthersModule;
          });
        },
        data: {
          title: 'Others',
          breadcrumb: 'OTHERS'
        }
      }, {
        path: 'tables',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-tables-tables-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("views-tables-tables-module")]).then(__webpack_require__.bind(null,
          /*! ./views/tables/tables.module */
          "./src/app/views/tables/tables.module.ts")).then(function (m) {
            return m.TablesModule;
          });
        },
        data: {
          title: 'Tables',
          breadcrumb: 'TABLES'
        }
      }, {
        path: 'tour',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-app-tour-app-tour-module */
          "views-app-tour-app-tour-module").then(__webpack_require__.bind(null,
          /*! ./views/app-tour/app-tour.module */
          "./src/app/views/app-tour/app-tour.module.ts")).then(function (m) {
            return m.AppTourModule;
          });
        },
        data: {
          title: 'Tour',
          breadcrumb: 'TOUR'
        }
      }, {
        path: 'forms',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-forms-forms-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("default~views-forms-forms-module~views-others-others-module~views-profile-profile-module"), __webpack_require__.e("default~views-forms-forms-module~views-sessions-sessions-module"), __webpack_require__.e("views-forms-forms-module")]).then(__webpack_require__.bind(null,
          /*! ./views/forms/forms.module */
          "./src/app/views/forms/forms.module.ts")).then(function (m) {
            return m.AppFormsModule;
          });
        },
        data: {
          title: 'Forms',
          breadcrumb: 'FORMS'
        }
      }, {
        path: 'chart',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-chart-example-view-chart-example-view-module */
          [__webpack_require__.e("default~views-chart-example-view-chart-example-view-module~views-dashboard-dashboard-module"), __webpack_require__.e("views-chart-example-view-chart-example-view-module")]).then(__webpack_require__.bind(null,
          /*! ./views/chart-example-view/chart-example-view.module */
          "./src/app/views/chart-example-view/chart-example-view.module.ts")).then(function (m) {
            return m.ChartExampleViewModule;
          });
        },
        data: {
          title: 'Charts',
          breadcrumb: 'CHARTS'
        }
      }, {
        path: 'charts',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-charts-charts-module */
          [__webpack_require__.e("default~views-charts-charts-module~views-dashboard-dashboard-module~views-others-others-module~views~8ea793c5"), __webpack_require__.e("views-charts-charts-module")]).then(__webpack_require__.bind(null,
          /*! ./views/charts/charts.module */
          "./src/app/views/charts/charts.module.ts")).then(function (m) {
            return m.AppChartsModule;
          });
        },
        data: {
          title: 'Charts',
          breadcrumb: 'CHARTS'
        }
      }, {
        path: 'map',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-map-map-module */
          "views-map-map-module").then(__webpack_require__.bind(null,
          /*! ./views/map/map.module */
          "./src/app/views/map/map.module.ts")).then(function (m) {
            return m.AppMapModule;
          });
        },
        data: {
          title: 'Map',
          breadcrumb: 'MAP'
        }
      }, {
        path: 'dragndrop',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-dragndrop-dragndrop-module */
          "views-dragndrop-dragndrop-module").then(__webpack_require__.bind(null,
          /*! ./views/dragndrop/dragndrop.module */
          "./src/app/views/dragndrop/dragndrop.module.ts")).then(function (m) {
            return m.DragndropModule;
          });
        },
        data: {
          title: 'Drag and Drop',
          breadcrumb: 'DND'
        }
      }, {
        path: 'inbox',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-app-inbox-app-inbox-module */
          "views-app-inbox-app-inbox-module").then(__webpack_require__.bind(null,
          /*! ./views/app-inbox/app-inbox.module */
          "./src/app/views/app-inbox/app-inbox.module.ts")).then(function (m) {
            return m.AppInboxModule;
          });
        },
        data: {
          title: 'Inbox',
          breadcrumb: 'INBOX'
        }
      }, {
        path: 'calendar',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-app-calendar-app-calendar-module */
          [__webpack_require__.e("common"), __webpack_require__.e("views-app-calendar-app-calendar-module")]).then(__webpack_require__.bind(null,
          /*! ./views/app-calendar/app-calendar.module */
          "./src/app/views/app-calendar/app-calendar.module.ts")).then(function (m) {
            return m.AppCalendarModule;
          });
        },
        data: {
          title: 'Calendar',
          breadcrumb: 'CALENDAR'
        }
      }, {
        path: 'chat',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-app-chats-app-chats-module */
          "views-app-chats-app-chats-module").then(__webpack_require__.bind(null,
          /*! ./views/app-chats/app-chats.module */
          "./src/app/views/app-chats/app-chats.module.ts")).then(function (m) {
            return m.AppChatsModule;
          });
        },
        data: {
          title: 'Chat',
          breadcrumb: 'CHAT'
        }
      }, {
        path: 'cruds',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-cruds-cruds-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("common"), __webpack_require__.e("views-cruds-cruds-module")]).then(__webpack_require__.bind(null,
          /*! ./views/cruds/cruds.module */
          "./src/app/views/cruds/cruds.module.ts")).then(function (m) {
            return m.CrudsModule;
          });
        },
        data: {
          title: 'CRUDs',
          breadcrumb: 'CRUDs'
        }
      }, {
        path: 'shop',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-shop-shop-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("common"), __webpack_require__.e("views-shop-shop-module")]).then(__webpack_require__.bind(null,
          /*! ./views/shop/shop.module */
          "./src/app/views/shop/shop.module.ts")).then(function (m) {
            return m.ShopModule;
          });
        },
        data: {
          title: 'Shop',
          breadcrumb: 'SHOP'
        }
      }, {
        path: 'search',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | views-search-view-search-view-module */
          [__webpack_require__.e("default~views-cruds-cruds-module~views-dashboard-dashboard-module~views-forms-forms-module~views-oth~663a73ec"), __webpack_require__.e("views-search-view-search-view-module")]).then(__webpack_require__.bind(null,
          /*! ./views/search-view/search-view.module */
          "./src/app/views/search-view/search-view.module.ts")).then(function (m) {
            return m.SearchViewModule;
          });
        }
      }, {
        path: 'invoice',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-invoice-invoice-module */
          "views-invoice-invoice-module").then(__webpack_require__.bind(null,
          /*! ./views/invoice/invoice.module */
          "./src/app/views/invoice/invoice.module.ts")).then(function (m) {
            return m.InvoiceModule;
          });
        }
      }, {
        path: 'todo',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-todo-todo-module */
          "views-todo-todo-module").then(__webpack_require__.bind(null,
          /*! ./views/todo/todo.module */
          "./src/app/views/todo/todo.module.ts")).then(function (m) {
            return m.TodoModule;
          });
        }
      }, {
        path: 'orders',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-order-order-module */
          "views-order-order-module").then(__webpack_require__.bind(null,
          /*! ./views/order/order.module */
          "./src/app/views/order/order.module.ts")).then(function (m) {
            return m.OrderModule;
          });
        },
        data: {
          title: 'Orders',
          breadcrumb: 'Orders'
        }
      }, {
        path: 'page-layouts',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-page-layouts-page-layouts-module */
          "views-page-layouts-page-layouts-module").then(__webpack_require__.bind(null,
          /*! ./views/page-layouts/page-layouts.module */
          "./src/app/views/page-layouts/page-layouts.module.ts")).then(function (m) {
            return m.PageLayoutsModule;
          });
        }
      }, {
        path: 'utilities',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-utilities-utilities-module */
          "views-utilities-utilities-module").then(__webpack_require__.bind(null,
          /*! ./views/utilities/utilities.module */
          "./src/app/views/utilities/utilities.module.ts")).then(function (m) {
            return m.UtilitiesModule;
          });
        }
      }, {
        path: 'icons',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | views-mat-icons-mat-icons-module */
          "views-mat-icons-mat-icons-module").then(__webpack_require__.bind(null,
          /*! ./views/mat-icons/mat-icons.module */
          "./src/app/views/mat-icons/mat-icons.module.ts")).then(function (m) {
            return m.MatIconsModule;
          });
        },
        data: {
          title: 'Icons',
          breadcrumb: 'MATICONS'
        }
      }]
    }, {
      path: '**',
      redirectTo: 'sessions/404'
    }];
    /***/
  },

  /***/
  "./src/app/auth.service.ts":
  /*!*********************************!*\
    !*** ./src/app/auth.service.ts ***!
    \*********************************/

  /*! exports provided: AuthService */

  /***/
  function srcAppAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthService", function () {
      return AuthService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AuthService =
    /*#__PURE__*/
    function () {
      function AuthService(angularFireAuth, client, router) {
        _classCallCheck(this, AuthService);

        this.angularFireAuth = angularFireAuth;
        this.client = client;
        this.router = router;
        this.wrong_pass = false;
        this.userData = angularFireAuth.authState;
      } // let httpOptions = 


      _createClass(AuthService, [{
        key: "CuserSignUp",
        value: function CuserSignUp(email, password, body) {
          var _this2 = this;

          var new_body = JSON.stringify(body);
          console.log(new_body);
          var id;
          this.client.post('https://agile-cove-96115.herokuapp.com/cuser/cusers/', new_body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          }).subscribe(function (res) {
            // console.log(res.id)
            _this2.AddToTeam(email);

            id = res['id'];

            _this2.SignUp(email, password, id);
          });
        }
      }, {
        key: "FirebaseSignUp",
        value: function FirebaseSignUp(body) {
          var new_body = JSON.stringify(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/cuser/firebaseuser/', new_body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          }).subscribe(function (res) {
            console.log(res); // this.angularFireAuth.auth.signInWithEmailLink(body['email'])
          });
        } // FirebaseUserSignUp()

      }, {
        key: "AddToTeam",
        value: function AddToTeam(email) {
          var body = {
            "email": email,
            "role": ""
          };
          var new_body = JSON.stringify(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/invitation/team/', new_body, {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          }).subscribe(function (res) {
            return console.log(res);
          });
        }
        /* Sign up */

      }, {
        key: "SignUp",
        value: function SignUp(email, password, id) {
          var _this3 = this;

          var uid;
          this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password).then(function (res) {
            console.log('Successfully signed up!', res);
            console.log(res.user.uid);
            uid = res.user.uid;
            console.log("THIS IS ID", id);
            console.log("This is UID", uid);
            var new_body = {
              "cuser": id,
              "firebase_uid": res.user.uid
            };

            _this3.FirebaseSignUp(new_body);

            return uid;
          }).catch(function (error) {
            console.log('Something is wrong:', error.message);
          });
        }
        /* Sign in */

      }, {
        key: "SignIn",
        value: function SignIn(email, password) {
          var _this4 = this;

          this.angularFireAuth.auth.signInWithEmailAndPassword(email, password).then(function (res) {
            // console.log('Successfully signed in!');
            // console.log(res.user.uid)
            var body = JSON.stringify({
              "uid": res.user.uid
            });
            console.log(body);

            _this4.client.post('https://agile-cove-96115.herokuapp.com/cuser/login', body, {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            }).subscribe(function (res) {
              console.log(res);
              localStorage.clear();
              sessionStorage.clear();
              localStorage.setItem('Token', res['token']), localStorage.setItem('verified', res['verified']); // console.log(localStorage)

              if (localStorage.getItem('verified') == "true") {
                localStorage.setItem('isSignedIn', "true");

                _this4.router.navigateByUrl('/dashboard');
              } else {
                console.log(localStorage.getItem('verified'));
              }

              console.log("FLAG");
              console.log(localStorage);
            });
          }).catch(function (err) {
            console.log('Something is wrong:', err.message); // localStorage.setItem("wrong","true")

            _this4.wrong_pass = true;
          });
        }
        /* Sign out */

      }, {
        key: "SignOut",
        value: function SignOut() {
          localStorage.removeItem('Token');
          localStorage.setItem('isSignedIn', "false");
          localStorage.removeItem('FB');
          localStorage.clear();
          console.log("THIS IS LOCAL STORAGE");
          console.log(localStorage);
          console.log(sessionStorage);
          console.log();
          this.angularFireAuth.auth.signOut();
          localStorage.removeItem('Token');
          localStorage.setItem('isSignedIn', "false");
          localStorage.removeItem('FB');
          localStorage.clear();
        }
      }, {
        key: "ResetPassword",
        value: function ResetPassword(email) {
          this.angularFireAuth.auth.sendPasswordResetEmail(email);
        }
      }]);

      return AuthService;
    }();

    AuthService.ctorParameters = function () {
      return [{
        type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }];
    };

    AuthService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: 'root'
    }), __metadata("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])], AuthService);
    /***/
  },

  /***/
  "./src/app/environment.ts":
  /*!********************************!*\
    !*** ./src/app/environment.ts ***!
    \********************************/

  /*! exports provided: environment */

  /***/
  function srcAppEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var environment = {
      firebase: {
        apiKey: "AIzaSyCpENRtd-xU_iRD3N8u6mPt_SyvUGY1Qak",
        authDomain: "cworkva.firebaseapp.com",
        databaseURL: "https://cworkva.firebaseio.com",
        projectId: "cworkva",
        storageBucket: "cworkva.appspot.com",
        messagingSenderId: "944036100209",
        appId: "1:944036100209:web:2ba19f39dfa1fda85983d0"
      }
    };
    /***/
  },

  /***/
  "./src/app/facebook/facebook.component.scss":
  /*!**************************************************!*\
    !*** ./src/app/facebook/facebook.component.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppFacebookFacebookComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvZmFjZWJvb2svZmFjZWJvb2suY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/facebook/facebook.component.ts":
  /*!************************************************!*\
    !*** ./src/app/facebook/facebook.component.ts ***!
    \************************************************/

  /*! exports provided: FacebookComponent */

  /***/
  function srcAppFacebookFacebookComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FacebookComponent", function () {
      return FacebookComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FacebookComponent =
    /*#__PURE__*/
    function () {
      function FacebookComponent() {
        _classCallCheck(this, FacebookComponent);
      }

      _createClass(FacebookComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return FacebookComponent;
    }();

    FacebookComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-facebook',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./facebook.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/facebook/facebook.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./facebook.component.scss */
      "./src/app/facebook/facebook.component.scss")).default]
    }), __metadata("design:paramtypes", [])], FacebookComponent);
    /***/
  },

  /***/
  "./src/app/httpOptions.ts":
  /*!********************************!*\
    !*** ./src/app/httpOptions.ts ***!
    \********************************/

  /*! exports provided: httpOptions */

  /***/
  function srcAppHttpOptionsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "httpOptions", function () {
      return httpOptions;
    });
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    function httpOptions(token) {
      return {
        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
          'Content-Type': 'application/json',
          'Authorization': 'token ' + localStorage.getItem('Token')
        })
      };
    }
    /***/

  },

  /***/
  "./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.scss":
  /*!****************************************************************************************!*\
    !*** ./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.scss ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsBottomSheetShareBottomSheetShareComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvYm90dG9tLXNoZWV0LXNoYXJlL2JvdHRvbS1zaGVldC1zaGFyZS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.ts":
  /*!**************************************************************************************!*\
    !*** ./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.ts ***!
    \**************************************************************************************/

  /*! exports provided: BottomSheetShareComponent */

  /***/
  function srcAppSharedComponentsBottomSheetShareBottomSheetShareComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BottomSheetShareComponent", function () {
      return BottomSheetShareComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var BottomSheetShareComponent =
    /*#__PURE__*/
    function () {
      function BottomSheetShareComponent(bottomSheetRef) {
        _classCallCheck(this, BottomSheetShareComponent);

        this.bottomSheetRef = bottomSheetRef;
      }

      _createClass(BottomSheetShareComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "openLink",
        value: function openLink(event) {
          this.bottomSheetRef.dismiss();
        }
      }]);

      return BottomSheetShareComponent;
    }();

    BottomSheetShareComponent.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatBottomSheetRef"]
      }];
    };

    BottomSheetShareComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-bottom-sheet-share',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./bottom-sheet-share.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./bottom-sheet-share.component.scss */
      "./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatBottomSheetRef"]])], BottomSheetShareComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/breadcrumb/breadcrumb.component.scss":
  /*!************************************************************************!*\
    !*** ./src/app/shared/components/breadcrumb/breadcrumb.component.scss ***!
    \************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsBreadcrumbBreadcrumbComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvYnJlYWRjcnVtYi9icmVhZGNydW1iLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/shared/components/breadcrumb/breadcrumb.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/components/breadcrumb/breadcrumb.component.ts ***!
    \**********************************************************************/

  /*! exports provided: BreadcrumbComponent */

  /***/
  function srcAppSharedComponentsBreadcrumbBreadcrumbComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BreadcrumbComponent", function () {
      return BreadcrumbComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shared/services/route-parts.service */
    "./src/app/shared/services/route-parts.service.ts");
    /* harmony import */


    var _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shared/services/layout.service */
    "./src/app/shared/services/layout.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var BreadcrumbComponent =
    /*#__PURE__*/
    function () {
      // public isEnabled: boolean = true;
      function BreadcrumbComponent(router, routePartsService, activeRoute, layout) {
        var _this5 = this;

        _classCallCheck(this, BreadcrumbComponent);

        this.router = router;
        this.routePartsService = routePartsService;
        this.activeRoute = activeRoute;
        this.layout = layout;
        this.routerEventSub = this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (event) {
          return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"];
        })).subscribe(function (routeChange) {
          _this5.routeParts = _this5.routePartsService.generateRouteParts(_this5.activeRoute.snapshot); // generate url from parts

          _this5.routeParts.reverse().map(function (item, i) {
            item.breadcrumb = _this5.parseText(item);
            item.urlSegments.forEach(function (urlSegment, j) {
              if (j === 0) return item.url = "".concat(urlSegment.path);
              item.url += "/".concat(urlSegment.path);
            });

            if (i === 0) {
              return item;
            } // prepend previous part to current part


            item.url = "".concat(_this5.routeParts[i - 1].url, "/").concat(item.url);
            return item;
          });
        });
      }

      _createClass(BreadcrumbComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
          }
        }
      }, {
        key: "parseText",
        value: function parseText(part) {
          if (!part.breadcrumb) {
            return '';
          }

          part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
            var r = part.params[b];
            return typeof r === 'string' ? r : a;
          });
          return part.breadcrumb;
        }
      }]);

      return BreadcrumbComponent;
    }();

    BreadcrumbComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }, {
        type: _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_2__["RoutePartsService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]
      }, {
        type: _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__["LayoutService"]
      }];
    };

    BreadcrumbComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-breadcrumb',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./breadcrumb.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/breadcrumb/breadcrumb.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./breadcrumb.component.scss */
      "./src/app/shared/components/breadcrumb/breadcrumb.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_2__["RoutePartsService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__["LayoutService"]])], BreadcrumbComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/button-loading/button-loading.component.scss":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/components/button-loading/button-loading.component.scss ***!
    \********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsButtonLoadingButtonLoadingComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvYnV0dG9uLWxvYWRpbmcvYnV0dG9uLWxvYWRpbmcuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/shared/components/button-loading/button-loading.component.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/shared/components/button-loading/button-loading.component.ts ***!
    \******************************************************************************/

  /*! exports provided: ButtonLoadingComponent */

  /***/
  function srcAppSharedComponentsButtonLoadingButtonLoadingComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ButtonLoadingComponent", function () {
      return ButtonLoadingComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ButtonLoadingComponent =
    /*#__PURE__*/
    function () {
      function ButtonLoadingComponent() {
        _classCallCheck(this, ButtonLoadingComponent);

        this.raised = true;
        this.loadingText = 'Please wait';
        this.type = 'submit';
      }

      _createClass(ButtonLoadingComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ButtonLoadingComponent;
    }();

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('loading'), __metadata("design:type", Boolean)], ButtonLoadingComponent.prototype, "loading", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('btnClass'), __metadata("design:type", String)], ButtonLoadingComponent.prototype, "btnClass", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('raised'), __metadata("design:type", Boolean)], ButtonLoadingComponent.prototype, "raised", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('loadingText'), __metadata("design:type", Object)], ButtonLoadingComponent.prototype, "loadingText", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('type'), __metadata("design:type", String)], ButtonLoadingComponent.prototype, "type", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('color'), __metadata("design:type", String)], ButtonLoadingComponent.prototype, "color", void 0);

    ButtonLoadingComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'button-loading',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./button-loading.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/button-loading/button-loading.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./button-loading.component.scss */
      "./src/app/shared/components/button-loading/button-loading.component.scss")).default]
    }), __metadata("design:paramtypes", [])], ButtonLoadingComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/customizer/customizer.component.scss":
  /*!************************************************************************!*\
    !*** ./src/app/shared/components/customizer/customizer.component.scss ***!
    \************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsCustomizerCustomizerComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.done {\n  text-decoration: line-through; }\n\n.item {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n          flex-direction: row-reverse;\n  -webkit-box-pack: space-evenly;\n          justify-content: space-evenly; }\n\ni {\n  margin-right: 7px;\n  cursor: pointer; }\n\n.handle {\n  position: fixed;\n  bottom: 30px;\n  right: 30px;\n  z-index: 99; }\n\n#app-customizer {\n  position: fixed;\n  bottom: 0px;\n  top: 0;\n  right: 0;\n  min-width: 180px;\n  max-width: 280px;\n  z-index: 999; }\n\n#app-customizer .title {\n    text-transform: uppercase;\n    font-size: 12px;\n    font-weight: bold;\n    margin: 0 0 1rem; }\n\n#app-customizer .mat-card {\n    margin: 0;\n    border-radius: 0; }\n\n#app-customizer .mat-card-content {\n    padding: 1rem 1.5rem 2rem;\n    max-height: calc(100vh - 80px); }\n\n.pos-rel {\n  position: relative;\n  z-index: 99; }\n\n.pos-rel .olay {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    background: rgba(0, 0, 0, 0.5);\n    z-index: 100; }\n\n.colors {\n  display: -webkit-box;\n  display: flex;\n  flex-wrap: wrap; }\n\n.colors .color {\n    position: relative;\n    width: 36px;\n    height: 36px;\n    display: inline-block;\n    border-radius: 50%;\n    margin: 8px;\n    text-align: center;\n    box-shadow: 0 4px 20px 1px rgba(0, 0, 0, 0.06), 0 1px 4px rgba(0, 0, 0, 0.03);\n    cursor: pointer; }\n\n.colors .color .active-icon {\n      position: absolute;\n      left: 0;\n      right: 0;\n      margin: auto;\n      top: 6px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvY3VzdG9taXplci9jdXN0b21pemVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXLEVBQUE7O0FBRWI7RUFDRSw2QkFBNkIsRUFBQTs7QUFFL0I7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw4QkFBMkI7RUFBM0IsOEJBQTJCO1VBQTNCLDJCQUEyQjtFQUMzQiw4QkFBNkI7VUFBN0IsNkJBQTZCLEVBQUE7O0FBTS9CO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFFakI7RUFDSSxlQUFlO0VBQ2YsWUFBWTtFQUNaLFdBQVc7RUFDWCxXQUFXLEVBQUE7O0FBRWY7RUFDRSxlQUFlO0VBQ2YsV0FBVztFQUNYLE1BQU07RUFDTixRQUFRO0VBQ1IsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixZQUFZLEVBQUE7O0FBUGQ7SUFTSSx5QkFBeUI7SUFDekIsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixnQkFBZ0IsRUFBQTs7QUFacEI7SUFlSSxTQUFTO0lBQ1QsZ0JBQWdCLEVBQUE7O0FBaEJwQjtJQW1CUSx5QkFBeUI7SUFDekIsOEJBQThCLEVBQUE7O0FBR3RDO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFGYjtJQUlJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE2QjtJQUM3QixZQUFZLEVBQUE7O0FBSWhCO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsZUFBZSxFQUFBOztBQUZqQjtJQUlJLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQiw2RUFBcUU7SUFDckUsZUFBZSxFQUFBOztBQVpuQjtNQWNNLGtCQUFrQjtNQUNsQixPQUFPO01BQ1AsUUFBUTtNQUNSLFlBQVk7TUFDWixRQUFRLEVBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbWl6ZXIvY3VzdG9taXplci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWZvcm0ge1xuICBtaW4td2lkdGg6IDE1MHB4O1xuICBtYXgtd2lkdGg6IDUwMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmRvbmV7XG4gIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xufVxuLml0ZW17XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG59XG5cbi8vIC5zaXple1xuLy8gICBmb250LXNpemU6IDIwcHg7XG4vLyB9XG5pe1xuICBtYXJnaW4tcmlnaHQ6IDdweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmhhbmRsZSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMzBweDtcbiAgICByaWdodDogMzBweDtcbiAgICB6LWluZGV4OiA5OTtcbn1cbiNhcHAtY3VzdG9taXplciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwcHg7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIG1pbi13aWR0aDogMTgwcHg7XG4gIG1heC13aWR0aDogMjgwcHg7XG4gIHotaW5kZXg6IDk5OTtcbiAgLnRpdGxlIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW46IDAgMCAxcmVtO1xuICB9XG4gIC5tYXQtY2FyZCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gIH1cbiAgLm1hdC1jYXJkLWNvbnRlbnQgIHtcbiAgICAgICAgcGFkZGluZzogMXJlbSAxLjVyZW0gMnJlbTtcbiAgICAgICAgbWF4LWhlaWdodDogY2FsYygxMDB2aCAtIDgwcHgpO1xuICAgIH1cbn1cbi5wb3MtcmVsIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiA5OTtcbiAgLm9sYXkge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAuNSk7XG4gICAgei1pbmRleDogMTAwO1xuICB9XG59XG5cbi5jb2xvcnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIC5jb2xvciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAzNnB4O1xuICAgIGhlaWdodDogMzZweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIG1hcmdpbjogOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3gtc2hhZG93OiAwIDRweCAyMHB4IDFweCByZ2JhKDAsMCwwLC4wNiksIDAgMXB4IDRweCByZ2JhKDAsMCwwLC4wMyk7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIC5hY3RpdmUtaWNvbiB7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiAwO1xuICAgICAgcmlnaHQ6IDA7XG4gICAgICBtYXJnaW46IGF1dG87XG4gICAgICB0b3A6IDZweDtcbiAgICB9XG4gIH1cbn1cblxuLy8gW2Rpcj1cInJ0bFwiXSB7XG4vLyAgIC5oYW5kbGUge31cbi8vICAgI2FwcC1jdXN0b21pemVyIHtcbi8vICAgICByaWdodDogYXV0bztcbi8vICAgICBsZWZ0OiAwO1xuLy8gICB9XG4vLyB9Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/components/customizer/customizer.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/components/customizer/customizer.component.ts ***!
    \**********************************************************************/

  /*! exports provided: CustomizerComponent */

  /***/
  function srcAppSharedComponentsCustomizerCustomizerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomizerComponent", function () {
      return CustomizerComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../shared/services/navigation.service */
    "./src/app/shared/services/navigation.service.ts");
    /* harmony import */


    var _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shared/services/layout.service */
    "./src/app/shared/services/layout.service.ts");
    /* harmony import */


    var app_shared_services_customizer_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! app/shared/services/customizer.service */
    "./src/app/shared/services/customizer.service.ts");
    /* harmony import */


    var app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! app/shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var CustomizerComponent =
    /*#__PURE__*/
    function () {
      function CustomizerComponent(navService, layout, themeService, customizer, renderer, client) {
        _classCallCheck(this, CustomizerComponent);

        this.navService = navService;
        this.layout = layout;
        this.themeService = themeService;
        this.customizer = customizer;
        this.renderer = renderer;
        this.client = client;
        this.task = "";
        this.isCustomizerOpen = false;
        this.viewMode = 'options';
        this.sidenavTypes = [{
          name: "Default Menu",
          value: "default-menu"
        }, {
          name: "Separator Menu",
          value: "separator-menu"
        }, {
          name: "Icon Menu",
          value: "icon-menu"
        }];
        this.selectedMenu = "icon-menu";
        this.isTopbarFixed = false;
        this.isRTL = false;
        this.perfectScrollbarEnabled = true;
      }

      _createClass(CustomizerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_5__["httpOptions"])('token');
          this.getTasks();
          this.layoutConf = this.layout.layoutConf;
          this.selectedLayout = this.layoutConf.navigationPos;
          this.isTopbarFixed = this.layoutConf.topbarFixed;
          this.isRTL = this.layoutConf.dir === "rtl";
          this.egretThemes = this.themeService.egretThemes;
        }
      }, {
        key: "addTask",
        value: function addTask() {
          var _this6 = this;

          console.log(this.task);
          var body = {
            task: this.task
          };
          this.client.post("https://agile-cove-96115.herokuapp.com/todolist/create", body, this.header).subscribe(function (res) {
            console.log(res);

            _this6.getTasks();
          });
          this.task = "";
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          var _this7 = this;

          console.log(id);
          var body = {
            id: id
          };
          this.client.put('https://agile-cove-96115.herokuapp.com/todolist/delete', body, this.header).subscribe(function (res) {
            console.log(res);

            _this7.getTasks();
          });
        }
      }, {
        key: "getTasks",
        value: function getTasks() {
          var _this8 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/todolist/todolist', this.header).subscribe(function (res) {
            console.log(res);
            _this8.tasks = res;
          });
        }
      }, {
        key: "open",
        value: function open() {
          this.isCustomizerOpen = true;
          this.getTasks();
        }
      }, {
        key: "changeTheme",
        value: function changeTheme(theme) {
          // this.themeService.changeTheme(theme);
          this.layout.publishLayoutChange({
            matTheme: theme.name
          });
        }
      }, {
        key: "changeLayoutStyle",
        value: function changeLayoutStyle(data) {
          this.layout.publishLayoutChange({
            navigationPos: this.selectedLayout
          });
        }
      }, {
        key: "changeSidenav",
        value: function changeSidenav(data) {
          this.navService.publishNavigationChange(data.value);
        }
      }, {
        key: "toggleBreadcrumb",
        value: function toggleBreadcrumb(data) {
          this.layout.publishLayoutChange({
            useBreadcrumb: data.checked
          });
        }
      }, {
        key: "toggleTopbarFixed",
        value: function toggleTopbarFixed(data) {
          this.layout.publishLayoutChange({
            topbarFixed: data.checked
          });
        }
      }, {
        key: "toggleDir",
        value: function toggleDir(data) {
          var dir = data.checked ? "rtl" : "ltr";
          this.layout.publishLayoutChange({
            dir: dir
          });
        }
      }, {
        key: "tooglePerfectScrollbar",
        value: function tooglePerfectScrollbar(data) {
          this.layout.publishLayoutChange({
            perfectScrollbar: this.perfectScrollbarEnabled
          });
        }
      }]);

      return CustomizerComponent;
    }();

    CustomizerComponent.ctorParameters = function () {
      return [{
        type: _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]
      }, {
        type: _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]
      }, {
        type: app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"]
      }, {
        type: app_shared_services_customizer_service__WEBPACK_IMPORTED_MODULE_3__["CustomizerService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]
      }];
    };

    CustomizerComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "app-customizer",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./customizer.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/customizer/customizer.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./customizer.component.scss */
      "./src/app/shared/components/customizer/customizer.component.scss")).default]
    }), __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"], _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"], app_shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"], app_shared_services_customizer_service__WEBPACK_IMPORTED_MODULE_3__["CustomizerService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]])], CustomizerComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/egret-sidebar/egret-sidebar-helper.service.ts":
  /*!*********************************************************************************!*\
    !*** ./src/app/shared/components/egret-sidebar/egret-sidebar-helper.service.ts ***!
    \*********************************************************************************/

  /*! exports provided: EgretSidebarHelperService */

  /***/
  function srcAppSharedComponentsEgretSidebarEgretSidebarHelperServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidebarHelperService", function () {
      return EgretSidebarHelperService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretSidebarHelperService =
    /*#__PURE__*/
    function () {
      function EgretSidebarHelperService() {
        _classCallCheck(this, EgretSidebarHelperService);

        this.sidebarList = [];
      }

      _createClass(EgretSidebarHelperService, [{
        key: "setSidebar",
        value: function setSidebar(name, sidebar) {
          this.sidebarList[name] = sidebar;
        }
      }, {
        key: "getSidebar",
        value: function getSidebar(name) {
          return this.sidebarList[name];
        }
      }, {
        key: "removeSidebar",
        value: function removeSidebar(name) {
          if (!this.sidebarList[name]) {
            console.warn("The sidebar with name '".concat(name, "' doesn't exist."));
          } // remove sidebar


          delete this.sidebarList[name];
        }
      }]);

      return EgretSidebarHelperService;
    }();

    EgretSidebarHelperService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: "root"
    }), __metadata("design:paramtypes", [])], EgretSidebarHelperService);
    /***/
  },

  /***/
  "./src/app/shared/components/egret-sidebar/egret-sidebar.component.scss":
  /*!******************************************************************************!*\
    !*** ./src/app/shared/components/egret-sidebar/egret-sidebar.component.scss ***!
    \******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsEgretSidebarEgretSidebarComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvZWdyZXQtc2lkZWJhci9lZ3JldC1zaWRlYmFyLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/shared/components/egret-sidebar/egret-sidebar.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/shared/components/egret-sidebar/egret-sidebar.component.ts ***!
    \****************************************************************************/

  /*! exports provided: EgretSidebarComponent, EgretSidebarTogglerDirective */

  /***/
  function srcAppSharedComponentsEgretSidebarEgretSidebarComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidebarComponent", function () {
      return EgretSidebarComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidebarTogglerDirective", function () {
      return EgretSidebarTogglerDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! app/shared/services/match-media.service */
    "./src/app/shared/services/match-media.service.ts");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _egret_sidebar_helper_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./egret-sidebar-helper.service */
    "./src/app/shared/components/egret-sidebar/egret-sidebar-helper.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretSidebarComponent =
    /*#__PURE__*/
    function () {
      function EgretSidebarComponent(matchMediaService, mediaObserver, sidebarHelperService, _renderer, _elementRef, cdr) {
        _classCallCheck(this, EgretSidebarComponent);

        this.matchMediaService = matchMediaService;
        this.mediaObserver = mediaObserver;
        this.sidebarHelperService = sidebarHelperService;
        this._renderer = _renderer;
        this._elementRef = _elementRef;
        this.cdr = cdr;
        this.backdrop = null;
        this.lockedBreakpoint = "gt-sm";
        this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
      }

      _createClass(EgretSidebarComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this9 = this;

          this.sidebarHelperService.setSidebar(this.name, this);

          if (this.mediaObserver.isActive(this.lockedBreakpoint)) {
            this.sidebarLockedOpen = true;
            this.opened = true;
          } else {
            this.sidebarLockedOpen = false;
            this.opened = false;
          }

          this.matchMediaService.onMediaChange.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribeAll)).subscribe(function () {
            // console.log("medua sub");
            if (_this9.mediaObserver.isActive(_this9.lockedBreakpoint)) {
              _this9.sidebarLockedOpen = true;
              _this9.opened = true;
            } else {
              _this9.sidebarLockedOpen = false;
              _this9.opened = false;
            }
          });
        }
      }, {
        key: "open",
        value: function open() {
          this.opened = true;

          if (!this.sidebarLockedOpen && !this.backdrop) {
            this.showBackdrop();
          }
        }
      }, {
        key: "close",
        value: function close() {
          this.opened = false;
          this.hideBackdrop();
        }
      }, {
        key: "toggle",
        value: function toggle() {
          if (this.opened) {
            this.close();
          } else {
            this.open();
          }
        }
      }, {
        key: "showBackdrop",
        value: function showBackdrop() {
          var _this10 = this;

          this.backdrop = this._renderer.createElement("div");
          this.backdrop.classList.add("egret-sidebar-overlay");

          this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this.backdrop); // Close sidebar onclick


          this.backdrop.addEventListener("click", function () {
            _this10.close();
          });
          this.cdr.markForCheck();
        }
      }, {
        key: "hideBackdrop",
        value: function hideBackdrop() {
          if (this.backdrop) {
            this.backdrop.parentNode.removeChild(this.backdrop);
            this.backdrop = null;
          }

          this.cdr.markForCheck();
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeAll.next();
          this.unsubscribeAll.complete();
          this.sidebarHelperService.removeSidebar(this.name);
        }
      }]);

      return EgretSidebarComponent;
    }();

    EgretSidebarComponent.ctorParameters = function () {
      return [{
        type: app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_1__["MatchMediaService"]
      }, {
        type: _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["MediaObserver"]
      }, {
        type: _egret_sidebar_helper_service__WEBPACK_IMPORTED_MODULE_5__["EgretSidebarHelperService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", String)], EgretSidebarComponent.prototype, "name", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])("class.position-right"), __metadata("design:type", Boolean)], EgretSidebarComponent.prototype, "right", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])("class.open"), __metadata("design:type", Boolean)], EgretSidebarComponent.prototype, "opened", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])("class.sidebar-locked-open"), __metadata("design:type", Boolean)], EgretSidebarComponent.prototype, "sidebarLockedOpen", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])("class.is-over"), __metadata("design:type", Boolean)], EgretSidebarComponent.prototype, "isOver", void 0);

    EgretSidebarComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "egret-sidebar",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./egret-sidebar.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/egret-sidebar/egret-sidebar.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./egret-sidebar.component.scss */
      "./src/app/shared/components/egret-sidebar/egret-sidebar.component.scss")).default]
    }), __metadata("design:paramtypes", [app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_1__["MatchMediaService"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["MediaObserver"], _egret_sidebar_helper_service__WEBPACK_IMPORTED_MODULE_5__["EgretSidebarHelperService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])], EgretSidebarComponent);

    var EgretSidebarTogglerDirective =
    /*#__PURE__*/
    function () {
      function EgretSidebarTogglerDirective(egretSidebarHelperService) {
        _classCallCheck(this, EgretSidebarTogglerDirective);

        this.egretSidebarHelperService = egretSidebarHelperService;
      }

      _createClass(EgretSidebarTogglerDirective, [{
        key: "onClick",
        value: function onClick() {
          this.egretSidebarHelperService.getSidebar(this.id).toggle();
        }
      }]);

      return EgretSidebarTogglerDirective;
    }();

    EgretSidebarTogglerDirective.ctorParameters = function () {
      return [{
        type: _egret_sidebar_helper_service__WEBPACK_IMPORTED_MODULE_5__["EgretSidebarHelperService"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("egretSidebarToggler"), __metadata("design:type", Object)], EgretSidebarTogglerDirective.prototype, "id", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])("click"), __metadata("design:type", Function), __metadata("design:paramtypes", []), __metadata("design:returntype", void 0)], EgretSidebarTogglerDirective.prototype, "onClick", null);

    EgretSidebarTogglerDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: "[egretSidebarToggler]"
    }), __metadata("design:paramtypes", [_egret_sidebar_helper_service__WEBPACK_IMPORTED_MODULE_5__["EgretSidebarHelperService"]])], EgretSidebarTogglerDirective);
    /***/
  },

  /***/
  "./src/app/shared/components/example-viewer-template/example-viewer-template.component.scss":
  /*!**************************************************************************************************!*\
    !*** ./src/app/shared/components/example-viewer-template/example-viewer-template.component.scss ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsExampleViewerTemplateExampleViewerTemplateComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvZXhhbXBsZS12aWV3ZXItdGVtcGxhdGUvZXhhbXBsZS12aWV3ZXItdGVtcGxhdGUuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/shared/components/example-viewer-template/example-viewer-template.component.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/shared/components/example-viewer-template/example-viewer-template.component.ts ***!
    \************************************************************************************************/

  /*! exports provided: EgretExampleViewerTemplateComponent */

  /***/
  function srcAppSharedComponentsExampleViewerTemplateExampleViewerTemplateComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretExampleViewerTemplateComponent", function () {
      return EgretExampleViewerTemplateComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretExampleViewerTemplateComponent =
    /*#__PURE__*/
    function () {
      function EgretExampleViewerTemplateComponent(route) {
        _classCallCheck(this, EgretExampleViewerTemplateComponent);

        this.route = route;
        this.exampleComponents = {};
        this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
      }

      _createClass(EgretExampleViewerTemplateComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this11 = this;

          Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(this.route.params, this.route.data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribeAll)).subscribe(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                params = _ref2[0],
                data = _ref2[1];

            _this11.id = params["id"];
            _this11.examples = data.map[_this11.id];
            _this11.exampleComponents = data.components;
            _this11.componentDirPath = data.path;

            var title = _this11.id.replace("-", " ");

            _this11.title = title.charAt(0).toUpperCase() + title.substring(1); // console.log(params, data);
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeAll.next();
          this.unsubscribeAll.complete();
        }
      }]);

      return EgretExampleViewerTemplateComponent;
    }();

    EgretExampleViewerTemplateComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]
      }];
    };

    EgretExampleViewerTemplateComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "egret-example-viewer-template",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./example-viewer-template.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer-template/example-viewer-template.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./example-viewer-template.component.scss */
      "./src/app/shared/components/example-viewer-template/example-viewer-template.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])], EgretExampleViewerTemplateComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/example-viewer/example-viewer.component.scss":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/components/example-viewer/example-viewer.component.scss ***!
    \********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedComponentsExampleViewerExampleViewerComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvc2hhcmVkL2NvbXBvbmVudHMvZXhhbXBsZS12aWV3ZXIvZXhhbXBsZS12aWV3ZXIuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/shared/components/example-viewer/example-viewer.component.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/shared/components/example-viewer/example-viewer.component.ts ***!
    \******************************************************************************/

  /*! exports provided: EgretExampleViewerComponent */

  /***/
  function srcAppSharedComponentsExampleViewerExampleViewerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretExampleViewerComponent", function () {
      return EgretExampleViewerComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }; // import { EXAMPLE_COMPONENTS } from "assets/examples/examples";


    var EgretExampleViewerComponent =
    /*#__PURE__*/
    function () {
      function EgretExampleViewerComponent(cfr) {
        _classCallCheck(this, EgretExampleViewerComponent);

        this.cfr = cfr;
      } // Component ID


      _createClass(EgretExampleViewerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.componentPath = this.path + this.exampleId + '/' + this.exampleId + '.component';
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          if (!this.data) {
            console.log('EXAMPLE COMPONENT MISSING');
            return;
          }

          var componentFactory = this.cfr.resolveComponentFactory(this.data.component);
          this.exampleViewRef = this.exampleContainer.createComponent(componentFactory);
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.exampleViewRef) {
            this.exampleViewRef.destroy();
          }
        }
      }, {
        key: "exampleId",
        set: function set(exampleId) {
          if (exampleId) {
            this._exampleId = exampleId;
          } else {
            console.log("EXAMPLE ID MISSING");
          }
        },
        get: function get() {
          return this._exampleId;
        }
      }]);

      return EgretExampleViewerComponent;
    }();

    EgretExampleViewerComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("exampleId"), __metadata("design:type", String), __metadata("design:paramtypes", [String])], EgretExampleViewerComponent.prototype, "exampleId", null);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('data'), __metadata("design:type", Object)], EgretExampleViewerComponent.prototype, "data", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('path'), __metadata("design:type", Object)], EgretExampleViewerComponent.prototype, "path", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('exampleContainer', {
      read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"],
      static: false
    }), __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"])], EgretExampleViewerComponent.prototype, "exampleContainer", void 0);

    EgretExampleViewerComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "egret-example-viewer",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./example-viewer.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/example-viewer/example-viewer.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./example-viewer.component.scss */
      "./src/app/shared/components/example-viewer/example-viewer.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"]])], EgretExampleViewerComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/header-side/header-side.component.ts":
  /*!************************************************************************!*\
    !*** ./src/app/shared/components/header-side/header-side.component.ts ***!
    \************************************************************************/

  /*! exports provided: HeaderSideComponent */

  /***/
  function srcAppSharedComponentsHeaderSideHeaderSideComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderSideComponent", function () {
      return HeaderSideComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_theme_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var _services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/layout.service */
    "./src/app/shared/services/layout.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/esm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../auth.service */
    "./src/app/auth.service.ts");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var HeaderSideComponent =
    /*#__PURE__*/
    function () {
      function HeaderSideComponent(themeService, layout, translate, renderer, authService, router, client) {
        _classCallCheck(this, HeaderSideComponent);

        this.themeService = themeService;
        this.layout = layout;
        this.translate = translate;
        this.renderer = renderer;
        this.authService = authService;
        this.router = router;
        this.client = client;
        this.availableLangs = [{
          name: 'EN',
          code: 'en',
          flag: 'flag-icon-us'
        }, {
          name: 'ES',
          code: 'es',
          flag: 'flag-icon-es'
        }];
        this.currentLang = this.availableLangs[0];
      }

      _createClass(HeaderSideComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.egretThemes = this.themeService.egretThemes;
          this.layoutConf = this.layout.layoutConf;
          this.translate.use(this.currentLang.code);
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_6__["httpOptions"])(localStorage.getItem('Token'));
          this.getUser();
        }
      }, {
        key: "setLang",
        value: function setLang(lng) {
          this.currentLang = lng;
          this.translate.use(lng.code);
        }
      }, {
        key: "changeTheme",
        value: function changeTheme(theme) {// this.themeService.changeTheme(theme);
        }
      }, {
        key: "toggleNotific",
        value: function toggleNotific() {
          this.notificPanel.toggle();
        }
      }, {
        key: "toggleSidenav",
        value: function toggleSidenav() {
          if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
              sidebarStyle: 'full'
            });
          }

          this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
          });
        }
      }, {
        key: "toggleCollapse",
        value: function toggleCollapse() {
          // compact --> full
          if (this.layoutConf.sidebarStyle === 'compact') {
            return this.layout.publishLayoutChange({
              sidebarStyle: 'full',
              sidebarCompactToggle: false
            }, {
              transitionClass: true
            });
          } // * --> compact


          this.layout.publishLayoutChange({
            sidebarStyle: 'compact',
            sidebarCompactToggle: true
          }, {
            transitionClass: true
          });
        }
      }, {
        key: "onSearch",
        value: function onSearch(e) {//   console.log(e)
        }
      }, {
        key: "getUser",
        value: function getUser() {
          var _this12 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/cuser/getUser', this.header).subscribe(function (res) {
            console.log(res[0]['user']['first_name']);
            _this12.name = res[0]['user']['first_name'] + " " + res[0]['user']['last_name'];
          });
        }
      }, {
        key: "SignOut",
        value: function SignOut() {
          this.authService.SignOut();
          this.router.navigateByUrl('/sessions/signin');
        }
      }]);

      return HeaderSideComponent;
    }();

    HeaderSideComponent.ctorParameters = function () {
      return [{
        type: _services_theme_service__WEBPACK_IMPORTED_MODULE_1__["ThemeService"]
      }, {
        type: _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Object)], HeaderSideComponent.prototype, "notificPanel", void 0);

    HeaderSideComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-header-side',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./header-side.template.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-side/header-side.template.html")).default
    }), __metadata("design:paramtypes", [_services_theme_service__WEBPACK_IMPORTED_MODULE_1__["ThemeService"], _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"], _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClient"]])], HeaderSideComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/header-top/header-top.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/components/header-top/header-top.component.ts ***!
    \**********************************************************************/

  /*! exports provided: HeaderTopComponent */

  /***/
  function srcAppSharedComponentsHeaderTopHeaderTopComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderTopComponent", function () {
      return HeaderTopComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../shared/services/navigation.service */
    "./src/app/shared/services/navigation.service.ts");
    /* harmony import */


    var _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shared/services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/esm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_layout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/layout.service */
    "./src/app/shared/services/layout.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var HeaderTopComponent =
    /*#__PURE__*/
    function () {
      function HeaderTopComponent(layout, navService, themeService, translate, renderer) {
        _classCallCheck(this, HeaderTopComponent);

        this.layout = layout;
        this.navService = navService;
        this.themeService = themeService;
        this.translate = translate;
        this.renderer = renderer;
        this.egretThemes = [];
        this.currentLang = 'en';
        this.availableLangs = [{
          name: 'English',
          code: 'en'
        }, {
          name: 'Spanish',
          code: 'es'
        }];
      }

      _createClass(HeaderTopComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this13 = this;

          this.layoutConf = this.layout.layoutConf;
          this.egretThemes = this.themeService.egretThemes;
          this.menuItemSub = this.navService.menuItems$.subscribe(function (res) {
            res = res.filter(function (item) {
              return item.type !== 'icon' && item.type !== 'separator';
            });
            var limit = 4;
            var mainItems = res.slice(0, limit);

            if (res.length <= limit) {
              return _this13.menuItems = mainItems;
            }

            var subItems = res.slice(limit, res.length - 1);
            mainItems.push({
              name: 'More',
              type: 'dropDown',
              tooltip: 'More',
              icon: 'more_horiz',
              sub: subItems
            });
            _this13.menuItems = mainItems;
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.menuItemSub.unsubscribe();
        }
      }, {
        key: "setLang",
        value: function setLang() {
          this.translate.use(this.currentLang);
        }
      }, {
        key: "changeTheme",
        value: function changeTheme(theme) {
          this.layout.publishLayoutChange({
            matTheme: theme.name
          });
        }
      }, {
        key: "toggleNotific",
        value: function toggleNotific() {
          this.notificPanel.toggle();
        }
      }, {
        key: "toggleSidenav",
        value: function toggleSidenav() {
          if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
              sidebarStyle: 'full'
            });
          }

          this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
          });
        }
      }]);

      return HeaderTopComponent;
    }();

    HeaderTopComponent.ctorParameters = function () {
      return [{
        type: _services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"]
      }, {
        type: _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]
      }, {
        type: _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Object)], HeaderTopComponent.prototype, "notificPanel", void 0);

    HeaderTopComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-header-top',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./header-top.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/header-top/header-top.component.html")).default
    }), __metadata("design:paramtypes", [_services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"], _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"], _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])], HeaderTopComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts":
  /*!**********************************************************************************!*\
    !*** ./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts ***!
    \**********************************************************************************/

  /*! exports provided: AdminLayoutComponent */

  /***/
  function srcAppSharedComponentsLayoutsAdminLayoutAdminLayoutComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function () {
      return AdminLayoutComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/esm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_theme_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var _services_layout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/layout.service */
    "./src/app/shared/services/layout.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AdminLayoutComponent =
    /*#__PURE__*/
    function () {
      function AdminLayoutComponent(router, translate, themeService, layout, cdr) {
        var _this14 = this;

        _classCallCheck(this, AdminLayoutComponent);

        this.router = router;
        this.translate = translate;
        this.themeService = themeService;
        this.layout = layout;
        this.cdr = cdr;
        this.isModuleLoading = false;
        this.scrollConfig = {};
        this.layoutConf = {};
        this.adminContainerClasses = {}; // Close sidenav after route change in mobile

        this.routerEventSub = router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (event) {
          return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"];
        })).subscribe(function (routeChange) {
          _this14.layout.adjustLayout({
            route: routeChange.url
          });

          _this14.scrollToTop();
        }); // Translator init

        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
      }

      _createClass(AdminLayoutComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this15 = this;

          // this.layoutConf = this.layout.layoutConf;
          this.layoutConfSub = this.layout.layoutConf$.subscribe(function (layoutConf) {
            _this15.layoutConf = layoutConf;
            _this15.adminContainerClasses = _this15.updateAdminContainerClasses(_this15.layoutConf);

            _this15.cdr.markForCheck();
          }); // FOR MODULE LOADER FLAG

          this.moduleLoaderSub = this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteConfigLoadStart"] || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["ResolveStart"]) {
              _this15.isModuleLoading = true;
            }

            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteConfigLoadEnd"] || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["ResolveEnd"]) {
              _this15.isModuleLoading = false;
            }
          });
        }
      }, {
        key: "onResize",
        value: function onResize(event) {
          this.layout.adjustLayout(event);
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "scrollToTop",
        value: function scrollToTop() {
          var _this16 = this;

          if (document) {
            setTimeout(function () {
              var element;

              if (_this16.layoutConf.topbarFixed) {
                element = document.querySelector('#rightside-content-hold');
              } else {
                element = document.querySelector('#main-content-wrap');
              }

              element.scrollTop = 0;
            });
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.moduleLoaderSub) {
            this.moduleLoaderSub.unsubscribe();
          }

          if (this.layoutConfSub) {
            this.layoutConfSub.unsubscribe();
          }

          if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
          }
        }
      }, {
        key: "closeSidebar",
        value: function closeSidebar() {
          this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
          });
        }
      }, {
        key: "sidebarMouseenter",
        value: function sidebarMouseenter(e) {
          // console.log(this.layoutConf);
          if (this.layoutConf.sidebarStyle === 'compact') {
            this.layout.publishLayoutChange({
              sidebarStyle: 'full'
            }, {
              transitionClass: true
            });
          }
        }
      }, {
        key: "sidebarMouseleave",
        value: function sidebarMouseleave(e) {
          // console.log(this.layoutConf);
          if (this.layoutConf.sidebarStyle === 'full' && this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({
              sidebarStyle: 'compact'
            }, {
              transitionClass: true
            });
          }
        }
      }, {
        key: "updateAdminContainerClasses",
        value: function updateAdminContainerClasses(layoutConf) {
          return {
            'navigation-top': layoutConf.navigationPos === 'top',
            'sidebar-full': layoutConf.sidebarStyle === 'full',
            'sidebar-compact': layoutConf.sidebarStyle === 'compact' && layoutConf.navigationPos === 'side',
            'compact-toggle-active': layoutConf.sidebarCompactToggle,
            'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' && layoutConf.navigationPos === 'side',
            'sidebar-opened': layoutConf.sidebarStyle !== 'closed' && layoutConf.navigationPos === 'side',
            'sidebar-closed': layoutConf.sidebarStyle === 'closed',
            'fixed-topbar': layoutConf.topbarFixed && layoutConf.navigationPos === 'side'
          };
        }
      }]);

      return AdminLayoutComponent;
    }();

    AdminLayoutComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]
      }, {
        type: _services_theme_service__WEBPACK_IMPORTED_MODULE_3__["ThemeService"]
      }, {
        type: _services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']), __metadata("design:type", Function), __metadata("design:paramtypes", [Object]), __metadata("design:returntype", void 0)], AdminLayoutComponent.prototype, "onResize", null);

    AdminLayoutComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-admin-layout',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./admin-layout.template.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/admin-layout/admin-layout.template.html")).default
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _services_theme_service__WEBPACK_IMPORTED_MODULE_3__["ThemeService"], _services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]])], AdminLayoutComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts ***!
    \********************************************************************************/

  /*! exports provided: AuthLayoutComponent */

  /***/
  function srcAppSharedComponentsLayoutsAuthLayoutAuthLayoutComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function () {
      return AuthLayoutComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AuthLayoutComponent =
    /*#__PURE__*/
    function () {
      function AuthLayoutComponent() {
        _classCallCheck(this, AuthLayoutComponent);
      }

      _createClass(AuthLayoutComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AuthLayoutComponent;
    }();

    AuthLayoutComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-auth-layout',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./auth-layout.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/layouts/auth-layout/auth-layout.component.html")).default
    }), __metadata("design:paramtypes", [])], AuthLayoutComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/notifications/notifications.component.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/shared/components/notifications/notifications.component.ts ***!
    \****************************************************************************/

  /*! exports provided: NotificationsComponent */

  /***/
  function srcAppSharedComponentsNotificationsNotificationsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NotificationsComponent", function () {
      return NotificationsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var NotificationsComponent =
    /*#__PURE__*/
    function () {
      function NotificationsComponent(router) {
        _classCallCheck(this, NotificationsComponent);

        this.router = router; // Dummy notifications

        this.notifications = [{
          message: 'New contact added',
          icon: 'assignment_ind',
          time: '1 min ago',
          route: '/inbox',
          color: 'primary'
        }, {
          message: 'New message',
          icon: 'chat',
          time: '4 min ago',
          route: '/chat',
          color: 'accent'
        }, {
          message: 'Server rebooted',
          icon: 'settings_backup_restore',
          time: '12 min ago',
          route: '/charts',
          color: 'warn'
        }];
      }

      _createClass(NotificationsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this17 = this;

          this.router.events.subscribe(function (routeChange) {
            if (routeChange instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
              _this17.notificPanel.close();
            }
          });
        }
      }, {
        key: "clearAll",
        value: function clearAll(e) {
          e.preventDefault();
          this.notifications = [];
        }
      }]);

      return NotificationsComponent;
    }();

    NotificationsComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Object)], NotificationsComponent.prototype, "notificPanel", void 0);

    NotificationsComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-notifications',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./notifications.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/notifications/notifications.component.html")).default
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])], NotificationsComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/shared-components.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/shared/components/shared-components.module.ts ***!
    \***************************************************************/

  /*! exports provided: SharedComponentsModule */

  /***/
  function srcAppSharedComponentsSharedComponentsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedComponentsModule", function () {
      return SharedComponentsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_material_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../shared-material.module */
    "./src/app/shared/shared-material.module.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/esm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _search_search_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../search/search.module */
    "./src/app/shared/search/search.module.ts");
    /* harmony import */


    var _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../pipes/shared-pipes.module */
    "./src/app/shared/pipes/shared-pipes.module.ts");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../directives/shared-directives.module */
    "./src/app/shared/directives/shared-directives.module.ts");
    /* harmony import */


    var _header_side_header_side_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./header-side/header-side.component */
    "./src/app/shared/components/header-side/header-side.component.ts");
    /* harmony import */


    var _sidebar_side_sidebar_side_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./sidebar-side/sidebar-side.component */
    "./src/app/shared/components/sidebar-side/sidebar-side.component.ts");
    /* harmony import */


    var _header_top_header_top_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./header-top/header-top.component */
    "./src/app/shared/components/header-top/header-top.component.ts");
    /* harmony import */


    var _sidebar_top_sidebar_top_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./sidebar-top/sidebar-top.component */
    "./src/app/shared/components/sidebar-top/sidebar-top.component.ts");
    /* harmony import */


    var _customizer_customizer_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./customizer/customizer.component */
    "./src/app/shared/components/customizer/customizer.component.ts");
    /* harmony import */


    var _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./layouts/admin-layout/admin-layout.component */
    "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts");
    /* harmony import */


    var _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./layouts/auth-layout/auth-layout.component */
    "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts");
    /* harmony import */


    var _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./notifications/notifications.component */
    "./src/app/shared/components/notifications/notifications.component.ts");
    /* harmony import */


    var _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./sidenav/sidenav.component */
    "./src/app/shared/components/sidenav/sidenav.component.ts");
    /* harmony import */


    var _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./breadcrumb/breadcrumb.component */
    "./src/app/shared/components/breadcrumb/breadcrumb.component.ts");
    /* harmony import */


    var _services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ../services/app-confirm/app-confirm.component */
    "./src/app/shared/services/app-confirm/app-confirm.component.ts");
    /* harmony import */


    var _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ../services/app-loader/app-loader.component */
    "./src/app/shared/services/app-loader/app-loader.component.ts");
    /* harmony import */


    var _button_loading_button_loading_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ./button-loading/button-loading.component */
    "./src/app/shared/components/button-loading/button-loading.component.ts");
    /* harmony import */


    var _egret_sidebar_egret_sidebar_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! ./egret-sidebar/egret-sidebar.component */
    "./src/app/shared/components/egret-sidebar/egret-sidebar.component.ts");
    /* harmony import */


    var _bottom_sheet_share_bottom_sheet_share_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! ./bottom-sheet-share/bottom-sheet-share.component */
    "./src/app/shared/components/bottom-sheet-share/bottom-sheet-share.component.ts");
    /* harmony import */


    var _example_viewer_example_viewer_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! ./example-viewer/example-viewer.component */
    "./src/app/shared/components/example-viewer/example-viewer.component.ts");
    /* harmony import */


    var _example_viewer_template_example_viewer_template_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! ./example-viewer-template/example-viewer-template.component */
    "./src/app/shared/components/example-viewer-template/example-viewer-template.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }; // ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT
    // ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT
    // ONLY FOR DEMO
    // ALWAYS REQUIRED 


    var components = [_header_top_header_top_component__WEBPACK_IMPORTED_MODULE_13__["HeaderTopComponent"], _sidebar_top_sidebar_top_component__WEBPACK_IMPORTED_MODULE_14__["SidebarTopComponent"], _sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_19__["SidenavComponent"], _notifications_notifications_component__WEBPACK_IMPORTED_MODULE_18__["NotificationsComponent"], _sidebar_side_sidebar_side_component__WEBPACK_IMPORTED_MODULE_12__["SidebarSideComponent"], _header_side_header_side_component__WEBPACK_IMPORTED_MODULE_11__["HeaderSideComponent"], _layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_16__["AdminLayoutComponent"], _layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_17__["AuthLayoutComponent"], _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_20__["BreadcrumbComponent"], _services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_21__["AppComfirmComponent"], _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_22__["AppLoaderComponent"], _customizer_customizer_component__WEBPACK_IMPORTED_MODULE_15__["CustomizerComponent"], _button_loading_button_loading_component__WEBPACK_IMPORTED_MODULE_23__["ButtonLoadingComponent"], _egret_sidebar_egret_sidebar_component__WEBPACK_IMPORTED_MODULE_24__["EgretSidebarComponent"], _egret_sidebar_egret_sidebar_component__WEBPACK_IMPORTED_MODULE_24__["EgretSidebarTogglerDirective"], _bottom_sheet_share_bottom_sheet_share_component__WEBPACK_IMPORTED_MODULE_25__["BottomSheetShareComponent"], _example_viewer_example_viewer_component__WEBPACK_IMPORTED_MODULE_26__["EgretExampleViewerComponent"], _example_viewer_template_example_viewer_template_component__WEBPACK_IMPORTED_MODULE_27__["EgretExampleViewerTemplateComponent"]];

    var SharedComponentsModule = function SharedComponentsModule() {
      _classCallCheck(this, SharedComponentsModule);
    };

    SharedComponentsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["PerfectScrollbarModule"], _search_search_module__WEBPACK_IMPORTED_MODULE_7__["SearchModule"], _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_8__["SharedPipesModule"], _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_10__["SharedDirectivesModule"], _shared_material_module__WEBPACK_IMPORTED_MODULE_2__["SharedMaterialModule"]],
      declarations: components,
      entryComponents: [_services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_21__["AppComfirmComponent"], _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_22__["AppLoaderComponent"], _bottom_sheet_share_bottom_sheet_share_component__WEBPACK_IMPORTED_MODULE_25__["BottomSheetShareComponent"]],
      exports: components
    })], SharedComponentsModule);
    /***/
  },

  /***/
  "./src/app/shared/components/sidebar-side/sidebar-side.component.ts":
  /*!**************************************************************************!*\
    !*** ./src/app/shared/components/sidebar-side/sidebar-side.component.ts ***!
    \**************************************************************************/

  /*! exports provided: SidebarSideComponent */

  /***/
  function srcAppSharedComponentsSidebarSideSidebarSideComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SidebarSideComponent", function () {
      return SidebarSideComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../shared/services/navigation.service */
    "./src/app/shared/services/navigation.service.ts");
    /* harmony import */


    var _services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! app/shared/services/layout.service */
    "./src/app/shared/services/layout.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SidebarSideComponent =
    /*#__PURE__*/
    function () {
      function SidebarSideComponent(navService, themeService, layout) {
        _classCallCheck(this, SidebarSideComponent);

        this.navService = navService;
        this.themeService = themeService;
        this.layout = layout;
      }

      _createClass(SidebarSideComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this18 = this;

          this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
          this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
            _this18.menuItems = menuItem; //Checks item list has any icon type.

            _this18.hasIconTypeMenuItem = !!_this18.menuItems.filter(function (item) {
              return item.type === "icon";
            }).length;
          });
          this.layoutConf = this.layout.layoutConf;
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
          }
        }
      }, {
        key: "toggleCollapse",
        value: function toggleCollapse() {
          if (this.layoutConf.sidebarCompactToggle) {
            this.layout.publishLayoutChange({
              sidebarCompactToggle: false
            });
          } else {
            this.layout.publishLayoutChange({
              // sidebarStyle: "compact",
              sidebarCompactToggle: true
            });
          }
        }
      }]);

      return SidebarSideComponent;
    }();

    SidebarSideComponent.ctorParameters = function () {
      return [{
        type: _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]
      }, {
        type: _services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"]
      }, {
        type: app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__["LayoutService"]
      }];
    };

    SidebarSideComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "app-sidebar-side",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./sidebar-side.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-side/sidebar-side.component.html")).default
    }), __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"], _services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], app_shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__["LayoutService"]])], SidebarSideComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/sidebar-top/sidebar-top.component.ts":
  /*!************************************************************************!*\
    !*** ./src/app/shared/components/sidebar-top/sidebar-top.component.ts ***!
    \************************************************************************/

  /*! exports provided: SidebarTopComponent */

  /***/
  function srcAppSharedComponentsSidebarTopSidebarTopComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SidebarTopComponent", function () {
      return SidebarTopComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../shared/services/navigation.service */
    "./src/app/shared/services/navigation.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }; // import PerfectScrollbar from 'perfect-scrollbar';


    var SidebarTopComponent =
    /*#__PURE__*/
    function () {
      function SidebarTopComponent(navService) {
        _classCallCheck(this, SidebarTopComponent);

        this.navService = navService;
      }

      _createClass(SidebarTopComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this19 = this;

          this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
            _this19.menuItems = menuItem.filter(function (item) {
              return item.type !== 'icon' && item.type !== 'separator';
            });
          });
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {// setTimeout(() => {
          //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
          //     suppressScrollX: true
          //   })
          // })
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // if(this.sidebarPS) {
          //   this.sidebarPS.destroy();
          // }
          if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
          }
        }
      }]);

      return SidebarTopComponent;
    }();

    SidebarTopComponent.ctorParameters = function () {
      return [{
        type: _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]
      }];
    };

    SidebarTopComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-sidebar-top',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./sidebar-top.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidebar-top/sidebar-top.component.html")).default
    }), __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]])], SidebarTopComponent);
    /***/
  },

  /***/
  "./src/app/shared/components/sidenav/sidenav.component.ts":
  /*!****************************************************************!*\
    !*** ./src/app/shared/components/sidenav/sidenav.component.ts ***!
    \****************************************************************/

  /*! exports provided: SidenavComponent */

  /***/
  function srcAppSharedComponentsSidenavSidenavComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SidenavComponent", function () {
      return SidenavComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SidenavComponent =
    /*#__PURE__*/
    function () {
      function SidenavComponent() {
        _classCallCheck(this, SidenavComponent);

        this.menuItems = [];
      }

      _createClass(SidenavComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // Only for demo purpose

      }, {
        key: "addMenuItem",
        value: function addMenuItem() {
          this.menuItems.push({
            name: 'ITEM',
            type: 'dropDown',
            tooltip: 'Item',
            icon: 'done',
            state: 'material',
            sub: [{
              name: 'SUBITEM',
              state: 'cards'
            }, {
              name: 'SUBITEM',
              state: 'buttons'
            }]
          });
        }
      }]);

      return SidenavComponent;
    }();

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('items'), __metadata("design:type", Array)], SidenavComponent.prototype, "menuItems", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('hasIconMenu'), __metadata("design:type", Boolean)], SidenavComponent.prototype, "hasIconTypeMenuItem", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('iconMenuTitle'), __metadata("design:type", String)], SidenavComponent.prototype, "iconTypeMenuTitle", void 0);

    SidenavComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-sidenav',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./sidenav.template.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/sidenav/sidenav.template.html")).default
    }), __metadata("design:paramtypes", [])], SidenavComponent);
    /***/
  },

  /***/
  "./src/app/shared/directives/dropdown-anchor.directive.ts":
  /*!****************************************************************!*\
    !*** ./src/app/shared/directives/dropdown-anchor.directive.ts ***!
    \****************************************************************/

  /*! exports provided: DropdownAnchorDirective */

  /***/
  function srcAppSharedDirectivesDropdownAnchorDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DropdownAnchorDirective", function () {
      return DropdownAnchorDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./dropdown-link.directive */
    "./src/app/shared/directives/dropdown-link.directive.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DropdownAnchorDirective =
    /*#__PURE__*/
    function () {
      function DropdownAnchorDirective(navlink) {
        _classCallCheck(this, DropdownAnchorDirective);

        this.navlink = navlink;
      }

      _createClass(DropdownAnchorDirective, [{
        key: "onClick",
        value: function onClick(e) {
          this.navlink.toggle();
        }
      }]);

      return DropdownAnchorDirective;
    }();

    DropdownAnchorDirective.ctorParameters = function () {
      return [{
        type: _dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"],
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"]]
        }]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']), __metadata("design:type", Function), __metadata("design:paramtypes", [Object]), __metadata("design:returntype", void 0)], DropdownAnchorDirective.prototype, "onClick", null);

    DropdownAnchorDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[appDropdownToggle]'
    }), __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"])), __metadata("design:paramtypes", [_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"]])], DropdownAnchorDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/dropdown-link.directive.ts":
  /*!**************************************************************!*\
    !*** ./src/app/shared/directives/dropdown-link.directive.ts ***!
    \**************************************************************/

  /*! exports provided: DropdownLinkDirective */

  /***/
  function srcAppSharedDirectivesDropdownLinkDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DropdownLinkDirective", function () {
      return DropdownLinkDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _dropdown_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./dropdown.directive */
    "./src/app/shared/directives/dropdown.directive.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var DropdownLinkDirective =
    /*#__PURE__*/
    function () {
      function DropdownLinkDirective(nav) {
        _classCallCheck(this, DropdownLinkDirective);

        this.nav = nav;
      }

      _createClass(DropdownLinkDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.nav.addLink(this);
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.nav.removeGroup(this);
        }
      }, {
        key: "toggle",
        value: function toggle() {
          this.open = !this.open;
        }
      }, {
        key: "open",
        get: function get() {
          return this._open;
        },
        set: function set(value) {
          this._open = value;

          if (value) {
            this.nav.closeOtherLinks(this);
          }
        }
      }]);

      return DropdownLinkDirective;
    }();

    DropdownLinkDirective.ctorParameters = function () {
      return [{
        type: _dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"],
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"]]
        }]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Object)], DropdownLinkDirective.prototype, "group", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.open'), Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Boolean), __metadata("design:paramtypes", [Boolean])], DropdownLinkDirective.prototype, "open", null);

    DropdownLinkDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[appDropdownLink]'
    }), __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"])), __metadata("design:paramtypes", [_dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"]])], DropdownLinkDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/dropdown.directive.ts":
  /*!*********************************************************!*\
    !*** ./src/app/shared/directives/dropdown.directive.ts ***!
    \*********************************************************/

  /*! exports provided: AppDropdownDirective */

  /***/
  function srcAppSharedDirectivesDropdownDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppDropdownDirective", function () {
      return AppDropdownDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppDropdownDirective =
    /*#__PURE__*/
    function () {
      function AppDropdownDirective(router) {
        _classCallCheck(this, AppDropdownDirective);

        this.router = router;
        this.navlinks = [];
      }

      _createClass(AppDropdownDirective, [{
        key: "closeOtherLinks",
        value: function closeOtherLinks(openLink) {
          this.navlinks.forEach(function (link) {
            if (link !== openLink) {
              link.open = false;
            }
          });
        }
      }, {
        key: "addLink",
        value: function addLink(link) {
          this.navlinks.push(link);
        }
      }, {
        key: "removeGroup",
        value: function removeGroup(link) {
          var index = this.navlinks.indexOf(link);

          if (index !== -1) {
            this.navlinks.splice(index, 1);
          }
        }
      }, {
        key: "getUrl",
        value: function getUrl() {
          return this.router.url;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this20 = this;

          this._router = this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (event) {
            return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"];
          })).subscribe(function (event) {
            _this20.navlinks.forEach(function (link) {
              if (link.group) {
                var routeUrl = _this20.getUrl();

                var currentUrl = routeUrl.split('/');

                if (currentUrl.indexOf(link.group) > 0) {
                  link.open = true;

                  _this20.closeOtherLinks(link);
                }
              }
            });
          });
        }
      }]);

      return AppDropdownDirective;
    }();

    AppDropdownDirective.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }];
    };

    AppDropdownDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[appDropdown]'
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])], AppDropdownDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/egret-highlight.directive.ts":
  /*!****************************************************************!*\
    !*** ./src/app/shared/directives/egret-highlight.directive.ts ***!
    \****************************************************************/

  /*! exports provided: EgretHighlightDirective */

  /***/
  function srcAppSharedDirectivesEgretHighlightDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretHighlightDirective", function () {
      return EgretHighlightDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var highlight_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! highlight.js */
    "./node_modules/highlight.js/lib/index.js");
    /* harmony import */


    var highlight_js__WEBPACK_IMPORTED_MODULE_1___default =
    /*#__PURE__*/
    __webpack_require__.n(highlight_js__WEBPACK_IMPORTED_MODULE_1__);
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretHighlightDirective =
    /*#__PURE__*/
    function () {
      function EgretHighlightDirective(el, cdr, _zone, http) {
        _classCallCheck(this, EgretHighlightDirective);

        this.el = el;
        this.cdr = cdr;
        this._zone = _zone;
        this.http = http;
        this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
      }

      _createClass(EgretHighlightDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this21 = this;

          if (this.code) {
            this.highlightElement(this.code);
          }

          if (this.path) {
            this.highlightedCode = "Loading...";
            this.http.get(this.path, {
              responseType: "text"
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribeAll)).subscribe(function (response) {
              _this21.highlightElement(response, _this21.languages);
            });
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeAll.next();
          this.unsubscribeAll.complete();
        }
      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          if (changes["code"] && changes["code"].currentValue && changes["code"].currentValue !== changes["code"].previousValue) {
            this.highlightElement(this.code); // console.log('hljs on change', changes)
          }
        }
      }, {
        key: "highlightElement",
        value: function highlightElement(code, languages) {
          var _this22 = this;

          this._zone.runOutsideAngular(function () {
            var res = highlight_js__WEBPACK_IMPORTED_MODULE_1__["highlightAuto"](code);
            _this22.highlightedCode = res.value; // this.cdr.detectChanges();
            // console.log(languages)
          });
        }
      }]);

      return EgretHighlightDirective;
    }();

    EgretHighlightDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", String)], EgretHighlightDirective.prototype, "path", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("egretHighlight"), __metadata("design:type", String)], EgretHighlightDirective.prototype, "code", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(), __metadata("design:type", Array)], EgretHighlightDirective.prototype, "languages", void 0);

    EgretHighlightDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      host: {
        "[class.hljs]": "true",
        "[innerHTML]": "highlightedCode"
      },
      selector: "[egretHighlight]"
    }), __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])], EgretHighlightDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/egret-side-nav-toggle.directive.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/directives/egret-side-nav-toggle.directive.ts ***!
    \**********************************************************************/

  /*! exports provided: EgretSideNavToggleDirective */

  /***/
  function srcAppSharedDirectivesEgretSideNavToggleDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSideNavToggleDirective", function () {
      return EgretSideNavToggleDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretSideNavToggleDirective =
    /*#__PURE__*/
    function () {
      function EgretSideNavToggleDirective(mediaObserver, sideNav) {
        _classCallCheck(this, EgretSideNavToggleDirective);

        this.mediaObserver = mediaObserver;
        this.sideNav = sideNav;
      }

      _createClass(EgretSideNavToggleDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initSideNav();
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.screenSizeWatcher) {
            this.screenSizeWatcher.unsubscribe();
          }
        }
      }, {
        key: "updateSidenav",
        value: function updateSidenav() {
          var self = this;
          setTimeout(function () {
            self.sideNav.opened = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
          });
        }
      }, {
        key: "initSideNav",
        value: function initSideNav() {
          var _this23 = this;

          this.isMobile = this.mediaObserver.isActive('xs') || this.mediaObserver.isActive('sm'); // console.log(this.isMobile)

          this.updateSidenav();
          this.screenSizeWatcher = this.mediaObserver.media$.subscribe(function (change) {
            _this23.isMobile = change.mqAlias == 'xs' || change.mqAlias == 'sm';

            _this23.updateSidenav();
          });
        }
      }]);

      return EgretSideNavToggleDirective;
    }();

    EgretSideNavToggleDirective.ctorParameters = function () {
      return [{
        type: _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["MediaObserver"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"],
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Host"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }]
      }];
    };

    EgretSideNavToggleDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[EgretSideNavToggle]'
    }), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Host"])()), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"])()), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["MediaObserver"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]])], EgretSideNavToggleDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.directive.ts":
  /*!******************************************************************************************!*\
    !*** ./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.directive.ts ***!
    \******************************************************************************************/

  /*! exports provided: EgretSidenavHelperDirective, EgretSidenavTogglerDirective */

  /***/
  function srcAppSharedDirectivesEgretSidenavHelperEgretSidenavHelperDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidenavHelperDirective", function () {
      return EgretSidenavHelperDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidenavTogglerDirective", function () {
      return EgretSidenavTogglerDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! app/shared/services/match-media.service */
    "./src/app/shared/services/match-media.service.ts");
    /* harmony import */


    var _egret_sidenav_helper_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./egret-sidenav-helper.service */
    "./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.service.ts");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretSidenavHelperDirective =
    /*#__PURE__*/
    function () {
      function EgretSidenavHelperDirective(matchMediaService, egretSidenavHelperService, matSidenav, mediaObserver) {
        _classCallCheck(this, EgretSidenavHelperDirective);

        this.matchMediaService = matchMediaService;
        this.egretSidenavHelperService = egretSidenavHelperService;
        this.matSidenav = matSidenav;
        this.mediaObserver = mediaObserver; // Set the default value

        this.isOpen = true;
        this.unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
      }

      _createClass(EgretSidenavHelperDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this24 = this;

          this.egretSidenavHelperService.setSidenav(this.id, this.matSidenav);

          if (this.mediaObserver.isActive(this.isOpenBreakpoint)) {
            this.isOpen = true;
            this.matSidenav.mode = "side";
            this.matSidenav.toggle(true);
          } else {
            this.isOpen = false;
            this.matSidenav.mode = "over";
            this.matSidenav.toggle(false);
          }

          this.matchMediaService.onMediaChange.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["takeUntil"])(this.unsubscribeAll)).subscribe(function () {
            if (_this24.mediaObserver.isActive(_this24.isOpenBreakpoint)) {
              _this24.isOpen = true;
              _this24.matSidenav.mode = "side";

              _this24.matSidenav.toggle(true);
            } else {
              _this24.isOpen = false;
              _this24.matSidenav.mode = "over";

              _this24.matSidenav.toggle(false);
            }
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.unsubscribeAll.next();
          this.unsubscribeAll.complete();
        }
      }]);

      return EgretSidenavHelperDirective;
    }();

    EgretSidenavHelperDirective.ctorParameters = function () {
      return [{
        type: app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_3__["MatchMediaService"]
      }, {
        type: _egret_sidenav_helper_service__WEBPACK_IMPORTED_MODULE_4__["EgretSidenavHelperService"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSidenav"]
      }, {
        type: _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["MediaObserver"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])("class.is-open"), __metadata("design:type", Boolean)], EgretSidenavHelperDirective.prototype, "isOpen", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("egretSidenavHelper"), __metadata("design:type", String)], EgretSidenavHelperDirective.prototype, "id", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("isOpen"), __metadata("design:type", String)], EgretSidenavHelperDirective.prototype, "isOpenBreakpoint", void 0);

    EgretSidenavHelperDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: "[egretSidenavHelper]"
    }), __metadata("design:paramtypes", [app_shared_services_match_media_service__WEBPACK_IMPORTED_MODULE_3__["MatchMediaService"], _egret_sidenav_helper_service__WEBPACK_IMPORTED_MODULE_4__["EgretSidenavHelperService"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSidenav"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["MediaObserver"]])], EgretSidenavHelperDirective);

    var EgretSidenavTogglerDirective =
    /*#__PURE__*/
    function () {
      function EgretSidenavTogglerDirective(egretSidenavHelperService) {
        _classCallCheck(this, EgretSidenavTogglerDirective);

        this.egretSidenavHelperService = egretSidenavHelperService;
      }

      _createClass(EgretSidenavTogglerDirective, [{
        key: "onClick",
        value: function onClick() {
          // console.log(this.egretSidenavHelperService.getSidenav(this.id))
          this.egretSidenavHelperService.getSidenav(this.id).toggle();
        }
      }]);

      return EgretSidenavTogglerDirective;
    }();

    EgretSidenavTogglerDirective.ctorParameters = function () {
      return [{
        type: _egret_sidenav_helper_service__WEBPACK_IMPORTED_MODULE_4__["EgretSidenavHelperService"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])("egretSidenavToggler"), __metadata("design:type", Object)], EgretSidenavTogglerDirective.prototype, "id", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])("click"), __metadata("design:type", Function), __metadata("design:paramtypes", []), __metadata("design:returntype", void 0)], EgretSidenavTogglerDirective.prototype, "onClick", null);

    EgretSidenavTogglerDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: "[egretSidenavToggler]"
    }), __metadata("design:paramtypes", [_egret_sidenav_helper_service__WEBPACK_IMPORTED_MODULE_4__["EgretSidenavHelperService"]])], EgretSidenavTogglerDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.service.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.service.ts ***!
    \****************************************************************************************/

  /*! exports provided: EgretSidenavHelperService */

  /***/
  function srcAppSharedDirectivesEgretSidenavHelperEgretSidenavHelperServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EgretSidenavHelperService", function () {
      return EgretSidenavHelperService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EgretSidenavHelperService =
    /*#__PURE__*/
    function () {
      function EgretSidenavHelperService() {
        _classCallCheck(this, EgretSidenavHelperService);

        this.sidenavList = [];
      }

      _createClass(EgretSidenavHelperService, [{
        key: "setSidenav",
        value: function setSidenav(id, sidenav) {
          this.sidenavList[id] = sidenav;
        }
      }, {
        key: "getSidenav",
        value: function getSidenav(id) {
          return this.sidenavList[id];
        }
      }]);

      return EgretSidenavHelperService;
    }();

    EgretSidenavHelperService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: "root"
    }), __metadata("design:paramtypes", [])], EgretSidenavHelperService);
    /***/
  },

  /***/
  "./src/app/shared/directives/font-size.directive.ts":
  /*!**********************************************************!*\
    !*** ./src/app/shared/directives/font-size.directive.ts ***!
    \**********************************************************/

  /*! exports provided: FontSizeDirective */

  /***/
  function srcAppSharedDirectivesFontSizeDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FontSizeDirective", function () {
      return FontSizeDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var FontSizeDirective =
    /*#__PURE__*/
    function () {
      function FontSizeDirective(fontSize, el) {
        _classCallCheck(this, FontSizeDirective);

        this.fontSize = fontSize;
        this.el = el;
      }

      _createClass(FontSizeDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.el.nativeElement.fontSize = this.fontSize;
        }
      }]);

      return FontSizeDirective;
    }();

    FontSizeDirective.ctorParameters = function () {
      return [{
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"],
          args: ['fontSize']
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }];
    };

    FontSizeDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[fontSize]'
    }), __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"])('fontSize')), __metadata("design:paramtypes", [String, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])], FontSizeDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/scroll-to.directive.ts":
  /*!**********************************************************!*\
    !*** ./src/app/shared/directives/scroll-to.directive.ts ***!
    \**********************************************************/

  /*! exports provided: ScrollToDirective */

  /***/
  function srcAppSharedDirectivesScrollToDirectiveTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScrollToDirective", function () {
      return ScrollToDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ScrollToDirective =
    /*#__PURE__*/
    function () {
      function ScrollToDirective(elmID, el) {
        _classCallCheck(this, ScrollToDirective);

        this.elmID = elmID;
        this.el = el;
      }

      _createClass(ScrollToDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "currentYPosition",
        value: function currentYPosition() {
          // Firefox, Chrome, Opera, Safari
          if (self.pageYOffset) return self.pageYOffset; // Internet Explorer 6 - standards mode

          if (document.documentElement && document.documentElement.scrollTop) return document.documentElement.scrollTop; // Internet Explorer 6, 7 and 8

          if (document.body.scrollTop) return document.body.scrollTop;
          return 0;
        }
      }, {
        key: "elmYPosition",
        value: function elmYPosition(eID) {
          var elm = document.getElementById(eID);
          var y = elm.offsetTop;
          var node = elm;

          while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
          }

          return y;
        }
      }, {
        key: "smoothScroll",
        value: function smoothScroll() {
          if (!this.elmID) return;
          var startY = this.currentYPosition();
          var stopY = this.elmYPosition(this.elmID);
          var distance = stopY > startY ? stopY - startY : startY - stopY;

          if (distance < 100) {
            scrollTo(0, stopY);
            return;
          }

          var speed = Math.round(distance / 50);
          if (speed >= 20) speed = 20;
          var step = Math.round(distance / 25);
          var leapY = stopY > startY ? startY + step : startY - step;
          var timer = 0;

          if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
              setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
              leapY += step;
              if (leapY > stopY) leapY = stopY;
              timer++;
            }

            return;
          }

          for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY) leapY = stopY;
            timer++;
          }

          return false;
        }
      }]);

      return ScrollToDirective;
    }();

    ScrollToDirective.ctorParameters = function () {
      return [{
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"],
          args: ['scrollTo']
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']), __metadata("design:type", Function), __metadata("design:paramtypes", []), __metadata("design:returntype", void 0)], ScrollToDirective.prototype, "smoothScroll", null);

    ScrollToDirective = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
      selector: '[scrollTo]'
    }), __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"])('scrollTo')), __metadata("design:paramtypes", [String, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])], ScrollToDirective);
    /***/
  },

  /***/
  "./src/app/shared/directives/shared-directives.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/shared/directives/shared-directives.module.ts ***!
    \***************************************************************/

  /*! exports provided: SharedDirectivesModule */

  /***/
  function srcAppSharedDirectivesSharedDirectivesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedDirectivesModule", function () {
      return SharedDirectivesModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _font_size_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./font-size.directive */
    "./src/app/shared/directives/font-size.directive.ts");
    /* harmony import */


    var _scroll_to_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./scroll-to.directive */
    "./src/app/shared/directives/scroll-to.directive.ts");
    /* harmony import */


    var _dropdown_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./dropdown.directive */
    "./src/app/shared/directives/dropdown.directive.ts");
    /* harmony import */


    var _dropdown_anchor_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./dropdown-anchor.directive */
    "./src/app/shared/directives/dropdown-anchor.directive.ts");
    /* harmony import */


    var _dropdown_link_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./dropdown-link.directive */
    "./src/app/shared/directives/dropdown-link.directive.ts");
    /* harmony import */


    var _egret_side_nav_toggle_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./egret-side-nav-toggle.directive */
    "./src/app/shared/directives/egret-side-nav-toggle.directive.ts");
    /* harmony import */


    var _egret_sidenav_helper_egret_sidenav_helper_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./egret-sidenav-helper/egret-sidenav-helper.directive */
    "./src/app/shared/directives/egret-sidenav-helper/egret-sidenav-helper.directive.ts");
    /* harmony import */


    var _egret_highlight_directive__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./egret-highlight.directive */
    "./src/app/shared/directives/egret-highlight.directive.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var directives = [_font_size_directive__WEBPACK_IMPORTED_MODULE_2__["FontSizeDirective"], _scroll_to_directive__WEBPACK_IMPORTED_MODULE_3__["ScrollToDirective"], _dropdown_directive__WEBPACK_IMPORTED_MODULE_4__["AppDropdownDirective"], _dropdown_anchor_directive__WEBPACK_IMPORTED_MODULE_5__["DropdownAnchorDirective"], _dropdown_link_directive__WEBPACK_IMPORTED_MODULE_6__["DropdownLinkDirective"], _egret_side_nav_toggle_directive__WEBPACK_IMPORTED_MODULE_7__["EgretSideNavToggleDirective"], _egret_sidenav_helper_egret_sidenav_helper_directive__WEBPACK_IMPORTED_MODULE_8__["EgretSidenavHelperDirective"], _egret_sidenav_helper_egret_sidenav_helper_directive__WEBPACK_IMPORTED_MODULE_8__["EgretSidenavTogglerDirective"], _egret_highlight_directive__WEBPACK_IMPORTED_MODULE_9__["EgretHighlightDirective"]];

    var SharedDirectivesModule = function SharedDirectivesModule() {
      _classCallCheck(this, SharedDirectivesModule);
    };

    SharedDirectivesModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
      declarations: directives,
      exports: directives
    })], SharedDirectivesModule);
    /***/
  },

  /***/
  "./src/app/shared/helpers/url.helper.ts":
  /*!**********************************************!*\
    !*** ./src/app/shared/helpers/url.helper.ts ***!
    \**********************************************/

  /*! exports provided: getQueryParam */

  /***/
  function srcAppSharedHelpersUrlHelperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getQueryParam", function () {
      return getQueryParam;
    });

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    function getQueryParam(prop) {
      var params = {};
      var search = decodeURIComponent(window.location.href.slice(window.location.href.indexOf('?') + 1));
      var definitions = search.split('&');
      definitions.forEach(function (val, key) {
        var parts = val.split('=', 2);
        params[parts[0]] = parts[1];
      });
      return prop && prop in params ? params[prop] : params;
    }
    /***/

  },

  /***/
  "./src/app/shared/inmemory-db/chat-db.ts":
  /*!***********************************************!*\
    !*** ./src/app/shared/inmemory-db/chat-db.ts ***!
    \***********************************************/

  /*! exports provided: ChatDB */

  /***/
  function srcAppSharedInmemoryDbChatDbTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChatDB", function () {
      return ChatDB;
    });

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ChatDB = function ChatDB() {
      _classCallCheck(this, ChatDB);
    };

    ChatDB.user = [{
      id: "7863a6802ez0e277a0f98534",
      name: "John Doe",
      avatar: "assets/images/face-1.jpg",
      status: "online",
      chatInfo: [{
        chatId: "89564a680b3249760ea21fe77",
        contactId: "323sa680b3249760ea21rt47",
        contactName: "Frank Powell",
        unread: 4,
        lastChatTime: "2017-06-12T02:10:18.931Z"
      }, {
        chatId: "3289564a680b2134760ea21fe7753",
        contactId: "14663a3406eb47ffa63d4fec9429cb71",
        contactName: "Betty Diaz",
        unread: 0,
        lastChatTime: "2019-03-10T02:10:18.931Z"
      }]
    }];
    ChatDB.contacts = [{
      id: "323sa680b3249760ea21rt47",
      name: "Frank Powell",
      avatar: "assets/images/faces/13.jpg",
      status: "online",
      mood: ""
    }, {
      id: "14663a3406eb47ffa63d4fec9429cb71",
      name: "Betty Diaz",
      avatar: "assets/images/faces/12.jpg",
      status: "online",
      mood: ""
    }, {
      id: "43bd9bc59d164b5aea498e3ae1c24c3c",
      name: "Brian Stephens",
      avatar: "assets/images/faces/3.jpg",
      status: "online",
      mood: ""
    }, {
      id: "3fc8e01f3ce649d1caf884fbf4f698e4",
      name: "Jacqueline Day",
      avatar: "assets/images/faces/16.jpg",
      status: "offline",
      mood: ""
    }, {
      id: "e929b1d790ab49968ed8e34648553df4",
      name: "Arthur Mendoza",
      avatar: "assets/images/faces/10.jpg",
      status: "online",
      mood: ""
    }, {
      id: "d6caf04bba614632b5fecf91aebf4564",
      name: "Jeremy Lee",
      avatar: "assets/images/faces/9.jpg",
      status: "offline",
      mood: ""
    }, {
      id: "be0fb188c8e242f097fafa24632107e4",
      name: "Johnny Newman",
      avatar: "assets/images/faces/5.jpg",
      status: "offline",
      mood: ""
    }, {
      id: "dea902191b964a68ba5f2d93cff37e13",
      name: "Jeffrey Little",
      avatar: "assets/images/faces/15.jpg",
      status: "online",
      mood: ""
    }, {
      id: "0bf58f5ccc4543a9f8747350b7bda3c7",
      name: "Barbara Romero",
      avatar: "assets/images/faces/4.jpg",
      status: "offline",
      mood: ""
    }, {
      id: "c5d7498bbcb84d81fc72168871ac6a6e",
      name: "Daniel James",
      avatar: "assets/images/faces/2.jpg",
      status: "offline",
      mood: ""
    }, {
      id: "97bfbdd9413e46efdaca2010400fe18c",
      name: "Alice Sanders",
      avatar: "assets/images/faces/17.jpg",
      status: "offline",
      mood: ""
    }];
    ChatDB.chatCollection = [{
      id: "89564a680b3249760ea21fe77",
      chats: [{
        contactId: "323sa680b3249760ea21rt47",
        text: "Do you ever find yourself falling into the “discount trap?”",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Giving away your knowledge or product just to gain clients?",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "323sa680b3249760ea21rt47",
        text: "Yes",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Don’t feel bad. It happens to a lot of us",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "323sa680b3249760ea21rt47",
        text: "Do you ever find yourself falling into the “discount trap?”",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Giving away your knowledge or product just to gain clients?",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "323sa680b3249760ea21rt47",
        text: "Yes",
        time: "2018-02-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Don’t feel bad. It happens to a lot of us",
        time: "2018-02-10T08:45:28.291Z"
      }]
    }, {
      id: "3289564a680b2134760ea21fe7753",
      chats: [{
        contactId: "14663a3406eb47ffa63d4fec9429cb71",
        text: "Do you ever find yourself falling into the “discount trap?”",
        time: "2019-03-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Giving away your knowledge or product just to gain clients?",
        time: "2019-03-10T08:45:28.291Z"
      }, {
        contactId: "14663a3406eb47ffa63d4fec9429cb71",
        text: "Yes",
        time: "2019-03-10T08:45:28.291Z"
      }, {
        contactId: "7863a6802ez0e277a0f98534",
        text: "Don’t feel bad. It happens to a lot of us",
        time: "2019-03-10T08:45:28.291Z"
      }]
    }];
    /***/
  },

  /***/
  "./src/app/shared/inmemory-db/inmemory-db.service.ts":
  /*!***********************************************************!*\
    !*** ./src/app/shared/inmemory-db/inmemory-db.service.ts ***!
    \***********************************************************/

  /*! exports provided: InMemoryDataService */

  /***/
  function srcAppSharedInmemoryDbInmemoryDbServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InMemoryDataService", function () {
      return InMemoryDataService;
    });
    /* harmony import */


    var _chat_db__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./chat-db */
    "./src/app/shared/inmemory-db/chat-db.ts");
    /* harmony import */


    var _invoices__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./invoices */
    "./src/app/shared/inmemory-db/invoices.ts");
    /* harmony import */


    var _todo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./todo */
    "./src/app/shared/inmemory-db/todo.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var InMemoryDataService =
    /*#__PURE__*/
    function () {
      function InMemoryDataService() {
        _classCallCheck(this, InMemoryDataService);
      }

      _createClass(InMemoryDataService, [{
        key: "createDb",
        value: function createDb() {
          return {
            'contacts': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].contacts,
            'chat-collections': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].chatCollection,
            'chat-user': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].user,
            'invoices': _invoices__WEBPACK_IMPORTED_MODULE_1__["InvoiceDB"].invoices,
            'todoList': _todo__WEBPACK_IMPORTED_MODULE_2__["Todo"].todoList,
            'todoTag': _todo__WEBPACK_IMPORTED_MODULE_2__["TodoTag"].tag
          };
        }
      }]);

      return InMemoryDataService;
    }();
    /***/

  },

  /***/
  "./src/app/shared/inmemory-db/invoices.ts":
  /*!************************************************!*\
    !*** ./src/app/shared/inmemory-db/invoices.ts ***!
    \************************************************/

  /*! exports provided: InvoiceDB */

  /***/
  function srcAppSharedInmemoryDbInvoicesTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvoiceDB", function () {
      return InvoiceDB;
    });

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var InvoiceDB = function InvoiceDB() {
      _classCallCheck(this, InvoiceDB);
    };

    InvoiceDB.invoices = [{
      id: '5a9ae2106518248b68251fd1',
      orderNo: '232',
      status: 'pending',
      date: new Date(),
      currency: '$',
      vat: 10,
      buyer: {
        name: 'Schoen, Conn and Mills',
        address: 'rodriguez.trent@senger.com \n 61 Johnson St. Shirley, NY 11967. \n \n +202-555-0170'
      },
      seller: {
        name: 'UI Lib',
        address: 'sales@ui-lib.com \n 8254 S. Garfield Street. Villa Rica, GA 30180. \n \n +1-202-555-0170'
      },
      item: [{
        name: 'Item 1',
        unit: 9,
        price: 200
      }, {
        name: 'Item 2',
        unit: 15,
        price: 300
      }]
    }, {
      id: '5a9ae2106518248b68251fd2',
      orderNo: '233',
      status: 'processing',
      date: new Date(),
      currency: '$',
      vat: 10,
      buyer: {
        name: 'New Age Inc.',
        address: 'this is a test address \n 7664 Rockcrest Road. Longview, TX 75604. \n \n +1-202-555-0153'
      },
      seller: {
        name: 'UI Lib',
        address: 'sales@ui-lib.com \n 8254 S. Garfield Street. Villa Rica, GA 30180. \n \n +1-202-555-0170'
      },
      item: [{
        name: 'Item 1',
        unit: 3,
        price: 2000
      }, {
        name: 'Item 2',
        unit: 2,
        price: 4000
      }]
    }, {
      id: '5a9ae2106518248b68251fd3',
      orderNo: '234',
      status: 'delivered',
      date: new Date(),
      currency: '$',
      vat: 10,
      buyer: {
        name: 'Predovic, Schowalter and Haag',
        address: 'linwood53@price.com \n 7178 Plumb Branch Dr. South Bend, IN 46614 \n \n +999 9999 9999'
      },
      seller: {
        name: 'UI Lib',
        address: 'sales@ui-lib.com \n 8254 S. Garfield Street. Villa Rica, GA 30180. \n \n +1-202-555-0170'
      },
      item: [{
        name: 'Item 1',
        unit: 5,
        price: 1000
      }, {
        name: 'Item 2',
        unit: 2,
        price: 4000
      }]
    }, {
      id: '5a9ae2106518248b68251fd4',
      orderNo: '235',
      status: 'delivered',
      date: new Date(),
      currency: '$',
      vat: 10,
      buyer: {
        name: 'Hane PLC',
        address: 'nader.savanna@lindgren.org \n 858 8th St. Nanuet, NY 10954. \n \n +202-555-0131'
      },
      seller: {
        name: 'UI Lib',
        address: 'sales@ui-lib.com \n 8254 S. Garfield Street. Villa Rica, GA 30180. \n \n +1-202-555-0170'
      },
      item: [{
        name: 'Item 1',
        unit: 3,
        price: 4000
      }, {
        name: 'Item 2',
        unit: 1,
        price: 5000
      }]
    }];
    /***/
  },

  /***/
  "./src/app/shared/inmemory-db/todo.ts":
  /*!********************************************!*\
    !*** ./src/app/shared/inmemory-db/todo.ts ***!
    \********************************************/

  /*! exports provided: Todo, TodoTag */

  /***/
  function srcAppSharedInmemoryDbTodoTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Todo", function () {
      return Todo;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TodoTag", function () {
      return TodoTag;
    });

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var Todo = function Todo() {
      _classCallCheck(this, Todo);
    };

    Todo.todoList = [{
      id: 1,
      title: "API problem",
      note: "API is malfunctioning. kindly fix it",
      important: true,
      starred: true,
      done: false,
      read: false,
      selected: false,
      startDate: new Date().toISOString(),
      dueDate: new Date().toISOString(),
      tag: [1, 2]
    }, {
      id: 2,
      title: "Mobile problem",
      note: "Mobile is malfunctioning. fix it",
      important: false,
      starred: false,
      done: true,
      read: true,
      selected: false,
      startDate: new Date().toISOString(),
      dueDate: new Date().toISOString(),
      tag: [2]
    }, {
      id: 3,
      title: "API problem",
      note: "API is malfunctioning. fix it",
      important: false,
      starred: false,
      done: true,
      read: false,
      selected: false,
      startDate: new Date().toISOString(),
      dueDate: new Date().toISOString(),
      tag: [1]
    }, {
      id: 4,
      title: "API problem",
      note: "API is malfunctioning. fix it",
      important: false,
      starred: false,
      done: false,
      read: true,
      selected: false,
      startDate: new Date().toISOString(),
      dueDate: new Date().toISOString(),
      tag: [1, 2, 3]
    }]; // ============================================

    var TodoTag = function TodoTag() {
      _classCallCheck(this, TodoTag);
    };

    TodoTag.tag = [{
      id: 1,
      name: "frontend"
    }, {
      id: 2,
      name: "backend"
    }, {
      id: 3,
      name: "API"
    }, {
      id: 4,
      name: "issue"
    }, {
      id: 5,
      name: "mobile"
    }];
    /***/
  },

  /***/
  "./src/app/shared/pipes/excerpt.pipe.ts":
  /*!**********************************************!*\
    !*** ./src/app/shared/pipes/excerpt.pipe.ts ***!
    \**********************************************/

  /*! exports provided: ExcerptPipe */

  /***/
  function srcAppSharedPipesExcerptPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExcerptPipe", function () {
      return ExcerptPipe;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ExcerptPipe =
    /*#__PURE__*/
    function () {
      function ExcerptPipe() {
        _classCallCheck(this, ExcerptPipe);
      }

      _createClass(ExcerptPipe, [{
        key: "transform",
        value: function transform(text) {
          var limit = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5;
          if (text.length <= limit) return text;
          return text.substring(0, limit) + '...';
        }
      }]);

      return ExcerptPipe;
    }();

    ExcerptPipe = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
      name: 'excerpt'
    })], ExcerptPipe);
    /***/
  },

  /***/
  "./src/app/shared/pipes/get-value-by-key.pipe.ts":
  /*!*******************************************************!*\
    !*** ./src/app/shared/pipes/get-value-by-key.pipe.ts ***!
    \*******************************************************/

  /*! exports provided: GetValueByKeyPipe */

  /***/
  function srcAppSharedPipesGetValueByKeyPipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GetValueByKeyPipe", function () {
      return GetValueByKeyPipe;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var GetValueByKeyPipe =
    /*#__PURE__*/
    function () {
      function GetValueByKeyPipe() {
        _classCallCheck(this, GetValueByKeyPipe);
      }

      _createClass(GetValueByKeyPipe, [{
        key: "transform",
        value: function transform(value, id, property) {
          var filteredObj = value.find(function (item) {
            if (item.id !== undefined) {
              return item.id === id;
            }

            return false;
          });

          if (filteredObj) {
            return filteredObj[property];
          }
        }
      }]);

      return GetValueByKeyPipe;
    }();

    GetValueByKeyPipe = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
      name: "getValueByKey",
      pure: false
    })], GetValueByKeyPipe);
    /***/
  },

  /***/
  "./src/app/shared/pipes/relative-time.pipe.ts":
  /*!****************************************************!*\
    !*** ./src/app/shared/pipes/relative-time.pipe.ts ***!
    \****************************************************/

  /*! exports provided: RelativeTimePipe */

  /***/
  function srcAppSharedPipesRelativeTimePipeTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RelativeTimePipe", function () {
      return RelativeTimePipe;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var RelativeTimePipe =
    /*#__PURE__*/
    function () {
      function RelativeTimePipe() {
        _classCallCheck(this, RelativeTimePipe);
      }

      _createClass(RelativeTimePipe, [{
        key: "transform",
        value: function transform(value) {
          if (!(value instanceof Date)) value = new Date(value);
          var seconds = Math.floor((new Date().getTime() - value.getTime()) / 1000);
          var interval = Math.floor(seconds / 31536000);

          if (interval > 1) {
            return interval + " years ago";
          }

          interval = Math.floor(seconds / 2592000);

          if (interval > 1) {
            return interval + " months ago";
          }

          interval = Math.floor(seconds / 86400);

          if (interval > 1) {
            return interval + " days ago";
          }

          interval = Math.floor(seconds / 3600);

          if (interval > 1) {
            return interval + " hours ago";
          }

          interval = Math.floor(seconds / 60);

          if (interval > 1) {
            return interval + " minutes ago";
          }

          return Math.floor(seconds) + " seconds ago";
        }
      }]);

      return RelativeTimePipe;
    }();

    RelativeTimePipe = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
      name: 'relativeTime'
    })], RelativeTimePipe);
    /***/
  },

  /***/
  "./src/app/shared/pipes/shared-pipes.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/shared/pipes/shared-pipes.module.ts ***!
    \*****************************************************/

  /*! exports provided: SharedPipesModule */

  /***/
  function srcAppSharedPipesSharedPipesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedPipesModule", function () {
      return SharedPipesModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _relative_time_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./relative-time.pipe */
    "./src/app/shared/pipes/relative-time.pipe.ts");
    /* harmony import */


    var _excerpt_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./excerpt.pipe */
    "./src/app/shared/pipes/excerpt.pipe.ts");
    /* harmony import */


    var _get_value_by_key_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./get-value-by-key.pipe */
    "./src/app/shared/pipes/get-value-by-key.pipe.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var pipes = [_relative_time_pipe__WEBPACK_IMPORTED_MODULE_2__["RelativeTimePipe"], _excerpt_pipe__WEBPACK_IMPORTED_MODULE_3__["ExcerptPipe"], _get_value_by_key_pipe__WEBPACK_IMPORTED_MODULE_4__["GetValueByKeyPipe"]];

    var SharedPipesModule = function SharedPipesModule() {
      _classCallCheck(this, SharedPipesModule);
    };

    SharedPipesModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
      declarations: pipes,
      exports: pipes
    })], SharedPipesModule);
    /***/
  },

  /***/
  "./src/app/shared/search/search-input-over/search-input-over.component.scss":
  /*!**********************************************************************************!*\
    !*** ./src/app/shared/search/search-input-over/search-input-over.component.scss ***!
    \**********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedSearchSearchInputOverSearchInputOverComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".search-bar-wide.open {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 64px;\n  z-index: 999; }\n  .search-bar-wide.open .search-icon-btn {\n    display: none; }\n  .search-bar-wide.open div {\n    display: -webkit-box;\n    display: flex; }\n  .search-bar-wide.open .search-close {\n    display: block; }\n  .search-bar-wide div {\n  display: none;\n  height: 100%;\n  width: 100%; }\n  .search-bar-wide div input {\n    height: 100%;\n    width: 100%;\n    border: 0;\n    outline: 0;\n    padding: 0;\n    font-weight: 700;\n    padding-left: 15px; }\n  .search-bar-wide .search-close {\n  display: none;\n  position: absolute;\n  top: 20px;\n  right: 15px;\n  cursor: pointer; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvc2hhcmVkL3NlYXJjaC9zZWFyY2gtaW5wdXQtb3Zlci9zZWFyY2gtaW5wdXQtb3Zlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWSxFQUFBO0VBUHBCO0lBU1ksYUFBYSxFQUFBO0VBVHpCO0lBWVksb0JBQWE7SUFBYixhQUFhLEVBQUE7RUFaekI7SUFlWSxjQUFjLEVBQUE7RUFmMUI7RUFtQlEsYUFBYTtFQUNiLFlBQVk7RUFDWixXQUFXLEVBQUE7RUFyQm5CO0lBdUJnQixZQUFZO0lBQ1osV0FBVztJQUNYLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixrQkFBa0IsRUFBQTtFQTdCbEM7RUFpQ1EsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsV0FBVztFQUNYLGVBQWUsRUFBQSIsImZpbGUiOiJhcHAvc2hhcmVkL3NlYXJjaC9zZWFyY2gtaW5wdXQtb3Zlci9zZWFyY2gtaW5wdXQtb3Zlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zZWFyY2gtYmFyLXdpZGUgeyAgICBcbiAgICAmLm9wZW4ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogNjRweDtcbiAgICAgICAgei1pbmRleDogOTk5O1xuICAgICAgICAuc2VhcmNoLWljb24tYnRuIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgIH1cbiAgICAgICAgZGl2IHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIH1cbiAgICAgICAgLnNlYXJjaC1jbG9zZSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgfVxuICAgIH1cbiAgICBkaXYge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaW5wdXQge1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBib3JkZXI6IDA7XG4gICAgICAgICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgICAgICAgICAgfVxuICAgIH1cbiAgICAuc2VhcmNoLWNsb3NlIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDIwcHg7XG4gICAgICAgIHJpZ2h0OiAxNXB4O1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/search/search-input-over/search-input-over.component.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/search/search-input-over/search-input-over.component.ts ***!
    \********************************************************************************/

  /*! exports provided: SearchInputOverComponent */

  /***/
  function srcAppSharedSearchSearchInputOverSearchInputOverComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchInputOverComponent", function () {
      return SearchInputOverComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _search_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../search.service */
    "./src/app/shared/search/search.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SearchInputOverComponent =
    /*#__PURE__*/
    function () {
      function SearchInputOverComponent(searchService, router, route) {
        _classCallCheck(this, SearchInputOverComponent);

        this.searchService = searchService;
        this.router = router;
        this.route = route;
        this.placeholder = "Search here";
        this.search = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.searchCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]();
      }

      _createClass(SearchInputOverComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this25 = this;

          this.searchCtrl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(200)).subscribe(function (value) {
            _this25.search.emit(value);

            _this25.searchService.searchTerm.next(value);
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          if (this.searchCtrlSub) {
            this.searchCtrlSub.unsubscribe();
          }
        }
      }, {
        key: "navigateToResult",
        value: function navigateToResult() {
          if (this.resultPage) {
            this.router.navigateByUrl(this.resultPage);
          }
        }
      }, {
        key: "open",
        value: function open() {
          this.isOpen = true;
          this.navigateToResult();
        }
      }, {
        key: "close",
        value: function close() {
          this.isOpen = false;
        }
      }, {
        key: "toggle",
        value: function toggle() {
          this.isOpen = !this.isOpen;
        }
      }]);

      return SearchInputOverComponent;
    }();

    SearchInputOverComponent.ctorParameters = function () {
      return [{
        type: _search_service__WEBPACK_IMPORTED_MODULE_3__["SearchService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }];
    };

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('resultPage'), __metadata("design:type", String)], SearchInputOverComponent.prototype, "resultPage", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('placeholder'), __metadata("design:type", String)], SearchInputOverComponent.prototype, "placeholder", void 0);

    __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])("search"), __metadata("design:type", Object)], SearchInputOverComponent.prototype, "search", void 0);

    SearchInputOverComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: "egret-search-input-over",
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./search-input-over.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search/search-input-over/search-input-over.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./search-input-over.component.scss */
      "./src/app/shared/search/search-input-over/search-input-over.component.scss")).default]
    }), __metadata("design:paramtypes", [_search_service__WEBPACK_IMPORTED_MODULE_3__["SearchService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])], SearchInputOverComponent);
    /***/
  },

  /***/
  "./src/app/shared/search/search.module.ts":
  /*!************************************************!*\
    !*** ./src/app/shared/search/search.module.ts ***!
    \************************************************/

  /*! exports provided: SearchModule */

  /***/
  function srcAppSharedSearchSearchModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchModule", function () {
      return SearchModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _search_input_over_search_input_over_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./search-input-over/search-input-over.component */
    "./src/app/shared/search/search-input-over/search-input-over.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SearchModule = function SearchModule() {
      _classCallCheck(this, SearchModule);
    };

    SearchModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      declarations: [_search_input_over_search_input_over_component__WEBPACK_IMPORTED_MODULE_2__["SearchInputOverComponent"]],
      exports: [_search_input_over_search_input_over_component__WEBPACK_IMPORTED_MODULE_2__["SearchInputOverComponent"]],
      imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]]
    })], SearchModule);
    /***/
  },

  /***/
  "./src/app/shared/search/search.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/shared/search/search.service.ts ***!
    \*************************************************/

  /*! exports provided: SearchService */

  /***/
  function srcAppSharedSearchSearchServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchService", function () {
      return SearchService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/BehaviorSubject */
    "./node_modules/rxjs-compat/_esm2015/BehaviorSubject.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SearchService = function SearchService() {
      _classCallCheck(this, SearchService);

      this.searchTerm = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]("");
      this.searchTerm$ = this.searchTerm.asObservable();
    };

    SearchService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: "root"
    }), __metadata("design:paramtypes", [])], SearchService);
    /***/
  },

  /***/
  "./src/app/shared/services/app-confirm/app-confirm.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/services/app-confirm/app-confirm.component.ts ***!
    \**********************************************************************/

  /*! exports provided: AppComfirmComponent */

  /***/
  function srcAppSharedServicesAppConfirmAppConfirmComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComfirmComponent", function () {
      return AppComfirmComponent;
    });
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppComfirmComponent = function AppComfirmComponent(dialogRef, data) {
      _classCallCheck(this, AppComfirmComponent);

      this.dialogRef = dialogRef;
      this.data = data;
    };

    AppComfirmComponent.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogRef"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
          args: [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"]]
        }]
      }];
    };

    AppComfirmComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-confirm',
      template: "<h1 matDialogTitle class=\"mb-05\">{{ data.title }}</h1>\n    <div mat-dialog-content class=\"mb-1\">{{ data.message }}</div>\n    <div mat-dialog-actions>\n    <button \n    type=\"button\" \n    mat-raised-button\n    color=\"primary\" \n    (click)=\"dialogRef.close(true)\">OK</button>\n    &nbsp;\n    <span fxFlex></span>\n    <button \n    type=\"button\"\n    color=\"accent\"\n    mat-raised-button \n    (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>"
    }), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"])), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogRef"], Object])], AppComfirmComponent);
    /***/
  },

  /***/
  "./src/app/shared/services/app-confirm/app-confirm.service.ts":
  /*!********************************************************************!*\
    !*** ./src/app/shared/services/app-confirm/app-confirm.service.ts ***!
    \********************************************************************/

  /*! exports provided: AppConfirmService */

  /***/
  function srcAppSharedServicesAppConfirmAppConfirmServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppConfirmService", function () {
      return AppConfirmService;
    });
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_confirm_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app-confirm.component */
    "./src/app/shared/services/app-confirm/app-confirm.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppConfirmService =
    /*#__PURE__*/
    function () {
      function AppConfirmService(dialog) {
        _classCallCheck(this, AppConfirmService);

        this.dialog = dialog;
      }

      _createClass(AppConfirmService, [{
        key: "confirm",
        value: function confirm() {
          var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
          data.title = data.title || 'Confirm';
          data.message = data.message || 'Are you sure?';
          var dialogRef;
          dialogRef = this.dialog.open(_app_confirm_component__WEBPACK_IMPORTED_MODULE_2__["AppComfirmComponent"], {
            width: '380px',
            disableClose: true,
            data: {
              title: data.title,
              message: data.message
            }
          });
          return dialogRef.afterClosed();
        }
      }]);

      return AppConfirmService;
    }();

    AppConfirmService.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialog"]
      }];
    };

    AppConfirmService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialog"]])], AppConfirmService);
    /***/
  },

  /***/
  "./src/app/shared/services/app-loader/app-loader.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/shared/services/app-loader/app-loader.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedServicesAppLoaderAppLoaderComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".mat-dialog-content {\n  min-height: 122px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvc2VydmljZXMvYXBwLWxvYWRlci9hcHAtbG9hZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkIiLCJmaWxlIjoiYXBwL3NoYXJlZC9zZXJ2aWNlcy9hcHAtbG9hZGVyL2FwcC1sb2FkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtZGlhbG9nLWNvbnRlbnQge1xuICBtaW4taGVpZ2h0OiAxMjJweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/services/app-loader/app-loader.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/shared/services/app-loader/app-loader.component.ts ***!
    \********************************************************************/

  /*! exports provided: AppLoaderComponent */

  /***/
  function srcAppSharedServicesAppLoaderAppLoaderComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppLoaderComponent", function () {
      return AppLoaderComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppLoaderComponent =
    /*#__PURE__*/
    function () {
      function AppLoaderComponent(dialogRef) {
        _classCallCheck(this, AppLoaderComponent);

        this.dialogRef = dialogRef;
      }

      _createClass(AppLoaderComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AppLoaderComponent;
    }();

    AppLoaderComponent.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]
      }];
    };

    AppLoaderComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-app-loader',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./app-loader.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/services/app-loader/app-loader.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./app-loader.component.css */
      "./src/app/shared/services/app-loader/app-loader.component.css")).default]
    }), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]])], AppLoaderComponent);
    /***/
  },

  /***/
  "./src/app/shared/services/app-loader/app-loader.service.ts":
  /*!******************************************************************!*\
    !*** ./src/app/shared/services/app-loader/app-loader.service.ts ***!
    \******************************************************************/

  /*! exports provided: AppLoaderService */

  /***/
  function srcAppSharedServicesAppLoaderAppLoaderServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppLoaderService", function () {
      return AppLoaderService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _app_loader_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app-loader.component */
    "./src/app/shared/services/app-loader/app-loader.component.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AppLoaderService =
    /*#__PURE__*/
    function () {
      function AppLoaderService(dialog) {
        _classCallCheck(this, AppLoaderService);

        this.dialog = dialog;
      }

      _createClass(AppLoaderService, [{
        key: "open",
        value: function open() {
          var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Please wait';
          this.dialogRef = this.dialog.open(_app_loader_component__WEBPACK_IMPORTED_MODULE_2__["AppLoaderComponent"], {
            disableClose: true,
            backdropClass: 'light-backdrop'
          });
          this.dialogRef.updateSize('200px');
          this.dialogRef.componentInstance.title = title;
          return this.dialogRef.afterClosed();
        }
      }, {
        key: "close",
        value: function close() {
          if (this.dialogRef) this.dialogRef.close();
        }
      }]);

      return AppLoaderService;
    }();

    AppLoaderService.ctorParameters = function () {
      return [{
        type: _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]
      }];
    };

    AppLoaderService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])], AppLoaderService);
    /***/
  },

  /***/
  "./src/app/shared/services/auth/auth.guard.ts":
  /*!****************************************************!*\
    !*** ./src/app/shared/services/auth/auth.guard.ts ***!
    \****************************************************/

  /*! exports provided: AuthGuard */

  /***/
  function srcAppSharedServicesAuthAuthGuardTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthGuard", function () {
      return AuthGuard;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var AuthGuard =
    /*#__PURE__*/
    function () {
      function AuthGuard(router) {
        _classCallCheck(this, AuthGuard);

        this.router = router;
        this.isAuthenticated = true; // Set this value dynamically
      }

      _createClass(AuthGuard, [{
        key: "canActivate",
        value: function canActivate(route, state) {
          if (this.isAuthenticated) {
            return true;
          }

          this.router.navigate(['/sessions/signin']);
          return false;
        }
      }]);

      return AuthGuard;
    }();

    AuthGuard.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }];
    };

    AuthGuard = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])], AuthGuard);
    /***/
  },

  /***/
  "./src/app/shared/services/customizer.service.ts":
  /*!*******************************************************!*\
    !*** ./src/app/shared/services/customizer.service.ts ***!
    \*******************************************************/

  /*! exports provided: CustomizerService */

  /***/
  function srcAppSharedServicesCustomizerServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CustomizerService", function () {
      return CustomizerService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./layout.service */
    "./src/app/shared/services/layout.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var CustomizerService =
    /*#__PURE__*/
    function () {
      function CustomizerService(router, layout) {
        _classCallCheck(this, CustomizerService);

        this.router = router;
        this.layout = layout;
        this.colors = [{
          class: "black",
          active: false
        }, {
          class: "white",
          active: false
        }, {
          class: "dark-blue",
          active: false
        }, {
          class: "grey",
          active: false
        }, {
          class: "brown",
          active: false
        }, {
          class: "gray",
          active: false
        }, {
          class: "purple",
          active: false
        }, {
          class: "blue",
          active: false
        }, {
          class: "indigo",
          active: false
        }, {
          class: "yellow",
          active: false
        }, {
          class: "green",
          active: false
        }, {
          class: "pink",
          active: false
        }, {
          class: "red",
          active: false
        }, {
          class: "slate",
          active: false
        }];
        this.topbarColors = this.getTopbarColors();
        this.sidebarColors = this.getSidebarColors();
      }

      _createClass(CustomizerService, [{
        key: "getSidebarColors",
        value: function getSidebarColors() {
          var _this26 = this;

          var sidebarColors = ['black', 'slate', 'white', 'grey', 'brown', 'purple', 'dark-blue'];
          return this.colors.filter(function (color) {
            return sidebarColors.includes(color.class);
          }).map(function (c) {
            c.active = c.class === _this26.layout.layoutConf.sidebarColor;
            return Object.assign({}, c);
          });
        }
      }, {
        key: "getTopbarColors",
        value: function getTopbarColors() {
          var _this27 = this;

          var topbarColors = ['black', 'slate', 'white', 'dark-gray', 'purple', 'dark-blue', 'indigo', 'pink', 'red', 'yellow', 'green'];
          return this.colors.filter(function (color) {
            return topbarColors.includes(color.class);
          }).map(function (c) {
            c.active = c.class === _this27.layout.layoutConf.topbarColor;
            return Object.assign({}, c);
          });
        }
      }, {
        key: "changeSidebarColor",
        value: function changeSidebarColor(color) {
          this.layout.publishLayoutChange({
            sidebarColor: color.class
          });
          this.sidebarColors = this.getSidebarColors();
        }
      }, {
        key: "changeTopbarColor",
        value: function changeTopbarColor(color) {
          this.layout.publishLayoutChange({
            topbarColor: color.class
          });
          this.topbarColors = this.getTopbarColors();
        }
      }, {
        key: "removeClass",
        value: function removeClass(el, className) {
          if (!el || el.length === 0) return;

          if (!el.length) {
            el.classList.remove(className);
          } else {
            for (var i = 0; i < el.length; i++) {
              el[i].classList.remove(className);
            }
          }
        }
      }, {
        key: "addClass",
        value: function addClass(el, className) {
          if (!el) return;

          if (!el.length) {
            el.classList.add(className);
          } else {
            for (var i = 0; i < el.length; i++) {
              el[i].classList.add(className);
            }
          }
        }
      }, {
        key: "findClosest",
        value: function findClosest(el, className) {
          if (!el) return;

          while (el) {
            var parent = el.parentElement;

            if (parent && this.hasClass(parent, className)) {
              return parent;
            }

            el = parent;
          }
        }
      }, {
        key: "hasClass",
        value: function hasClass(el, className) {
          if (!el) return;
          return " ".concat(el.className, " ").replace(/[\n\t]/g, " ").indexOf(" ".concat(className, " ")) > -1;
        }
      }, {
        key: "toggleClass",
        value: function toggleClass(el, className) {
          if (!el) return;

          if (this.hasClass(el, className)) {
            this.removeClass(el, className);
          } else {
            this.addClass(el, className);
          }
        }
      }]);

      return CustomizerService;
    }();

    CustomizerService.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }, {
        type: _layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]
      }];
    };

    CustomizerService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: "root"
    }), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]])], CustomizerService);
    /***/
  },

  /***/
  "./src/app/shared/services/error-handler.service.ts":
  /*!**********************************************************!*\
    !*** ./src/app/shared/services/error-handler.service.ts ***!
    \**********************************************************/

  /*! exports provided: ErrorHandlerService */

  /***/
  function srcAppSharedServicesErrorHandlerServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ErrorHandlerService", function () {
      return ErrorHandlerService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ErrorHandlerService =
    /*#__PURE__*/
    function (_angular_core__WEBPAC) {
      _inherits(ErrorHandlerService, _angular_core__WEBPAC);

      function ErrorHandlerService(injector) {
        var _this28;

        _classCallCheck(this, ErrorHandlerService);

        _this28 = _possibleConstructorReturn(this, _getPrototypeOf(ErrorHandlerService).call(this));
        _this28.injector = injector;
        _this28.errorCount = 0;
        return _this28;
      } // https://github.com/angular/angular/issues/17010


      _createClass(ErrorHandlerService, [{
        key: "handleError",
        value: function handleError(error) {
          var increment = 5;
          var max = 50; // Prevents change detection

          var debugCtx = error['ngDebugContext'];
          var changeDetectorRef = debugCtx && debugCtx.injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]);
          if (changeDetectorRef) changeDetectorRef.detach();
          this.errorCount = this.errorCount + 1;

          if (this.errorCount % increment === 0) {
            console.log(' ');
            console.log("errorHandler() was called ".concat(this.errorCount, " times."));
            console.log(' ');

            _get(_getPrototypeOf(ErrorHandlerService.prototype), "handleError", this).call(this, error);

            if (this.errorCount === max) {
              console.log(' ');
              console.log("Preventing recursive error after ".concat(this.errorCount, " recursive errors."));
              console.log(' ');
              var appRef = this.injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ApplicationRef"]);
              appRef.tick();
            }
          } else if (this.errorCount === 1) {
            _get(_getPrototypeOf(ErrorHandlerService.prototype), "handleError", this).call(this, error);
          }
        }
      }]);

      return ErrorHandlerService;
    }(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"]);

    ErrorHandlerService.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]
      }];
    };

    ErrorHandlerService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"]])], ErrorHandlerService);
    /***/
  },

  /***/
  "./src/app/shared/services/layout.service.ts":
  /*!***************************************************!*\
    !*** ./src/app/shared/services/layout.service.ts ***!
    \***************************************************/

  /*! exports provided: LayoutService */

  /***/
  function srcAppSharedServicesLayoutServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LayoutService", function () {
      return LayoutService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../helpers/url.helper */
    "./src/app/shared/helpers/url.helper.ts");
    /* harmony import */


    var _theme_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./theme.service */
    "./src/app/shared/services/theme.service.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var LayoutService =
    /*#__PURE__*/
    function () {
      function LayoutService(themeService) {
        _classCallCheck(this, LayoutService);

        this.themeService = themeService;
        this.layoutConfSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.layoutConf);
        this.layoutConf$ = this.layoutConfSubject.asObservable();
        this.fullWidthRoutes = ["shop"];
        this.setAppLayout( //******** SET YOUR LAYOUT OPTIONS HERE *********
        {
          navigationPos: "side",
          sidebarStyle: "full",
          sidebarColor: "white",
          sidebarCompactToggle: false,
          dir: "ltr",
          useBreadcrumb: true,
          topbarFixed: false,
          topbarColor: "white",
          matTheme: "egret-blue",
          breadcrumb: "simple",
          perfectScrollbar: true
        });
      }

      _createClass(LayoutService, [{
        key: "setAppLayout",
        value: function setAppLayout(layoutConf) {
          this.layoutConf = Object.assign({}, this.layoutConf, layoutConf);
          this.applyMatTheme(this.layoutConf.matTheme); //******* Only for demo purpose ***

          this.setLayoutFromQuery(); //**********************
        }
      }, {
        key: "publishLayoutChange",
        value: function publishLayoutChange(lc) {
          var opt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

          if (this.layoutConf.matTheme !== lc.matTheme && lc.matTheme) {
            this.themeService.changeTheme(this.layoutConf.matTheme, lc.matTheme);
          }

          this.layoutConf = Object.assign(this.layoutConf, lc);
          this.layoutConfSubject.next(this.layoutConf);
        }
      }, {
        key: "applyMatTheme",
        value: function applyMatTheme(theme) {
          this.themeService.applyMatTheme(this.layoutConf.matTheme);
        }
      }, {
        key: "setLayoutFromQuery",
        value: function setLayoutFromQuery() {
          var layoutConfString = Object(_helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__["getQueryParam"])("layout");
          var prevTheme = this.layoutConf.matTheme;

          try {
            this.layoutConf = JSON.parse(layoutConfString);
            this.themeService.changeTheme(prevTheme, this.layoutConf.matTheme);
          } catch (e) {}
        }
      }, {
        key: "adjustLayout",
        value: function adjustLayout() {
          var _this29 = this;

          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
          var sidebarStyle;
          this.isMobile = this.isSm();
          this.currentRoute = options.route || this.currentRoute;
          sidebarStyle = this.isMobile ? "closed" : this.layoutConf.sidebarStyle;

          if (this.currentRoute) {
            this.fullWidthRoutes.forEach(function (route) {
              if (_this29.currentRoute.indexOf(route) !== -1) {
                sidebarStyle = "closed";
              }
            });
          }

          this.publishLayoutChange({
            isMobile: this.isMobile,
            sidebarStyle: sidebarStyle
          });
        }
      }, {
        key: "isSm",
        value: function isSm() {
          return window.matchMedia("(max-width: 959px)").matches;
        }
      }]);

      return LayoutService;
    }();

    LayoutService.ctorParameters = function () {
      return [{
        type: _theme_service__WEBPACK_IMPORTED_MODULE_3__["ThemeService"]
      }];
    };

    LayoutService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: "root"
    }), __metadata("design:paramtypes", [_theme_service__WEBPACK_IMPORTED_MODULE_3__["ThemeService"]])], LayoutService);
    /***/
  },

  /***/
  "./src/app/shared/services/match-media.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/shared/services/match-media.service.ts ***!
    \********************************************************/

  /*! exports provided: MatchMediaService */

  /***/
  function srcAppSharedServicesMatchMediaServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MatchMediaService", function () {
      return MatchMediaService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var MatchMediaService =
    /*#__PURE__*/
    function () {
      function MatchMediaService(mediaObserver) {
        _classCallCheck(this, MatchMediaService);

        this.mediaObserver = mediaObserver;
        this.onMediaChange = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('');
        this.activeMediaQuery = '';
        this.init();
      }

      _createClass(MatchMediaService, [{
        key: "init",
        value: function init() {
          var _this30 = this;

          this.mediaObserver.media$.subscribe(function (change) {
            if (_this30.activeMediaQuery !== change.mqAlias) {
              _this30.activeMediaQuery = change.mqAlias;

              _this30.onMediaChange.next(change.mqAlias);
            }
          });
        }
      }]);

      return MatchMediaService;
    }();

    MatchMediaService.ctorParameters = function () {
      return [{
        type: _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["MediaObserver"]
      }];
    };

    MatchMediaService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
      providedIn: 'root'
    }), __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["MediaObserver"]])], MatchMediaService);
    /***/
  },

  /***/
  "./src/app/shared/services/navigation.service.ts":
  /*!*******************************************************!*\
    !*** ./src/app/shared/services/navigation.service.ts ***!
    \*******************************************************/

  /*! exports provided: NavigationService */

  /***/
  function srcAppSharedServicesNavigationServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NavigationService", function () {
      return NavigationService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var NavigationService =
    /*#__PURE__*/
    function () {
      function NavigationService() {
        _classCallCheck(this, NavigationService);

        this.iconMenu = [// {
        //   name: "HOME",
        //   type: "icon",
        //   tooltip: "Home",
        //   icon: "home",
        //   state: "home"
        // },
        // {
        //   name: "PROFILE",
        //   type: "icon",
        //   tooltip: "Profile",
        //   icon: "person",
        //   state: "profile/overview"
        // },
        // {
        //   name: "TOUR",
        //   type: "icon",
        //   tooltip: "Tour",
        //   icon: "flight_takeoff",
        //   state: "tour"
        // },
        // {
        //   type: "separator",
        //   name: "Main Items"
        // },
        {
          name: "Dashboard",
          type: "link",
          tooltip: "CRUD Table",
          icon: "home",
          state: "dashboard"
        }, {
          name: "Administration",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "home",
          state: ""
        }, {
          name: "Settings",
          type: "dropDown",
          tooltip: "Dashboard",
          icon: "settings_applications",
          state: "",
          sub: [{
            name: "Social Media Settings",
            state: "facebook"
          }, {
            name: "SMS Settings",
            state: "comingsoon/show"
          }, {
            name: "Chatbot Settings",
            state: "comingsoon/show"
          }, {
            name: "Email Settings",
            state: "emailsettings/show"
          }, {
            name: "Marketing API",
            state: "comingsoon/show"
          }, {
            name: "Payment",
            state: "comingsoon/show"
          }]
        }, {
          name: "Team Management",
          type: "link",
          tooltip: "CRUD Table",
          icon: "people",
          state: "dashboard/team"
        }, {
          name: "CRM",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "home",
          state: ""
        }, {
          name: "Contacts",
          type: "link",
          tooltip: "CRUD Table",
          icon: "perm_contact_calendar",
          state: "contact/show"
        }, {
          name: "Digital Marketing",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "home",
          state: "comingsoon/show"
        }, {
          name: "Social Posting",
          type: "link",
          tooltip: "CRUD Table",
          icon: "calendar_view_day",
          state: "calendar"
        }, {
          name: "Paid Social Media Campaign",
          type: "link",
          tooltip: "CRUD Table",
          icon: "attach_money",
          state: "comingsoon/show"
        }, {
          name: "SMS Campaign",
          type: "link",
          tooltip: "CRUD Table",
          icon: "textsms",
          state: "comingsoon/show"
        }, {
          name: "Email Campaign",
          type: "link",
          tooltip: "CRUD Table",
          icon: "email",
          state: "email/show"
        }, {
          name: "AI Bot",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "home",
          state: ""
        }, {
          name: "Chatbot Dialog Management",
          type: "link",
          tooltip: "CRUD Table",
          icon: "face",
          state: "comingsoon/show"
        }, {
          name: "Comment Management",
          type: "link",
          tooltip: "CRUD Table",
          icon: "comment",
          state: "comingsoon/show"
        }, {
          name: "Team Communication",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "home",
          state: ""
        }, {
          name: "Team Feed",
          type: "link",
          tooltip: "CRUD Table",
          icon: "post_add",
          state: "postmanagement"
        }, {
          name: "Tasks",
          type: "link",
          tooltip: "CRUD Table",
          icon: "list",
          state: "tasks/show"
        }, {
          name: "Hire",
          type: "link",
          tooltip: "CRUD Table",
          icon: "supervisor_account",
          state: "tasks/show"
        }, {
          name: "Service",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "post_add",
          state: "postmanagement"
        }, {
          name: "Reservation",
          type: "link",
          tooltip: "CRUD Table",
          icon: "hotel",
          state: "reservation"
        }, {
          name: "Content Storage",
          type: "link",
          tooltip: "CRUD Table",
          icon: "cloud",
          state: "storage"
        }, {
          name: "Inventory",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: "inventory"
        }, {
          name: "Crowd Sourcing",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "post_add",
          state: ""
        }, {
          name: "Mechanical Turk",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: "inventory"
        }, {
          name: "Facebook Group Posting",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: "inventory"
        }, {
          name: "LinkedIn Posting",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: "inventory"
        }, {
          name: "Web Scraping",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: "inventory"
        }, {
          name: "Internet Of Things",
          type: "separator",
          tooltip: "CRUD Table",
          icon: "post_add",
          state: ""
        }, {
          name: "Smart Watch",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: ""
        }, {
          name: "Beacon",
          type: "link",
          tooltip: "CRUD Table",
          icon: "open_with",
          state: ""
        }]; // Icon menu TITLE at the very top of navigation.
        // This title will appear if any icon type item is present in menu.

        this.iconTypeMenuTitle = "Frequently Accessed"; // sets iconMenu as default;

        this.menuItems = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.iconMenu); // navigation component has subscribed to this Observable

        this.menuItems$ = this.menuItems.asObservable();
      } // Customizer component uses this method to change menu.
      // You can remove this method and customizer component.
      // Or you can customize this method to supply different menu for
      // different user type.


      _createClass(NavigationService, [{
        key: "publishNavigationChange",
        value: function publishNavigationChange(menuType) {
          switch (menuType) {
            case "separator-menu":
              // this.menuItems.next(this.separatorMenu);
              break;

            case "icon-menu":
              this.menuItems.next(this.iconMenu);
              break;

            default: // this.menuItems.next(this.plainMenu);

          }
        }
      }]);

      return NavigationService;
    }();

    NavigationService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __metadata("design:paramtypes", [])], NavigationService);
    /***/
  },

  /***/
  "./src/app/shared/services/route-parts.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/shared/services/route-parts.service.ts ***!
    \********************************************************/

  /*! exports provided: RoutePartsService */

  /***/
  function srcAppSharedServicesRoutePartsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoutePartsService", function () {
      return RoutePartsService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var RoutePartsService =
    /*#__PURE__*/
    function () {
      function RoutePartsService(router) {
        _classCallCheck(this, RoutePartsService);

        this.router = router;
      }

      _createClass(RoutePartsService, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "generateRouteParts",
        value: function generateRouteParts(snapshot) {
          var routeParts = [];

          if (snapshot) {
            if (snapshot.firstChild) {
              routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }

            if (snapshot.data['title'] && snapshot.url.length) {
              // console.log(snapshot.data['title'], snapshot.url)
              routeParts.push({
                title: snapshot.data['title'],
                breadcrumb: snapshot.data['breadcrumb'],
                url: snapshot.url[0].path,
                urlSegments: snapshot.url,
                params: snapshot.params
              });
            }
          }

          return routeParts;
        }
      }]);

      return RoutePartsService;
    }();

    RoutePartsService.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }];
    };

    RoutePartsService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])], RoutePartsService);
    /***/
  },

  /***/
  "./src/app/shared/services/theme.service.ts":
  /*!**************************************************!*\
    !*** ./src/app/shared/services/theme.service.ts ***!
    \**************************************************/

  /*! exports provided: ThemeService */

  /***/
  function srcAppSharedServicesThemeServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ThemeService", function () {
      return ThemeService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../helpers/url.helper */
    "./src/app/shared/helpers/url.helper.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __param = undefined && undefined.__param || function (paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ThemeService =
    /*#__PURE__*/
    function () {
      function ThemeService(document, rendererFactory) {
        _classCallCheck(this, ThemeService);

        this.document = document;
        this.onThemeChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.egretThemes = [{
          "name": "egret-dark-purple",
          "baseColor": "#9c27b0",
          "isActive": false
        }, {
          "name": "egret-dark-pink",
          "baseColor": "#e91e63",
          "isActive": false
        }, {
          "name": "egret-blue",
          "baseColor": "#03a9f4",
          "isActive": true
        }, {
          "name": "egret-light-purple",
          "baseColor": "#7367f0",
          "isActive": false
        }, {
          "name": "egret-navy",
          "baseColor": "#10174c",
          "isActive": false
        }];
        this.renderer = rendererFactory.createRenderer(null, null);
      } // Invoked in AppComponent and apply 'activatedTheme' on startup


      _createClass(ThemeService, [{
        key: "applyMatTheme",
        value: function applyMatTheme(themeName) {
          // this.renderer = r;
          this.activatedTheme = this.egretThemes[2]; // *********** ONLY FOR DEMO **********

          this.setThemeFromQuery(); // ************************************
          // this.changeTheme(themeName);

          this.renderer.addClass(this.document.body, themeName);
        }
      }, {
        key: "changeTheme",
        value: function changeTheme(prevTheme, themeName) {
          this.renderer.removeClass(this.document.body, prevTheme);
          this.renderer.addClass(this.document.body, themeName);
          this.flipActiveFlag(themeName);
          this.onThemeChange.emit(this.activatedTheme);
        }
      }, {
        key: "flipActiveFlag",
        value: function flipActiveFlag(themeName) {
          var _this31 = this;

          this.egretThemes.forEach(function (t) {
            t.isActive = false;

            if (t.name === themeName) {
              t.isActive = true;
              _this31.activatedTheme = t;
            }
          });
        } // *********** ONLY FOR DEMO **********

      }, {
        key: "setThemeFromQuery",
        value: function setThemeFromQuery() {
          var themeStr = Object(_helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__["getQueryParam"])('theme');

          try {
            this.activatedTheme = JSON.parse(themeStr);
            console.log(this.activatedTheme);
            this.flipActiveFlag(this.activatedTheme.name);
          } catch (e) {}
        }
      }]);

      return ThemeService;
    }();

    ThemeService.ctorParameters = function () {
      return [{
        type: Document,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"]]
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"]
      }];
    };

    ThemeService = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(), __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"])), __metadata("design:paramtypes", [Document, _angular_core__WEBPACK_IMPORTED_MODULE_0__["RendererFactory2"]])], ThemeService);
    /***/
  },

  /***/
  "./src/app/shared/shared-material.module.ts":
  /*!**************************************************!*\
    !*** ./src/app/shared/shared-material.module.ts ***!
    \**************************************************/

  /*! exports provided: SharedMaterialModule */

  /***/
  function srcAppSharedSharedMaterialModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedMaterialModule", function () {
      return SharedMaterialModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material-moment-adapter */
    "./node_modules/@angular/material-moment-adapter/esm2015/material-moment-adapter.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var SharedMaterialModule = function SharedMaterialModule() {
      _classCallCheck(this, SharedMaterialModule);
    };

    SharedMaterialModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      exports: [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSliderModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSlideToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenavModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatGridListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatStepperModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatButtonToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatChipsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressSpinnerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatProgressBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSortModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"], _angular_material_moment_adapter__WEBPACK_IMPORTED_MODULE_2__["MatMomentDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTreeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatRippleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatBadgeModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatBottomSheetModule"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDividerModule"]]
    })], SharedMaterialModule);
    /***/
  },

  /***/
  "./src/app/shared/shared.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/shared/shared.module.ts ***!
    \*****************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcAppSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./services/theme.service */
    "./src/app/shared/services/theme.service.ts");
    /* harmony import */


    var _services_navigation_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./services/navigation.service */
    "./src/app/shared/services/navigation.service.ts");
    /* harmony import */


    var _services_route_parts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./services/route-parts.service */
    "./src/app/shared/services/route-parts.service.ts");
    /* harmony import */


    var _services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./services/auth/auth.guard */
    "./src/app/shared/services/auth/auth.guard.ts");
    /* harmony import */


    var _services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./services/app-confirm/app-confirm.service */
    "./src/app/shared/services/app-confirm/app-confirm.service.ts");
    /* harmony import */


    var _services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./services/app-loader/app-loader.service */
    "./src/app/shared/services/app-loader/app-loader.service.ts");
    /* harmony import */


    var _components_shared_components_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/shared-components.module */
    "./src/app/shared/components/shared-components.module.ts");
    /* harmony import */


    var _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./pipes/shared-pipes.module */
    "./src/app/shared/pipes/shared-pipes.module.ts");
    /* harmony import */


    var _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./directives/shared-directives.module */
    "./src/app/shared/directives/shared-directives.module.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }; // SERVICES


    var SharedModule = function SharedModule() {
      _classCallCheck(this, SharedModule);
    };

    SharedModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _components_shared_components_module__WEBPACK_IMPORTED_MODULE_8__["SharedComponentsModule"], _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_9__["SharedPipesModule"], _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_10__["SharedDirectivesModule"]],
      providers: [_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"], _services_navigation_service__WEBPACK_IMPORTED_MODULE_3__["NavigationService"], _services_route_parts_service__WEBPACK_IMPORTED_MODULE_4__["RoutePartsService"], _services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_6__["AppConfirmService"], _services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_7__["AppLoaderService"]],
      exports: [_components_shared_components_module__WEBPACK_IMPORTED_MODULE_8__["SharedComponentsModule"], _pipes_shared_pipes_module__WEBPACK_IMPORTED_MODULE_9__["SharedPipesModule"], _directives_shared_directives_module__WEBPACK_IMPORTED_MODULE_10__["SharedDirectivesModule"]]
    })], SharedModule);
    /***/
  },

  /***/
  "./src/app/views/comingsoon/comingsoon.component.scss":
  /*!************************************************************!*\
    !*** ./src/app/views/comingsoon/comingsoon.component.scss ***!
    \************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsComingsoonComingsoonComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvY29taW5nc29vbi9jb21pbmdzb29uLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/comingsoon/comingsoon.component.ts":
  /*!**********************************************************!*\
    !*** ./src/app/views/comingsoon/comingsoon.component.ts ***!
    \**********************************************************/

  /*! exports provided: ComingsoonComponent */

  /***/
  function srcAppViewsComingsoonComingsoonComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ComingsoonComponent", function () {
      return ComingsoonComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ComingsoonComponent =
    /*#__PURE__*/
    function () {
      function ComingsoonComponent() {
        _classCallCheck(this, ComingsoonComponent);
      }

      _createClass(ComingsoonComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ComingsoonComponent;
    }();

    ComingsoonComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-comingsoon',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./comingsoon.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/comingsoon/comingsoon.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./comingsoon.component.scss */
      "./src/app/views/comingsoon/comingsoon.component.scss")).default]
    }), __metadata("design:paramtypes", [])], ComingsoonComponent);
    /***/
  },

  /***/
  "./src/app/views/comingsoon/comingsoon.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/views/comingsoon/comingsoon.module.ts ***!
    \*******************************************************/

  /*! exports provided: comingsoon */

  /***/
  function srcAppViewsComingsoonComingsoonModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "comingsoon", function () {
      return comingsoon;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _comingsoon_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./comingsoon.component */
    "./src/app/views/comingsoon/comingsoon.component.ts");
    /* harmony import */


    var _comingsoon_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./comingsoon.routing */
    "./src/app/views/comingsoon/comingsoon.routing.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var comingsoon = function comingsoon() {
      _classCallCheck(this, comingsoon);
    };

    comingsoon = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_comingsoon_routing__WEBPACK_IMPORTED_MODULE_7__["ComingsoonRoutes"])],
      declarations: [_comingsoon_component__WEBPACK_IMPORTED_MODULE_6__["ComingsoonComponent"]]
    })], comingsoon);
    /***/
  },

  /***/
  "./src/app/views/comingsoon/comingsoon.routing.ts":
  /*!********************************************************!*\
    !*** ./src/app/views/comingsoon/comingsoon.routing.ts ***!
    \********************************************************/

  /*! exports provided: ComingsoonRoutes */

  /***/
  function srcAppViewsComingsoonComingsoonRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ComingsoonRoutes", function () {
      return ComingsoonRoutes;
    });
    /* harmony import */


    var _comingsoon_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./comingsoon.component */
    "./src/app/views/comingsoon/comingsoon.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ComingsoonRoutes = [{
      path: 'show',
      component: _comingsoon_component__WEBPACK_IMPORTED_MODULE_0__["ComingsoonComponent"]
    }];
    /***/
  },

  /***/
  "./src/app/views/contact/contact.component.scss":
  /*!******************************************************!*\
    !*** ./src/app/views/contact/contact.component.scss ***!
    \******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsContactContactComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".fix {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksb0JBQWE7RUFBYixhQUFhO0VBQ2IsOEJBQW1CO0VBQW5CLDZCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIseUJBQThCO1VBQTlCLDhCQUE4QixFQUFBIiwiZmlsZSI6ImFwcC92aWV3cy9jb250YWN0L2NvbnRhY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZml4e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/views/contact/contact.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/views/contact/contact.component.ts ***!
    \****************************************************/

  /*! exports provided: ContactComponent */

  /***/
  function srcAppViewsContactContactComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactComponent", function () {
      return ContactComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ContactComponent =
    /*#__PURE__*/
    function () {
      function ContactComponent(client) {
        _classCallCheck(this, ContactComponent);

        this.client = client;
      }

      _createClass(ContactComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])('token');
          this.getContacts();
        }
      }, {
        key: "getContacts",
        value: function getContacts() {
          var _this32 = this;

          console.log("getting contacts");
          this.client.get('https://agile-cove-96115.herokuapp.com/contact/organization_contact/', this.header).subscribe(function (res) {
            console.log(res);
            _this32.contacts = res;
          });
        }
      }, {
        key: "addContact",
        value: function addContact() {
          var _this33 = this;

          var body = {
            name: this.name,
            email: this.email,
            address: this.address,
            phone_number: this.phone_number,
            company_name: this.company_name,
            designation: this.designation
          };
          console.log(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/contact/contact/', body, this.header).subscribe(function (res) {
            console.log("This is the contact post");
            var new_body = {
              contact_id: res['id']
            };

            _this33.client.post('https://agile-cove-96115.herokuapp.com/contact/organization_contact/', new_body, _this33.header).subscribe(function (res) {
              console.log(res);

              _this33.getContacts();

              _this33.name = "";
              _this33.email = "";
              _this33.address = "";
              _this33.phone_number = "";
              _this33.company_name = "";
              _this33.designation = "";
            });
          });
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          var _this34 = this;

          this.client.delete('https://agile-cove-96115.herokuapp.com/contact/organization_contact/' + id, this.header).subscribe(function (res) {
            console.log("Successfully deleted!");

            _this34.getContacts();
          });
        }
      }]);

      return ContactComponent;
    }();

    ContactComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }];
    };

    ContactComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-contact',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./contact.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/contact/contact.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./contact.component.scss */
      "./src/app/views/contact/contact.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])], ContactComponent);
    /***/
  },

  /***/
  "./src/app/views/contact/contact.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/views/contact/contact.module.ts ***!
    \*************************************************/

  /*! exports provided: ContactModule */

  /***/
  function srcAppViewsContactContactModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactModule", function () {
      return ContactModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _contact_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./contact.component */
    "./src/app/views/contact/contact.component.ts");
    /* harmony import */


    var _contact_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./contact.routing */
    "./src/app/views/contact/contact.routing.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ContactModule = function ContactModule() {
      _classCallCheck(this, ContactModule);
    };

    ContactModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_contact_routing__WEBPACK_IMPORTED_MODULE_7__["ContactRoutes"])],
      declarations: [_contact_component__WEBPACK_IMPORTED_MODULE_6__["ContactComponent"]]
    })], ContactModule);
    /***/
  },

  /***/
  "./src/app/views/contact/contact.routing.ts":
  /*!**************************************************!*\
    !*** ./src/app/views/contact/contact.routing.ts ***!
    \**************************************************/

  /*! exports provided: ContactRoutes */

  /***/
  function srcAppViewsContactContactRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactRoutes", function () {
      return ContactRoutes;
    });
    /* harmony import */


    var _contact_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./contact.component */
    "./src/app/views/contact/contact.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var ContactRoutes = [{
      path: 'show',
      component: _contact_component__WEBPACK_IMPORTED_MODULE_0__["ContactComponent"]
    }];
    /***/
  },

  /***/
  "./src/app/views/email/email.component.scss":
  /*!**************************************************!*\
    !*** ./src/app/views/email/email.component.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsEmailEmailComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".email-header {\n  display: -webkit-box;\n  display: flex; }\n\n.el {\n  margin-right: 5px; }\n\n.boxsizingBorder {\n  width: 100%; }\n\n.textareaContainer {\n  display: block;\n  border: 1px solid #eeeeee;\n  padding: 10px; }\n\n.member-section {\n  display: -webkit-box;\n  display: flex;\n  margin-right: 10px;\n  -webkit-box-pack: justify;\n          justify-content: space-between; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvZW1haWwvZW1haWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBYTtFQUFiLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxpQkFBaUIsRUFBQTs7QUFFckI7RUFDSSxXQUFXLEVBQUE7O0FBRWY7RUFDQyxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGFBQWEsRUFBQTs7QUFHZDtFQUNJLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQix5QkFBOEI7VUFBOUIsOEJBQThCLEVBQUEiLCJmaWxlIjoiYXBwL3ZpZXdzL2VtYWlsL2VtYWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVtYWlsLWhlYWRlcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5lbHtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcbi5ib3hzaXppbmdCb3JkZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLnRleHRhcmVhQ29udGFpbmVyIHtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRib3JkZXI6IDFweCBzb2xpZCAjZWVlZWVlO1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcbiAgfVxyXG5cclxuLm1lbWJlci1zZWN0aW9ue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/views/email/email.component.ts":
  /*!************************************************!*\
    !*** ./src/app/views/email/email.component.ts ***!
    \************************************************/

  /*! exports provided: EmailComponent */

  /***/
  function srcAppViewsEmailEmailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailComponent", function () {
      return EmailComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailComponent =
    /*#__PURE__*/
    function () {
      function EmailComponent(client) {
        _classCallCheck(this, EmailComponent);

        this.client = client;
      }

      _createClass(EmailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])('token');
          this.getList();
        }
      }, {
        key: "getList",
        value: function getList() {
          var _this35 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/schedule/getLists', this.header).subscribe(function (res) {
            //@ts-ignore
            _this35.lists = JSON.parse(res);
            console.log(_this35.lists);

            _this35.getMembers();
          });
        }
      }, {
        key: "addListMember",
        value: function addListMember() {
          var _this36 = this;

          var body = {
            id: this.lists.lists[0].id,
            email_address: this.add_email
          };
          console.log(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/addmember', body, this.header).subscribe(function (res) {
            // console.log(res)
            _this36.add_email = "";

            _this36.getMembers();
          });
          this.add_email = "";
        }
      }, {
        key: "removeEmail",
        value: function removeEmail(email) {
          var _this37 = this;

          var body = {
            id: this.lists.lists[0].id,
            email_address: email
          };
          console.log(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/removemember', body, this.header).subscribe(function (res) {
            console.log(res);
            _this37.add_email = "";

            _this37.getMembers();
          });
        }
      }, {
        key: "getMembers",
        value: function getMembers() {
          var _this38 = this;

          var body = {
            id: this.lists.lists[0].id
          };
          console.log(body);
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/getmembers', body, this.header).subscribe(function (res) {
            //@ts-ignore
            _this38.members = JSON.parse(res);
            console.log("these are members");
            console.log(_this38.members);
          });
        }
      }, {
        key: "runCampaign",
        value: function runCampaign() {
          var _this39 = this;

          var body = {
            subject_line: this.subject_line,
            preview_text: this.preview_text,
            title: this.title,
            from_name: this.from_name,
            reply_to: this.reply_to,
            list_id: this.lists.lists[0].id
          };
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/createCampaign', body, this.header).subscribe(function (res) {
            //@ts-ignore
            _this39.campaign = JSON.parse(res); // console.log(this.campaign)

            _this39.addText();
          });
        }
      }, {
        key: "addText",
        value: function addText() {
          var _this40 = this;

          var body = {
            plain_text: this.plain_text,
            campaign_id: this.campaign.id
          };
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/addText', body, this.header).subscribe(function (res) {
            // console.log(res)
            _this40.sendEmail();
          });
        }
      }, {
        key: "sendEmail",
        value: function sendEmail() {
          var _this41 = this;

          var body = {
            campaign_id: this.campaign.id
          };
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/runCampaign', body, this.header).subscribe(function (res) {
            // console.log(res)
            _this41.subject_line = "";
            _this41.preview_text = "";
            _this41.title = "";
            _this41.from_name = "";
            _this41.reply_to = "";
            _this41.plain_text = "";
          });
          this.subject_line = "";
          this.preview_text = "";
          this.title = "";
          this.from_name = "";
          this.reply_to = "";
          this.plain_text = "";
        }
      }, {
        key: "isDisabled",
        value: function isDisabled() {
          if (this.subject_line && this.preview_text && this.title && this.from_name && this.reply_to && this.plain_text) {
            return false;
          }

          return true;
        }
      }]);

      return EmailComponent;
    }();

    EmailComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    EmailComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-email',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./email.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/email/email.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./email.component.scss */
      "./src/app/views/email/email.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])], EmailComponent);
    /***/
  },

  /***/
  "./src/app/views/email/email.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/views/email/email.module.ts ***!
    \*********************************************/

  /*! exports provided: EmailModule */

  /***/
  function srcAppViewsEmailEmailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailModule", function () {
      return EmailModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _email_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./email.component */
    "./src/app/views/email/email.component.ts");
    /* harmony import */


    var _email_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./email.routing */
    "./src/app/views/email/email.routing.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_quill__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-quill */
    "./node_modules/ngx-quill/fesm2015/ngx-quill.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailModule = function EmailModule() {
      _classCallCheck(this, EmailModule);
    };

    EmailModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"], ngx_quill__WEBPACK_IMPORTED_MODULE_9__["QuillModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_email_routing__WEBPACK_IMPORTED_MODULE_7__["EmailRoutes"])],
      declarations: [_email_component__WEBPACK_IMPORTED_MODULE_6__["EmailComponent"]]
    })], EmailModule);
    /***/
  },

  /***/
  "./src/app/views/email/email.routing.ts":
  /*!**********************************************!*\
    !*** ./src/app/views/email/email.routing.ts ***!
    \**********************************************/

  /*! exports provided: EmailRoutes */

  /***/
  function srcAppViewsEmailEmailRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailRoutes", function () {
      return EmailRoutes;
    });
    /* harmony import */


    var _email_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./email.component */
    "./src/app/views/email/email.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailRoutes = [{
      path: 'show',
      component: _email_component__WEBPACK_IMPORTED_MODULE_0__["EmailComponent"]
    }];
    /***/
  },

  /***/
  "./src/app/views/emailsettings/emailsettings.component.scss":
  /*!******************************************************************!*\
    !*** ./src/app/views/emailsettings/emailsettings.component.scss ***!
    \******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsEmailsettingsEmailsettingsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".social-card {\n  width: 150px;\n  height: 150px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center; }\n\n.content {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9DV09SSy9hbmd1bGFyL0FuZ3VsYXIvQW5ndWxhckN3b3JrL0VncmV0LXY4LjAuMC9mdWxsL3NyYy9hcHAvdmlld3MvZW1haWxzZXR0aW5ncy9lbWFpbHNldHRpbmdzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHlCQUFtQjtVQUFuQixtQkFBbUIsRUFBQTs7QUFJdkI7RUFDSSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw4QkFBbUI7RUFBbkIsNkJBQW1CO1VBQW5CLG1CQUFtQixFQUFBIiwiZmlsZSI6ImFwcC92aWV3cy9lbWFpbHNldHRpbmdzL2VtYWlsc2V0dGluZ3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc29jaWFsLWNhcmR7XHJcbiAgICB3aWR0aDogMTUwcHg7XHJcbiAgICBoZWlnaHQ6IDE1MHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBcclxufVxyXG5cclxuLmNvbnRlbnR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/views/emailsettings/emailsettings.component.ts":
  /*!****************************************************************!*\
    !*** ./src/app/views/emailsettings/emailsettings.component.ts ***!
    \****************************************************************/

  /*! exports provided: EmailsettingsComponent */

  /***/
  function srcAppViewsEmailsettingsEmailsettingsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailsettingsComponent", function () {
      return EmailsettingsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailsettingsComponent =
    /*#__PURE__*/
    function () {
      function EmailsettingsComponent(client, router, route) {
        _classCallCheck(this, EmailsettingsComponent);

        this.client = client;
        this.router = router;
        this.route = route;
        this.mail_chimp_httpOptions = {
          headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        };
      }

      _createClass(EmailsettingsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this42 = this;

          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])('token'); // let oauth = MailChimpOAuth(this.mailchimp_options)
          //@ts-ignore

          var param1 = this.route.snapshot.paramMap.get('code');
          console.log("THIS IS THE CODE");
          console.log(param1); // console.log(SessionStorage)

          this.route.queryParamMap.subscribe(function (params) {
            console.log(params.get('code'));
            _this42.code = params.get('code');

            if (_this42.code) {
              console.log("there is a code");
              var body = {
                code: _this42.code
              };

              _this42.client.post("https://agile-cove-96115.herokuapp.com/schedule/chimp/", body, _this42.header).subscribe(function (res) {
                console.log(res);
              });
            } else {
              console.log("there is no code");
            }
          });
        }
      }, {
        key: "mail_oauth",
        value: function mail_oauth() {
          // body = {
          //   grant_type=authorization_code&client_id={client_id}&client_secret={client_secret}&redirect_uri={encoded_url}&code={code}
          // }
          // this.client.get('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639&redirect_uri=https://www.cworkva.firebaseapp.com', this.mail_chimp_httpOptions).subscribe((res)=>{
          //   console.log(res)
          // })
          var mailchimp = window.open('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639');
        }
      }]);

      return EmailsettingsComponent;
    }();

    EmailsettingsComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    EmailsettingsComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-emailsettings',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./emailsettings.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/emailsettings/emailsettings.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./emailsettings.component.scss */
      "./src/app/views/emailsettings/emailsettings.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])], EmailsettingsComponent);
    /***/
  },

  /***/
  "./src/app/views/emailsettings/emailsettings.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/views/emailsettings/emailsettings.module.ts ***!
    \*************************************************************/

  /*! exports provided: EmailsettingsModule */

  /***/
  function srcAppViewsEmailsettingsEmailsettingsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailsettingsModule", function () {
      return EmailsettingsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-perfect-scrollbar */
    "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
    /* harmony import */


    var _emailsettings_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./emailsettings.component */
    "./src/app/views/emailsettings/emailsettings.component.ts");
    /* harmony import */


    var _emailsettings_routing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./emailsettings.routing */
    "./src/app/views/emailsettings/emailsettings.routing.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailsettingsModule = function EmailsettingsModule() {
      _classCallCheck(this, EmailsettingsModule);
    };

    EmailsettingsModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [// MailchimpModule,
      _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTooltipModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"], ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_5__["PerfectScrollbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_emailsettings_routing__WEBPACK_IMPORTED_MODULE_7__["EmailsettingsRoutes"])],
      declarations: [_emailsettings_component__WEBPACK_IMPORTED_MODULE_6__["EmailsettingsComponent"]]
    })], EmailsettingsModule);
    /***/
  },

  /***/
  "./src/app/views/emailsettings/emailsettings.routing.ts":
  /*!**************************************************************!*\
    !*** ./src/app/views/emailsettings/emailsettings.routing.ts ***!
    \**************************************************************/

  /*! exports provided: EmailsettingsRoutes */

  /***/
  function srcAppViewsEmailsettingsEmailsettingsRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailsettingsRoutes", function () {
      return EmailsettingsRoutes;
    });
    /* harmony import */


    var _emailsettings_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./emailsettings.component */
    "./src/app/views/emailsettings/emailsettings.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var EmailsettingsRoutes = [{
      path: 'show',
      component: _emailsettings_component__WEBPACK_IMPORTED_MODULE_0__["EmailsettingsComponent"]
    }];
    /***/
  },

  /***/
  "./src/app/views/task/task.component.scss":
  /*!************************************************!*\
    !*** ./src/app/views/task/task.component.scss ***!
    \************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsTaskTaskComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvdGFzay90YXNrLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/views/task/task.component.ts":
  /*!**********************************************!*\
    !*** ./src/app/views/task/task.component.ts ***!
    \**********************************************/

  /*! exports provided: TaskComponent */

  /***/
  function srcAppViewsTaskTaskComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TaskComponent", function () {
      return TaskComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var TaskComponent =
    /*#__PURE__*/
    function () {
      function TaskComponent(client) {
        _classCallCheck(this, TaskComponent);

        this.client = client;
        this.defaultShow = true;
        this.completeShow = true;
      }

      _createClass(TaskComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_1__["httpOptions"])('token');
          this.getTeamTasks();
          this.getCompleted();
        }
      }, {
        key: "getTeamTasks",
        value: function getTeamTasks() {
          var _this43 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/todolist/teamtasks', this.header).subscribe(function (res) {
            console.log(res);
            _this43.teamtasks = res; //@ts-ignore

            if (_this43.teamtasks.length == 0) {
              _this43.defaultShow = false;
            } //@ts-ignore


            if (_this43.teamtasks.length > 0) {
              _this43.defaultShow = true;
            }
          });
        }
      }, {
        key: "completeTask",
        value: function completeTask(id) {
          var _this44 = this;

          console.log(id);
          var body = {
            id: id
          };
          this.client.put('https://agile-cove-96115.herokuapp.com/todolist/update', body, this.header).subscribe(function (res) {
            console.log(res);

            _this44.getTeamTasks();

            _this44.getCompleted();
          });
        }
      }, {
        key: "getCompleted",
        value: function getCompleted() {
          var _this45 = this;

          this.client.get('https://agile-cove-96115.herokuapp.com/todolist/completed', this.header).subscribe(function (res) {
            console.log("completed");
            console.log(res);
            _this45.completed = res; //@ts-ignore

            if (_this45.completed.length == 0) {
              _this45.completeShow = false;
            } //@ts-ignore


            if (_this45.completed.length > 0) {
              _this45.completeShow = true;
            }
          });
        }
      }]);

      return TaskComponent;
    }();

    TaskComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    TaskComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-task',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./task.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/task/task.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./task.component.scss */
      "./src/app/views/task/task.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])], TaskComponent);
    /***/
  },

  /***/
  "./src/app/views/task/task.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/views/task/task.module.ts ***!
    \*******************************************/

  /*! exports provided: TaskModule */

  /***/
  function srcAppViewsTaskTaskModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TaskModule", function () {
      return TaskModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var angular_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-calendar */
    "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-calendar/date-adapters/date-fns */
    "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-color-picker */
    "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js");
    /* harmony import */


    var _task_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./task.component */
    "./src/app/views/task/task.component.ts");
    /* harmony import */


    var _task_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./task.routing */
    "./src/app/views/task/task.routing.ts");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var app_environment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! app/environment */
    "./src/app/environment.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var TaskModule = function TaskModule() {
      _classCallCheck(this, TaskModule);
    };

    TaskModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCheckboxModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__["ColorPickerModule"], angular_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"].forRoot({
        provide: angular_calendar__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"],
        useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__["adapterFactory"]
      }), _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_task_routing__WEBPACK_IMPORTED_MODULE_11__["TaskRoutes"]), _angular_fire__WEBPACK_IMPORTED_MODULE_12__["AngularFireModule"].initializeApp({
        apiKey: "AIzaSyCpENRtd-xU_iRD3N8u6mPt_SyvUGY1Qak",
        authDomain: "cworkva.firebaseapp.com",
        databaseURL: "https://cworkva.firebaseio.com",
        projectId: "cworkva",
        storageBucket: "cworkva.appspot.com",
        messagingSenderId: "944036100209",
        appId: "1:944036100209:web:2ba19f39dfa1fda85983d0"
      }), _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorageModule"]],
      providers: [{
        provide: _angular_fire__WEBPACK_IMPORTED_MODULE_12__["FirebaseOptionsToken"],
        useValue: app_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].firebase
      }],
      entryComponents: [],
      declarations: [_task_component__WEBPACK_IMPORTED_MODULE_10__["TaskComponent"]]
    })], TaskModule);
    /***/
  },

  /***/
  "./src/app/views/task/task.routing.ts":
  /*!********************************************!*\
    !*** ./src/app/views/task/task.routing.ts ***!
    \********************************************/

  /*! exports provided: TaskRoutes */

  /***/
  function srcAppViewsTaskTaskRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TaskRoutes", function () {
      return TaskRoutes;
    });
    /* harmony import */


    var _task_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./task.component */
    "./src/app/views/task/task.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var TaskRoutes = [{
      path: 'show',
      component: _task_component__WEBPACK_IMPORTED_MODULE_0__["TaskComponent"],
      data: {
        title: 'Tasks'
      }
    }];
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // The file contents for the current environment will overwrite these during build.
    // The build system defaults to the dev environment which uses `environment.ts`, but if you do
    // `ng build --env=prod` then `environment.prod.ts` will be used instead.
    // The list of which env maps to which file can be found in `.angular-cli.json`.


    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var environment = {
      production: false,
      apiURL: 'developmentApi'
    };
    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]).catch(function (err) {
      return console.log(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /mnt/c/CWORK/angular/Angular/AngularCwork/Egret-v8.0.0/full/src/main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map