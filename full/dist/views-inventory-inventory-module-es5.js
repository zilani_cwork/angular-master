function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-inventory-inventory-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/views/inventory/inventory.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/views/inventory/inventory.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewsInventoryInventoryComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div>\n  <table class=\"table\">\n\n\n    <thead>\n      <tr>\n        <th scope=\"col\">Organization</th>\n        <th scope=\"col\">Product Image</th>\n        <th scope=\"col\">Product Title</th>\n        <th scope=\"col\">Product Description</th>\n        <th scope=\"col\">Product Tag</th>\n        <th scope=\"col\">Category</th>\n        <th scope=\"col\">Price</th>\n\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let item of items\">\n        <td>{{item.organization.organization_name}}</td>\n        <td><img src={{item.product_image}} alt=\"\" width=\"50px\" height=\"50px\"></td>\n        <td>{{item.product_title}}</td>\n        <td>{{item.product_description}}</td>\n        <td>{{item.product_tag}}</td>\n        <td>{{item.category}}</td>\n        <td>{{item.price}}</td>\n      </tr>\n\n\n    </tbody>\n  </table>\n\n</div>\n\n\n\n\n\n\n\n\n\n<!-- Button trigger modal -->\n<button mat-raised-button type=\"button\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n  Add New Service</button>\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\"\n  aria-hidden=\"true\" data-backdrop=\"false\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">{{name}}</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" style=\"display: flex; flex-wrap: wrap; justify-content: space-evenly;\">\n        \n        <div>\n        <label for=\"img\" id=\"upload-icon\">\n    \n          <!-- <i style=\"margin-top: 20px;\" class=\"fas fa-image fa-3x\"></i> -->\n\n          <i class=\"material-icons icon-color\"  style=\"font-size: 40px;\">\n              add_photo_alternate\n          </i>\n\n      </label>\n\n        <input type=\"file\" name=\"imgUrl\" id=\"img\" (change)=\"preview($event)\" style=\"display: none;\" />\n        </div>\n        <br>\n        <mat-form-field class=\"example-full-width\">\n          <input matInput [value]=\"product_title\" (input)=\"product_title = $event.target.value\"  placeholder=\"Product Title\" >\n        </mat-form-field>\n        <mat-form-field class=\"example-full-width\">\n          <input matInput [value]=\"product_description\" (input)=\"product_description = $event.target.value\" placeholder=\"Product Description\" >\n        </mat-form-field>\n\n        <mat-form-field class=\"example-full-width\">\n          <input matInput [value]=\"product_tag\" (input)=\"product_tag = $event.target.value\" placeholder=\"Product Tag\" >\n        </mat-form-field>\n\n        <mat-form-field class=\"example-full-width\">\n          <input matInput [value]=\"category\" (input)=\"category = $event.target.value\" placeholder=\"Category\" >\n        </mat-form-field>\n\n        <mat-form-field class=\"example-full-width\">\n          <input matInput [value]=\"price\" (input)=\"price = $event.target.value\" placeholder=\"Price\">\n        </mat-form-field>\n\n\n\n      </div>\n      <div class=\"modal-footer\">\n        <button mat-raised-button type=\"button\" class=\"\" data-dismiss=\"modal\" (click)=\"addService()\">Add Service</button> \n      </div>\n    </div>\n  </div>\n</div>";
    /***/
  },

  /***/
  "./src/app/views/inventory/inventory.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/views/inventory/inventory.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewsInventoryInventoryComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAvdmlld3MvaW52ZW50b3J5L2ludmVudG9yeS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/views/inventory/inventory.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/views/inventory/inventory.component.ts ***!
    \********************************************************/

  /*! exports provided: InventoryComponent */

  /***/
  function srcAppViewsInventoryInventoryComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InventoryComponent", function () {
      return InventoryComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _httpOptions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../httpOptions */
    "./src/app/httpOptions.ts");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __metadata = undefined && undefined.__metadata || function (k, v) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var InventoryComponent =
    /*#__PURE__*/
    function () {
      function InventoryComponent(client, fireStorage) {
        _classCallCheck(this, InventoryComponent);

        this.client = client;
        this.fireStorage = fireStorage;
        this.httpOptions2 = {
          headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json',
            'Authorization': 'token ' + '	4b1e50f9ee7751b593ed4f7cd494af6f68013d22'
          })
        };
        this.product_title = "";
        this.product_description = "";
        this.product_tag = "";
        this.category = "";
        this.price = "";
      }

      _createClass(InventoryComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.header = Object(_httpOptions__WEBPACK_IMPORTED_MODULE_2__["httpOptions"])('token');
          this.getServices();
        }
      }, {
        key: "preview",
        value: function preview(event) {
          var _this = this;

          // console.log(event.target.files)
          // console.log(event.target.files[0].name)
          // this.imgUrl = event.target.files[0].name
          this.file = event.target.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(event.target.files[0]);

          reader.onload = function (_event) {
            _this.imgUrl = reader.result;
            console.log(_this.imgUrl);
          }; // this.fireStorage.upload('',this.file)
          // ref.getDownloadURL().subscribe((res)=>console.log(res))
          // task.then()

        }
      }, {
        key: "getServices",
        value: function getServices() {
          var _this2 = this;

          this.client.get("https://agile-cove-96115.herokuapp.com/services/services", this.header).subscribe(function (res) {
            console.log(res);
            _this2.items = res;
            _this2.name = _this2.items[0]['organization']['organization_name'];
            _this2.id = _this2.items[0]['organization']['organization_id'];
          });
        }
      }, {
        key: "addService",
        value: function addService() {
          var _this3 = this;

          var randomId = Math.random().toString(36).substring(2);
          this.ref = this.fireStorage.ref(randomId);
          this.task = this.ref.put(this.file).then(function (res) {
            _this3.ref.getDownloadURL().subscribe(function (res) {
              console.log("This is the url");
              console.log(res);
              var body = {
                "organization_id": _this3.id,
                "product_image": res,
                "product_title": _this3.product_title,
                "product_description": _this3.product_description,
                "product_tag": _this3.product_tag,
                "category": _this3.category,
                "price": _this3.price,
                "review": 10
              };
              var new_body = JSON.stringify(body);
              console.log(new_body);

              _this3.client.post('https://agile-cove-96115.herokuapp.com/services/services/', new_body, _this3.header).subscribe(function (r) {
                console.log(r);
                _this3.product_tag = "";
                _this3.product_title = "";
                _this3.category = "";
                _this3.price = "";
                _this3.product_description = "";

                _this3.getServices();
              });
            });
          });
        }
      }]);

      return InventoryComponent;
    }();

    InventoryComponent.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"]
      }];
    };

    InventoryComponent = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
      selector: 'app-inventory',
      template: __importDefault(__webpack_require__(
      /*! raw-loader!./inventory.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/views/inventory/inventory.component.html")).default,
      styles: [__importDefault(__webpack_require__(
      /*! ./inventory.component.scss */
      "./src/app/views/inventory/inventory.component.scss")).default]
    }), __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"]])], InventoryComponent);
    /***/
  },

  /***/
  "./src/app/views/inventory/inventory.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/views/inventory/inventory.module.ts ***!
    \*****************************************************/

  /*! exports provided: InventoryModule */

  /***/
  function srcAppViewsInventoryInventoryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InventoryModule", function () {
      return InventoryModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/flex-layout */
    "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
    /* harmony import */


    var angular_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-calendar */
    "./node_modules/angular-calendar/fesm2015/angular-calendar.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-calendar/date-adapters/date-fns */
    "./node_modules/angular-calendar/date-adapters/date-fns/index.js");
    /* harmony import */


    var angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__);
    /* harmony import */


    var ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-color-picker */
    "./node_modules/ngx-color-picker/dist/ngx-color-picker.es5.js");
    /* harmony import */


    var _inventory_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./inventory.component */
    "./src/app/views/inventory/inventory.component.ts");
    /* harmony import */


    var _inventory_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./inventory.routing */
    "./src/app/views/inventory/inventory.routing.ts");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var app_environment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! app/environment */
    "./src/app/environment.ts");

    var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var InventoryModule = function InventoryModule() {
      _classCallCheck(this, InventoryModule);
    };

    InventoryModule = __decorate([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"], _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"], ngx_color_picker__WEBPACK_IMPORTED_MODULE_9__["ColorPickerModule"], angular_calendar__WEBPACK_IMPORTED_MODULE_7__["CalendarModule"].forRoot({
        provide: angular_calendar__WEBPACK_IMPORTED_MODULE_7__["DateAdapter"],
        useFactory: angular_calendar_date_adapters_date_fns__WEBPACK_IMPORTED_MODULE_8__["adapterFactory"]
      }), _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_inventory_routing__WEBPACK_IMPORTED_MODULE_11__["InventoryRoutes"]), _angular_fire__WEBPACK_IMPORTED_MODULE_12__["AngularFireModule"].initializeApp({
        apiKey: "AIzaSyCpENRtd-xU_iRD3N8u6mPt_SyvUGY1Qak",
        authDomain: "cworkva.firebaseapp.com",
        databaseURL: "https://cworkva.firebaseio.com",
        projectId: "cworkva",
        storageBucket: "cworkva.appspot.com",
        messagingSenderId: "944036100209",
        appId: "1:944036100209:web:2ba19f39dfa1fda85983d0"
      }), _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorageModule"]],
      providers: [{
        provide: _angular_fire__WEBPACK_IMPORTED_MODULE_12__["FirebaseOptionsToken"],
        useValue: app_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].firebase
      }],
      entryComponents: [],
      declarations: [_inventory_component__WEBPACK_IMPORTED_MODULE_10__["InventoryComponent"]]
    })], InventoryModule);
    /***/
  },

  /***/
  "./src/app/views/inventory/inventory.routing.ts":
  /*!******************************************************!*\
    !*** ./src/app/views/inventory/inventory.routing.ts ***!
    \******************************************************/

  /*! exports provided: InventoryRoutes */

  /***/
  function srcAppViewsInventoryInventoryRoutingTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InventoryRoutes", function () {
      return InventoryRoutes;
    });
    /* harmony import */


    var _inventory_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ./inventory.component */
    "./src/app/views/inventory/inventory.component.ts");

    var __importDefault = undefined && undefined.__importDefault || function (mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    };

    var InventoryRoutes = [{
      path: '',
      component: _inventory_component__WEBPACK_IMPORTED_MODULE_0__["InventoryComponent"],
      data: {
        title: 'Inventory'
      }
    }];
    /***/
  }
}]);
//# sourceMappingURL=views-inventory-inventory-module-es5.js.map