import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: Observable<firebase.User>;
  public wrong_pass = false
  

  constructor(private angularFireAuth: AngularFireAuth, private client: HttpClient,public router:Router) {
    this.userData = angularFireAuth.authState;
  }

  // let httpOptions = 

//this is the function for signing up to the application
  CuserSignUp(email, password, body: object) {
    
    let new_body = JSON.stringify(body) // the body contains attributes like email, password
    console.log(new_body) // converted object to body for string request
    let id;

    //this is the post request using httpclient which is provided by angular
    this.client.post('https://agile-cove-96115.herokuapp.com/cuser/cusers/', new_body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe((res) => {
      // console.log(res.id)

      this.AddToTeam(email)
      id = res['id']
      this.SignUp(email, password, id)



    })

  }

  // our system uses firebase authentication so we we create a firebase object with the firebase UID of the user 
  FirebaseSignUp(body: object) {
    let new_body = JSON.stringify(body)
    this.client.post('https://agile-cove-96115.herokuapp.com/cuser/firebaseuser/', new_body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe((res) => {
      console.log(res)

      // this.angularFireAuth.auth.signInWithEmailLink(body['email'])
      
    })
  }

  // FirebaseUserSignUp()


// Invitation app is heavily commented in the python django portion of the code, 
// but basically this checks if you were invited to the system by any user and if you were
// then you are added to a team where this that user is the owner of the team
  AddToTeam(email:String){
    let body = {
      "email":email,
      "role":""
    }

    let new_body = JSON.stringify(body)
    this.client.post('https://agile-cove-96115.herokuapp.com/invitation/team/', new_body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }).subscribe((res) => console.log(res))
  
  }

  /* Sign up */

  // Signup using firebase these codes were provided by firebase
  SignUp(email: string, password: string, id: Number) {

    let uid;
    this.angularFireAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(res => {
        console.log('Successfully signed up!', res);
        console.log(res.user.uid)
        uid = res.user.uid
        console.log("THIS IS ID", id)
        console.log("This is UID", uid)
        let new_body = {
          "cuser": id,
          "firebase_uid": res.user.uid
        }

        this.FirebaseSignUp(new_body)


        return uid
      })
      .catch(error => {
        console.log('Something is wrong:', error.message);
      });


  }

  /* Sign in */

  // this function takes the email and the password
  // checks if the user exists in firebase
  // then checks the django database for user with that UID
  // if everything checks alright then a TOKEN is sent as response then that
  // TOKEN is stored in the local storage which is used by the httpOptions.ts file

  SignIn(email: string, password: string) {
    this.angularFireAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        // console.log('Successfully signed in!');
        // console.log(res.user.uid)

        let body = JSON.stringify({ "uid": res.user.uid })
        console.log(body)
        this.client.post('https://agile-cove-96115.herokuapp.com/cuser/login', body, {
          headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        }).subscribe((res) => {
          console.log(res)
          localStorage.clear()
          sessionStorage.clear()
          localStorage.setItem('Token', res['token']),
          localStorage.setItem('verified', res['verified'])
          
          // console.log(localStorage)
          if(localStorage.getItem('verified')=="true"){
            localStorage.setItem('isSignedIn', "true")

            this.router.navigateByUrl('/dashboard')
          }else{
            console.log(localStorage.getItem('verified'))
          }

          console.log("FLAG")
          console.log(localStorage)

        
          

        })
      })
      .catch(err => {
        console.log('Something is wrong:', err.message);
        // localStorage.setItem("wrong","true")
        this.wrong_pass = true
      });
  }

  /* Sign out */

  // just removes the token from local storage
  // sets signedin attribute to false
  SignOut() {
    localStorage.removeItem('Token')
    localStorage.setItem('isSignedIn', "false")
    localStorage.removeItem('FB')
    localStorage.clear()

    console.log("THIS IS LOCAL STORAGE")
    console.log(localStorage)
    console.log(sessionStorage)
    
    console.log()
    this.angularFireAuth
      .auth
      .signOut();

    localStorage.removeItem('Token')
    localStorage.setItem('isSignedIn', "false")
    localStorage.removeItem('FB')

    localStorage.clear()
    
  }


  // this code is provided by firebase to reset password

  ResetPassword(email:string){
    this.angularFireAuth.auth.sendPasswordResetEmail(email);
  }

  

}
