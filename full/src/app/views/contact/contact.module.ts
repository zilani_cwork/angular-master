import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatListModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatTooltipModule,
  MatInputModule,
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { ContactComponent } from './contact.component';
import { ContactRoutes } from './contact.routing';
import { FormsModule } from '@angular/forms'
@NgModule({
  imports: [
    MatInputModule,
    FormsModule,
    CommonModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(ContactRoutes)
  ],
  declarations: [ContactComponent,]
})
export class ContactModule { }
