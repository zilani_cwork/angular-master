import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// USED TO RETRIEVE THE HEADER FOR AUTHENTICATION
import {httpOptions} from '../../httpOptions'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {


  // DEFINING VARIABLES
  header:any
  contacts:any
  name:any
  email:any
  phone_number:any
  address:any
  company_name:any
  designation:any


  constructor(private client:HttpClient) { }

  ngOnInit() {
    this.header = httpOptions('token') // THIS IS THE HEADER
    this.getContacts()

  }

  // FOR GETTING ALL THE CONTACTS

  getContacts(){
    console.log("getting contacts")
    this.client.get('https://agile-cove-96115.herokuapp.com/contact/organization_contact/', this.header).subscribe((res)=>{
      console.log(res)
      this.contacts = res
    })


  }

  //FOR ADDING NEW CONTACTS
  addContact(){
    //POST BODY
    let body = {
      name:this.name, email:this.email, address:this.address, phone_number:this.phone_number, company_name:this.company_name, designation:this.designation
    }

    console.log(body)

    //MAKING THE POST REQUEST TO CREATE CONTACT
    this.client.post('https://agile-cove-96115.herokuapp.com/contact/contact/', body, this.header).subscribe((res)=>{
    console.log("This is the contact post")  
    let new_body = {
        contact_id:res['id']
      }
      //THIS REQUEST IS TO ADD THE CONTACT TO THE ORGANIZATION'S CONTACT LIST
      this.client.post('https://agile-cove-96115.herokuapp.com/contact/organization_contact/', new_body, this.header).subscribe((res)=>{
        console.log(res)
        this.getContacts()
        this.name = ""
        this.email = ""
        this.address = ""
        this.phone_number = ""
        this.company_name = ""
        this.designation = ""
      })
    })
  }

  // THIS IS TO DELETE A CONTACT BY SENDING THE ID
  delete(id){
    this.client.delete('https://agile-cove-96115.herokuapp.com/contact/organization_contact/'+id, this.header).subscribe((res)=>{
      console.log("Successfully deleted!")
      this.getContacts()
    })
  }



}
