import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { 
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatToolbarModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule
 } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { ColorPickerModule } from 'ngx-color-picker';
import { InventoryComponent } from './inventory.component';
import { InventoryRoutes } from "./inventory.routing";
import {AngularFireModule, FirebaseOptionsToken} from '@angular/fire'
import {AngularFireStorageModule} from '@angular/fire/storage'
import { environment } from 'app/environment';

@NgModule({
  imports: [

    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    FlexLayoutModule,
    ColorPickerModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    RouterModule.forChild(InventoryRoutes),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyCpENRtd-xU_iRD3N8u6mPt_SyvUGY1Qak",
      authDomain: "cworkva.firebaseapp.com",
      databaseURL: "https://cworkva.firebaseio.com",
      projectId: "cworkva",
      storageBucket: "cworkva.appspot.com",
      messagingSenderId: "944036100209",
      appId: "1:944036100209:web:2ba19f39dfa1fda85983d0"
    }),
    AngularFireStorageModule,
  ],
  providers: [{ provide: FirebaseOptionsToken, useValue: environment.firebase }],
  entryComponents: [],
  declarations: [
    InventoryComponent,
  ]
})
export class InventoryModule { }
