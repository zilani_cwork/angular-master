import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {httpOptions} from '../../httpOptions' // for auth
import { AngularFireStorage } from '@angular/fire/storage'// for firebase storage

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})

export class InventoryComponent implements OnInit {


  //no longer used
  // it was used before
  httpOptions2 = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'token ' + '	4b1e50f9ee7751b593ed4f7cd494af6f68013d22'
    })
  };


  //defining variables
  items:any
  name :any

  img: any

  imgUrl: any;
  file:any;

  ref:any
  task:any
  product_title=""
  product_description=""
  product_tag=""
  category=""
  price=""
  id:any
  header:any


  constructor(private client:HttpClient, public fireStorage: AngularFireStorage) { }

  ngOnInit() {
    this.header = httpOptions('token')
    this.getServices()
  }


  // showing the preview of the image
  preview(event) {
    // console.log(event.target.files)
    // console.log(event.target.files[0].name)
    // this.imgUrl = event.target.files[0].name
    this.file = event.target.files[0]
    //converts file into binary
    let reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload = (_event) => {
      // sets imgurl variable to the binary
      this.imgUrl = reader.result;
      console.log(this.imgUrl)
    }
    // this.fireStorage.upload('',this.file)

    // ref.getDownloadURL().subscribe((res)=>console.log(res))
    // task.then()

  }
  //getting all the services
  getServices(){
    this.client.get("https://agile-cove-96115.herokuapp.com/services/services", this.header).subscribe((res)=>{
      console.log(res)
      this.items = res
      this.name = this.items[0]['organization']['organization_name']
      this.id = this.items[0]['organization']['organization_id']

    })
  }


  //adding a service under a specific organization
  //  organization_ID is required
  // and rest of the data as shown below
  addService(){

    const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(this.file).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          let body = {
            "organization_id":this.id,
            "product_image":res,
            "product_title":this.product_title,
            "product_description":this.product_description,
            "product_tag":this.product_tag,
            "category":this.category,
            "price":this.price,
            "review":10
          }
          let new_body = JSON.stringify(body)
          console.log(new_body)
          this.client.post('https://agile-cove-96115.herokuapp.com/services/services/', new_body, this.header).subscribe((r) => {
            console.log(r)
            this.product_tag=""
            this.product_title=""
            this.category=""
            this.price=""
            this.product_description=""
            this.getServices()
          })
        })
      })
    
  }

}
