import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'

import { HttpHeaders } from '@angular/common/http';
import {httpOptions} from '../../httpOptions'
@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.scss']
})
export class UpgradeComponent implements OnInit {

  url = "https://api-sandbox.portwallet.com/api/v1/"
  params = {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'token '+localStorage.getItem('Token')
    })
  };
  header:any


  name = localStorage.getItem('UserFirstName')+" "+localStorage.getItem('UserLastName')
  email = localStorage.getItem('UserEmail')
  phone=""
  street=""
  city=""
  state =""
  zipcode=""
  country="BGD"

  custom = ""
  

  constructor(public client:HttpClient ) { }

  ngOnInit() {
    this.header = httpOptions(localStorage.getItem('Token'))

    
  }

  getInvoice(amount, name, description){
    
    
    console.log(amount)
    console.log(localStorage)

    let body = {

      amount: amount,
      name: this.name,
      email:this.email,
      phone: this.phone,
      street: this.street,
      city:this.city,
      state:this.state,
      zipcode:this.zipcode,
      country:this.country,
      productName:name,
      productDescription:description

    }

    let new_body = JSON.stringify(body)

    this.client.post('https://agile-cove-96115.herokuapp.com/wallet/createInvoice',new_body,this.header).subscribe((res)=>{
      console.log(res)
      let url = res['data']['action']['url'] 
      console.log(typeof url)
      console.log(url)
      window.open(url)
    })
  }

}
