import { Routes } from '@angular/router';

import {UpgradeComponent} from './upgrade.component'

export const UpgradeRoutes: Routes = [
  { path: '', component: UpgradeComponent, data: { title: 'Upgrade' } }
];