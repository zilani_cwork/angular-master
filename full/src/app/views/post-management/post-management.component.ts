import { Component, OnInit, Inject } from '@angular/core';
import { httpOptions } from '../../httpOptions'
// for auth
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AngularFireStorage } from '@angular/fire/storage'



@Component({
  selector: 'app-post-management',
  templateUrl: './post-management.component.html',
  styleUrls: ['./post-management.component.scss']
})
export class PostManagementComponent implements OnInit {
//setting variables
  caption = ""
  img: any

  imgUrl: any;
  file: any;
  header:any
  posts: any

  post_versions: any

  comments: any

  version_comments: any

  ref: any
  task: any

  constructor(public client: HttpClient, public dialog: MatDialog, public fireStorage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.header = httpOptions(localStorage.getItem('Token'))//auth header

    // getting all the posts
    this.getPosts()

    //getting all the versions of all the posts
    this.getVersions()
    console.log(localStorage)

    //getting the comments of the posts
    this.getComments()

    //getting comments of all the versions of the posts
    this.getVersionComments()

    console.log(this.fireStorage)
  }



  //converts file to binary data and sets imgURL to binary data to show preview
  preview(event) {
    // console.log(event.target.files)
    // console.log(event.target.files[0].name)
    // this.imgUrl = event.target.files[0].name
    this.file = event.target.files[0]
    let reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload = (_event) => {
      this.imgUrl = reader.result;
      // console.log(this.imgUrl)
    }
    // this.fireStorage.upload('',this.file)

    // ref.getDownloadURL().subscribe((res)=>console.log(res))
    // task.then()

  }


  //Scheduling the post with image
  Schedule() {
    // console.log("Schedule button was clicked")
    if (this.file) {


      // console.log(new_body)



      const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(this.file).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          let body = {
            image: res,
            caption: this.caption,
            post_type: "GRAPHICS"
          }
          let new_body = JSON.stringify(body)
          this.client.post('https://agile-cove-96115.herokuapp.com/version/post/', new_body, this.header).subscribe((r) => {
            // console.log(r)
            this.getPosts()
          })
        })
      })









    }
    else {
      // console.log("No Image here")
      // console.log("Content Writing")

      // scheduling the post without image
      let body = {
        image: "",
        caption: this.caption,
        post_type: "CONTENT_WRITING"
      }
      let new_body = JSON.stringify(body)
      // console.log(new_body)
      this.client.post('https://agile-cove-96115.herokuapp.com/version/post/', new_body, this.header).subscribe((r) => {
        console.log(r)
        this.caption = ""
        this.getPosts()
      })
    }
  }

  getPosts() {
    //getting all the posts
    this.client.get('https://agile-cove-96115.herokuapp.com/version/post/', this.header).subscribe((res) => {

      console.log(res)
      this.posts = res

    })
  }

  like(id) {
    console.log(id)
    //liking a post for this post id is sent here ID is the post ID

    let body = {
      id: id
    }

    let new_body = JSON.stringify(body)

    this.client.patch('https://agile-cove-96115.herokuapp.com/version/likePost', new_body, this.header).subscribe((res) => {
      console.log(res)
      this.getPosts()
    })
  }
  delete(id) {
    this.client.delete('https://agile-cove-96115.herokuapp.com/version/post/' + id, this.header).subscribe((res) => {
      console.log(res)
      this.getPosts()
    })
  }

  // modal to edit the post
  editDialog(obj): void {
    const dialogRef = this.dialog.open(PostAddModal, {
      width: '600px',
      data: { obj: obj, action: "edit" }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('Here I will call all the events again');
      console.log("calling the events")
      //retrieving posts and all versions after edit
      this.getPosts()
      this.getVersions()

      // setTimeout(()=>{this.getEvents()}, 2000)
      // this.getEvents()
      // this.getEvents()
      // this.getEvents()
    });
    this.getPosts()
    this.getVersions()

  }

  getVersions() {
    this.client.get('https://agile-cove-96115.herokuapp.com/version/version/', this.header).subscribe((res) => {
      console.log(res)
      this.post_versions = res
    })
  }

  //getting all data again
  generate() {
    this.getPosts()
    this.getVersions()
    this.getVersionComments()
  }


  //addint comments to a post and at the same time adding comments to that version in backend
  // here id is post id 

  addComment(e, id) {
    console.log(e.target.value)
    console.log(id)




    let body = {
      post_id: id,
      comment: e.target.value
    }

    // console.log(body)

    let new_body = JSON.stringify(body)

    console.log(new_body)

    this.client.post('https://agile-cove-96115.herokuapp.com/version/commentcreate/', new_body, this.header).subscribe((res) => {
      // console.log(res)
      // console.log("Comment Working")


      /// retreiving all data after adding the comments
      this.getComments()
      this.getVersionComments()
      this.getVersions()
      this.getPosts()



    })
    this.getPosts()
    this.getComments()


  }

  //getting all the comments

  getComments() {
    this.client.get('https://agile-cove-96115.herokuapp.com/version/commentcreate/', this.header).subscribe((res) => {
      // console.log(res)
      this.comments = res
    })
  }


  //getting all the comments of all the versions

  getVersionComments() {

    this.client.get('https://agile-cove-96115.herokuapp.com/version/versioncomment/', this.header).subscribe((res) => {
      console.log("THese are version comments")
      console.log(res)

      this.version_comments = res
    })

  }



  // finalizing the post. just setting finalize to true 

  finalize(id) {
    console.log(id)
    console.log(localStorage.getItem('Token'))

    let body = {
      id: id
    }

    let new_body = JSON.stringify(body)

    this.client.patch('https://agile-cove-96115.herokuapp.com/version/finalizePost', new_body, this.header).subscribe((res) => {
      console.log(res)
      this.getPosts()
    })


  }

}









// post add modal
@Component({
  selector: 'post-add-modal',
  templateUrl: 'post-add-modal.html',
  styleUrls: ['./post-management.component.scss']
})
export class PostAddModal {

  caption = ""
  img: any

  imgUrl2: any;
  file2: any;

  ref: any
  task: any
  header:any


  constructor(
    public dialogRef: MatDialogRef<PostAddModal>,
    @Inject(MAT_DIALOG_DATA) public data: {}, public client: HttpClient, public fireStorage:AngularFireStorage) { }

  ngOnInit() {
    this.header = httpOptions(localStorage.getItem('Token'))
    console.log(this.data)
    //data passed
    if (this.data['obj']['image']) {
      console.log("There is an image")
      this.caption = this.data['obj']['caption']
      this.imgUrl2 = this.data['obj']['image']
    }
    else {
      console.log("There is no image")
      this.caption = this.data['obj']['caption']
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  changeCaption() {

    this.caption = (<HTMLTextAreaElement>document.getElementById('caption')).value
    console.log(this.caption)
  }

  // same as before converts file to binary
  preview_post(event) {
    // console.log(event.target.files)
    // console.log(event.target.files[0].name)
    // this.imgUrl = event.target.files[0].name
    this.file2 = event.target.files[0]
    let reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload = (_event) => {
      this.imgUrl2 = reader.result;
      // console.log(this.imgUrl2)
    }
    // console.log(this.imgUrl2)

  }

  
  Edit_post() {
    this.dialogRef.close();
    if (this.file2) {
      console.log("A file was changed")
      let f = (<HTMLInputElement>document.getElementById('img2')).files[0]
      console.log(f)

      let formData = new FormData()
      let id = this.data['obj']['id']
      let version = this.data['obj']['version_number'] + 1
      formData.append('image', f)
      formData.append('id', id)
      formData.append('caption', this.caption)
      formData.append('version', version)


      const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(f).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          formData.append('image', res)
          formData.append('id', id)
          formData.append('caption', this.caption)
          formData.append('version', version)

          let body = {
            image:res,
            id:id,
            caption:this.caption,
            version:version
          }

          



          
          let test_body = JSON.stringify(body)

          console.log(test_body)
    
          this.client.put('https://agile-cove-96115.herokuapp.com/version/UpdatePostVersion', test_body, this.header).subscribe((res) => {
            console.log(res)
          })
        })
      })










      
    }
    else {
      console.log("Only Caption")

      let body = {
        id: this.data['obj']['id'],
        caption: this.caption,
        version: this.data['obj']['version_number'] + 1
      }

      let new_body = JSON.stringify(body)

      this.client.patch('https://agile-cove-96115.herokuapp.com/version/UpdateCaptionVersion', new_body, this.header).subscribe((res) => {
        console.log(res)
      })


    }

  }



}
