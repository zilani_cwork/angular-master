import { Routes } from '@angular/router';

import {PostManagementComponent} from './post-management.component'

export const PostManagementRoutes: Routes = [
  { path: '', component: PostManagementComponent, data: { title: 'PostManagement' } }
];