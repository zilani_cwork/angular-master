import { Component, OnInit } from '@angular/core';
import {httpOptions} from '../../httpOptions'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  header:any
  teamtasks:any
  defaultShow = true
  completeShow = true
  completed:any
  constructor(private client:HttpClient) { }

  ngOnInit() {
    this.header = httpOptions('token')
    this.getTeamTasks()
    this.getCompleted()
  }
  //basic post and get requests
  getTeamTasks(){
    this.client.get('https://agile-cove-96115.herokuapp.com/todolist/teamtasks', this.header).subscribe((res)=>{
      console.log(res)
      this.teamtasks = res
      //@ts-ignore
      if(this.teamtasks.length==0){
        this.defaultShow = false
      }
      //@ts-ignore
      if(this.teamtasks.length>0){
        this.defaultShow = true
      }
      
    })
  }
  completeTask(id){
    console.log(id)
    let body = {
      id:id
    }
    this.client.put('https://agile-cove-96115.herokuapp.com/todolist/update', body, this.header).subscribe((res)=>{
      console.log(res)
      this.getTeamTasks()
      this.getCompleted()
    })

  }

  getCompleted(){
    this.client.get('https://agile-cove-96115.herokuapp.com/todolist/completed', this.header).subscribe((res)=>{
    console.log("completed")  
    console.log(res)
    this.completed = res
    
    //@ts-ignore
    if(this.completed.length==0){
      this.completeShow = false
    }
    //@ts-ignore
    if(this.completed.length>0){
      this.completeShow = true
    }
    })
  }

}
