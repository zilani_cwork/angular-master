import { Routes } from '@angular/router';


import {TaskComponent} from './task.component'

export const TaskRoutes: Routes = [
  { path: 'show', component: TaskComponent, data: { title: 'Tasks' } }
];