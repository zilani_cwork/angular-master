import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

import { HttpHeaders } from '@angular/common/http';
// for authentication header
import { httpOptions } from '../../httpOptions'
import { Chart } from 'chart.js'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

// defining variables
  reservation: any
  checkin: any
  unhandled: any
  daily_users: any
  count: any
  CHART: any
  log: any

  // start and end of the pagination
  start = 0
  end = this.start + 10
  total: any
  number_of_chats_today: any

  //this was hardcoded for the demo this is the token of Mustafa Al Momin
  httpOptions2 = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'token ' + '	4b1e50f9ee7751b593ed4f7cd494af6f68013d22'
    })
  };
  organization: any

  org_name:any

  header: any
  constructor(public client: HttpClient) { }

  ngOnInit() {
    // the header authentication
    this.header = httpOptions(localStorage.getItem('Token'))
    // console.log('new new new')
    // console.log(this.header)

    //getting chart data
    this.getPostTypeCount()


    // console.log(Date.prototype.getFullYear())
    // console.log(Date.prototype.getMonth())
    // console.log(Date.prototype.getDay())
    // console.log(Date.prototype.getUTCDate)
    // console.log("THESE ARE OPTIONS")
    // getting current user
    this.getUser()
    // console.log(httpOptions)
    // console.log(localStorage)
    // getting the organization
    this.getOrganization()


  }

  // only the name has to be provided and the organization will be created
  createOrg(){
    this.client.post('https://agile-cove-96115.herokuapp.com/organization/userOrganization/', {organization_name:this.org_name}, this.header).subscribe((res)=>{
      console.log(res)
      this.getOrganization()
      this.org_name = ""
    })
  }


  // getting the user's organization
getOrganization() {
    this.client.get('https://agile-cove-96115.herokuapp.com/organization/userOrganization', this.header).subscribe((res) => {
      // console.log(res)
      this.organization = res[0]
      //@ts-ignore
      if (res.length > 0) {
        
        this.getChatbotLog()

        // // console.log(Date.now())

        // getting number of daily chats
        this.dailyChat()
        //getting number of daily unique users
        this.dailyUser()
        // getting number of unhandled chats
        this.getUnhandled()

        /// getting the check in count
        this.getCheckIn()
        this.getReservation()
      }
    })
  }


// THE FUNCTIONS BELOW ARE JUST CALLING THE API AND SETTING VARIABLE VALUES
  getUser() {
    this.client.get('https://agile-cove-96115.herokuapp.com/cuser/getUser', this.header).subscribe((res) => {
      // console.log(res[0]['user']['first_name'])
      localStorage.setItem('name', res[0]['user']['first_name'])
    })
  }

  getPostTypeCount() {
    this.client.get('https://agile-cove-96115.herokuapp.com/version/count', this.header).subscribe((res) => {
      // console.log(res)
      this.count = res
      // console.log(this.count['content_writing'])
      // console.log(this.count['graphics'])
      this.CHART = (<HTMLCanvasElement>document.getElementById('post-chart')).getContext('2d')

      let chart = new Chart(this.CHART, {
        type: 'doughnut',
        data: {

          labels: ['GRAPHICS', 'CONTENT WRITING'],
          datasets: [
            {
              label: 'Points',
              backgroundColor: ['#e67e22', '#2980b9'],
              data: [this.count['graphics'], this.count['content_writing']]
            }
          ]

        },
        options: {
          responsive: true,
          maintainAspectRatio: false
        }
      })


    })
  }

  getChatbotLog() {

    this.client.get('https://agile-cove-96115.herokuapp.com/organization/botConversations/', this.header).subscribe((res) => {
      // console.log(res)
      // console.log("LOG LOG LOG")
      this.total = res
      // console.log(typeof res)
      // @ts-ignore
      res = res.reverse()
      this.log = Array.prototype.slice.call(res, this.start, this.end)
    })

  }



  // this was used to create a csv file from the data
  download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
  }

  export_table_to_csv(html, filename) {
    var csv = [];
    var rows = document.querySelectorAll("table tr");

    for (var i = 0; i < rows.length; i++) {
      var row = [], cols = rows[i].querySelectorAll("td, th");

      for (var j = 0; j < cols.length; j++)
        row.push(cols[j].innerHTML);


      csv.push(row.join(","));
    }

    // Download CSV
    this.download_csv(csv.join("\n"), filename);
  }

  downloadAction() {
    var html = document.querySelector("table").outerHTML;
    this.export_table_to_csv(html, "data.csv");

  }
// end of csv download



//this function and next are used for pagination
  next() {

    // this.end = this.start+10
    // if(this.end+10>this.total.length){
    //   this.start = this.end
    //   this.end = this.total.length
    // }
    // else{
    //   this.start+= 10
    //   this.end = this.end+10
    // }
    if (this.total.length - this.end > 10) {
      this.start = this.end
      this.end = this.end + 10
    }
    else {
      this.start = this.end
      this.end = this.total.length

    }
    this.getChatbotLog()
  }

  prev() {
    if (this.start - 10 >= 0) {
      this.start -= 10
      this.end -= 10
      this.getChatbotLog()
    }
  }


  /// sending the current date and retrieving the number of daily chats

  dailyChat() {

    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let new_day = ""
    let new_month = ""
    if (day < 10) {
      new_day = "0" + day.toString()
    }
    else {
      new_day = day.toString()
    }
    if (month < 10) {
      new_month = "0" + month.toString()
    }
    else {
      new_month = month.toString()
    }

    let req_day = year.toString() + '-' + new_month + '-' + new_day
    // console.log("daily chats")
    // console.log(req_day)
    let body = {
      date: req_day
    }
    // console.log("This is body")
    // console.log(body)


    let new_body = JSON.stringify(body)
    // console.log(new_body)
    this.client.post('https://agile-cove-96115.herokuapp.com/organization/dailyChats', new_body, this.header).subscribe((res) => {
      // console.log("THESE ARE THE DAILY CHATS")

      this.number_of_chats_today = res['DailyChats']
      // console.log(this.number_of_chats_today)
    })




  }

    /// sending the current date and retrieving the number of daily users


  dailyUser() {

    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()


    let new_day = ""
    let new_month = ""
    if (day < 10) {
      new_day = "0" + day.toString()
    }
    else {
      new_day = day.toString()
    }
    if (month < 10) {
      new_month = "0" + month.toString()
    }
    else {
      new_month = month.toString()
    }



    let req_day = year.toString() + '-' + new_month + '-' + new_day
    // console.log(req_day)
    let body = {
      date: req_day
    }
    // console.log("This is body")
    // console.log(body)


    let new_body = JSON.stringify(body)
    // console.log(new_body)
    this.client.post('https://agile-cove-96115.herokuapp.com/organization/dailyUsers', new_body, this.header).subscribe((res) => {
      // console.log("THESE ARE THE DAILY CHATS")

      this.daily_users = res['users']

      // console.log(this.number_of_chats_today)
    })






  }

    /// sending the current date and retrieving the number of daily unhandled chatts

  getUnhandled() {

    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let new_day = ""
    let new_month = ""
    if (day < 10) {
      new_day = "0" + day.toString()
    }
    else {
      new_day = day.toString()
    }

    if (month < 10) {
      new_month = "0" + month.toString()
    }
    else {
      new_month = month.toString()
    }

    let req_day = year.toString() + '-' + new_month + '-' + new_day
    // console.log(req_day)
    let body = {
      date: req_day
    }
    // console.log("This is body")
    // console.log(body)


    let new_body = JSON.stringify(body)
    // console.log(new_body)
    this.client.post('https://agile-cove-96115.herokuapp.com/organization/unhandled', new_body, this.header).subscribe((res) => {
      // console.log("THESE ARE THE DAILY CHATS")

      this.unhandled = res['unhandled']

      // console.log(this.number_of_chats_today)
    })




  }
  /// sending the current date and retrieving the number of daily checkins

  getCheckIn() {

    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let new_day = ""
    let new_month = ""
    if (day < 10) {
      new_day = "0" + day.toString()
    }
    else {
      new_day = day.toString()
    }

    if (month < 10) {
      new_month = "0" + month.toString()
    }
    else {
      new_month = month.toString()
    }

    let req_day = year.toString() + '-' + new_month + '-' + new_day
    // console.log(req_day)
    let body = {
      date: req_day
    }

    let new_body = JSON.stringify(body)

    this.client.post('https://agile-cove-96115.herokuapp.com/organization/countcheckin', body).subscribe((res) => {
      this.checkin = res['check_in']
      // console.log("THIS IS THE CHECKIN")
      // console.log(this.checkin)
    })

  }


    /// sending the current date and retrieving the number of daily reservations


  getReservation() {

    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let new_day = ""
    let new_month = ""
    if (day < 10) {
      new_day = "0" + day.toString()
    }
    else {
      new_day = day.toString()
    }

    if (month < 10) {
      new_month = "0" + month.toString()
    }
    else {
      new_month = month.toString()
    }

    let req_day = year.toString() + '-' + new_month + '-' + new_day
    // console.log(req_day)
    let body = {
      date: req_day
    }

    let new_body = JSON.stringify(body)

    this.client.post('https://agile-cove-96115.herokuapp.com/services/countreserve', body).subscribe((res) => {
      this.reservation = res['ReservationCount']
      // console.log("THIS IS THE CHECKIN")
      // console.log(this.checkin)
    })

  }




}
