import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {httpOptions} from '../../../httpOptions'
// for the authentication header

import {} from '@angular/material'
@Component({
  selector: 'app-manage-team',
  templateUrl: './manage-team.component.html',
  styleUrls: ['./manage-team.component.scss']
})
export class ManageTeamComponent implements OnInit {


  /// defining variables
  email = ""
  active_email = ""
  search_results:any
  team:any
  c_teams:any
  header:any
  

  
 
  


  constructor(public client:HttpClient) { }

  ngOnInit() {
    //  defining the header
    this.header = httpOptions(localStorage.getItem('Token'))
    this.getTeam()
    this.currentTeams()

  }

  invite(i_type){
    // inviting the user for the time being the only role that was used was R, which is Read but basically everything can be done because not enough time was given

    if(i_type=='cWork'){
      console.log(i_type)
      console.log(this.email)
      let body = {
        "recipient":this.email,
        "role": 'R'
      }
  
      let new_body = JSON.stringify(body)
      
      // with the email the invitaion is sent
      this.client.post('https://agile-cove-96115.herokuapp.com/invitation/invitation/', new_body, this.header).subscribe((res)=>{
        console.log(res)
        this.email = ""
        // after the post request all the team members are retrieved again
        this.getTeam()
      })

    }
    else{
      console.log(i_type)
      // if the user is an active application user
      console.log(this.active_email)

      let body = {
        "email":this.active_email
      }

      let new_body = JSON.stringify(body)
      // the user is simply added to the team by clicking
      this.client.post('https://agile-cove-96115.herokuapp.com/invitation/addTeam/', new_body, this.header).subscribe((res)=>{
        console.log(res)
        this.active_email = ""
        this.getTeam()
      })


    }
  }
  searchUser(e){
    // through inpu in the form all active users can be selected and by clicking any one the addteam is triggered
    this.active_email = e.target.value

    if(this.active_email==""){
      this.search_results = []
    }
    else{

      this.client.get('https://agile-cove-96115.herokuapp.com/invitation/searchUser?search='+this.active_email).subscribe((res)=>{
        console.log(res)
        this.search_results = res
        console.log(this.search_results)
        
      })

    }

    
    
  }

  setActiveUser(a_email){
    this.search_results = []
    this.active_email = a_email
    console.log("clicked")

  }


  //retrieving all current teams
  getTeam(){
    this.client.get('https://agile-cove-96115.herokuapp.com/invitation/teams/', this.header).subscribe((res)=>{
      console.log("This is team")  
    // console.log(res)
      this.team = res
      console.log(this.team)
    })
  }

  // the owner can remove team members through this function

  removeMember(id){
    console.log(id)
    this.client.delete('https://agile-cove-96115.herokuapp.com/invitation/teams/'+id, this.header).subscribe((res)=>{
      this.getTeam()
    })
  }
  // any member can leave the team through this function
  quitTeam(id){
    this.client.delete('https://agile-cove-96115.herokuapp.com/invitation/deleteTeam/'+id+'/', this.header).subscribe((res)=>{
      this.currentTeams()
    })
  }


  // all the current teams of the user is shown through this

  currentTeams(){
    this.client.get('https://agile-cove-96115.herokuapp.com/invitation/currentTeams', this.header).subscribe((res)=>{
      console.log("These are the curent teams the user belongs to.")
      console.log(res)
      this.c_teams = res
    })
  }

}
