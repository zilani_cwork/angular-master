import { Routes } from "@angular/router";

import { AnalyticsComponent } from "./analytics/analytics.component";
import { DashboardDarkComponent } from "./dashboard-dark/dashboard-dark.component";
import { CryptocurrencyComponent } from "./cryptocurrency/cryptocurrency.component";
import { DefaultDashboardComponent } from "./default-dashboard/default-dashboard.component";
import {DashboardComponent} from './dashboard.component'
import {ManageTeamComponent} from './manage-team/manage-team.component'
export const DashboardRoutes: Routes = [
  {
    path: "default",
    component: DefaultDashboardComponent,
    data: { title: "Default", breadcrumb: "Default" }
  },
  {
    path: "team",
    component: ManageTeamComponent,
    data: { title: "Team Management", breadcrumb: "Manage" }
  },
  {
    path: "analytics",
    component: AnalyticsComponent,
    data: { title: "Analytics", breadcrumb: "Analytics" }
  },
  {
    path: "crypto",
    component: CryptocurrencyComponent,
    data: { title: "Cryptocurrency", breadcrumb: "Cryptocurrency" }
  },
  {
    path: "dark",
    component: DashboardDarkComponent,
    data: { title: "Dark Cards", breadcrumb: "Dark Cards" }
  }
  ,
  {
    path: "",
    component: DashboardComponent,
    data: { title: "Dashboard", breadcrumb: "Dashboard" }
  }
];
