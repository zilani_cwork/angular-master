import { Routes } from '@angular/router';

import { FacebookComponent } from './facebook.component';


export const FacebookRoutes: Routes = [
  { path: '', component: FacebookComponent }
];