import { Component, OnInit } from '@angular/core';
import {httpOptions} from '../../httpOptions'
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import {MailChimpOAuth} from 'mailchimp' // not required here



// authentication header
import { HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.scss']
})
export class FacebookComponent implements OnInit {
  // initializing the variables
  FB:any

  fb_id:any
  fb_accesstoken:any

  data_array:any
  page_select:any

  selected_value:any
  header:any

  req_arr:any
  mail_chimp_httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',

      'Access-Control-Allow-Origin':'*'
      
      
    })
  };

  code:any
  
  


  //this is not required
  mailchimp_options = {
    clientId: '471281846639',
    clientSecret: 'b9ca5027afbf1032194f65f87340f1da73656a1801ceaf00fa',
    redirectUri: 'https://cworkva.firebaseapp.com',
    ownServer: true,
    addPort: true,
    finalUri: 'https://cworkva.firebaseapp.com',
    code:'f76ba9aa66572120ca387fa8bc05bfb2'

    //this is not required
};

  constructor(private client:HttpClient, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.header = httpOptions('token')//auth header
    // facebook initialization
    this.facebookinit()



    //these codes are unnecessary these events will never take place because mailchimp was moved to a different componnetn
    // let oauth = MailChimpOAuth(this.mailchimp_options)
    //@ts-ignore
    let param1 = this.route.snapshot.paramMap.get('code');
    console.log("THIS IS THE CODE")
    console.log(param1)
    // console.log(SessionStorage)
    this.route.queryParamMap.subscribe((params)=>{
      console.log(params.get('code'))
      this.code = params.get('code')
      if(this.code){
        console.log("there is a code")
        let body = {
          code:this.code
        }
        this.client.post("https://agile-cove-96115.herokuapp.com/schedule/chimp/", body, this.header).subscribe((res)=>{
          console.log(res)
        })
      }
      else{
        console.log("there is no code")
      }
    })
        
    
  }
  // this code was provided by oauth facebook
  facebookinit(){
    (window as any).fbAsyncInit = ()=> {
      //@ts-ignore
      FB.init({
        appId      : '3004517459564703',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      //@ts-ignore
      FB.AppEvents.logPageView();
      // this.submitLogin()
      
      
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
       
     }(document, 'script', 'facebook-jssdk'));
     
    //  console.log(this.FB)
    //@ts-ignore
    
  }

  // once the user logs in post request is sent to the database to store userdata
  submitLogin(){
    console.log("submit login to facebook");
    // FB.login();
    //@ts-ignore
    FB.login((response)=>
        {
          
          console.log('submitLogin',response);
          if (response.authResponse)
          {
            //login success
            //login success code here
            //redirect to home page
            console.log(response.status)
            this.fb_id = response.authResponse['userID']
            // the response recieved from the response is stored in the django database
            this.client.post('https://agile-cove-96115.herokuapp.com/schedule/fbuser/', {id:this.fb_id, access_token:response.authResponse['accessToken']}, this.header).subscribe((res)=>{
              console.log(res)
            })
            this.fb_accesstoken = response.authResponse['accessToken']
            console.log(this.fb_id)
            console.log(this.fb_accesstoken)
            this.getdata(this.fb_id)

            
           }
           else
           {
           console.log('User login failed');
           this.router.navigateByUrl('/calendar')
           
           
         }
      });
      //@ts-ignore
      

  }

  getdata(user_id){

  
    //@ts-ignore
    FB.api(
      "/"+user_id+"/accounts?type=page",
      (response) =>{
        if (response && !response.error) {
          /* handle the result */
          console.log("User data")
          console.log(response)

          this.data_array = response.data
          let count = 0
          //@ts-ignore
          for(let i=0; i<this.data_array.length; i++){
            let body = {
              user_id:this.data_array[i]['id'],
              access_token:this.data_array[i]['access_token'],
              page_name:this.data_array[i]['name']

            }
            this.client.post('https://agile-cove-96115.herokuapp.com/schedule/ptcreate', body, this.header).subscribe((res)=>{
              console.log(res)
              count++


            })
            console.log(body)
          }
          //@ts-ignore
          
            this.router.navigateByUrl('/calendar')
          




          console.log(this.data_array)
          console.log(this.data_array)
        }
        else{
          console.log(response)
        }
      }
  );
  }
  t:any

  mail_oauth(){
    // body = {
    //   grant_type=authorization_code&client_id={client_id}&client_secret={client_secret}&redirect_uri={encoded_url}&code={code}
    // }
    // this.client.get('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639&redirect_uri=https://www.cworkva.firebaseapp.com', this.mail_chimp_httpOptions).subscribe((res)=>{
    //   console.log(res)
    // })
    let mailchimp = window.open('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639')

    

    
   
    
  }

  
  


  

}
