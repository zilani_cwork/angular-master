import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatListModule,
  MatCardModule,
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatInputModule,
  MatDialogModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatSelectModule,
  
  
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { EmailComponent } from './email.component';
import { EmailRoutes } from './email.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    MatSelectModule,
    QuillModule,
    MatListModule,
  MatCardModule,
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatInputModule,
  MatDialogModule,
  MatCheckboxModule,
  MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(EmailRoutes)
  ],
  declarations: [EmailComponent],

})
export class EmailModule { }
