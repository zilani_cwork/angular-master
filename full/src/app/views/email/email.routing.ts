import { Routes } from '@angular/router';

import { EmailComponent } from './email.component';


export const EmailRoutes: Routes = [
  { path: 'show', component: EmailComponent }
];