import { Component, OnInit } from '@angular/core';
import { httpOptions } from '../../httpOptions'
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {
  header: any
  access_token: any
  api_endpoint: any
  email: any
  mc_header: any
  lists: any
  members: any
  add_email: any

  subject_line: any
  preview_text: any
  title: any
  from_name: any
  reply_to: any

  campaign:any

  plain_text:any


  constructor(private client: HttpClient) { }

  ngOnInit() {
    this.header = httpOptions('token')
    this.getList()
  }


  //getting the list
  getList() {
    this.client.get('https://agile-cove-96115.herokuapp.com/schedule/getLists', this.header).subscribe((res) => {

      //@ts-ignore
      this.lists = JSON.parse(res)
      console.log(this.lists)
      this.getMembers()

    })
  }

  //adding member to the list of recievers
  addListMember() {
    let body = {
      id: this.lists.lists[0].id,
      email_address: this.add_email
    }
    console.log(body)
    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/addmember', body, this.header).subscribe((res) => {
      // console.log(res)
      this.add_email = ""
      this.getMembers()
    })
    this.add_email = ""
  }

  //removing certain members from the list
  removeEmail(email){

    let body = {
      id: this.lists.lists[0].id,
      email_address: email
    }
    console.log(body)
    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/removemember', body, this.header).subscribe((res) => {
      console.log(res)
      this.add_email = ""
      this.getMembers()
    })

  }
  // getting all added members
  getMembers(){

    let body = {
      id: this.lists.lists[0].id,
     
    }
    console.log(body)
    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/getmembers', body, this.header).subscribe((res) => {
      //@ts-ignore
      this.members = JSON.parse(res)
      console.log("these are members")
      console.log(this.members)
    })

  }

  // this api is used to create the campaign with the data taken below
  runCampaign() {
    let body = {
      subject_line: this.subject_line,
      preview_text: this.preview_text,
      title: this.title,
      from_name: this.from_name,
      reply_to: this.reply_to,
      list_id: this.lists.lists[0].id

    }

    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/createCampaign', body, this.header).subscribe((res)=>{

    //@ts-ignore
    this.campaign = JSON.parse(res)
    // console.log(this.campaign)

    this.addText()

    })


  }
//adding the text to the campaign. WHICH HAS TO BE DONE SEPARATELY VIA THIS API
  addText(){
    let body = {
      plain_text:this.plain_text,
      campaign_id:this.campaign.id
    }

    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/addText', body, this.header).subscribe((res)=>{
      // console.log(res)

      this.sendEmail()

    })
  }

  sendEmail(){
    let body={
      campaign_id:this.campaign.id
    }
    this.client.post('https://agile-cove-96115.herokuapp.com/schedule/runCampaign', body, this.header).subscribe((res)=>{
      // console.log(res)
      this.subject_line=""
      this.preview_text=""
      this.title=""
      this.from_name=""
      this.reply_to=""
      this.plain_text=""
    })

    this.subject_line=""
      this.preview_text=""
      this.title=""
      this.from_name=""
      this.reply_to=""
      this.plain_text=""
  }

  /// if the required variables are empty then the run campaign button is disabled
  isDisabled(){
    if(this.subject_line && this.preview_text && this.title && this.from_name && this.reply_to && this.plain_text){
      return false
    }
    return true
  }




}
