import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatListModule,
  MatCardModule,
  MatButtonModule,
  MatIconModule,
  MatTooltipModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { EmailsettingsComponent } from './emailsettings.component';
import { EmailsettingsRoutes } from './emailsettings.routing';

@NgModule({
  imports: [
    // MailchimpModule,
    CommonModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    RouterModule.forChild(EmailsettingsRoutes)
  ],
  declarations: [EmailsettingsComponent,],

})
export class EmailsettingsModule { }
