import { Routes } from '@angular/router';

import { EmailsettingsComponent } from './emailsettings.component';


export const EmailsettingsRoutes: Routes = [
  { path: 'show', component: EmailsettingsComponent }
];