import { Component, OnInit } from '@angular/core';
import {httpOptions} from '../../httpOptions'
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import {MailChimpOAuth} from 'mailchimp' // this is an npm package


//header for authentication
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-emailsettings',
  templateUrl: './emailsettings.component.html',
  styleUrls: ['./emailsettings.component.scss']
})
export class EmailsettingsComponent implements OnInit {
  FB:any

  fb_id:any
  fb_accesstoken:any

  data_array:any
  page_select:any

  selected_value:any
  header:any

  req_arr:any
  mail_chimp_httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',

      'Access-Control-Allow-Origin':'*'
      
      
    })
  };

  code:any
  

  constructor(private client:HttpClient, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {
    //header for authentication
    this.header = httpOptions('token')

    // let oauth = MailChimpOAuth(this.mailchimp_options)
    //@ts-ignore

    // if the parameter has a code key word arguement then api is called to add the mailchimp detail of the user
    // this will happen once the user logs in to mail chimp using api and will be redirected to this page with code kwarg
    let param1 = this.route.snapshot.paramMap.get('code');
    console.log("THIS IS THE CODE")
    console.log(param1)
    // console.log(SessionStorage)
    this.route.queryParamMap.subscribe((params)=>{
      console.log(params.get('code'))
      this.code = params.get('code')
      if(this.code){
        console.log("there is a code")
        let body = {
          code:this.code
        }
        this.client.post("https://agile-cove-96115.herokuapp.com/schedule/chimp/", body, this.header).subscribe((res)=>{
          console.log(res)
        })
      }
      else{
        console.log("there is no code")
      }
    })
  }


  //this is mailchimp oauth
  mail_oauth(){
    // body = {
    //   grant_type=authorization_code&client_id={client_id}&client_secret={client_secret}&redirect_uri={encoded_url}&code={code}
    // }
    // this.client.get('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639&redirect_uri=https://www.cworkva.firebaseapp.com', this.mail_chimp_httpOptions).subscribe((res)=>{
    //   console.log(res)
    // })
    let mailchimp = window.open('https://login.mailchimp.com/oauth2/authorize?response_type=code&client_id=471281846639')

    

    
   
    
  }

}
