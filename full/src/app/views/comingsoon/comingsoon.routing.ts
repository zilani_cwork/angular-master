import { Routes } from '@angular/router';

import { ComingsoonComponent } from './comingsoon.component';


export const ComingsoonRoutes: Routes = [
  { path: 'show', component: ComingsoonComponent }
];