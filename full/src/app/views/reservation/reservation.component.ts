import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {

  reservations:any
  constructor(private client:HttpClient) { }

  ngOnInit() {
    this.getReservations()
  }
  //was done for the hotel demo

  // gets all reservation from the backend

  getReservations(){
    this.client.get('https://agile-cove-96115.herokuapp.com/services/reserve/').subscribe((res)=>{
      this.reservations = res
      console.log(this.reservations)
      console.log(this.reservations[0]['user_id'])
    })
  }

  // senderid is passed
  //

  sendMessage(id){
    console.log(id)

    let body = {
      "recipient":id
    }

    let new_body = JSON.stringify(body)
    console.log(new_body)

    this.client.post("https://agile-cove-96115.herokuapp.com/services/sendmessage", body).subscribe((res)=>{
      console.log(res)
    })

    
  }

}
