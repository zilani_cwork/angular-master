import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../../auth.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar, {static: false}) progressBar: MatProgressBar;
  @ViewChild(MatButton, {static: false}) submitButton: MatButton;

  signinForm: FormGroup;
  wrongPass=true
  
  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {

    
    
    let is_sign_in = localStorage.getItem('isSignedIn')
    if (is_sign_in == "true") {
    //   // window.location.href='http://localhost:4200/dashboard'
      this.router.navigate(['/dashboard'])
    //   window.location.reload()

      
      
    }

    // window.location.href = window.location.href

    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    })
  }

  signin() {
    const signinData = this.signinForm.value
    console.log(signinData);
    const email = signinData['username']
    const password = signinData['password']
    console.log(email)
    console.log(password)
    

    this.authService.SignIn(email, password)
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    this.wrongPass = this.authService.wrong_pass
    console.log("This is wrong pass ", this.wrongPass)
    console.log(localStorage)
    // window.location.reload()
  }

}
