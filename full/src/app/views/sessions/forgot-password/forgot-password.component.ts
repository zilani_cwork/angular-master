import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { AuthService } from '../../../auth.service'
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  userEmail;
  showMessage:boolean;
  @ViewChild(MatProgressBar, {static: false}) progressBar: MatProgressBar;
  @ViewChild(MatButton, {static: false}) submitButton: MatButton;
  constructor(public authService:AuthService) { }

  ngOnInit() {
    this.showMessage = false
  }
  submitEmail() {
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    console.log(this.userEmail)
    this.authService.ResetPassword(this.userEmail)
    this.userEmail = "  " 
    this.showMessage = true
    
  }
}
