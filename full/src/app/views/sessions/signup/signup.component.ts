import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../../auth.service'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  @ViewChild(MatProgressBar, {static: false}) progressBar: MatProgressBar;
  @ViewChild(MatButton, {static: false}) submitButton: MatButton;

  showMessage: boolean
  signupForm: FormGroup
  constructor(public authService: AuthService) {}

  ngOnInit() {
    this.showMessage = false
    const password = new FormControl('', Validators.required);
    const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.signupForm = new FormGroup({
      firstName: new FormControl('',[Validators.required,]),
      lastName: new FormControl('', [Validators.required,]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: password,
      confirmPassword: confirmPassword,
      agreed: new FormControl('', (control: FormControl) => {
        const agreed = control.value;
        if(!agreed) {
          return { agreed: true }
        }
        return null;
      })
    })
  }

  signup() {
    const signupData = this.signupForm.value;
    console.log(signupData);
    console.log(signupData['email'])
    console.log(signupData['password'])
    console.log(signupData['firstName'])
    console.log(signupData['lastName'])

    let body = {
      "user": {
        "first_name": signupData['firstName'],
        "last_name": signupData['lastName'],
        "email": signupData['email']
      }
    }
    let cuser_id = this.authService.CuserSignUp(signupData['email'], signupData['password'], body)

    this.showMessage = true
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
  }

}
