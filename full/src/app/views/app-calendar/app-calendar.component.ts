import { Component, OnInit, ViewChild, TemplateRef, HostListener, Inject } from '@angular/core';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  isSameDay,
  isSameMonth
} from 'date-fns';
import { egretAnimations } from "../../shared/animations/egret-animations";
import { EgretCalendarEvent } from '../../shared/models/event.model';
import { AppCalendarService } from './app-calendar.service';
import { CalendarFormDialogComponent } from './calendar-form-dialog/calendar-form-dialog.component';
import { AppConfirmService } from '../../shared/services/app-confirm/app-confirm.service';

//THIS IS TO GET THE TOKEN TO BE SENT TO THE HEADER

import { httpOptions } from '../../httpOptions'



import { HttpClient } from '@angular/common/http';
import { AngularFireStorage } from '@angular/fire/storage'


@Component({
  selector: 'app-calendar',
  templateUrl: './app-calendar.component.html',
  styleUrls: ['./app-calendar.component.css'],
  animations: egretAnimations
})
export class AppCalendarComponent implements OnInit {
  public view = 'month';
  public viewDate = new Date();
  private dialogRef: MatDialogRef<CalendarFormDialogComponent>;
  public activeDayIsOpen: boolean = true;
  public refresh: Subject<any> = new Subject();
  // public events: EgretCalendarEvent[];
  private actions: CalendarEventAction[];

  //MY VARIABLES
// setting all my variables
// this is for the scheduling of the posts
  header:any // THIS IS THE HEADER TO BE SENT FOR AUTHENTICATION
  today = {}
  tomorrow = {}
  day_after = {}

  currentDate: any



  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

  days = [this.today, this.tomorrow, this.day_after]

  times: []

  events: any
  FB:any

  fb_id:any
  fb_accesstoken:any

  data_array:any
  page_select:any

  selected_value:any



  //MY VARIABLES

  constructor(
    public dialog: MatDialog,
    private calendarService: AppCalendarService,
    private confirmService: AppConfirmService,
    public client:HttpClient,
  ) {
    this.actions = [{
      label: '<i class="material-icons icon-sm">edit</i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('edit', event);
      }
    }, {
      label: '<i class="material-icons icon-sm">close</i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.removeEvent(event);
      }
    }];
  }

  ngOnInit() {

    //MY CODE

    this.header = httpOptions(localStorage.getItem('Token'))
    
    //this today is a Date object
    let today = new Date()
    let month = today.getMonth()
    let day = today.getDate()




    //// THREE OBJECTS ARE SET HERE WITH A FEW ATTRIBUTES
    // ONE A FULL DATE OBJECT OF THE DAY OF THE POST
    // THE POST TEXT
    // IMAGE URL
    // THESE ARE GOING TO BE PASSED INTO A ANOTHER TS OBJECT SO THESE HAS TO CONTAIN THESE DATA
    // THEY ALL HAVE AN ID ATTRIBUTE SET TO -1
    // EACH HAS 4 TIMES WHICH CAN BE SEEN BELOW
    
    // this today is a TS object for the current day



    this.today['date'] = today
    this.today['day'] = day
    this.today['month'] = this.months[month]
    this.today['9.17'] = { time: JSON.stringify(new Date(today.getFullYear(), month, day, 9, 17, 0, 0)), post: "", image: "", id: -1 };
    this.today['2.09'] = { time: JSON.stringify(new Date(today.getFullYear(), month, day, 14, 9, 0, 0)), post: "", image: "", id: -1 };
    this.today['3.45'] = { time: JSON.stringify(new Date(today.getFullYear(), month, day, 15, 45, 0, 0)), post: "", image: "", id: -1 };
    this.today['9.38'] = { time: JSON.stringify(new Date(today.getFullYear(), month, day, 21, 38, 0, 0)), post: "", image: "", id: -1 };

  

    // tomorrow is for the next day

    let tomorrow = new Date()
    tomorrow.setDate(today.getDate() + 1)
    this.tomorrow['date'] = tomorrow
    this.tomorrow['day'] = tomorrow.getDate()
    this.tomorrow['month'] = this.months[tomorrow.getMonth()]
    this.tomorrow['9.17'] = { time: JSON.stringify(new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 9, 17, 0, 0)), post: "", image: "", id: -1 };
    this.tomorrow['2.09'] = { time: JSON.stringify(new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 14, 9, 0, 0)), post: "", image: "", id: -1 };
    this.tomorrow['3.45'] = { time: JSON.stringify(new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 15, 45, 0, 0)), post: "", image: "", id: -1 };
    this.tomorrow['9.38'] = { time: JSON.stringify(new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate(), 21, 38, 0, 0)), post: "", image: "", id: -1 };


    // day aftertomorrow

    let day_after = new Date()
    day_after.setDate(tomorrow.getDate() + 1)
    this.day_after['date'] = day_after
    this.day_after['day'] = day_after.getDate()
    this.day_after['month'] = this.months[day_after.getMonth()]
    this.day_after['9.17'] = { time: JSON.stringify(new Date(day_after.getFullYear(), day_after.getMonth(), day_after.getDate(), 9, 17, 0, 0)), post: "", image: "", id: -1 };
    this.day_after['2.09'] = { time: JSON.stringify(new Date(day_after.getFullYear(), day_after.getMonth(), day_after.getDate(), 14, 9, 0, 0)), post: "", image: "", id: -1 };
    this.day_after['3.45'] = { time: JSON.stringify(new Date(day_after.getFullYear(), day_after.getMonth(), day_after.getDate(), 15, 45, 0, 0)), post: "", image: "", id: -1 };
    this.day_after['9.38'] = { time: JSON.stringify(new Date(day_after.getFullYear(), day_after.getMonth(), day_after.getDate(), 21, 38, 0, 0)), post: "", image: "", id: -1 };

    // console.log(this.days)
    this.currentDate = new Date()
    this.currentDate.setDate(day_after.getDate() + 1)






    //MY CODE
    console.log("this is today")
    console.log(this.today)






    // THESE ARE TO GET THE SCHEDULED FACEBOOK POST EVENTS
    this.getEvents()
    // THIS IS FOR FACEBOOK AUTHENTICATION
    this.facebookinit()
    //THIS IS FOR GETTING AUTHENTICATED FACEBOOK USER PAGE TOKENS LIST
    this.getPages()

  }

  selected(){
    // console.log(this.selected_value)
  }

  // GETTING THE PAGE TOKEN TABLE DATA FROM DJANGO
  // CONTAINS THE PAGE NAME AND ACCESS TOKEN
  getPages(){
    this.client.get('https://agile-cove-96115.herokuapp.com/schedule/ptlist', this.header).subscribe((res)=>{
      // console.log("These are the pages")
      // console.log(res)
      this.data_array = res
    })
  }

  // THIS IS FOR FACEBOOK AUTH PROVIDED BY OAUTH

  facebookinit(){
    (window as any).fbAsyncInit = function() {
      //@ts-ignore
      FB.init({
        appId      : '3004517459564703',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      //@ts-ignore
      FB.AppEvents.logPageView();
      
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

    //  console.log(this.FB)
  }

// PROVIDED BY OAUTH
  submitLogin(){
    // console.log("submit login to facebook");
    // FB.login();
    //@ts-ignore
    FB.login((response)=>
        {
          // console.log('submitLogin',response);
          if (response.authResponse)
          {
            //login success
            //login success code here
            //redirect to home page
            // console.log(response)
            this.fb_id = response.authResponse['userID']
            this.fb_accesstoken = response.authResponse['accessToken']
            // console.log(this.fb_id)
            // console.log(this.fb_accesstoken)
            this.getdata(this.fb_id)

            
           }
           else
           {
           console.log('User login failed');
         }
      });

  }


  // FORMATTING THE DATE
  formatDate( s:string){
    
    return new Date(s.slice(3,27))
    
  }
// SENDING AN INTEGER FOR EACH OF THE DATES
  get_month_int(month){
    if(month=='Jan'){
      return 0
    }
    else if(month =='Feb'){
      return 1
    }
    else if(month =='Mar'){
      return 2
    }
    else if(month =='Apr'){
      return 3
    }
    else if(month =='May'){
      return 4
    }
    else if(month =='Jun'){
      return 5
    }
    else if(month =='Jul'){
      return 6
    }
    else if(month =='Aug'){
      return 7
    }
    else if(month =='Sep'){
      return 8
    }
    else if(month =='Oct'){
      return 9
    }
    else if(month =='Nov'){
      return 10
    }
    else if(month =='Dec'){
      return 11
    }
  }

  compare(month, day, date:Object){
    // console.log("HELLO THERE LOOKIE HERE")
    // console.log(month.slice(0,3))
    // console.log(day)
    // console.log(date.getDate())
    month = this.get_month_int(month.slice(0,3))
    let that_day = this.formatDate(date['time'])
    let test_month = that_day.getMonth()
    let test_day = that_day.getDate()
    // console.log('asfasf')
    // console.log(test_month)
    // console.log(test_day)

    if(date['done']==false && day==test_day && month==test_month){
      return true
    }
    else{
      return false
    }


  }

  getdata(user_id){

  
    //@ts-ignore
    FB.api(
      "/"+user_id+"/accounts?type=page",
      (response) =>{
        if (response && !response.error) {
          /* handle the result */
          // console.log("User data")
          // console.log(response)

          this.data_array = response.data


          // console.log(this.data_array)
          // console.log(this.data_array)
        }
        else{
          console.log(response)
        }
      }
  );
  }

  private initEvents(events): EgretCalendarEvent[] {
    return events.map(event => {
      event.actions = this.actions;
      return new EgretCalendarEvent(event);
    });
  }

  public loadEvents() {
    this.calendarService
      .getEvents()
      .subscribe((events: CalendarEvent[]) => {
        this.events = this.initEvents(events);
      });
  }

  public removeEvent(event) {
    this.confirmService
      .confirm({
        title: 'Delete Event?'
      })
      .subscribe(res => {
        if (!res) {
          return;
        }

        this.calendarService
          .deleteEvent(event._id)
          .subscribe(events => {
            this.events = this.initEvents(events);
            this.refresh.next();
          })
      })
  }

  public addEvent() {
    this.dialogRef = this.dialog.open(CalendarFormDialogComponent, {
      panelClass: 'calendar-form-dialog',
      data: {
        action: 'add',
        date: new Date()
      },
      width: '450px'
    });
    this.dialogRef.afterClosed()
      .subscribe((res) => {
        if (!res) {
          return;
        }
        let dialogAction = res.action;
        let responseEvent = res.event;
        this.calendarService
          .addEvent(responseEvent)
          .subscribe(events => {
            this.events = this.initEvents(events);
            this.refresh.next(true);
          });
      });
  }

  public handleEvent(action: string, event: EgretCalendarEvent): void {
    // console.log(event)
    this.dialogRef = this.dialog.open(CalendarFormDialogComponent, {
      panelClass: 'calendar-form-dialog',
      data: { event, action },
      width: '450px'
    });

    this.dialogRef
      .afterClosed()
      .subscribe(res => {
        if (!res) {
          return;
        }
        let dialogAction = res.action;
        let responseEvent = res.event;

        if (dialogAction === 'save') {
          this.calendarService
            .updateEvent(responseEvent)
            .subscribe(events => {
              this.events = this.initEvents(events);
              this.refresh.next();
            })
        } else if (dialogAction === 'delete') {
          this.removeEvent(event);
        }

      })
  }


  public dayClicked({ date, events }: { date: Date, events: CalendarEvent[] }): void {

    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  public eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;

    this.calendarService
      .updateEvent(event)
      .subscribe(events => {
        this.events = this.initEvents(events);
        this.refresh.next();
      });
  }



  /// THIS IS AN ONSCROLL EVENT
  // WHICH CREATES MORE DAYS FOR SCHEDULING POSTS 
  // LIKE TODAY TOMORROW AND DAY AFTER OBJECTS IN THE ONINIT FUNCTUION
  // ALL OF THEM CONTAIN THE SAME OBJECTS
  // AND DATA
  // AS THE USER SCROLLS MORE DATES WILL BE GENERATED
  @HostListener('window:scroll', ['$event'])
  onScroll(e) {

    setTimeout(()=>{
      let new_day = {}
    new_day['date'] = this.currentDate
    new_day['day'] = this.currentDate.getDate()
    new_day['month'] = this.months[this.currentDate.getMonth()]
    new_day['9.17'] = { time: JSON.stringify(new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 9, 17, 0, 0)), post: "", image: "", id: -1 };
    new_day['2.09'] = { time: JSON.stringify(new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 14, 9, 0, 0)), post: "", image: "", id: -1 };
    new_day['3.45'] = { time: JSON.stringify(new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 15, 45, 0, 0)), post: "", image: "", id: -1 };
    new_day['9.38'] = { time: JSON.stringify(new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate(), 21, 38, 0, 0)), post: "", image: "", id: -1 };

    this.days.push(new_day)

    this.currentDate.setDate(this.currentDate.getDate() + 1)

    },2000)


    // ALL EVENTS ARE RETRIEVED
    
    this.getEvents()

  }





  //Custom


// THIS IS THE MODAL CREATED FOR ADDING CUSTOM EVENTS WHICH IS DESCRIBED BELOW
  customDialog(): void {
    const dialogRef = this.dialog.open(EventAddModal, {
      width: '600px',
      data: {action: "custom" }
    });





    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');

      

    });

    
  }







  //Custom




// MODAL TO ADD EVENTS ON SPECIFIC DEFINED MODALS

  addEventDialog(date, time): void {
    const dialogRef = this.dialog.open(EventAddModal, {
      width: '600px',
      data: { date: date, time: time, post: "", image: "", action: "create" }
    });





    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');

      this.getEvents()

    });

    this.getEvents()
    this.getEvents()
    this.getEvents()
    setInterval(()=>{this.getEvents()}, 3000)
  }


  // EDIT SCHEDULED EVENT MODAL
  editDialog(obj): void {
    const dialogRef = this.dialog.open(EventAddModal, {
      width: '600px',
      data: { id: obj.id, post: obj.post, image: obj.image, action: "edit" }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('Here I will call all the events again');
      // console.log("calling the events")

      // setTimeout(()=>{this.getEvents()}, 2000)
      this.getEvents()
      this.getEvents()
      this.getEvents()
    });
    this.getEvents()
  }



  // THIS IS USED TO GET ALL THE EVENTS FROM THE DJANGO DATABASE

  getEvents() {
    
    this.client.get('https://agile-cove-96115.herokuapp.com/schedule/schedule/', this.header).subscribe((res) => {
      // console.log(res)
      // console.log('These are the events')
      this.events = res
      // console.log(this.events[0].time)
      let len = Object.keys(res).length

      for (let i = 0; i < len; i++) {
        res[i].time = JSON.stringify(res[i].time)
      }

      this.events = res
      // console.log(this.today['9.17'])

      // console.log(this.events)

      // console.log("CHECK THIS")
      // console.log(this.formatDate(this.events[0].time))

      // console.log(this.correctFormat(this.events[0].time) == this.today[9.17].time)

      for(let i=0; i<this.events.length; i++){
        this.events[i].done =false
      }

      for (let i = 0; i < this.days.length; i++) {

        for (let j = 0; j < this.events.length; j++) {

          if (this.days[i]['9.17'].time == this.correctFormat(this.events[j]['time'])) {
            this.days[i]['9.17'].post = this.events[j]['description']
            this.days[i]['9.17'].image = this.events[j]['image']
            this.days[i]['9.17'].id = this.events[j]['id']
            this.events[j].done = true


          }
          else if (this.days[i]['2.09'].time == this.correctFormat(this.events[j]['time'])) {
            this.days[i]['2.09'].post = this.events[j]['description']
            this.days[i]['2.09'].image = this.events[j]['image']
            this.days[i]['2.09'].id = this.events[j]['id']
            this.events[j].done = true

          }
          else if (this.days[i]['3.45'].time == this.correctFormat(this.events[j]['time'])) {
            this.days[i]['3.45'].post = this.events[j]['description']
            this.days[i]['3.45'].image = this.events[j]['image']
            this.days[i]['3.45'].id = this.events[j]['id']
            this.events[j].done = true
          }
          else if (this.days[i]['9.38'].time == this.correctFormat(this.events[j]['time'])) {
            this.days[i]['9.38'].post = this.events[j]['description']
            this.days[i]['9.38'].image = this.events[j]['image']
            this.days[i]['9.38'].id = this.events[j]['id']
            this.events[j].done = true
          }



        }

        // console.log(this.days)
        // console.log("this is another")
        // console.log(this.events)

      }






    })
  }









  correctFormat(t) {

    return JSON.parse(t)

  }

  // THIS FUNCTION MAKES SURE ONLY PART OF THE POST IS SHOWN WHEN CLICKED IT WILL SHOW THE WHOLE POST

  postPreview(s){
    // console.log(s)
    if(s.length<80){
      return s
    }
    else{
      s = s.substring(0,80)+"......"
      return s
    }
  }


  // THIS IS USED TO DELETE A SCHEDULED POST

  deletePost(obj) {




    this.client.delete('https://agile-cove-96115.herokuapp.com/schedule/schedule/' + obj.id + '/', this.header).subscribe((res) => {
      obj.post = ""
      obj.id = -1
      obj.image = ""

      this.getEvents()
    })
  }



}


@Component({
  selector: 'event-add-modal',
  templateUrl: 'event-add-modal.html',
  styleUrls: ['./app-calendar.component.css']
})
export class EventAddModal {

  caption = ""
  img: any

  imgUrl: any;
  file: any;

  ref: any
  task: any

  header:any
  data_array: ArrayBuffer;
  selected_value: any;

  date_input:any
  hour = new Array()
  minute = new Array()
  hour_for_time = 0
  minute_for_time = 0


  constructor(
    public dialogRef: MatDialogRef<EventAddModal>,
    @Inject(MAT_DIALOG_DATA) public data: {}, public client: HttpClient, public fireStorage: AngularFireStorage) { }

  ngOnInit() {
    
    this.header = httpOptions(localStorage.getItem('Token'))
    for(let i=0;i<24;i++){
      this.hour.push(i)
    }
    for(let i=0; i<60;i++){
      this.minute.push(i)
    }
    this.getPages()
    // console.log("Opened")
    // console.log(this.data)
    if(this.data['image']){
      this.imgUrl = this.data['image']
    }
    if(this.data['post']){
      this.caption = this.data['post']
    }
    if(this.data['action']=="edit_post"){
      this.imgUrl = this.data['obj']['image']
      this.caption = this.data['obj']['caption']

    }
  }

  get_month_int(month){
    if(month=='Jan'){
      return 0
    }
    else if(month =='Feb'){
      return 1
    }
    else if(month =='Mar'){
      return 2
    }
    else if(month =='Apr'){
      return 3
    }
    else if(month =='May'){
      return 4
    }
    else if(month =='Jun'){
      return 5
    }
    else if(month =='Jul'){
      return 6
    }
    else if(month =='Aug'){
      return 7
    }
    else if(month =='Sep'){
      return 8
    }
    else if(month =='Oct'){
      return 9
    }
    else if(month =='Nov'){
      return 10
    }
    else if(month =='Dec'){
      return 11
    }
  }

  test_show(){
    this.dialogRef.close();

    let current_format = this.date_input.toString()
    let current_values = current_format.split(' ')
    // console.log(current_values)
    let month = this.get_month_int(current_values[1])
    let day = parseInt(current_values[2])
    let year = parseInt(current_values[3])
    console.log(month)
    console.log(day)
    console.log(year)
    console.log(this.hour_for_time)
    console.log(this.minute_for_time)
    console.log(typeof this.hour_for_time)

    let custom_date = new Date(year, month, day, this.hour_for_time, this.minute_for_time, 0, 0)
    console.log(custom_date)
    
    let c_d = JSON.stringify(custom_date)
    console.log(c_d)
    console.log(this.selected_value)

    console.log(this.caption)
    console.log(this.file)


    // COPIED Section


    if (this.imgUrl) {
      // console.log("An image was uploaded")

      let formData = new FormData()
      formData.append('image', this.file)

      
      // console.log(new_body)

      const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(this.file).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          let body = {
            description: this.caption,
            image: res,
            time: c_d,
            page_name:this.selected_value
    
    
          }
          let new_body = JSON.stringify(body)
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/schedule/', new_body, this.header).subscribe((r) => {
          console.log(r)
        })
        })
      })

    }
    else {
      console.log("An image was not uploaded")
      let body = {
        description: this.caption,
        image: "",
        time: c_d,
        page_name:this.selected_value


      }
      let new_body = JSON.stringify(body)
      this.client.post('https://agile-cove-96115.herokuapp.com/schedule/schedule/', new_body, this.header).subscribe((res)=>{
        console.log(res)
      })
    }



    // COPIED Section

    
    
  }


  selected(){
    // console.log(this.selected_value)
  }
  getPages(){
    this.client.get('https://agile-cove-96115.herokuapp.com/schedule/ptlist', this.header).subscribe((res)=>{
      // console.log("These are the pages")
      // console.log(res)
      this.data_array = res
    })
  }




  onNoClick(): void {
    this.dialogRef.close();
  }

  changeCaption() {

    this.caption = (<HTMLTextAreaElement>document.getElementById('caption')).value
    // console.log(this.caption)
  }
  preview(event) {
    // console.log(event.target.files)
    // console.log(event.target.files[0].name)
    // this.imgUrl = event.target.files[0].name
    this.file = event.target.files[0]
    let reader = new FileReader()
    reader.readAsDataURL(event.target.files[0])
    reader.onload = (_event) => {
      this.imgUrl = reader.result;
      // console.log(this.imgUrl)
    }
  }
  Schedule() {
    this.dialogRef.close()

    if (this.imgUrl) {
      // console.log("An image was uploaded")

      let formData = new FormData()
      formData.append('image', this.file)

      
      // console.log(new_body)

      const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(this.file).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          let body = {
            description: this.caption,
            image: res,
            time: this.data['time'],
            page_name:this.selected_value
    
    
          }
          let new_body = JSON.stringify(body)
          this.client.post('https://agile-cove-96115.herokuapp.com/schedule/schedule/', new_body, this.header).subscribe((r) => {
          console.log(r)
        })
        })
      })

    }
    else {
      console.log("An image was not uploaded")
      let body = {
        description: this.caption,
        image: "",
        time: this.data['time'],
        page_name:this.selected_value


      }
      let new_body = JSON.stringify(body)
      this.client.post('https://agile-cove-96115.herokuapp.com/schedule/schedule/', new_body, this.header).subscribe((res)=>{
        console.log(res)
      })
    }
  }





  Edit() {

    this.dialogRef.close();
    let f = (<HTMLInputElement>document.getElementById('img')).files[0]

    if (f) {
      console.log(true)
      let formData = new FormData();
      formData.append('image', f);






      const randomId = Math.random().toString(36).substring(2);
      this.ref = this.fireStorage.ref(randomId)
      this.task = this.ref.put(this.file).then((res) => {
        this.ref.getDownloadURL().subscribe((res) => {
          console.log("This is the url")
          console.log(res)

          let body = {
            id: this.data['id'],
            description: this.caption,
            image: res,
    
          }
    
          let new_body = JSON.stringify(body)
          this.client.put('https://agile-cove-96115.herokuapp.com/schedule/schedule/' + this.data['id'] + '/', new_body, this.header).subscribe((r) => {
          console.log(r)
        })
        })
      })


      

      

      



    }
    else {
      console.log(false)

      let str = 'https://agile-cove-96115.herokuapp.com/'
      console.log("The length of the string is " + str.length)

      let url = this.data['image'].slice(45, this.data['image'].length)
      console.log(url)
      let body = {
        id: this.data['id'],
        description: this.caption,
        image: this.data['image'],

      }

      let new_body = JSON.stringify(body)

      this.client.put('https://agile-cove-96115.herokuapp.com/schedule/schedule/' + this.data['id'] + '/', new_body, this.header).subscribe((res) => {
        console.log(res)
      })

    }





  }

  Edit_post(){
    this.dialogRef.close();
    console.log("A post was probably edited")
    if(this.imgUrl){
      console.log("Present")
      console.log(this.data['obj']['image'])
      console.log(this.file)
    }
    else{
      console.log("No present")
    }

  }

  

}

