import { Routes } from '@angular/router';

import {StorageComponent} from './storage.component'

export const StorageRoutes: Routes = [
  { path: '', component: StorageComponent, data: { title: 'Storage' } }
];