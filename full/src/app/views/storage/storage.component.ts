import { Component, OnInit } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import {httpOptions} from '../../httpOptions'


@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent implements OnInit {
  files:any
  header:any
  card_data = ""
  constructor(public client: HttpClient) { }

  ngOnInit() {
    this.header = httpOptions(localStorage.getItem('Token'))
    this.getFiles()
  }


  //getting all posts with finalize true
  //those are the finalized post

  getFiles(){
    
      this.client.get('https://agile-cove-96115.herokuapp.com/version/finalizedList', this.header).subscribe((res)=>{
        console.log(res)
        this.files = res
      })
    
  }

  fix(url_name:String){
    return url_name.slice(45,url_name.length)
  }
  fixString(s:string){
    if(s.length<40){
      return s
    }
    else{
      return s.substring(0,40)+"...."
    }
  }



}
