import { HttpHeaders } from '@angular/common/http';

//this function returns the httpoptions for the authenticated user
export function httpOptions(token) {
  
  return {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'token ' + localStorage.getItem('Token')
    })
  };
}