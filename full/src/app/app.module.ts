import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig } from '@angular/material';
import { 
  PerfectScrollbarModule, 
  PERFECT_SCROLLBAR_CONFIG, 
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';


import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './shared/inmemory-db/inmemory-db.service';

import { rootRouterConfig } from './app.routing';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ErrorHandlerService } from './shared/services/error-handler.service';


import { AngularFireModule, FirebaseOptionsToken } from "@angular/fire";

import {AngularFireStorage } from "@angular/fire/storage"
import { AuthService } from './auth.service'

// import { AngularFireAuthModule } from '@angular/fire/auth';

// import { AngularFireModule } from "@angular/fire/";
// import {AngularFireModule} from '@angular/fire'
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from './environment';
import { FacebookComponent } from './facebook/facebook.component';
import { comingsoon } from './views/comingsoon/comingsoon.module';
import { TaskModule } from './views/task/task.module';
import { ContactModule } from './views/contact/contact.module';
import { EmailsettingsModule } from './views/emailsettings/emailsettings.module';
import { EmailComponent } from './views/email/email.component';
import {EmailModule} from './views/email/email.module'




// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    EmailModule,
    EmailsettingsModule,
    ContactModule,
    TaskModule,
    comingsoon,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    PerfectScrollbarModule,
    
    AngularFireAuthModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    InMemoryWebApiModule.forRoot(InMemoryDataService, { passThruUnknownUrl: true}),
    RouterModule.forRoot(rootRouterConfig, { useHash: false })
  ],
  declarations: [AppComponent, FacebookComponent,  ],
  providers: [
    { provide: FirebaseOptionsToken, useValue: environment.firebase },
    { provide: ErrorHandler, useClass: ErrorHandlerService },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }